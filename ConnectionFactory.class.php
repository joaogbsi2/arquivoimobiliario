<?php
/**
 * Description of ConnectionFactory
 *
 * @author Daniel /updated by Edilson Justiniano
 */
class ConnectionFactory {
	private static $instance;
	private $connection;
	public static function getInstance() {
		if (! self::$instance) {
			self::$instance = new ConnectionFactory ();
		}
		return self::$instance;
	}
	public function createConnection($startTransaction) {
		$filename = '/home/arqui937/public_html/config.php';
		if (file_exists ( $filename )) {
			$dsn = "mysql:dbname=arqui937_webmovel;host=localhost";
			$userName = "arqui937_root";
			$passwd = "webm@ve1";
		} else {
			$dsn = "mysql:dbname=arquivoImobiliarioNew;host=localhost";
			$userName = "root";
			$passwd = "";
		}
		
		$opcoes = array (
				PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8' 
		);
		$this->connection = new PDO ( $dsn, $userName, $passwd, $opcoes );
		
		// === Turn off auto commit
		if ($startTransaction) {
			$this->connection->beginTransaction ();
		}
		
		return $this->connection;
	}
	public function releaseConnection($connection) {
		if ($connection != NULL) {
			// === Close connection
			$connection = NULL;
		}
	}
	public function commitConnection($connection, $releaseConnection) {
		if ($connection != NULL) {
			$connection->commit ();
		}
		if ($releaseConnection) {
			$this->releaseConnection ( $connection );
		}
	}
	public function rollBackConnection($connection, $releaseConnection) {
		if ($connection != NULL) {
			$connection->rollBack ();
			if ($releaseConnection) {
				$this->releaseConnection ( $connection );
			}
		}
	}
}

?>
