<?php
/**
* 
*/
class FaixaPrecos{
	
	public static function buscaFaixa($preco){

		// print_r($preco);
		$faixaValue = NULL;
		if ($preco < 150000)
			$faixaValue = "Até R$ 150 mil";
		else if ($preco < 175000)
			$faixaValue = "Entre R$ 150 mil e R$ 175 mil";
		else if ($preco < 200000)
			$faixaValue = "Entre R$ 175 mil e R$ 200 mil";
		else if ($preco < 225000)
			$faixaValue = "Entre R$ 200 mil e R$ 225 mil";
		else if ($preco < 250000)
			$faixaValue = "Entre R$ 225 mil e R$ 250 mil";
		else if ($preco < 275000)
			$faixaValue = "Entre R$ 250 mil e R$ 275 mil";
		else if ($preco < 300000)
			$faixaValue = "Entre R$ 275 mil e R$ 300 mil";
		else if ($preco < 325000)
			$faixaValue = "Entre R$ 300 mil e R$ 325 mil";
		else if ($preco < 350000)
			$faixaValue = "Entre R$ 325 mil e R$ 350 mil";
		else if ($preco < 375000)
			$faixaValue = "Entre R$ 350 mil e R$ 375 mil";
		else if ($preco < 400000)
			$faixaValue = "Entre R$ 375 mil e R$ 400 mil";
		else if ($preco < 425000)
			$faixaValue = "Entre R$ 400 mil e R$ 425 mil";
		else if ($preco < 450000)
			$faixaValue = "Entre R$ 425 mil e R$ 450 mil";
		else if ($preco < 475000)
			$faixaValue = "Entre R$ 450 mil e R$ 475 mil";
		else if ($preco < 500000)
			$faixaValue = "Entre R$ 475 mil e R$ 500 mil";
		else if ($preco < 525000)
			$faixaValue = "Entre R$ 500 mil e R$ 525 mil";
		else if ($preco < 550000)
			$faixaValue = "Entre R$ 525 mil e R$ 550 mil";
		else if ($preco < 575000)
			$faixaValue = "Entre R$ 550 mil e R$ 575 mil";
		else if ($preco < 600000)
			$faixaValue = "Entre R$ 575 mil e R$ 600 mil";
		else if ($preco < 625000)
			$faixaValue = "Entre R$ 600 mil e R$ 625 mil";
		else if ($preco < 650000)
			$faixaValue = "Entre R$ 625 mil e R$ 650 mil";
		else if ($preco < 675000)
			$faixaValue = "Entre R$ 650 mil e R$ 675 mil";
		else if ($preco < 700000)
			$faixaValue = "Entre R$ 675 mil e R$ 700 mil";
		else if ($preco < 725000)
			$faixaValue = "Entre R$ 700 mil e R$ 725 mil";
		else if ($preco < 750000)
			$faixaValue = "Entre R$ 725 mil e R$ 750 mil";
		else if ($preco < 775000)
			$faixaValue = "Entre R$ 750 mil e R$ 775 mil";
		else if ($preco < 800000)
			$faixaValue = "Entre R$ 775 mil e R$ 800 mil";
		else if ($preco < 825000)
			$faixaValue = "Entre R$ 800 mil e R$ 825 mil";
		else if ($preco < 850000)
			$faixaValue = "Entre R$ 825 mil e R$ 850 mil";
		else if ($preco < 875000)
			$faixaValue = "Entre R$ 850 mil e R$ 875 mil";
		else if ($preco < 900000)
			$faixaValue = "Entre R$ 875 mil e R$ 900 mil";
		else if ($preco < 925000)
			$faixaValue = "Entre R$ 900 mil e R$ 925 mil";
		else if ($preco < 950000)
			$faixaValue = "Entre R$ 925 mil e R$ 950 mil";
		else if ($preco < 975000)
			$faixaValue = "Entre R$ 950 mil e R$ 975 mil";
		else if ($preco < 1000000)
			$faixaValue = "Entre R$ 975 mil e R$ 1 milhão";
		else if ($preco < 1250000)
			$faixaValue = "Entre R$ 1 milhão e R$ 1,250 milhão";
		else if ($preco < 1500000)
			$faixaValue = "Entre R$ 1,250 milhão e R$ 1,500 milhão";
		else if ($preco < 1750000)
			$faixaValue = "Entre R$ 1,500 milhão e R$ 1,750 milhão";
		else if ($preco < 2000000)
			$faixaValue = "Entre R$ 1,750 milhão e R$ 2 milhões";
		else
			$faixaValue = "acima de R$ 2 milhões";

		return $faixaValue;
	}
}
