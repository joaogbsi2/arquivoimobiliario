<?php
class Ortografia{
	static $vogais = array (
			"a",
			"á",
			"â",
			"ã",
			"A",
			"Á",
			"Â",
			"Ã",
			"e",
			"é",
			"ê",
			"E",
			"É",
			"Ê",
			"i",
			"í",
			"î",
			"I",
			"Í",
			"Î",
			"o",
			"ó",
			"ô",
			"õ",
			"O",
			"Ó",
			"Ô",
			"Õ",
			"u",
			"ú",
			"û",
			"U",
			"Ú",
			"Û"
	);
	public static function retirarAcento($string){
		$string = strtolower($string);
		$string = str_replace('á', 'a', $string);
		$string = str_replace('à', 'a', $string);
		$string = str_replace('â', 'a', $string);
		$string = str_replace('ã', 'a', $string);
		$string = str_replace('é', 'e', $string);
		$string = str_replace('ê', 'e', $string);
		$string = str_replace('ẽ', 'e', $string);
		$string = str_replace('í', 'i', $string);
		$string = str_replace('î', 'i', $string);
		$string = str_replace('ó', 'o', $string);
		$string = str_replace('ô', 'o', $string);
		$string = str_replace('õ', 'o', $string);
		$string = str_replace('ú', 'u', $string);
		$string = str_replace('û', 'u', $string);
		$string = str_replace('ç', 'c', $string);
		
		return $string;
	}
	
	public static function tudoMinusculo($string){
		return strtolower($string);
	}
	
	public static function tudoMaiusculo($string){
		return strtoupper($string);
	}
	
	public static function tiraEspaco($string){
		return str_replace(' ', '', $string);
	}
}