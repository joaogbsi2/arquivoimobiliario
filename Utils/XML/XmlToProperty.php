<?php
$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}
require_once PATH_INCLUDES_ADMIN . 'session.php';
require_once PATH_ACTION . 'cadProperty.action.php';
require_once PATH_MODEL_ENTITIES . 'State.class.php';
require_once PATH_MODEL_ENTITIES . 'City.class.php';
require_once PATH_MODEL_ENTITIES . 'Status.class.php';
require_once PATH_MODEL_ENTITIES . 'PropertyType.class.php';

/*
 * Create a new field. Combobox of Store
 */
require_once PATH_CONTROLLER . 'StoreController.class.php';
require_once PATH_MODEL_ENTITIES . 'Store.class.php';

/*
 * listar os detalhes
 */
require_once PATH_CONTROLLER . 'DetailController.class.php';
require_once PATH_MODEL_ENTITIES . 'Detail.class.php';

/*
 * listar as proximidades
 */
require_once PATH_CONTROLLER . 'NearController.class.php';
require_once PATH_MODEL_ENTITIES . 'Near.class.php';
function imovelValido($dadoImovel) {
	if (strcasecmp ( ( string ) $dadoImovel->Estado, 'MG' )== 0)
		return true;
	else
		return false;
}
function lerXml($dadoImovel, $store) {
	$params ['referencia'] = ( string ) $dadoImovel->CodigoImovel;
	$params ['propertyType'] = getTipoImovel ( $dadoImovel );
	$params ['precos'] = montaPrecos ( $dadoImovel );
// 	if ($params ['status'] == 2)
// 		$params ['price'] = floor ( $dadoImovel->PrecoVenda ); // Tratar preco
// 	else
// 		$params ['price'] = floor ( $dadoImovel->PrecoLocacao ); // Tratar preco
	$params ['cep'] = str_replace ( "-", "", $dadoImovel->CEP );
	$cityController = new CityController ();
	$city = $cityController->getCitiesByName ( ( string ) $dadoImovel->Cidade, null, null );
	if (! is_null ( $city )) {
		// $params ['city'] = ( string ) $dadoImovel->Cidade;
		$params ['city'] = $city [0];
	} else {
		$paramsCidade ['name'] = ( string ) $dadoImovel->Cidade;
		
		$stateController = new StateController ();
		$state = new State ();
		
		$state = $stateController->getPorUf ( (string)$dadoImovel->Estado );
		$paramsCidade ['state'] = $state;
		$cityController->addCity ( $paramsCidade );
		$city = $cityController->getCitiesByName ( ( string ) $dadoImovel->Cidade, null, null );
		$params ['city'] = $city [0];
	}
	
	$params ['neighborhood'] = ( string ) $dadoImovel->Bairro;
	$params ['street'] = ( string ) $dadoImovel->Endereco;
	$params ['number'] = ( string ) $dadoImovel->Numero;
	$params ['description'] = getDescricao ( $dadoImovel );
	$params ['qtdRooms'] = ( string ) $dadoImovel->QtdDormitorios;
	if (intval ( $dadoImovel->Publicar ) == 1 && intval ( $dadoImovel->Locado ) == 0)
		$params ['active'] = 1;
	else if (intval ( $dadoImovel->Publicar ) == 0)
		$params ['active'] = 0;
		// $property->setActive ( 0 );
	
	if (getVagasGaragem ( $dadoImovel ) > 1) {
		$params ['garagem'] = getVagasGaragem ( $dadoImovel ) . ' vagas';
	} else {
		$params ['garagem'] = getVagasGaragem ( $dadoImovel ) . ' vaga';
	}
	$params ['pavimento'] = '';
	// $property->setPavimento ($dadoImovel->);
	if (strcasecmp ( $dadoImovel->FaceImovel, "" ) != 0)
		$params ['posicao'] = ( string ) $dadoImovel->FaceImovel;
	else
		$params ['posicao'] = "NULL";
	
	$params ['areaConstruida'] = ( string ) $dadoImovel->AreaUtil;
	$params ['areaTotal'] = ( string ) $dadoImovel->AreaTotal;
	$params ['valorCondominio'] = ( string ) $dadoImovel->PrecoCondominio;
	$params ['valorIPTU'] = floor ( $dadoImovel->PrecoIptu );
	$params ['edicula'] = '';
	$params ['comprimento'] = 0;
	$params ['largura'] = 0;
	$params ['stores'] = $store;
	$params ['exclusive_store'] = NULL;
	// $property->setEdicula ($dadoImovel->);
	// $property->setComprimento ($dadoImovel->);
	// $property->setLargura ($dadoImovel->);
	$aux = 0;
	
	foreach ($dadoImovel->Fotos->Foto as $foto){
		$paramsFoto['name'] = $params['referencia'].'_'.$aux++;
		$paramsFoto['url'] = (string)$foto->URLArquivo;
		$paramsFoto['principal'] = (string)$foto->Principal;
		$params['property_photo'][]=$paramsFoto;
		
	}
	return $params;
	
	// $propertyId = $propertyBI->createProperty ( $property );
}
function getTipoImovel($dadoImovel) {
	switch ($dadoImovel->TipoImovel) {
		case 'Casa' :
			return 1;
			break;
		case 'Apartamento' :
			return 3;
			break;
		case 'Flat' :
			return 4;
			break;
		case 'Kitnet' :
			return 5;
			break;
		case 'Galpão' :
			return 7;
			break;
		case 'Cobertura' :
			return 8;
			break;
		case 'Terreno' :
			return 9;
			break;
		case 'Loja' :
			return 10;
			break;
		case 'Chácara' :
			return 11;
			break;
		case 'Sítio' :
			return 12;
			break;
		case 'Fazenda' :
			return 13;
			break;
		case 'Área' :
			return 14;
			break;
		case 'Ponto' :
			return 16;
			break;
		case 'Hotel' :
			return 17;
			break;
		case 'Prédio' :
			return 18;
			break;
		case 'Conjunto' :
			return 19;
			break;
		case 'Salão' :
			return 20;
			break;
		case 'Pousada' :
			return 21;
			break;
		case 'Sobrado' :
			return 22;
			break;
		case 'Sala' :
			return 23;
			break;
		case 'Barracão' :
			return 24;
			break;
	}
}
function getVagasGaragem($dadoImovel) {
	$vagasGaragem = 0;
	if (intval ( $dadoImovel->QtdVagasCobertas ) != 0)
		$vagasGaragem += $dadoImovel->QtdVagasCobertas;
	if (intval ( $dadoImovel->QtdVagasDescobertas ) != 0)
		$vagasGaragem += $dadoImovel->QtdVagasDescobertas;
	if (intval ( $dadoImovel->QtdVagas ) != 0)
		$vagasGaragem += $dadoImovel->QtdVagas;
	return $vagasGaragem;
}
function montaPrecos($dadoImovel) {
	echo '';
	$precos;
	$cont = 0;
	if (( string ) $dadoImovel->PrecoLocacao != ""){
		$precos[$cont]['status'] = 1;
		$precos[$cont]['price'] = floor( $dadoImovel->PrecoLocacao);
		$cont++;
	}
	if(( string ) $dadoImovel->PrecoVenda != ""){
		$precos[$cont]['status'] = 2;
		$precos[$cont]['price'] = floor( $dadoImovel->PrecoVenda);
		
	}
	return $precos;
}
function getDescricao($dadoImovel) {
	$vazio = true;
	$descricao = '';
	
	if (intval ( $dadoImovel->QtdSuites ) != 0) {
		$descricao = $dadoImovel->QtdSuites . ' suite';
		if (intval ( $dadoImovel->QtdSuites ) > 1)
			$descricao .= 's';
	}
	if (intval ( $dadoImovel->QtdBanheiros ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		if (intval ( $dadoImovel->QtdBanheiros ) > 1)
			$descricao .= $dadoImovel->QtdBanheiros . ' banheiros';
		else
			$descricao .= 'banheiro';
	}
	if (intval ( $dadoImovel->QtdSalas ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		if (intval ( $dadoImovel->QtdSalas ) > 1)
			$descricao .= $dadoImovel->QtdSalas . ' salas';
		else
			$descricao .= 'sala';
	}
	if (intval ( $dadoImovel->PortaoEletronico ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'portão eletronico';
	}
	if (intval ( $dadoImovel->Mezanino ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'mezanino';
	}
	if (intval ( $dadoImovel->PeDireitoDuplo ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'pé direito duplo';
	}
	if (intval ( $dadoImovel->Terraco ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= $dadoImovel->Terraco;
	}
	if (intval ( $dadoImovel->JardimInverno ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'jardim inverno';
	}
	if (intval ( $dadoImovel->ServicoCozinha ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'cozinha serviço';
	}
	if (intval ( $dadoImovel->DormitorioEmpregada ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'dormitorio para empregada';
	}
	if (intval ( $dadoImovel->Zelador ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'zelador';
	}
	if (intval ( $dadoImovel->Adega ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'adega';
	}
	if (intval ( $dadoImovel->Solarium ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'solarium';
	}
	if (intval ( $dadoImovel->Sacada ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'sacada';
	}
	if (intval ( $dadoImovel->Lavabo ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'lavabo';
	}
	if (intval ( $dadoImovel->DormitorioReversivel ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'dormitorio reversivel';
	}
	if (intval ( $dadoImovel->ArmarioCorredor ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'armário embutido no corredor';
	}
	if (intval ( $dadoImovel->ArmarioCloset ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'armário no closet';
	}
	if (intval ( $dadoImovel->ArmarioDormitorio ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'guarda roupa embutido';
	}
	if (intval ( $dadoImovel->ArmarioBanheiro ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'armário de banheiro';
	}
	if (intval ( $dadoImovel->ArmarioSala ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'armário embutido na sala';
	}
	if (intval ( $dadoImovel->ArmarioEscritorio ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'armário embutido no escritório';
	}
	if (intval ( $dadoImovel->ArmarioHomeTheater ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'armário para home theater';
	}
	if (intval ( $dadoImovel->ArmarioDormitorioEmpregada ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'armário no dormitório de empregado';
	}
	if (intval ( $dadoImovel->ArmarioAreaServico ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'armário na area de serviço';
	}
	if (intval ( $dadoImovel->PisoAquecido ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'piso aquecido';
	}
	if (intval ( $dadoImovel->PisoArdosia ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'piso ardosia';
	}
	if (intval ( $dadoImovel->PisoBloquete ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'piso bloquete';
	}
	if (intval ( $dadoImovel->Carpete ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'carpete';
	}
	if (intval ( $dadoImovel->CarpeteAcrilico ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'carpete acrílico';
	}
	if (intval ( $dadoImovel->CarpeteMadeira ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'carpete madeira';
	}
	if (intval ( $dadoImovel->CarpeteNylon ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'carpete nylon';
	}
	if (intval ( $dadoImovel->PisoCeramica ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'piso de cerâmica';
	}
	if (intval ( $dadoImovel->CimentoQueimado ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'cimento queimado';
	}
	if (intval ( $dadoImovel->CimentoQueimado ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'cimento queimado';
	}
	if (intval ( $dadoImovel->ContraPiso ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'contra piso';
	}
	if (intval ( $dadoImovel->PisoEmborrachado ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'piso emborrachado';
	}
	if (intval ( $dadoImovel->PisoGranito ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'piso granito';
	}
	if (intval ( $dadoImovel->PisoLaminado ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'piso laminado';
	}
	if (intval ( $dadoImovel->PisoMarmore ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'piso marmore';
	}
	if (intval ( $dadoImovel->PisoTacoMadeira ) != 0) {
		if (! empty ( $descricao ))
			$descricao .= ', ';
		$descricao .= 'piso taco de madeira';
	}
	
	// <NumeroAndar>11</NumeroAndar>
	
	return $descricao . '.';
}
