<?php

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}
require_once PATH_MODEL_ENTITIES . 'Advertising.class.php';
require_once PATH_CONTROLLER . 'AdminServicesController.class.php';

/*
 * This class was required because of requirements solicited by client
 * on day 2013-08-06, It will be send the request of active or deactive
 * a client cadastred on system because was add this require bellow.
 */
require_once PATH_CONTROLLER . 'UserController.class.php';
require_once PATH_CONTROLLER . 'PropertyController.class.php';
require_once PATH_CONTROLLER . 'PropertyStoreController.class.php';
require_once PATH_MODEL_ENTITIES . 'PropertyStore.class.php';

/*
 * Will be needed add the UserServiceController. Because it
 * will be search if a user has some service attribute him
 */
require_once PATH_CONTROLLER . 'UserServiceController.class.php';
require_once PATH_MODEL_ENTITIES . 'UserService.class.php';

/*
 * Will be needed add the StoreServiceController. Because it
 * will be search if a store there are some property attribute him
 */
require_once PATH_CONTROLLER . 'StoreController.class.php';
require_once PATH_MODEL_ENTITIES . 'Store.class.php';

/*
 * Cadastro das propagandas principais
 */
require_once PATH_MODEL_ENTITIES . 'MainAdvertising.class.php';

/*
 * Cadastro dos parceiros
 */
require_once PATH_MODEL_ENTITIES . 'Parceiro.class.php';

/*
 * Validar as Imagens
 */
require_once PATH_CONTROLLER . 'ValidatePhotoController.class.php';

// action will be passed to define what the admin user is making now
$action = isset ( $_POST ['action'] ) ? $_POST ['action'] : NULL;

$adminServicesController = new AdminServicesController ();

$advertisings = $adminServicesController->getAllAdvertisings ();

if ($action == "advertising") // STORE ADVERTISING
	cadastreAdvertising ();
else if ($action == "alterstatus") { // ALTER STATUS OF USER
	
	$userController = new UserController ();
	$result = $userController->alterStatusOfClient ( $_POST ['id_user'], $_POST ['active'] );
	
	if ($result)
		returnToPage ( "success", "active_user" );
	else
		returnToPage ( "failure", "active_user" );
} else if ($action == "deleteProperty") { // DELETE PROPERTY
	
	$propertyId = $_POST ['property_id']; // take ID of property to make the delete it.
	
	$aux = new PropertyController ();
	$property = $aux->getById ( $propertyId );
	
	$propertyHasStores = new PropertyStoreController ();
	$storesInProperty = $propertyHasStores->getPropertyStore ( $property->getidUnico () );
	
	if (! is_null ( $storesInProperty ))
		$wasDeleted = $propertyHasStores->deleteStoresInProperty($property->getidUnico ());
// 		deleteAllStoresInProperty ( $property->getidUnico () );
	$propertyController = new PropertyController ();
	$wasDeleted = $propertyController->deleteProperty ( $propertyId );
	
	/*
	 * Agora fazer o retorna a página inicial do administrador com a devida mensagem
	 */
	if ($wasDeleted)
		returnToPage ( "success", "del_property" );
		// header("Location:". URL_ADMIN_PAGE ."?success=del_property");
	else
		returnToPage ( "failure", "del_property" );
	// header("Location:". URL_ADMIN_PAGE ."?failure=del_property");
} else if ($action == "updateProperty") {
	
	$propertyId = $_POST ['property_id']; // take ID of property to make the delete it.
	
	/*
	 * Enviar o usuário a página para atualizar o imóvel
	 */
	header ( "Location:" . URL . "adm_updateImovel.php?property_id=" . $propertyId );
} else if ($action == "alterPassword") {
	
	$password = $_POST ['password'];
	$idUser = $_POST ['id_user'];
	
	$userController = new UserController ();
	$wasUpdated = $userController->updatePassword ( $idUser, $password );
	
	if ($wasUpdated)
		returnToPage ( "success", "update_password" );
	else
		returnToPage ( "failure", "update_password" );
} else if ($action == "deleteUser") {
	
	$idUser = $_POST ['user_id'];
	
	$userController = new UserController ();
	$wasDeleted = $userController->delete ( $idUser );
	
	if ($wasDeleted) {
		returnToPage ( "success", "delete_user" );
	} else {
		returnToPage ( "failure", "delete_user" );
	}
} else if ($action == "updateUser") {
	
	$userId = $_POST ['user_id']; // take ID of user to make the update him.
	
	/*
	 * Enviar o usuário a página para atualizar o cliente
	 */
	header ( "Location:" . URL . "adm_updateUser.php?user_id=" . $userId );
} else if ($action == "deleteStore") {
	
	$idStore = $_POST ['store_id'];
	
	$storeController = new StoreController ();
	$wasDeleted = $storeController->delete ( $idStore );
	
	if ($wasDeleted) {
		returnToPage ( "success", "delete_store" );
	} else {
		returnToPage ( "failure", "delete_store" );
	}
} else if ($action == "updateStore") {
	
	$storeId = $_POST ['store_id'];
	
	/*
	 * Enviar o usuário a página para atualizar o cliente
	 */
	header ( "Location:" . URL . "adm_updateImob.php?store_id=" . $storeId );
} else if ($action == "mainAdvertising") { // cadastrar as principais propagandas a do banner interativo
	
	cadastreMainAdvertising ();
} else if ($action == "parceiro") {
	
	cadastreParceiro ();
}

// === Upload info.
function cadastreAdvertising() {
	if ($_FILES ['photo'] ['error'] === 0) {
		
		/*
		 * validar a imagem
		 */
		$validatePhotoController = new ValidatePhotoController ();
		if (! $validatePhotoController->isValidWidthAndHeight ( 300, 600, 300, 600, $_FILES ['photo'] ['tmp_name'] )) {
			header ( "Location:" . URL . "adm_cadParc.php?error=size" );
			exit ();
		}
		// Rename the file totimestamp and extension.
		$finalName = basename ( $_FILES ['photo'] ['name'] ) . time () . "." . substr ( $_FILES ['photo'] ['name'], (strlen ( $_FILES ['photo'] ['name'] ) - 3) );
		
		if (move_uploaded_file ( $_FILES ['photo'] ['tmp_name'], PATH_ADVERTISING_PHOTOS . $finalName )) {
			
			$newFileName = PATH_ADVERTISING_PHOTOS . $finalName;
			
			$position = isset ( $_POST ["position"] ) ? $_POST ["position"] : NULL;
			
			$advertising = new Advertising ();
			$advertising->setPhoto ( $newFileName );
			$advertising->setPosition ( $position );
			
			$adminServicesController = new AdminServicesController ();
			
			// verify If there is a photo cadastred before
			$advertisingVerify = new Advertising ();
			$advertisingVerify = $adminServicesController->getByPosition ( $position );
			
			/*
			 * gera foto menor
			 */
			gera_thumb ( $newFileName, PATH_ADVERTISING_PHOTOS, 320, 240 );
			
			if ($advertisingVerify != NULL) {
				$adminServicesController->updateAdvertising ( $advertising );
				returnToPage ( "success", "cad_advertising" );
			} else {
				$adminServicesController->cadastreAdvertising ( $advertising );
				returnToPage ( "success", "update_advertising" );
			}
		} else {
			echo "Falha ao mover a imagem dentro do servidor. Por favor, tente novamente mais tarde!";
			exit ();
		}
	} else
		returnToPage ( "failure", "cad_advertising" );
} // eof function cadastreAdvertising
function getAllAdvertisings() {
	$adminServicesController = new AdminServicesController ();
	
	$advertisings = $adminServicesController->getAllAdvertisings ();
	
	return $advertisings;
}

// === Upload info.
function cadastreMainAdvertising() {
	if ($_FILES ['photo'] ['error'] === 0) {
		
		/*
		 * validar a imagem
		 */
		$validatePhotoController = new ValidatePhotoController ();
		if (! $validatePhotoController->isValidWidthAndHeight ( 600, 2000, 300, 1400, $_FILES ['photo'] ['tmp_name'] )) {
			header ( "Location:" . URL . "adm_cadParc.php?error=size" );
			exit ();
		}
		
		// Rename the file totimestamp and extension.
		$finalName = basename ( $_FILES ['photo'] ['name'] ) . time () . "." . substr ( $_FILES ['photo'] ['name'], (strlen ( $_FILES ['photo'] ['name'] ) - 3) );
		
		if (move_uploaded_file ( $_FILES ['photo'] ['tmp_name'], PATH_MAIN_ADVERTISING_PHOTOS . $finalName )) {
			
			$newFileName = PATH_MAIN_ADVERTISING_PHOTOS . $finalName;
			
			$mainAdvertising = new MainAdvertising ();
			$mainAdvertising->setPhoto ( $newFileName );
			$mainAdvertising->setPosition ( $_POST ["position"] );
			$mainAdvertising->setLink ( $_POST ["link"] );
			$mainAdvertising->setTitle ( $_POST ["title"] );
			$mainAdvertising->setSubTitle ( $_POST ["subtitle"] );
			
			$adminServicesController = new AdminServicesController ();
			
			// verify If there is a photo cadastred before
			$mainAdvertisingVerify = new MainAdvertising ();
			$mainAdvertisingVerify = $adminServicesController->getMainAdversitingByPosition ( intval ( $_POST ["position"] ) );
			
			/*
			 * gerar a foto menor
			 */
			gera_thumb ( $newFileName, PATH_MAIN_ADVERTISING_PHOTOS, 500, 420 );
			
			if ($mainAdvertisingVerify != NULL) {
				$adminServicesController->updateMainAdvertising ( $mainAdvertising );
				returnToPage ( "success", "update_main_advertising" );
			} else {
				$adminServicesController->cadastreMainAdvertising ( $mainAdvertising );
				returnToPage ( "success", "cad_main_advertising" );
			}
		} else {
			echo "Falha ao mover a imagem dentro do servidor. Por favor, tente novamente mais tarde!";
			exit ();
		}
	} else
		returnToPage ( "failure", "cad_main_advertising" );
} // eof function cadastreAdvertising
function getAllMainAdvertisings() {
	$adminServicesController = new AdminServicesController ();
	
	$mainAdvertisings = $adminServicesController->getAllMainAdvertisings ();
	
	return $mainAdvertisings;
}

// === Upload info.
function cadastreParceiro() {
	if ($_FILES ['photo'] ['error'] === 0) {
		
		/*
		 * validar a imagem
		 */
		$validatePhotoController = new ValidatePhotoController ();
		if (! $validatePhotoController->isValidWidthAndHeight ( 300, 500, 300, 500, $_FILES ['photo'] ['tmp_name'] )) {
			header ( "Location:" . URL . "adm_cadParc.php?error=size" );
			exit ();
		}
		
		// Rename the file totimestamp and extension.
		$finalName = basename ( $_FILES ['photo'] ['name'] ) . time () . "." . substr ( $_FILES ['photo'] ['name'], (strlen ( $_FILES ['photo'] ['name'] ) - 3) );
		
		if (move_uploaded_file ( $_FILES ['photo'] ['tmp_name'], PATH_PARCEIRO_PHOTOS . $finalName )) {
			
			$newFileName = PATH_PARCEIRO_PHOTOS . $finalName;
			
			$parc = new Parceiro ();
			$parc->setPhoto ( $newFileName );
			$parc->setPosition ( $_POST ["position"] );
			
			$adminServicesController = new AdminServicesController ();
			
			// verify If there is a photo cadastred before
			$parceirosVerify = new Parceiro ();
			$parceirosVerify = $adminServicesController->getParceiroByPosition ( intval ( $_POST ["position"] ) );
			
			/*
			 * gerar a foto menor
			 */
			gera_thumb ( $newFileName, PATH_PARCEIRO_PHOTOS, 300, 220 );
			
			if ($parceirosVerify != NULL) {
				$adminServicesController->updateParceiro ( $parc );
				returnToPage ( "success", "update_parceiro" );
			} else {
				$adminServicesController->cadastreParceiro ( $parc );
				returnToPage ( "success", "cad_parceiro" );
			}
		} else {
			echo "Falha ao mover a imagem dentro do servidor. Por favor, tente novamente mais tarde!";
			exit ();
		}
	} else
		returnToPage ( "failure", "cad_parceiro" );
} // eof function cadastreAdvertising
function getAllParceiros() {
	$adminServicesController = new AdminServicesController ();
	
	$parceiros = $adminServicesController->getAllParceiros ();
	
	return $parceiros;
}

/*
 * Método para retornar a página inicial de administrador
 * e apresentar as mensagens de acordo com a ação tomada
 * pelo usuário
 */
function returnToPage($success = "success", $code) {
	header ( "Location:" . URL_ADMIN_PAGE . "?" . $success . "=" . $code );
}

/*
 * Função pegada na net que irá gerar o ThumbNail das imagens
 * para abrir de forma mais rápida no server
 */
function gera_thumb($image_path/*, $widthOld, $heightOld*/, $pathToSave, $widthToCalc, $heightToCalc) {
	
	// Pega onde está a imagem
	// $image_file = str_replace('..', '', $_SERVER['QUERY_STRING']);
	// $image_path = PATH_IMG . '/' . $image_file;
	
	// Carrega a imagem
	$img = null;
	
	$extensao = strtolower ( end ( explode ( '.', $image_path ) ) );
	
	if ($extensao == 'jpg' || $extensao == 'jpeg') {
		$img = @imagecreatefromjpeg ( $image_path );
	} else if ($extensao == 'png') {
		$img = @imagecreatefrompng ( $image_path );
		// Se a versão do GD incluir suporte a GIF, mostra...
	} elseif ($extensao == 'gif') {
		$img = @imagecreatefromgif ( $image_path );
	}
	
	// Se a imagem foi carregada com sucesso, testa o tamanho da mesma
	if ($img) {
		// Pega o tamanho da imagem e proporção de resize
		$width = imagesx ( $img );
		$height = imagesy ( $img );
		$scale = min ( $widthToCalc / $width, $heightToCalc / $height );
		
		// echo "WIDTH: ".$widthOld. " HEIGHT: ".$heightOld;
		
		// $rotate = 0;
		// if (floatval($heightOld) < floatval($widthOld))
		// $rotate = 270;
		
		// echo "Rotate:: ". $rotate;
		// exit();
		
		// Se a imagem é maior que o permitido, encolhe ela
		if ($scale < 1) {
			$new_width = floor ( $scale * $width );
			$new_height = floor ( $scale * $height );
			// Cria uma imagem temporária
			$tmp_img = imagecreatetruecolor ( $new_width, $new_height );
			// Copia e resize a imagem velha na nova
			imagecopyresized ( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
			imagedestroy ( $img );
			unlink ( $image_path );
			$img = $tmp_img;
			
			$ext = strtolower ( end ( explode ( '.', $image_path ) ) );
			if ($ext == "jpg" || $ext == "jpeg") {
				imagejpeg ( $img, $pathToSave . basename ( $image_path ), IMAGE_QUALITY );
				return true;
			} elseif ($ext == "gif") {
				imagepng ( $img, $pathToSave . basename ( $image_path ) );
				return true;
			} elseif ($ext == "png") {
				imagepng ( $img, $pathToSave . basename ( $image_path ) );
				return true;
			} else {
				echo ("Formato de destino nao suportado");
				exit ();
			}
		}
	}
	
	// Cria uma imagem de erro se necessário
	if (! $img) {
		$img = imagecreate ( MAX_WIDTH, MAX_HEIGHT );
		imagecolorallocate ( $img, 204, 204, 204 );
		$c = imagecolorallocate ( $img, 153, 153, 153 );
		$c1 = imagecolorallocate ( $img, 0, 0, 0 );
		imageline ( $img, 0, 0, MAX_WIDTH, MAX_HEIGHT, $c );
		imageline ( $img, MAX_WIDTH, 0, 0, MAX_HEIGHT, $c );
		imagestring ( $img, 2, 12, 55, 'erro ao carregar imagem', $c1 );
	}
}

?>
