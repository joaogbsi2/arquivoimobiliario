<?php



$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_CONTROLLER. 'CityController.class.php';


$action = isset($_POST['action']) ? $_POST['action'] : NULL;


if ($action == "cadastre")
    cadastreCity ();
else if ($action == "update")
    showPageToUpdateCity ();
else if ($action == "delete")
    deleteCity();
else if ($action == "performUpdate")
    updateCity ();

function cadastreCity(){
    try{
     $params = array(
                  "name" => $_POST["city"],
                  "state" => $_POST["state"]
               );

      $cityController = new CityController();
      $cityController->addCity($params);
          
    } catch (Exception $exc) {
      echo $exc->getTraceAsString();
      returnToPage("failure","cad_city");
    }
    
    returnToPage("success","cad_city");

}//eof function cadastreUser




function deleteCity(){
    
    $cityId = $_POST['city_id'];
    
    $cityController = new CityController();
    
    $wasDeleted = $cityController->deleteCity($cityId);
    if ($wasDeleted)
        returnToPage ("success", "del_city");
    else
        returnToPage ("failure", "del_city");
}

/*
 * Function that will go make update of user selected
 */
function updateCity(){
    
    try{
     $params = array(
                  "id" => $_POST['id'],
                  "name" => $_POST["city"],
                  "state" => $_POST["state"]
               );

      $cityController = new CityController();
      $wasUpdated = $cityController->updateCity($params);
      
      if (!$wasUpdated)
            returnToPage ("failure","update_city");
        else 
            returnToPage ("success","update_city");
        
          
    } catch (Exception $exc) {
      echo $exc->getTraceAsString();
      returnToPage("failure","cad_city");
    }
    
}


//mostrar a página para atualização dos dados
function showPageToUpdateCity(){
    
    $cityId = $_POST['city_id'];
    
    header("Location:". URL. "adm_updateCity.php?city_id=".$cityId);
}
/*
 * return to admin page home with success or failure message
 */
function returnToPage($success = "success",$code){
    
    header("Location:" .URL_ADMIN_PAGE. "?" .$success. "=" .$code);
}
?>
