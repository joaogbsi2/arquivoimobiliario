<?php

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_CONTROLLER. 'DecoracaoController.class.php';

/*
 * Validar as Imagens
 */
require_once PATH_CONTROLLER.       'ValidatePhotoController.class.php';

$action = isset($_POST['action']) ? $_POST['action'] : NULL;


if ($action == "cadastre")
    cadastreDecoracao ();

function cadastreDecoracao(){
    try {
      //=== Upload info.
    //  $photoFolder = $_SERVER['DOCUMENT_ROOT'] . "/TM_Project1/images/user_photo/";
      $photoFolder = PATH_DECORACAO_PHOTOS;

      $params = array(
                  "title" => $_POST["title"],
                  "subTitle" => $_POST["subTitle"],
                  "description" => $_POST["description"],
                  "photo" => ""
                );
      
      //=== If don't capture the photo, add user.
      if ($_FILES['photo']['error'] != 0) {

        $decoController = new DecoracaoController();
        $decoController->addDecoracao($params);

      } else {
          
        removeAllFiles();
        //Rename the file, for email+timestamp now and extension.
        $finalName = "deco" . time() . "." . substr($_FILES['photo']['name'],
                     (strlen($_FILES['photo']['name']) - 3));

        $imageIsValid = true;
        /*
         * validar a imagem
         */
        $validatePhotoController = new ValidatePhotoController();
        if (!$validatePhotoController->isValidWidthAndHeight(600, 1200, 600, 1200, $_FILES['photo']['tmp_name'])) {
            $imageIsValid = false;            
        }
        
        if ($imageIsValid){ 
        	if (move_uploaded_file($_FILES['photo']['tmp_name'], $photoFolder . $finalName)) {

	          $newFileName = $photoFolder . $finalName;
	          $decoController = new DecoracaoController();
	        
	          $params["photo"] = $newFileName;
	
	          $decoController->addDecoracao($params);
	          
	          /*
	           * gerar foto menor
	           */
	          gera_thumb($newFileName);
	
	        } else {
	          echo "Falha ao mover a imagem dentro do servidor. Por favor, tente novamente mais tarde!";
	          exit();
	        }
	      
	}else {
		$decoController = new DecoracaoController();
		$decoController->addDecoracao($params);
		header ("Location:" .URL. "adm_cadDeco.php?error=size");
            	exit();
	}
      }
    } catch (Exception $exc) {
      echo $exc->getTraceAsString();
    }
    
    returnToPage("success","cad_deco");

}//eof function cadastreUser


/*
 * return to admin page home with success or failure message
 */
function returnToPage($success = "success",$code){
    
    header("Location:" .URL_ADMIN_PAGE. "?" .$success. "=" .$code);
}


// function to remove All Photos
function removeAllFiles(){
    
    $files = glob(PATH_DECORACAO_PHOTOS.'*'); // get all file names
    foreach($files as $file){ // iterate files
        if(is_file($file))
            unlink($file); // delete file
    }
}

/*
 * Função pegada na net que irá gerar o ThumbNail das imagens
 * para abrir de forma mais rápida no server
 */
function gera_thumb($image_path/*, $widthOld, $heightOld*/) { 
    
    # Pega onde está a imagem
    #$image_file = str_replace('..', '', $_SERVER['QUERY_STRING']);
    #$image_path = PATH_IMG . '/' . $image_file;

    # Carrega a imagem
    $img = null;

    $extensao = strtolower(end(explode('.',$image_path)));

    if ($extensao == 'jpg' || $extensao == 'jpeg') {
        $img = @imagecreatefromjpeg($image_path);
    } else if ($extensao == 'png') {
        $img = @imagecreatefrompng($image_path);
        // Se a versão do GD incluir suporte a GIF, mostra...
    } elseif ($extensao == 'gif') {
        $img = @imagecreatefromgif($image_path);
    }

    // Se a imagem foi carregada com sucesso, testa o tamanho da mesma
    if ($img) {
        // Pega o tamanho da imagem e proporção de resize
        $width = imagesx($img);
        $height = imagesy($img);
        $scale = min(500/$width, 420/$height);
        
//        echo "WIDTH: ".$widthOld. " HEIGHT: ".$heightOld;
        
//        $rotate = 0;
//        if (floatval($heightOld) < floatval($widthOld))
//            $rotate = 270;
        
//        echo "Rotate:: ". $rotate;
//        exit();
        
        // Se a imagem é maior que o permitido, encolhe ela
        if ($scale < 1) {
            $new_width = floor($scale * $width);
            $new_height = floor($scale * $height);
            // Cria uma imagem temporária
            $tmp_img = imagecreatetruecolor($new_width, $new_height);
            // Copia e resize a imagem velha na nova
            imagecopyresized($tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
            imagedestroy($img);
            unlink($image_path);
            $img = $tmp_img;
            
            $ext = strtolower(end(explode('.', $image_path)));
            if($ext == "jpg" || $ext == "jpeg"){
                imagejpeg($img, PATH_DECORACAO_PHOTOS . basename($image_path), IMAGE_QUALITY);
                return true;
            }
            elseif ($ext == "gif"){
                imagepng($img, PATH_DECORACAO_PHOTOS . basename($image_path));
                return true;
            }
            elseif ($ext == "png"){
                imagepng($img, PATH_DECORACAO_PHOTOS . basename($image_path));
                return true;
            }
            else {
                echo("Formato de destino nao suportado");
                exit();
            }
            
        }
    }

    // Cria uma imagem de erro se necessário
    if (!$img) {
        $img = imagecreate(MAX_WIDTH, MAX_HEIGHT);
        imagecolorallocate($img, 204, 204, 204);
        $c = imagecolorallocate($img, 153, 153, 153);
        $c1 = imagecolorallocate($img, 0, 0, 0);
        imageline($img, 0, 0, MAX_WIDTH, MAX_HEIGHT, $c);
        imageline($img, MAX_WIDTH, 0, 0, MAX_HEIGHT, $c);
        imagestring($img, 2, 12, 55, 'erro ao carregar imagem', $c1);
    }
}
?>