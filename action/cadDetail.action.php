<?php

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_CONTROLLER. 'DetailController.class.php';


$action = isset($_POST['action']) ? $_POST['action'] : NULL;


if ($action == "cadastre")
    cadastreDetail ();
else if ($action == "update")
    showPageToUpdateDetail ();
else if ($action == "delete")
    deleteDetail();
else if ($action == "performUpdate")
    updateDetail ();

function cadastreDetail(){
    try{
      $detailController = new DetailController();
      $detailController->addDetail(trim($_POST["detail"]));
          
    } catch (Exception $exc) {
      echo $exc->getTraceAsString();
      returnToPage("failure","cad_detail");
    }
    
    returnToPage("success","cad_detail");

}//eof function cadastreUser




function deleteDetail(){
    
    $detailId = $_POST['detail_id'];
    
    $detailController = new DetailController();
    
    $wasDeleted = $detailController->deleteDetail($detailId);
    if ($wasDeleted)
        returnToPage ("success", "del_detail");
    else
        returnToPage ("failure", "del_detail");
}

/*
 * Function that will go make update of user selected
 */
function updateDetail(){
    
    try{
     $params = array(
                  "id" => $_POST['detail_id'],
                  "name" => $_POST["detail"]
               );

      $detailController = new DetailController();
      $wasUpdated = $detailController->updateDetail($params);
      
      if (!$wasUpdated)
            returnToPage ("failure","update_detail");
        else 
            returnToPage ("success","update_detail");
        
          
    } catch (Exception $exc) {
      echo $exc->getTraceAsString();
      returnToPage("failure","cad_city");
    }
    
}


//mostrar a página para atualização dos dados
function showPageToUpdateDetail(){
    
    $detailId = $_POST['detail_id'];
    
    header("Location:". URL. "adm_updateDetail.php?detail_id=".$detailId);
}
/*
 * return to admin page home with success or failure message
 */
function returnToPage($success = "success",$code){
    
    header("Location:" .URL_ADMIN_PAGE. "?" .$success. "=" .$code);
}
?>
