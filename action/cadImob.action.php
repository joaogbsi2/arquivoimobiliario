<?php

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_CONTROLLER . 'StoreController.class.php';
require_once PATH_UTIL.'Ortografia.php';

/*
 * Validar as Imagens
 */
require_once PATH_CONTROLLER . 'ValidatePhotoController.class.php';

$action = isset ( $_POST ['action'] ) ? $_POST ['action'] : NULL;

if ($action == "cadastre")
	cadastreImob ();
if ($action == "update")
	updateImob ();
function cadastreImob() {
	try {
		// === Upload info.
		// $photoFolder = $_SERVER['DOCUMENT_ROOT'] . "/TM_Project1/images/user_photo/";
		$photoFolder = PATH_STORE_PHOTOS;
		
		$params = array (
				"name" => $_POST ["name"],
// 				"description" => $_POST ["description"],
				"mail" => $_POST ["mail"],
				"photo" => "",
				"endereco"=> $_POST["endereco"], 
				"telefone"=> $_POST["telefone"], 
				"responsavel"=> $_POST["responsavel"]
		)
		;
		
		// === If don't capture the photo, add user.
		if ($_FILES ['photo'] ['error'] != 0) {
			
			$storeController = new StoreController ();
			$storeController->addStore ( $params );
		} else {
			// Rename the file, for email+timestamp now and extension.
			$finalName = $_POST ["name"] . time () . "." . substr ( $_FILES ['photo'] ['name'], (strlen ( $_FILES ['photo'] ['name'] ) - 3) );
			
			$imageIsValid = true;
			/*
			 * validar a imagem
			 */
			$validatePhotoController = new ValidatePhotoController ();
			if (! $validatePhotoController->isValidWidthAndHeight ( 70, 1200, 70, 1200, $_FILES ['photo'] ['tmp_name'] )) {
				$imageIsValid = false;
			}
			
			if ($imageIsValid) {
				if (move_uploaded_file ( $_FILES ['photo'] ['tmp_name'], $photoFolder . $finalName )) {
					
					$newFileName = $photoFolder . $finalName;
					$storeController = new StoreController ();
					
					$params ["photo"] = $newFileName;
					
					$storeController->addStore ( $params );
					
					/*
					 * gera a foto menor
					 */
					gera_thumb ( $newFileName );
				} else {
					echo "Falha ao mover a imagem dentro do servidor. Por favor, tente novamente mais tarde!";
					exit ();
				}
			} else {
				$storeController = new StoreController ();
				$storeController->addStore ( $params );
				header ( "Location:" . URL . "adm_cadImob.php?error=size" );
				exit ();
			}
		}
	} catch ( Exception $exc ) {
		echo $exc->getTraceAsString ();
	}
	
	returnToPage ( "success", "cad_store" );
} // eof function cadastreUser

/*
 * Function that will go make update of user selected
 */
function updateImob() {
	try {
		// === Upload info.
		// $photoFolder = $_SERVER['DOCUMENT_ROOT'] . "/TM_Project1/images/user_photo/";
		$photoFolder = PATH_STORE_PHOTOS;
		
		$params = array (
				"id" => $_POST['store_id'],
				"name" => $_POST ["name"],
// 				"description" => $_POST ["description"],
				"mail" => $_POST ["mail"],
				"photo" => "",
				"endereco"=> $_POST["endereco"], 
				"telefone"=> $_POST["telefone"], 
				"responsavel"=> $_POST["responsavel"]
		)
		;
		
		// === If don't capture the photo, add user.
		if ($_FILES ['photo'] ['error'] != 0) {
			
			$storeController = new StoreController ();
			$wasUpdated = $storeController->updateStore ( $params );
			
			if (! $wasUpdated)
				returnToPage ( "failure", "update_store" );
			else
				returnToPage ( "success", "update_store" );
		} else {
			
			// Rename the file, for email+timestamp now and extension.
			$finalName = Ortografia::retirarAcento($_POST ["name"] . time () . "." . substr ( $_FILES ['photo'] ['name'], (strlen ( $_FILES ['photo'] ['name'] ) - 3) ));
			
			$imageIsValid = true;
			/*
			 * validar a imagem
			 */
			$validatePhotoController = new ValidatePhotoController ();
			if (! $validatePhotoController->isValidWidthAndHeight ( 600, 1200, 600, 1200, $_FILES ['photo'] ['tmp_name'] )) {
				$imageIsValid = false;
			}
			
			if ($imageIsValid) {
				if (move_uploaded_file ( $_FILES ['photo'] ['tmp_name'], $photoFolder . $finalName )) {
					
					$newFileName = $photoFolder . $finalName;
					$storeController = new StoreController ();
					
					$params ["photo"] = $newFileName;
					
					$wasUpdated = $storeController->updateStore ( $params );
					
					/*
					 * gera a fot menor
					 */
					gera_thumb ( $newFileName );
					
					if (! $wasUpdated)
						returnToPage ( "failure", "update_store" );
					else
						returnToPage ( "success", "update_store" );
				} else {
					echo "Falha ao mover a imagem dentro do servidor. Por favor, tente novamente mais tarde!";
					exit ();
				}
			} else {
				$storeController = new StoreController ();
				$wasUpdated = $storeController->updateStore ( $params );
				header ( "Location:" . URL . "adm_cadImob.php?error=size" );
				exit ();
			}
		}
	} catch ( Exception $exc ) {
		echo $exc->getTraceAsString ();
	}
}

/*
 * return to admin page home with success or failure message
 */
function returnToPage($success = "success", $code) {
	header ( "Location:" . URL_ADMIN_PAGE . "?" . $success . "=" . $code );
}

/*
 * Função pegada na net que irá gerar o ThumbNail das imagens
 * para abrir de forma mais rápida no server
 */
function gera_thumb($image_path/*, $widthOld, $heightOld*/) {
	
	// Pega onde está a imagem
	// $image_file = str_replace('..', '', $_SERVER['QUERY_STRING']);
	// $image_path = PATH_IMG . '/' . $image_file;
	
	// Carrega a imagem
	$img = null;
	
	$extensao = strtolower ( end ( explode ( '.', $image_path ) ) );
	
	if ($extensao == 'jpg' || $extensao == 'jpeg') {
		$img = @imagecreatefromjpeg ( $image_path );
	} else if ($extensao == 'png') {
		$img = @imagecreatefrompng ( $image_path );
		// Se a versão do GD incluir suporte a GIF, mostra...
	} elseif ($extensao == 'gif') {
		$img = @imagecreatefromgif ( $image_path );
	}
	
	// Se a imagem foi carregada com sucesso, testa o tamanho da mesma
	if ($img) {
		// Pega o tamanho da imagem e proporção de resize
		$width = imagesx ( $img );
		$height = imagesy ( $img );
		$scale = min ( MAX_WIDTH / $width, MAX_HEIGHT / $height );
		
		// echo "WIDTH: ".$widthOld. " HEIGHT: ".$heightOld;
		
		// $rotate = 0;
		// if (floatval($heightOld) < floatval($widthOld))
		// $rotate = 270;
		
		// echo "Rotate:: ". $rotate;
		// exit();
		
		// Se a imagem é maior que o permitido, encolhe ela
		if ($scale < 1) {
			$new_width = floor ( $scale * $width );
			$new_height = floor ( $scale * $height );
			// Cria uma imagem temporária
			$tmp_img = imagecreatetruecolor ( $new_width, $new_height );
			// Copia e resize a imagem velha na nova
			imagecopyresized ( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
			imagedestroy ( $img );
			unlink ( $image_path );
			$img = $tmp_img;
			
			$ext = strtolower ( end ( explode ( '.', $image_path ) ) );
			if ($ext == "jpg" || $ext == "jpeg") {
				imagejpeg ( $img, PATH_STORE_PHOTOS . basename ( $image_path ), IMAGE_QUALITY );
				return true;
			} elseif ($ext == "gif") {
				imagepng ( $img, PATH_STORE_PHOTOS . basename ( $image_path ) );
				return true;
			} elseif ($ext == "png") {
				imagepng ( $img, PATH_STORE_PHOTOS . basename ( $image_path ) );
				return true;
			} else {
				echo ("Formato de destino nao suportado");
				exit ();
			}
		}
	}
	
	// Cria uma imagem de erro se necessário
	if (! $img) {
		$img = imagecreate ( MAX_WIDTH, MAX_HEIGHT );
		imagecolorallocate ( $img, 204, 204, 204 );
		$c = imagecolorallocate ( $img, 153, 153, 153 );
		$c1 = imagecolorallocate ( $img, 0, 0, 0 );
		imageline ( $img, 0, 0, MAX_WIDTH, MAX_HEIGHT, $c );
		imageline ( $img, MAX_WIDTH, 0, 0, MAX_HEIGHT, $c );
		imagestring ( $img, 2, 12, 55, 'erro ao carregar imagem', $c1 );
	}
}
?>