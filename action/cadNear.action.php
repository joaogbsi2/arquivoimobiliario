<?php

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_CONTROLLER. 'NearController.class.php';


$action = isset($_POST['action']) ? $_POST['action'] : NULL;


if ($action == "cadastre")
    cadastreNear ();
else if ($action == "update")
    showPageToUpdateNear ();
else if ($action == "delete")
    deleteNear();
else if ($action == "performUpdate")
    updateNear ();

function cadastreNear(){
    try{
      $nearController = new NearController();
      $nearController->addNear(trim($_POST["near"]));
          
    } catch (Exception $exc) {
      echo $exc->getTraceAsString();
      returnToPage("failure","cad_near");
    }
    
    returnToPage("success","cad_near");

}//eof function cadastreUser




function deleteNear(){
    
    $nearId = $_POST['near_id'];
    
    $nearController = new NearController();
    
    $wasDeleted = $nearController->deleteNear($nearId);
    if ($wasDeleted)
        returnToPage ("success", "del_near");
    else
        returnToPage ("failure", "del_near");
}

/*
 * Function that will go make update of user selected
 */
function updateNear(){
    
    try{
     $params = array(
                  "id" => $_POST['near_id'],
                  "name" => $_POST["near"]
               );

      $nearController = new NearController();
      $wasUpdated = $nearController->updateNear($params);
      
      if (!$wasUpdated)
            returnToPage ("failure","update_near");
        else 
            returnToPage ("success","update_near");
        
          
    } catch (Exception $exc) {
      echo $exc->getTraceAsString();
    }
    
}


//mostrar a página para atualização dos dados
function showPageToUpdateNear(){
    
    $nearId = $_POST['near_id'];
    
    header("Location:". URL. "adm_updateNear.php?near_id=".$nearId);
}
/*
 * return to admin page home with success or failure message
 */
function returnToPage($success = "success",$code){
    
    header("Location:" .URL_ADMIN_PAGE. "?" .$success. "=" .$code);
}
?>
