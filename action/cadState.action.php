<?php



$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_CONTROLLER. 'StateController.class.php';


$action = isset($_POST['action']) ? $_POST['action'] : NULL;


if ($action == "cadastre")
	cadastreState ();
else if ($action == "update")
	showPageToUpdateState ();
else if ($action == "delete")
	deleteState();
else if ($action == "performUpdate")
	updateState ();

function cadastreState(){
	try{
		$params = array(
				"name" => $_POST["state"],
				"uf" => $_POST["uf"]
		);

		$stateController = new StateController();
		$stateController->addState($params);

	} catch (Exception $exc) {
		echo $exc->getTraceAsString();
		returnToPage("failure","cad_state");
	}

	returnToPage("success","cad_state");

}//eof function cadastreUser




function deleteState(){

	$stateId = $_POST['state_id'];

	$stateController = new StateController();

	$wasDeleted = $stateController->deleteState($stateId);
	if ($wasDeleted)
		returnToPage ("success", "del_state");
	else
		returnToPage ("failure", "del_state");
}

/*
 * Function that will go make update of user selected
 */
function updateState(){

	try{
		$params = array(
				"id" => $_POST['id'],
				"name" =>$_POST['state'], 
				"uf"=>$_POST['uf']
		);

		$stateController = new StateController();
		$wasUpdated = $stateController->updateState($params);

		if (!$wasUpdated)
			returnToPage ("failure","update_state");
		else
			returnToPage ("success","update_state");


	} catch (Exception $exc) {
		echo $exc->getTraceAsString();
		returnToPage("failure","cad_state");
	}

}


//mostrar a página para atualização dos dados
function showPageToUpdateState(){

	$stateId = $_POST['state_id'];

	header("Location:". URL. "adm_updateState.php?state_id=".$stateId);
}
/*
 * return to admin page home with success or failure message
 */
function returnToPage($success = "success",$code){

	header("Location:" .URL_ADMIN_PAGE. "?" .$success. "=" .$code);
}
?>
