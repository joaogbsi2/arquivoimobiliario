<?php

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_CONTROLLER. 'UserController.class.php';
require_once PATH_CONTROLLER. 'ServiceController.class.php';
require_once PATH_CONTROLLER. 'UserServiceController.class.php';

require_once PATH_CONTROLLER. 'ValidatePhotoController.class.php';


$action = isset($_POST['action']) ? $_POST['action'] : NULL;


if ($action == "cadastre")
    cadastreUser ();
if ($action == "update")
    updateUser();

function cadastreUser(){
	echo "";
    try {
      //=== Upload info.
    //  $photoFolder = $_SERVER['DOCUMENT_ROOT'] . "/TM_Project1/images/user_photo/";
      $photoFolder = PATH_USER_PHOTOS;

      $params = array(
                  "name" => $_POST["name"],
                  "password" => isset($_POST["passwd1"]) ? $_POST["passwd1"] : 'webmovel123' ,
                  "mobilePhone" => $_POST["mobile_phone"],
                  "phone" => $_POST["phone"],
                  "email" => $_POST["email"], 
                  "photo" => ""
                );


      //=== If don't capture the photo, add user.
      if ($_FILES['photo']['error'] != 0) {

        $userController = new UserController();
        $userController->addUser($params);

      } else {
        //Rename the file, for email+timestamp now and extension.
        $finalName = $_POST["email"] . time() . "." . substr($_FILES['photo']['name'],
                     (strlen($_FILES['photo']['name']) - 3));

	$imageIsValid = true;
        /*
         * validar a imagem
         */
        $validatePhotoController = new ValidatePhotoController();
        if (!$validatePhotoController->isValidWidthAndHeight(600, 1200, 600, 1000, $_FILES['photo']['tmp_name'])) {
		$imageIsValid = false;
        }
        
        if ($imageIsValid) {
        
	        if (move_uploaded_file($_FILES['photo']['tmp_name'], $photoFolder . $finalName)) {
	
	          $newFileName = $photoFolder . $finalName;
	          $userController = new UserController();
	
	          $params["photo"] = $newFileName;
	
	          $userController->addUser($params);
	
	          /*
	           * gera a foto menor
	           */
	          gera_thumb($newFileName);
	          
	        } else {
	          echo "Falha ao mover a imagem dentro do servidor. Por favor, tente novamente mais tarde!";
	          exit();
	        }
	} else {
		$userController = new UserController();
		$userController->addUser($params);
		header ("Location:" .URL. "adm_cadUser.php?error=size");
            	exit();
	}
      }

      $isServiceProvider = isServiceProvider();
      if ($isServiceProvider){
          cadastreServiceProvider($userController->getByUserEmail($params['email'])->getId());
      } else {
          returnToPage("success","cad_user");
      }

    } catch (Exception $exc) {
      echo $exc->getTraceAsString();
    }

}//eof function cadastreUser



/*
 * Função que irá ser chamada para verificar se o usuário informou que é provedor de serviços
 * e se for fazer o cadastro do mesmo como provedor de serviços
 */
function isServiceProvider(){
    
    /*
     * se selecionou no radio que é um provedor de serviço! Então retorna true
     */
    if (isset($_POST['youAreServiceProvider']) && $_POST['youAreServiceProvider'] == "yes")
        return true;
    
    return false;
}



/*
 * Método para atribuir e cadastrar os serviços ao usuário recém-cadastrado
 */
function cadastreServiceProvider($idUser){
    
    //Get user for record in user_service.
    if ((!empty($idUser) ) && ($idUser != "")) {
      $userController = new UserController();
      $userServiceController = new UserServiceController();

      $user = $userController->getById($idUser);

      if ($user instanceof User) {
        if (isset($_POST['serviceProvider'])) {

          foreach ($_POST['serviceProvider'] as $choice) {
            $userChoice[] = $choice;
          }
          
          /*
           * Alterei o if abaixo, ao invés de ficar dando includes e location
           * essa página pelo que notei apenas apresentava os dados na tela
           * como não irei querer isso então eu apenas mando a página inicial
           * PAGINA DO ADMIN 
           */
          if ($userServiceController->createServiceProvider($userChoice, $user)) {
//              echo "retornou aqui";
//              exit();
//            header("Location: confirmRecordedUser.php?user=" . $user->getId());
              returnToPage("success","cad_upd_user");
          } else {
            $error['ServiceProviderNotRecorded'] = TRUE;
            echo 'Erro! Infelizmente não foi possível cadastrar-se como um provedor
                  de serviços neste momento. Tente mais tarde! Agradecemos sua paciência.';
          }
          
        } elseif ($_POST['choiceNo'] == 'no') {//caso não tenha informado ser provedor de serviço
            returnToPage("success","cad_upd_user");
//          header("Location: confirmRecordedUser.php?user=" . $user->getId());
        }
      } else {
        $error['UserNotFound'] = TRUE;
        header("Content-Type: text/html; charset=UTF-8");
        echo "Erro! O usuário informado não foi encontrado.";
        exit();
      }
    } else {
      $error['DontUserParam'] = TRUE;
      header("Content-Type: text/html; charset=UTF-8");
      echo 'Erro! Você não informou um usuário.';
      exit();
    }
}



/*
 * Function that will go make update of user selected
 */
function updateUser(){
    
    try {
      //=== Upload info.
    //  $photoFolder = $_SERVER['DOCUMENT_ROOT'] . "/TM_Project1/images/user_photo/";
      $photoFolder = PATH_USER_PHOTOS;

      $params = array(
                  "name" => $_POST["name"],
//                  "password" => $_POST["passwd1"],
                  "mobilePhone" => $_POST["mobile_phone"],
                  "phone" => $_POST["phone"],
                  "email" => $_POST["email"], 
                  "photo" => "",
                  "user_id" => $_POST['user_id']
                );

      //=== If don't capture the photo, add user.
      if ($_FILES['photo']['error'] != 0) {

        $userController = new UserController();
        $wasUpdated = $userController->updateUser($params);
        
        if (!$wasUpdated)
            returnToPage ("failure","update_user");
        
        else {
            /*
             * se antes ele era provedor de serviço e continua a ser
             * então deleta todas e adiciona os selecionados
             */
            if ($_POST['wasServiceProvider'] == "true" && trim($_POST['youAreServiceProvider']) == "yes"){

                if (!is_null(userHasServiceProvider($_POST['user_id'])))
                    deleteAllServiceProviderToUser($_POST['user_id']);

            } else if ($_POST['wasServiceProvider'] == "true" && trim($_POST['youAreServiceProvider']) == "no") {
                /*
                 * se era provedor de serviço e não é mais então deleta tudo
                 */
                if (!is_null(userHasServiceProvider($_POST['user_id'])))
                    deleteAllServiceProviderToUser($_POST['user_id']);

            }
        }
        
      } else {
          
//          echo "com foto";
//          exit();
        //Rename the file, for email+timestamp now and extension.
        $finalName = $_POST["email"] . time() . "." . substr($_FILES['photo']['name'],
                     (strlen($_FILES['photo']['name']) - 3));

	$imageIsValid = true;
        /*
         * validar a imagem
         */
        $validatePhotoController = new ValidatePhotoController();
        if (!$validatePhotoController->isValidWidthAndHeight(600, 1200, 600, 1000, $_FILES['photo']['tmp_name'])) {
		$imageIsValid = false;
        }
        
        if ($imageIsValid) {
	        if (move_uploaded_file($_FILES['photo']['tmp_name'], $photoFolder . $finalName)) {
	
	          $newFileName = $photoFolder . $finalName;
	          $userController = new UserController();
	
	          $params["photo"] = $newFileName;
	
	          $wasUpdated = $userController->updateUser($params);
	          
	          /*
	           * gera a foto menor
	           */
	          gera_thumb($newFileName);
	          
	          if (!$wasUpdated)
	              
	            returnToPage ("failure","update_user");
	          
	          else {
	            
	            /*
	             * se antes ele era provedor de serviço e continua a ser
	             * então deleta todas e adiciona os selecionados
	             */
	            if ($_POST['wasServiceProvider'] == "true" && trim($_POST['youAreServiceProvider']) == "yes"){
	
	                if (!is_null(userHasServiceProvider($_POST['user_id'])))
	                    deleteAllServiceProviderToUser($_POST['user_id']);
	
	            } else if ($_POST['wasServiceProvider'] == "true" && trim($_POST['youAreServiceProvider']) == "no") {
	                /*
	                 * se era provedor de serviço e não é mais então deleta tudo
	                 */
	                if (!is_null(userHasServiceProvider($_POST['user_id'])))
	                    deleteAllServiceProviderToUser($_POST['user_id']);
	
	            } 
	          }
	        } else {
	          echo "Falha ao mover a imagem dentro do servidor. Por favor, tente novamente mais tarde!";
	          exit();
	        }
	} else {
		$userController = new UserController();
		$wasUpdated = $userController->updateUser($params);
		
		/*
	         * se antes ele era provedor de serviço e continua a ser
	         * então deleta todas e adiciona os selecionados
	         */
	        if ($_POST['wasServiceProvider'] == "true" && trim($_POST['youAreServiceProvider']) == "yes"){
	
	            if (!is_null(userHasServiceProvider($_POST['user_id'])))
	                 deleteAllServiceProviderToUser($_POST['user_id']);
	
	        } else if ($_POST['wasServiceProvider'] == "true" && trim($_POST['youAreServiceProvider']) == "no") {
	                /*
	                 * se era provedor de serviço e não é mais então deleta tudo
	                 */
	                if (!is_null(userHasServiceProvider($_POST['user_id'])))
	                    deleteAllServiceProviderToUser($_POST['user_id']);
	
	        } 
		header ("Location:" .URL. "adm_listUser.php?error=size");
           	exit();
	}
      }

      $isServiceProvider = isServiceProvider();
      if ($isServiceProvider){
          cadastreServiceProvider($_POST['user_id']);
      } else {
          returnToPage("success","update_user");
      }

    } catch (Exception $exc) {
      echo $exc->getTraceAsString();
    }
}



/*
 * Function that will see if user has service provider. If has 
 * this function return true. Otherwise return false
 */
function userHasServiceProvider($idUser){
    
    $userHasService = new UserServiceController();
    $servicesProviderToUser = $userHasService->getServiceProvider($idUser);
    
    if (!is_null($servicesProviderToUser))
        return $servicesProviderToUser;
    
    return NULL;
}


/*
 * function that will remove all service provider to this user
 */
function deleteAllServiceProviderToUser($idUser) {
   
   $userServiceControllerDelete = new UserServiceController();
   $wasDeleted = $userServiceControllerDelete->deleteServiceProvider($idUser);
        
   return $wasDeleted;
}

/*
 * return to admin page home with success or failure message
 */
function returnToPage($success = "success",$code){
    
    header("Location:" .URL_ADMIN_PAGE. "?" .$success. "=" .$code);
}



/*
 * Função pegada na net que irá gerar o ThumbNail das imagens
 * para abrir de forma mais rápida no server
 */
function gera_thumb($image_path/*, $widthOld, $heightOld*/) { 
    
    # Pega onde está a imagem
    #$image_file = str_replace('..', '', $_SERVER['QUERY_STRING']);
    #$image_path = PATH_IMG . '/' . $image_file;

    # Carrega a imagem
    $img = null;

    $extensao = strtolower(end(explode('.',$image_path)));

    if ($extensao == 'jpg' || $extensao == 'jpeg') {
        $img = @imagecreatefromjpeg($image_path);
    } else if ($extensao == 'png') {
        $img = @imagecreatefrompng($image_path);
        // Se a versão do GD incluir suporte a GIF, mostra...
    } elseif ($extensao == 'gif') {
        $img = @imagecreatefromgif($image_path);
    }

    // Se a imagem foi carregada com sucesso, testa o tamanho da mesma
    if ($img) {
        // Pega o tamanho da imagem e proporção de resize
        $width = imagesx($img);
        $height = imagesy($img);
        $scale = min(MAX_WIDTH/$width, MAX_HEIGHT/$height);
        
//        echo "WIDTH: ".$widthOld. " HEIGHT: ".$heightOld;
        
//        $rotate = 0;
//        if (floatval($heightOld) < floatval($widthOld))
//            $rotate = 270;
        
//        echo "Rotate:: ". $rotate;
//        exit();
        
        // Se a imagem é maior que o permitido, encolhe ela
        if ($scale < 1) {
            $new_width = floor($scale * $width);
            $new_height = floor($scale * $height);
            // Cria uma imagem temporária
            $tmp_img = imagecreatetruecolor($new_width, $new_height);
            // Copia e resize a imagem velha na nova
            imagecopyresized($tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
            imagedestroy($img);
            unlink($image_path);
            $img = $tmp_img;
            
            $ext = strtolower(end(explode('.', $image_path)));
            if($ext == "jpg" || $ext == "jpeg"){
                imagejpeg($img, PATH_USER_PHOTOS . basename($image_path), IMAGE_QUALITY);
                return true;
            }
            elseif ($ext == "gif"){
                imagepng($img, PATH_USER_PHOTOS . basename($image_path));
                return true;
            }
            elseif ($ext == "png"){
                imagepng($img, PATH_USER_PHOTOS . basename($image_path));
                return true;
            }
            else {
                echo("Formato de destino nao suportado");
                exit();
            }
            
        }
    }

    // Cria uma imagem de erro se necessário
    if (!$img) {
        $img = imagecreate(MAX_WIDTH, MAX_HEIGHT);
        imagecolorallocate($img, 204, 204, 204);
        $c = imagecolorallocate($img, 153, 153, 153);
        $c1 = imagecolorallocate($img, 0, 0, 0);
        imageline($img, 0, 0, MAX_WIDTH, MAX_HEIGHT, $c);
        imageline($img, MAX_WIDTH, 0, 0, MAX_HEIGHT, $c);
        imagestring($img, 2, 12, 55, 'erro ao carregar imagem', $c1);
    }
}

?>