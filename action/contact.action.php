<?php

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_CONTROLLER.       'ContactController.class.php';


//receive data
$params = array(
                  "name" => trim($_POST['name']),
                  "mail" => trim($_POST['mail']),
//                  "phone" => trim($_POST['phone']),
                  "subject" => trim($_POST['subject']),
                  "msg" => nl2br(trim($_POST['msg']))
               );

               
$contactController = new ContactController();
$errorCode = NULL;
if (empty($params["name"])) {
    $errorCode = 1;
} else if (empty($params["mail"])) {
    $errorCode = 2;
} else if (!(filter_var($params["mail"], FILTER_VALIDATE_EMAIL))) {
    $errorCode = 2;
} else if (empty ($params["msg"])) {
    $errorCode = 3;
}

if (!is_null($errorCode))
    header ("Location:". URL."contact.php?error=".$errorCode);
else
    if ($contactController->sendMail($params))
        header("Location:". URL. "contact.php?success");
    else
        header("Location:". URL. "contact.php?error=4");

?>
