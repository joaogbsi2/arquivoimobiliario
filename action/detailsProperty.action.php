<?php

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_CONTROLLER .'PropertyController.class.php';
require_once PATH_CONTROLLER .'PropertyPhotoController.class.php';


$propertyId = $_GET['propertyId'];
$statusId = $_GET['statusProperty'];
$propertyController = new PropertyController();
$propertyPhotoController = new PropertyPhotoController();

// $property = $propertyController->getById($propertyId);
$property = $propertyController->findProperty_status($propertyId, $statusId);
$propertyPhoto = $propertyPhotoController->getPhotosByProperty($propertyId);


?>
