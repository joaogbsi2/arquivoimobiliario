<?php

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_CONTROLLER .'CityController.class.php';
require_once PATH_CONTROLLER .'PropertyController.class.php';


$cityId = $_GET['city'];



$cityController = new CityController();
$propertyController = new PropertyController();

$city = $cityController->getById($cityId);

$districts = $propertyController->findNeighborhoodByCity($city);

echo json_encode($districts);

?>
