<?php
$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

// require_once PATH . 'vendor/autoload.php';
require_once PATH . '/constraint/property.php';
require_once PATH_CONTROLLER . 'PropertyController.class.php';
require_once PATH_CONTROLLER . 'StreetController.class.php';
require_once PATH_CONTROLLER . 'CityController.class.php';
require_once PATH_CONTROLLER . 'PropertyStoreController.class.php';
require_once PATH_CONTROLLER . 'PropertyStatusController.class.php';
require_once PATH_CONTROLLER . 'ValidatePhotoController.class.php';
require_once PATH_CONTROLLER . 'PropertyPhotoController.class.php';
require_once PATH_UTIL . 'Ortografia.php';
require_once PATH_UTIL_XML . 'XmlToProperty.php';

// tentativa2 -----------------------------------------------------------------------------------
try {
	$fooTag = $_POST ['webService'];
	$propertyStoreController = new PropertyStoreController();
	$vPropertyStore = $propertyStoreController->getVetorPropertyStore($_POST['store']); 
	// print_r($vPropertyStore.', <br />');
	$xml_arquivo = nomeArquivoXml ( $_POST ['store'] );
	$cache_file = xmlWebservice ( $fooTag, $xml_arquivo );
	
	// $dadosXml = new SimpleXMLElement ( $fooTag, NULL, true );
	$dadosXml = simplexml_load_file ( $cache_file );
	// $dadosXml = simplexml_load_file ( '/opt/lampp/htdocs/arquivoImobiliario/xml/modelo2.xml' );
	$count = 0;
	foreach ( $dadosXml->Imoveis->Imovel as $dadoImovel ) {
		try {
			echo $count ++.', ';
			if ($count == 169) {
				echo '';
			}
			if (! imovelValido ( $dadoImovel )) {
				continue;
			}
			$params = lerXml ( $dadoImovel, $_POST ['store'] );
			$street = new Street ();
			$aux = explode ( " ", $params ['street'], 2 );
			$streetController = new StreetController ();
			$street = $streetController->getStreetsByBairroName ( $params ['neighborhood'], $aux [1] );
			if (is_null ( $street )) {
				$streetController->addStreet ( $params );
				$street = $streetController->getStreetsByBairroName ( $params ['neighborhood'], $aux [1], null, null );
			}
			$params ['idUnico'] = mb_strtoupper ( str_replace ( " ", "", str_replace ( Ortografia::$vogais, "", $params ['neighborhood'] ) ) ) . '*' . $street [0]->getId () . '*' . $params ['number'];
			
			$property = new Property ();
			$propertyController = new PropertyController ();
			
			$property = $propertyController->getByIdUnico ( $params ['idUnico'] );
			
			if (! is_null ( $property )) {
				$params ['property_id'] = $property->getId ();
				$propertyController->updateProperty ( $params );
				$propertyId = $params ['property_id'];
				cadastreStatusInProrety ( $params ['precos'], $_POST['store'], $propertyId );
				cadastreStoresInProperty ( $params, $propertyId );
			} else {
				$propertyId = $propertyController->recordProperty ( $params );
				if (! is_null ( $propertyId )) {
					cadastreStatusInProrety ( $params ['precos'], $_POST['store'], $propertyId );
					cadastreStoresInProperty ( $params, $propertyId );
				}
				$property = $propertyController->getByIdUnico ( $params ['idUnico'] );
			}
			if (! empty ( $params ['property_photo'] )) {
				$controleFoto = 0;
				foreach ( $params ['property_photo'] as $foto ) {
					$url = $foto ['url'];
					// $nomeFoto = $foto['name'];
					$nomeFoto = $params ['idUnico'] . '_' . $controleFoto;
					salvarFoto ( $url, $nomeFoto, $_POST ['store'], $property->getId () );
					$controleFoto ++;
					if ($controleFoto >= 5)
						break;
				}
			}
			
			// $propertyId = $propertyController->recordProperty ( $params );
			// cadastreStoresInProperty ( $params, $propertyId );
			$key = array_search($params['idUnico'], $vPropertyStore);			
			if($key != null){
			print_r($params['idUnico'].'<br />');
// 			if(array_key_exists($params['idUnico'], $vPropertyStore)){
// 				unset($vPropertyStore[$key]);
				unset($vPropertyStore[$key]);
			}
			unset ( $params );
		} catch ( Exception $e ) {
			header ( "location:" . URL_ADMIN_PAGE . "?failure=importXml" );
		}
	}
	if(count($vPropertyStore) != 0){
		$cont = 0;
		foreach ($vPropertyStore as $aux){
			// print_r($cont++.' '.$aux .' aqui <br />');
			if($propertyController->updatePropertyEnable($aux))
				echo'';
			}
	}
	header ( "location:" . URL_ADMIN_PAGE . "?success=index" );
} catch ( Exception $e ) {
	echo $e;
}
function cadastreStatusInProrety($precos, $store, $propertyId) {
	if ((! empty ( $propertyId )) && ($propertyId != "")) {
		
		$propertyStatusController = new PropertyStatusController ();
		$propertyController = new PropertyController ();
		
		$property = $propertyController->getById($propertyId);
		
			$arrayPrices = array ();
			$arrayStatus = array ();
			$arrayStore = array ();
			
		if ($property instanceof Property) {
			for($i = 0; $i < sizeof ( $precos ); $i ++) {
				$arrayPrices [$i] = $precos [$i] ['price'];
				$arrayStatus [$i] = $precos [$i] ['status'];
				$arrayStore[$i] = $store;
			}
		}
// 		$propertyStatusController->createPropertyStatus ( $arrayPrices, $arrayStatus, $property );
		
		$propertyStatusController->createPropertyStatus($arrayPrices, $arrayStatus, $arrayStore, $property);
	}
}
function cadastreStoresInProperty($params, $propertyId) {
	
	// Get user for record in user_service.
	if ((! empty ( $propertyId )) && ($propertyId != "")) {
		
		$propertyController = new PropertyController ();
		$propertyStoreController = new PropertyStoreController ();
		
		$property = $propertyController->getById ( $propertyId );
		
		if ($property instanceof Property) {
			// if (isset($_POST['serviceProvider'])) {
			
			if (! is_array ( $params ['stores'] ) && isset ( $params ['stores'] )) {
				$storesToInsert [] = $params ['stores'];
				$storeReferencia [] = $params ['referencia'];
			} else {
				foreach ( $params ['referencia'] as $referencia ) {
					if ($referencia != "") {
						$storeReferencia [] = $referencia;
					}
				}
				
				foreach ( $params ['stores'] as $storesValue ) {
					$storesToInsert [] = $storesValue;
					// if (isset($_POST['stores']))
					// $property->setNumReferencia ( $_POST ( 'referencia' ) );
				}
			}
			
			/*
			 * Alterei o if abaixo, ao invés de ficar dando includes e location essa página pelo que notei apenas apresentava os dados na tela como não irei querer isso então eu apenas mando a página inicial PAGINA DO ADMI
			 */
			if (isset ( $params ['stores'] ))
				
				if ($propertyStoreController->createPropertyStore ( $storeReferencia, $storesToInsert, $property )) {
					// echo "retornou aqui";
					// exit();
					// header("Location: confirmRecordedUser.php?user=" . $user->getId());
					// returnToPage("success","cad_upd_user");
				} else {
					// $error['ServiceProviderNotRecorded'] = TRUE;
					
					echo 'Erro! Infelizmente não foi possível cadastrar-se como um provedor
                    de serviços neste momento. Tente mais tarde! Agradecemos sua paciência.';
					exit ();
				}
			
			// } elseif ($_POST['choiceNo'] == 'no') {//caso não tenha informado ser provedor de serviço
			// returnToPage("success","cad_upd_user");
			// // header("Location: confirmRecordedUser.php?user=" . $user->getId());
			// }
		} else {
			// $error['UserNotFound'] = TRUE;
			// header("Content-Type: text/html; charset=UTF-8");
			echo "Erro! O imóvel informado não foi encontrado.";
			exit ();
		}
	} else {
		// $error['DontUserParam'] = TRUE;
		// header("Content-Type: text/html; charset=UTF-8");
		echo 'Erro! Você não informou um imóvel.';
		exit ();
	}
}
function curl_get_file_contents($URL) {
	$conn = curl_init ();
	curl_setopt ( $conn, CURLOPT_URL, $URL );
	curl_setopt ( $conn, CURLOPT_HEADER, false );
	// curl_setopt($conn, CURLOPT_TIMEOUT, 10);
	curl_setopt ( $conn, CURLOPT_USERAGENT, 'NOME DO PORTAL' );
	curl_setopt ( $conn, CURLOPT_RETURNTRANSFER, true );
	curl_setopt ( $conn, CURLOPT_FOLLOWLOCATION, true );
	curl_setopt ( $conn, CURLOPT_SSL_VERIFYHOST, 0 );
	curl_setopt ( $conn, CURLOPT_SSL_VERIFYPEER, 0 );
	curl_setopt ( $conn, CURLOPT_HTTPHEADER, array (
			'Content-type: text/xml' 
	) );
	// 'Content-type: image/jpeg'
	
	$result = curl_exec ( $conn );
	$error = curl_error ( $conn );
	curl_close ( $conn );
	if ($result)
		return $result;
}
function curl_get_image_contents($URL, $photoFolder, $fileName) {
	$ch = curl_init ( $URL );
	curl_setopt ( $ch, CURLOPT_HEADER, 0 );
	curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
	curl_setopt ( $ch, CURLOPT_BINARYTRANSFER, 1 );
	// curl_setopt ( $ch, CURLOPT_HTTPHEADER, array (
	// // 'Content-type: text/xml' ,
	// 'Content-type: image/jpeg'
	// ) );
	$rawdata = curl_exec ( $ch );
	curl_close ( $ch );
	
	$cache_Image = $photoFolder . $fileName . '.jpg';
	
	$fp = fopen ( $cache_Image, 'w' );
	fwrite ( $fp, $rawdata );
	fclose ( $fp );
	
	chmod ( $cache_Image, 0755 );
	// chmod ( $cache_Image, 0777 );
	
	return $cache_Image;
}
function nomeArquivoXml($store) {
	switch ($store) {
		case 11 :
			return 'canovas.xml';
			break;
		case 15 :
			return 'MSIimoveis.xml';
			break;
		case 16 :
			return 'tadeuImoveis.xml';
			break;
	}
}
function xmlWebservice($linkRss, $file) {
	$cache_dir = PATH . "cache";
	if (! is_dir ( $cache_dir )) {
		mkdir ( $cache_dir );
		chmod ( $cache_dir, 0777 );
		// chmod ( $cache_dir, 0755 );
	}
	$cache_file = $cache_dir . "/" . $file;
	// tempo em minutos para cada atualizacao de cache
	$tUp = 240;
	if (file_exists ( $cache_file )) {
		// chmod ($cache_file, 0775);
		// $tSeg = (time () - filemtime ( $cache_file ));
		// $tMin = floor ( $tSeg / 60 );
		// if ($tMin > $tUp) {
			$rss = curl_get_file_contents ( $linkRss );
			
			if ($rss) {
				$rssCache = fopen ( $cache_file, "w" );
				fwrite ( $rssCache, $rss );
				fclose ( $rssCache );
			} else {
				geraLog ( "Falha no carregamento do XML.\n\r" );
				exit ( "Falha no carregamento do XML" );
			}
		// }
	} else {
		$rss = curl_get_file_contents ( $linkRss );
		if ($rss) {
			$rssCache = fopen ( $cache_file, "w" );
			fwrite ( $rssCache, $rss );
			fclose ( $rssCache );
			chmod ( $cache_file, 0777 );
			// chmod ( $cache_file, 0755 );
		}
	}
	if (file_exists ( $cache_file ))
		echo '';
	return $cache_file;
}
function salvarFoto($url, $nomeFoto, $store, $property_id) {
	$photoFolder = PATH_IMAGES . "property_photo/";
	// $fileName = $store . '_' . $nomeFoto;
	$propertyPhotoController = new PropertyPhotoController ();
	if (is_null ( $propertyPhotoController->getPhotosByPath ( $photoFolder . $nomeFoto ) )) {
		
		$image = curl_get_image_contents ( $url, $photoFolder, $nomeFoto );
		
		if (file_exists ( $image )) {
			// if (! $validatePhotoController->isValidWidthAndHeight ( 800, 2000, 800, 1800, $image )) {
			// header ( "Location:" . URL . "adm_importXml.php?error=size" );
			// exit ();
			// }else{
			// if (move_uploaded_file ( $image, $photoFolder . $finalName )) {
			
			$propertyController = new PropertyController ();
			$property = $propertyController->getById ( $property_id );
			
			$propertyPhoto = new PropertyPhoto ();
			$propertyPhoto->setPath ( $image );
			$propertyPhoto->setProperty ( $property );
			
			$propertyPhotoController->insertPropertyPhoto ( $propertyPhoto );
			
			/*
			 * Agora converter a imagem para ThumbNail
			 */
			// $info = getimagesize($newFileName);
			
			// echo "width: " .$info[0]. " height: ".$info[1];
			
			// gera_thumb ( $newFileName/*, $info[0], $info[1]*/);
			// }
			// }
		} else {
			echo "Falha ao mover a imagem dentro do servidor. Por favor, tente novamente mais tarde!";
			exit ();
		}
	}
}