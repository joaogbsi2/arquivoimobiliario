<?php

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH . '/vendor/autoload.php';
require_once PATH . 'constraint/user.php';
require_once PATH_CONTROLLER . 'UserController.class.php';
require_once PATH_CONTROLLER . 'UserServiceController.class.php';
require_once PATH_UTIL . 'Ortografia.php';


// Pasta onde o arquivo vai ser salvo
$_UP ['pasta'] = 'upload/';

// Tamanho máximo do arquivo (em Bytes)
// $_UP['tamanho'] = 1024 * 1024 * 2; // 2Mb

// Array com as extensões permitidas
$_UP ['extensoes'] = array (
		'xls',
		'xlsx' 
);

// Renomeia o arquivo? (Se true, o arquivo será salvo como .jpg e um nome único)
$_UP ['renomeia'] = false;

// Array com os tipos de erros de upload do PHP
$_UP ['erros'] [0] = 'Não houve erro';
$_UP ['erros'] [1] = 'O arquivo no upload é maior do que o limite do PHP';
$_UP ['erros'] [2] = 'O arquivo ultrapassa o limite de tamanho especifiado no HTML';
$_UP ['erros'] [3] = 'O upload do arquivo foi feito parcialmente';
$_UP ['erros'] [4] = 'Não foi feito o upload do arquivo';

// Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
if ($_FILES ['arquivo'] ['error'] != 0) {
	die ( "Não foi possível fazer o upload, erro:" . $_UP ['erros'] [$_FILES ['arquivo'] ['error']] );
	exit();  // Para a execução do script
}

// Caso script chegue a esse ponto, não houve erro com o upload e o PHP pode continuar

// Faz a verificação da extensão do arquivo
$extensao = strtolower ( end ( explode ( '.', $_FILES ['arquivo'] ['name'] ) ) );
if (array_search ( $extensao, $_UP ['extensoes'] ) === false) {
	echo "Por favor, envie arquivos com as seguintes extensões: xls ou xlsx";
	exit ();
}

// Faz a verificação do tamanho do arquivo
// if ($_UP['tamanho'] < $_FILES['arquivo']['size']) {
// echo "O arquivo enviado é muito grande, envie arquivos de até 2Mb.";
// exit;
// }

// O arquivo passou em todas as verificações, hora de tentar movê-lo para a pasta

// Primeiro verifica se deve trocar o nome do arquivo
if ($_UP ['renomeia'] == true) {
	// Cria um nome baseado no UNIX TIMESTAMP atual e com extensão .jpg
	$nome_final = md5 ( time () ) . '.jpg';
} else {
	// Mantém o nome original do arquivo
	$nome_final = basename ( $_FILES ['arquivo'] ['name'] );
}

$diretorio = PATH . $_UP ['pasta'] . $nome_final;

if (move_uploaded_file ( $_FILES ['arquivo'] ['tmp_name'], $diretorio )) {
	echo "Upload efetuado com sucesso!";
	echo '<a href="' . $_UP ['pasta'] . $nome_final . '">Clique aqui para acessar o arquivo</a>';
	// $streetController = new StreetController ();
	// $propertyController = new PropertyController ();
	// $listaRuas = $streetController->getListStreets ();
	$userController = new UserController();
	$excelReader = PHPExcel_IOFactory::load ( $diretorio );
	$excelReader->setActiveSheetIndex ( 0 );
	$dados = array ();
	$cont = 0;
	foreach ( $excelReader->getWorksheetIterator () as $worksheet ) {
		$worksheetTitle = $worksheet->getTitle ();
		$highestRow = $worksheet->getHighestDataRow ();
		$highestColumn = $worksheet->getHighestDataColumn ();
		$highestColumnIndex = PHPExcel_Cell::columnIndexFromString ( $highestColumn );
		$nrColumns = ord ( $highestColumn );
		echo '<table border="1"><tr>';
		foreach ( $worksheet->getRowIterator () as $row ){
			$cellIterator = $row->getCellIterator ();
			$cellIterator->setIterateOnlyExistingCells ( false );
			// $cont = 0;
			foreach ( $cellIterator as $cell ) {
				$dados [] = $cell->getValue ();
				// echo $cont++.' - '.$dados[$cont].'<br />';
			}
			$params['name'] = $dados [NAME];
			$params['password'] = $dados [PASSWORD];
			$params['mobile_phone'] = $dados [MOBILE_PHONE];
			$params['phone'] = $dados [PHONE];
			$params['email'] = $dados [EMAIL];
			$params['photo'] = $dados [USER_PHOTO_PATH];
			$role = Ortografia::retirarAcento($dados [ROLE]);
			$role = strtolower($role);
			$role = str_replace(' ', '', $role);
			$params['role'] = defineRole($role);
			$params['is_active'] = $dados [IS_ACTIVE];
			unset ( $dados );
			echo $cont++.', ';
			if($cont == 30 || $cont == 31){
				echo $role;}
			
			$user = new User();
			$user = $userController->getByUserNameTelefone($params['name'], $params['phone']);

			
			if(!is_null($user)){
				// $userController->updateUser($params);
				cadastreSUserInService($params, $user);

			}else{
				$userController->addUser($params);
				$user = $userController->getByUserNameTelefone($params['name'], $params['phone']);				
				cadastreSUserInService($params, $user);
			}
					
		}
	}
	header ( "location:" . URL_ADMIN_PAGE . "?success=cad_property" );
} else {
	// Não foi possível fazer o upload, provavelmente a pasta está incorreta
	header ( "location:" . URL_ADMIN_PAGE . "?failure=cad_property" );
}

function cadastreSUserInService($params, $user) {
	$userServiceController = new UserServiceController();
	
	$userService = $userServiceController->findServiceProviderByUser($params['role'], $user->getId());
	if(is_null($userService)){
		$userChoice[] = $params['role'];
	 	$userService = $userServiceController->createServiceProvider($userChoice, $user);
	}
	
}

function defineRole($role){
	echo '';
	switch ($role){ 
		case 'alarmesresidenciais';
			return 13;
			break;
		case 'antenas';
			return 14;
			break;
		case 'aquecedores';
			return 15;
			break;
		case 'arcondicionado';
			return 16;
			break;
		case 'armarioembutidos';
			return 17;
			break;
		case 'arquitetos';
			return 18;
			break;
		case 'assoalhoeconservacao';
			return 19;
			break;
		case 'azulejos';
			return 20;
			break;
		case 'baba';
			return 4;
			break;
		case 'betoneiras';
			return 87;
			break;
		case 'blocosdeconcreto';
			return 21;
			break;
		case 'bombasdagua';
			return 134;
			break;
		case 'boxparabanheiro';
			return 22;
			break;
		case 'brita';
			return 23;
			break;
		case 'calhas';
			return 88;
			break;
		case 'camerasdeseguranca';
			return 89;
			break;
		case 'carpetesetapetes';
			return 24;
			break;
		case 'carpete';
			return 24;
			break;
		case 'carpinteiro';
			return 11;
			break;
		case 'carpintaria';
			return 11;
			break;		
		case 'carretos';
			return 26;
			break;
		case 'chaveiro';
			return 27;
			break;
		case 'cimento';
			return 28;
			break;
		case 'coleta';
			return 38;
			break;
		case 'construtoras';
			return 92;
			break;
		case 'cuidador';
			return 3;
			break;
		case 'decoracaodeinteriores';
			return 93;
			break;
		case 'decoracaodeinterior';
			return 93;
			break;
		case 'dedetizacao';
			return 94;
			break;
		case 'desemtupidoras';
			return 29;
			break;
		case 'desratizacao';
			return 30;
			break;
		case 'disque24horas';
			return 32;
			break;
		case 'disqueagua';
			return 31;
			break;
		case 'distribuidoradebebidas';
			return 95;
			break;
		case 'divisorias';
			return 33;
			break;
		case 'eletricidadeempresa';
			return 34;
			break;
		case 'eletricista';
			return 35;
			break;
		case 'eletricistapredial';
			return 9;
			break;
		case 'eletricistaresidencial';
			return 10;
			break;
		case 'eletrodomesticoconserto';
			return 97;
			break;
		case 'encanador';
			return 8;
			break;
		case 'engenheiros';
			return 39;
			break;
		case 'entulho';
			return 37;
			break;
		case 'entulhocoleta';
			return 98;
			break;
		case 'espelhos';
			return 40;
			break;
		case 'esquadriadealuminio';
			return 41;
			break;
		case 'estofador';
			return 42;
			break;
		case 'estruturasmetalicas';
			return 100;
			break;
		case 'extintoresdeincendio';
			return 43;
			break;
		case 'farmaciasedrogarias';
			return 102;
			break;
		case 'fechaduras';
			return 44;
			break;
		case 'ferragens';
			return 103;
			break;
		case 'filtrosdagua';
			return 45;
			break;
		case 'gasfornecedor';
			return 46;
			break;
		case 'geladeiraconserto';
			return 47;
			break;
		case 'gesso';
			return 106;
			break;
		case 'gessoartefatoedecoracao';
			return 48;
			break;
		case 'grama';
			return 49;
			break;		
		case 'granito';
			return 108;
			break;		
		case 'informaticaassistenciatecnica';
			return 2;
			break;
		case 'interfones';
			return 50;
			break;
		case 'interfone';
			return 50;
			break;
		case 'jardineiro';
			return 51;
			break;
		case 'lajespremoldadas';
			return 111;
			break;
		case 'lavanderias';
			return 52;
			break;
		case 'lenha';
			return 53;
			break;
		case 'limpezadecaixadagua';
			return 114;
			break;
		case 'limpezadefossa';
			return 55;
			break;
		case 'lustres';
			return 56;
			break;
		case 'madeiras';
			return 57;
			break;
		case 'maquinadelavarroupaassistencia';
			return 89;
			break;
		case 'marceneiro';
			return 12;
			break;
		case 'marcenaria';
			return 12;
			break;
		case 'marmitex';
			return 60;
			break;
		case 'marmoraria';
			return 124;
			break;
		case 'materialdeconstrucao';
			return 61;
			break;
		case 'materialdeletrico';
			return 62;
			break;
		case 'materialhidraulico';
			return 63;
			break;
		case 'microondasassistencia';
			return 64;
			break;
		case 'mototaxieentrega';
			return 65;
			break;
		case 'mouroes';
			return 66;
			break;
		case 'moveis';
			return 119;
			break;
		case 'moveisusados';
			return 119;
			break;
		case 'mudadeplanta';
			return 67;
			break;
		case 'paisagismo';
			return 68;
			break;
		case 'parafusos';
			return 121;
			break;
		case 'pavimentacao';
			return 123;
			break;
		case 'pedreiro(acabamento)';
			return 6;
			break;
		case 'pedreiro(alvenaria)';
			return 5;
			break;
		case 'persianas';
			return 103;
			break;
		case 'pintor';
			return 7;
			break;
		case 'piscinas';
			return 126;
			break;
		case 'piscinaassistencia';
			return 71;
			break;
		case 'pisoerevestimento';
			return 72;
			break;
		case 'pocoartesiano';
			return 73;
			break;
		case 'portaoeletronico';
			return 75;
			break;
		case 'protaseacessorios';
			return 74;
			break;
		case 'quadrosemolduras';
			return 76;
			break;
		case 'redesdeprotecao';
			return 127;
			break;
		case 'segurançapatrimonial';
			return 103;
			break;
		case 'serralheria';
			return 78;
			break;
		case 'serraria';
			return 79;
			break;
		case 'sinteko';
			return 80;
			break;
		case 'tapeceiro';
			return 81;
			break;
		case 'tapecaria';
			return 81;
			break;
		case 'tecdeenfermage,';
			return 1;
			break;
		case 'telasdeprotecao';
			return 82;
			break;
		case 'tijolos';
			return 84;
			break;
		case 'tintas';
			return 83;
			break;
		case 'toldos';
			return 85;
			break;
		case 'transporteescolar';
			return 132;
			break;
		case 'vidracaria';
			return 86;
			break;
		
	}
	
}
?>