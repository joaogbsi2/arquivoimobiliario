<?php

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH . 'vendor/autoload.php';
require_once PATH . 'constraint/property.php';
require_once PATH_CONTROLLER . 'PropertyController.class.php';
require_once PATH_CONTROLLER . 'StreetController.class.php';
require_once PATH_CONTROLLER . 'CityController.class.php';
require_once PATH_CONTROLLER . 'PropertyStoreController.class.php';
require_once PATH_UTIL . 'Ortografia.php';


// Pasta onde o arquivo vai ser salvo
$_UP ['pasta'] = 'upload/';

// Tamanho máximo do arquivo (em Bytes)
// $_UP['tamanho'] = 1024 * 1024 * 2; // 2Mb

// Array com as extensões permitidas
$_UP ['extensoes'] = array (
		'xls',
		'xlsx' 
);

// Renomeia o arquivo? (Se true, o arquivo será salvo como .jpg e um nome único)
$_UP ['renomeia'] = false;

// Array com os tipos de erros de upload do PHP
$_UP ['erros'] [0] = 'Não houve erro';
$_UP ['erros'] [1] = 'O arquivo no upload é maior do que o limite do PHP';
$_UP ['erros'] [2] = 'O arquivo ultrapassa o limite de tamanho especifiado no HTML';
$_UP ['erros'] [3] = 'O upload do arquivo foi feito parcialmente';
$_UP ['erros'] [4] = 'Não foi feito o upload do arquivo';

// Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
if ($_FILES ['arquivo'] ['error'] != 0) {
	die ( "Não foi possível fazer o upload, erro:" . $_UP ['erros'] [$_FILES ['arquivo'] ['error']] );
	exit();  // Para a execução do script
}

// Caso script chegue a esse ponto, não houve erro com o upload e o PHP pode continuar

// Faz a verificação da extensão do arquivo
$extensao = strtolower ( end ( explode ( '.', $_FILES ['arquivo'] ['name'] ) ) );
if (array_search ( $extensao, $_UP ['extensoes'] ) === false) {
	echo "Por favor, envie arquivos com as seguintes extensões: xls ou xlsx";
	exit ();
}

// Faz a verificação do tamanho do arquivo
// if ($_UP['tamanho'] < $_FILES['arquivo']['size']) {
// echo "O arquivo enviado é muito grande, envie arquivos de até 2Mb.";
// exit;
// }

// O arquivo passou em todas as verificações, hora de tentar movê-lo para a pasta

// Primeiro verifica se deve trocar o nome do arquivo
if ($_UP ['renomeia'] == true) {
	// Cria um nome baseado no UNIX TIMESTAMP atual e com extensão .jpg
	$nome_final = md5 ( time () ) . '.jpg';
} else {
	// Mantém o nome original do arquivo
	$nome_final = basename ( $_FILES ['arquivo'] ['name'] );
}

$diretorio = PATH . $_UP ['pasta'] . $nome_final;
// Depois verifica se é possível mover o arquivo para a pasta escolhida
if (move_uploaded_file ( $_FILES ['arquivo'] ['tmp_name'], $diretorio )) {

	// if (move_uploaded_file($_FILES['arquivo']['tmp_name'], $arquivoLeitura)) {
	// Upload efetuado com sucesso, exibe uma mensagem e um link para o arquivo
	echo "Upload efetuado com sucesso!";
	echo '<a href="' . $_UP ['pasta'] . $nome_final . '">Clique aqui para acessar o arquivo</a>';
	$streetController = new StreetController ();
	$propertyController = new PropertyController ();
	// $listaRuas = $streetController->getListStreets ();
	$excelReader = PHPExcel_IOFactory::load ( $diretorio );
	$excelReader->setActiveSheetIndex ( 0 );
	$dados = array ();
	$cont = 0;
	foreach ( $excelReader->getWorksheetIterator () as $worksheet ) {
		$worksheetTitle = $worksheet->getTitle ();
		$highestRow = $worksheet->getHighestDataRow ();
		$highestColumn = $worksheet->getHighestDataColumn ();
		$highestColumnIndex = PHPExcel_Cell::columnIndexFromString ( $highestColumn );
		$nrColumns = ord ( $highestColumn );
		echo '<table border="1"><tr>';
		foreach ( $worksheet->getRowIterator () as $row ) {
			$cellIterator = $row->getCellIterator ();
			$cellIterator->setIterateOnlyExistingCells ( false );
			// $cont = 0;
			foreach ( $cellIterator as $cell ) {
				$dados [] = $cell->getValue ();
				// echo $cont++.' - '.$dados[$cont].'<br />';
			}
			$params ['referencia'] = $dados [REFERENCIA];
			$params ['propertyType'] = $dados [TYPE];
			//			$params ['status'] = $dados [STATUS];
			//			$params ['price'] = str_replace ( ",", "", $dados [PRICE] );
			$precos = array();
			$precos[0]['status'] = $dados [STATUS];
			$precos[0]['precos'] = str_replace ( ",", "", $dados [PRICE] );
			$params ['cep'] = $dados [CEP];
			$cityController = new CityController ();
			$params ['city'] = $cityController->getById ( $dados [CITY] );
			$params ['neighborhood'] = $dados [NEIGHBORHOOD];
			$params ['street'] = $dados [STREET];
			$street = new Street ();
			$aux = explode ( " ", $params ['street'], 2 );
			$street = $streetController->getStreetsByBairroName ( $params ['neighborhood'], $aux [1] );
			if (is_null ( $street )) {
				$streetController->addStreet ( $params );
				$street = $streetController->getStreetsByBairroName ( $params ['neighborhood'], $aux [1], null, null );
			}
				
			$params ['number'] = $dados [IDENTIFER];
			$params ['idUnico'] = mb_strtoupper ( str_replace ( " ", "", str_replace ( Ortografia::$vogais, "", $dados [NEIGHBORHOOD] ) ) ) . '*' . $street [0]->getId () . '*' . $dados [IDENTIFER];
			$params ['description'] = $dados [DESCRIPTION];
			$params ['qtdRooms'] = trim ( $dados [QTD_ROONS] ) == "" ? 0 : $dados [QTD_ROONS];
			// 			$params ['contactEmail'] = $dados [CONTACT_EMAIL];
			// 			$params ['contactPhone'] = $dados [CONTACT_PHONE];
			$params ['daysOfWeek'] = '';
			$params ['monthOfYear'] = '';
			$params ['stores'] = $dados [STORE];
				
			/**
			 * Aqui agora deve chamar o método para salvar os dados do imóvel e retorna o id do imovel
			 * cadastrado para ser utilizado para cadastrar as imagens a ele
			 */
				
			/*
			 * Add new fields on webmovel version2
			 */
			$params ['garagem'] = isset ( $dados [GARAGEM] ) ? $dados [GARAGEM] : "NULL";
			$params ['pavimento'] = isset ( $dados [PAVIMENTO] ) ? $dados [PAVIMENTO] : "NULL";
			$params ['posicao'] = isset ( $dados [POSICAO] ) ? $dados [POSICAO] : "NULL";
			$params ['areaConstruida'] = isset ( $dados [AREA_CONSTRUIDA] ) ? $dados [AREA_CONSTRUIDA] : "NULL";
			$params ['areaTotal'] = isset ( $dados [AREA_TOTAL] ) ? $dados [AREA_TOTAL] : "NULL";
			$params ['valorCondominio'] = isset ( $dados [VALOR_CONDOMINIO] ) ? $dados [VALOR_CONDOMINIO] : "NULL";
			$params ['valorIPTU'] = isset ( $dados [VALOR_IPTU] ) ? $dados [VALOR_IPTU] : "NULL";
				
			$params ['comprimento'] = isset ( $dados [COMPRIMENTO] ) ? $dados [COMPRIMENTO] : "NULL";
			$params ['largura'] = isset ( $dados [LARGURA] ) ? $dados [LARGURA] : "NULL";
			$params ['edicula'] = isset ( $dados [EDICULA] ) ? $dados [EDICULA] : "NULL";
				
			$params ['exclusive_store'] = $dados [EXCLUSIVE_STORE];
				
			$params ['areaConstruida'] = str_replace ( ",", "", $params ['areaConstruida'] );
			$params ['areaTotal'] = str_replace ( ",", "", $params ['areaTotal'] );
			$params ['comprimento'] = str_replace ( ",", "", $params ['comprimento'] );
			$params ['largura'] = str_replace ( ",", "", $params ['largura'] );
			$params ['edicula'] = str_replace ( ",", "", $params ['edicula'] );
			$params ['valorCondominio'] = str_replace ( ",", "", $params ['valorCondominio'] );
			$params ['valorIPTU'] = str_replace ( ",", "", $params ['valorIPTU'] );
			unset ( $dados );
			echo $cont ++ . '<br />';
			// $propertyController = new PropertyController ();
				
			$property = new Property ();
			$property = $propertyController->getByIdUnico ( $params ['idUnico'] );
				
			if (! is_null ( $property )) {
				cadastreStoresInProperty ( $params, $property->getId () );
				cadastreStatusInProrety($precos, $property);
			} else {
				$propertyId = $propertyController->recordProperty ( $params );
				if (! is_null ( $propertyId )) {
					cadastreStoresInProperty ( $params, $propertyId );
					cadastreStatusInProrety($precos, $property);
				}
			}
				
				
			// $propertyId = $propertyController->recordProperty ( $params );
			// cadastreStoresInProperty ( $params, $propertyId );
			unset ( $params );
			PHPExcel_CachedObjectStorage_Memory::cacheMethodIsAvailable ();
			// echo $propertyId;
		}
	}
	header ( "location:" . URL_ADMIN_PAGE . "?success=cad_property" );
} else {
	// Não foi possível fazer o upload, provavelmente a pasta está incorreta
	header ( "location:" . URL_ADMIN_PAGE . "?failure=cad_property" );
}

function cadastreStoresInProperty($params, $propertyId) {

	// Get user for record in user_service.
	if ((! empty ( $propertyId )) && ($propertyId != "")) {

		$propertyController = new PropertyController ();
		$propertyStoreController = new PropertyStoreController ();

		$property = $propertyController->getById ( $propertyId );

		if ($property instanceof Property) {
			// if (isset($_POST['serviceProvider'])) {
				
			if (! is_array ( $params ['stores'] ) && isset ( $params ['stores'] )) {
				$storesToInsert [] = $params ['stores'];
				$storeReferencia [] = $params ['referencia'];
			} else {
				foreach ( $params ['referencia'] as $referencia ) {
					if ($referencia != "") {
						$storeReferencia [] = $referencia;
					}
				}

				foreach ( $params ['stores'] as $storesValue ) {
					$storesToInsert [] = $storesValue;
					// if (isset($_POST['stores']))
					// $property->setNumReferencia ( $_POST ( 'referencia' ) );
				}
			}
				
			/*
			 * Alterei o if abaixo, ao invés de ficar dando includes e location
			 * essa página pelo que notei apenas apresentava os dados na tela
			 * como não irei querer isso então eu apenas mando a página inicial
			 * PAGINA DO ADMIN
			 */
			if (isset ( $params ['stores'] ))

			if ($propertyStoreController->createPropertyStore ( $storeReferencia, $storesToInsert, $property )) {
				// echo "retornou aqui";
				// exit();
				// header("Location: confirmRecordedUser.php?user=" . $user->getId());
				// returnToPage("success","cad_upd_user");
			} else {
				// $error['ServiceProviderNotRecorded'] = TRUE;
					
				echo 'Erro! Infelizmente não foi possível cadastrar-se como um provedor
                    de serviços neste momento. Tente mais tarde! Agradecemos sua paciência.';
				exit ();
			}
				
			// } elseif ($_POST['choiceNo'] == 'no') {//caso não tenha informado ser provedor de serviço
			// returnToPage("success","cad_upd_user");
			// // header("Location: confirmRecordedUser.php?user=" . $user->getId());
			// }
		} else {
			// $error['UserNotFound'] = TRUE;
			// header("Content-Type: text/html; charset=UTF-8");
			echo "Erro! O imóvel informado não foi encontrado.";
			exit ();
		}
	} else {
		// $error['DontUserParam'] = TRUE;
		// header("Content-Type: text/html; charset=UTF-8");
		echo 'Erro! Você não informou um imóvel.';
		exit ();
	}
}

function cadastreStatusInProrety($precos, $property){
	if (!empty($property)){
		$propertyStatusController = new PropertyStatusController();
		$arrayPrices=array();
		$arrayStatus=array();
		for ($i = 0; $i < sizeof($precos); $i++) {
			$arrayPrices[$i]=$precos[$i]['price'];
			$arrayStatus[$i]=$precos[$i]['status'];
		}
		$propertyStatusController->createPropertyStatus($arrayPrices, $arrayStatus, $property);
	}

}