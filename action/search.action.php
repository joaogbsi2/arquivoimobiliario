<?php
$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_CONTROLLER . 'PropertyController.class.php';
require_once PATH_CONTROLLER . 'PropertyPhotoController.class.php';
require_once PATH_MODEL_ENTITIES . 'PropertyPhoto.class.php';

$city = $_GET ['city'];
$cityPaginate = intval ( $city );

// echo "CITY do GET :: ". $city;
if (empty ( $_GET ['neighborhood'] )) {
	$district = null;
} else {
	$district = $_GET ['neighborhood'];
}
if (empty ( $_GET ['property_type'] )) {
	$type = null;
} else {
	$type = $_GET ['property_type'];
}
if (empty ( $_GET ['qtdMinRooms'] )) {
	$qtdMinRooms = null;
} else {
	$qtdMinRooms = $_GET ['qtdMinRooms'];
}
if (empty ( $_GET ['qtdMaxRooms'] )) {
	$qtdMaxRooms = null;
} else {
	$qtdMaxRooms = $_GET ['qtdMaxRooms'];
}
if (empty ( $_GET ['min_price'] )) {
	$min = null;
} else {
	$min = str_replace ( ",", "", $_GET ['min_price'] );
}
if (empty ( $_GET ['max_price'] )) {
	$max = null;
} else {
	$max = str_replace ( ",", "", $_GET ['max_price'] );
}
/*
 * Field add by Edilson Justiniano, this field was add
 * on new layout.
 */
$code = null;
if (empty ( $_GET ['codigo'] )) {
	$code = null;
} else {
	$code = $_GET ['codigo'];
	$pesquisaPorId = true;
}

$status = $_GET ['statusProperty'];
$statusPaginate = intval ( $status );
$offset = $_GET ['offset'];

if (empty ( $_GET ['de'] )) {
	$de = null;
} else {
	$de = $_GET ['de'];
}
if (empty ( $_GET ['ate'] )) {
	$ate = null;
} else {
	$ate = $_GET ['ate'];
}

$controller = new PropertyController ();
$controllerPhoto = new PropertyPhotoController ();

/*
 * Varible $limit define the limit of propertys presented
 * on imoveis busca page. Earlier (before) on old version
 * is defined as 9. updated by Edilson Justiniano
 */
$limit = 8;

/*
 * Change by Edilson Justiniano on day 2013-15-11
 * I want to try take only a property when user
 * inform a property's code on search form
 */
if (! is_null ( $code ) && $code != "") {
	
	$result = $controller->getByIdJSON ( intval ( $code ) );
} else { // case user hasn`t informed a property's code then search by values in fields informed
	
	if ($status == 3) {
		$date1 = explode ( '/', $de );
		$date2 = explode ( '/', $ate );
		$deDate = mktime ( 0, 0, 0, $date1 [1], $date1 [0], $date1 [2] );
		$ateDate = mktime ( 0, 0, 0, $date2 [1], $date2 [0], $date2 [2] );
	} else {
		$deDate = null;
		$ateDate = null;
	}
	
	// Add a new parameter code by Edilson Justiniano. This Field was add by new layout on the search
	$result = $controller->search ( $city, $district, $type, $min, $max, $qtdMinRooms, $qtdMaxRooms, $statusPaginate, $limit, $offset, $deDate, $ateDate, $code );
}

$total = $result ['total'];
$propertys = $result ['root'];
$numberOfPages = ceil ( $total / $limit );
$paginaAtual = ($offset / $limit) + 1;
function paginate() {
	global $paginaAtual, $numberOfPages, $cityPaginate, $district, $type, $qtdMinRooms, $qtdMaxRooms, $measure, $min, $max, $statusPaginate, $offset, $limit, $de, $ate;
	echo '<ul>';
	// echo "CITY DO PAGINATE:: ".$cityPaginate;
	$params = array (
			"de" => $de,
			"ate" => $ate 
	);
	$urlDate = http_build_query ( $params );
	$paginas = montarPagina ( $paginaAtual );
	$aux = $offset;
	$urlPrimeira = getUrl ( $statusPaginate, $cityPaginate, $qtdMinRooms, $qtdMaxRooms, $measure, $min, $max, $district, $type, 1 );
	?>
<a href="<?php echo $urlPrimeira.="&".$urlDate; ?>"><?php echo 'Primeira'?></a>
<?php
	if ($numberOfPages > 5 && $paginaAtual >=4)
		echo '...';
	for($i = 0; $i < sizeof ( $paginas ); $i ++) {
		$url = getUrl ( $statusPaginate, $cityPaginate, $qtdMinRooms, $qtdMaxRooms, $measure, $min, $max, $district, $type, $paginas[$i] );
		?>
        <a href="<?php echo $url.="&".$urlDate; ?>"><?php echo $paginas[$i]?></a>
        <?php
	}
	if ($numberOfPages > 5 && $paginaAtual < ($numberOfPages -2))
		echo '...';
	$urlUltima = getUrl ( $statusPaginate, $cityPaginate, $qtdMinRooms, $qtdMaxRooms, $measure, $min, $max, $district, $type, $numberOfPages );
	?>
	<a href="<?php echo $urlUltima.="&".$urlDate; ?>"><?php echo 'Última'?></a>
	<?php
	
}
function montarPagina($paginaAtual) {
	global $numberOfPages;
	$paginas = array ();
	if($numberOfPages <5){
		for ($i = 0; $i<$numberOfPages; $i++){
			$aux = $i+1;
			$paginas[$i]= $aux;
		}
		
	}else{
		
	if ($paginaAtual < 3) {
		$paginas [0] = 1;
		$paginas [1] = 2;
		$paginas [2] = 3;
		$paginas [3] = 4;
		$paginas [4] = 5;
	} else if ($paginaAtual >= $numberOfPages - 2) {
		$paginas [0] = $numberOfPages - 4;
		$paginas [1] = $numberOfPages - 3;
		$paginas [2] = $numberOfPages - 2;
		$paginas [3] = $numberOfPages - 1;
		$paginas [4] = $numberOfPages;
	} else {
		$paginas [0] = $paginaAtual - 2;
		$paginas [1] = $paginaAtual - 1;
		$paginas [2] = $paginaAtual;
		$paginas [3] = $paginaAtual + 1;
		$paginas [4] = $paginaAtual + 2;
	}
	}
	return $paginas;
}
function getUrl($statusPaginate, $cityPaginate, $qtdMinRooms, $qtdMaxRooms, $measure, $min, $max, $district, $type, $pagina) {
	global $limit;
	
	$offset = ($pagina - 1) * $limit;
	$url = 'imoveisbusca.php?statusProperty=' . $statusPaginate . '&city=' . $cityPaginate . '&qtdMinRooms=' . $qtdMinRooms . '&qtdMaxRooms=' . $qtdMaxRooms . '&min_measure=' . $measure . '&min_price=' . $min . '&max_price=' . $max . '&offset=' . $offset;
	if ($district != null) {
		foreach ( $district as $d ) {
			$url .= '&' . urlencode ( 'neighborhood[]' ) . '=' . urlencode ( $d );
		}
	}
	if ($type != null) {
		foreach ( $type as $t ) {
			$url .= '&' . urlencode ( 'property_type[]' ) . '=' . urlencode ( $t );
		}
	}
	return $url;
}
?>
