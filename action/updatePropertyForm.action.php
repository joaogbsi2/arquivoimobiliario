<?php

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_CONTROLLER . 'PropertyController.class.php';
require_once PATH_CONTROLLER . 'StreetController.class.php';
require_once PATH_CONTROLLER . 'PropertyPhotoController.class.php';
require_once PATH_MODEL_ENTITIES . 'PropertyPhoto.class.php';

require_once PATH_CONTROLLER . 'PropertyDetailController.class.php';
require_once PATH_CONTROLLER . 'PropertyNearController.class.php';
require_once PATH_CONTROLLER . 'PropertyStoreController.class.php';

require_once PATH_CONTROLLER . 'ValidatePhotoController.class.php';

$vogais = array (
		"a",
		"á",
		"â",
		"ã",
		"A",
		"Á",
		"Â",
		"Ã",
		"e",
		"é",
		"ê",
		"E",
		"É",
		"Ê",
		"i",
		"í",
		"î",
		"I",
		"Í",
		"Î",
		"o",
		"ó",
		"ô",
		"õ",
		"O",
		"Ó",
		"Ô",
		"Õ",
		"u",
		"ú",
		"û",
		"U",
		"Ú",
		"Û" 
);

/**
 * Description of cadProperty2
 *
 * @author Daniel / Edilson Justiniano
 */
$propertyController = new PropertyController ();
$streetController = new StreetController ();

if (isset ( $_GET ['action'] ) && $_GET ['action'] == "deletePropertyPhoto") {
	deletePhoto ( $_GET ['idPropertyPhoto'] );
	header ( "Location: " . URL . "adm_updateImovel.php?property_id=" . $_GET ['idProperty'] );
	exit ();
}

$params ['propertyType'] = $_POST ['propertyType'];
$params ['status'] = $_POST ['statusProperty'];
$params ['price'] = str_replace ( ",", "", $_POST ['price'] );
$params ['cep'] = $_POST ['cep'];
$cityController = new CityController ();
$params ['city'] = $cityController->getById ( $_POST ['city'] );
$params ['neighborhood'] = $_POST ['neighborhood'];
$params ['street'] = $_POST ['street'];
$params ['number'] = $_POST ['number'];
$aux = explode ( " ", $params ['street'], 2 );
$street = new Street ();
$street = $streetController->getStreetsByBairroName ( $params ['neighborhood'], $aux [1] );

if (is_null ( $street )) {
	$streetController->addStreet ( $params );
	$street = $streetController->getStreetsByBairroName ( $params ['neighborhood'], $aux [1], null, null );
}
$params ['idUnico'] = mb_strtoupper ( str_replace ( " ", "", str_replace ( $vogais, "", $params ['neighborhood'] ) ) ) . '*' . $street [0]->getId () . '*' . $params ['number'];
$params ['description'] = $_POST ['description'];
$params ['qtdRooms'] = trim ( $_POST ['qtdRooms'] ) == "" ? 0 : $_POST ['qtdRooms'];
$params ['contactEmail'] = $_POST ['contactEmail'];
$params ['contactPhone'] = $_POST ['contactPhone'];
// $params['daysOfWeek'] = $_POST['daysOfWeek'];
// $params['monthOfYear'] = $_POST['monthOfYear'];

/*
 * Campo para pegar o antigo status, fiz isso, pois irei utilizar,
 * esse antigo status para descobrir se o old status era = 3 e o
 * novo status tambem é = 3. Nesse caso apenas faço o update
 * nas tabelas:
 * rent_to_these_days
 * rent_to_these_months
 * Já caso o old status < 3 e o novo status = 3
 * só preciso fazer o update.
 * E caso o old status = 3 mas o novo status < 3
 * então devo deletar todos os registros desse imóvel
 * nas tabelas:
 * rent_to_these_days
 * rent_to_these_months
 * assim fico sem sugeira no banco de dados
 */
$params ['oldStatus'] = $_POST ['oldStatus'];
$params ['property_id'] = $_POST ['property_id'];

/*
 * New field added by Edilson Justiniano to cadastre that store this property belong to
 */
// $params['store'] = $_POST['store'];

/*
 * Add new fields on webmovel version2
 */
$params ['garagem'] = isset ( $_POST ['garagem'] ) ? $_POST ['garagem'] : '';
$params ['pavimento'] = isset ( $_POST ['pavimento'] ) ? $_POST ['pavimento'] : '';
$params ['posicao'] = isset ( $_POST ['posicao'] ) ? $_POST ['posicao'] : '';
$params ['areaConstruida'] = isset ( $_POST ['areaConstruida'] ) ? $_POST ['areaConstruida'] : NULL;
$params ['areaTotal'] = isset ( $_POST ['areaTotal'] ) ? $_POST ['areaTotal'] : NULL;
$params ['valorCondominio'] = isset ( $_POST ['valorCondominio'] ) ? $_POST ['valorCondominio'] : NULL;
$params ['valorIPTU'] = isset ( $_POST ['valorIPTU'] ) ? $_POST ['valorIPTU'] : NULL;

$params ['comprimento'] = isset ( $_POST ['comprimento'] ) ? $_POST ['comprimento'] : "NULL";
$params ['largura'] = isset ( $_POST ['largura'] ) ? $_POST ['largura'] : "NULL";
$params ['edicula'] = isset ( $_POST ['edicula'] ) ? $_POST ['edicula'] : "NULL";

$params ['exclusive_store'] = $_POST ['exclusive_store'];

$params ['areaConstruida'] = str_replace ( ",", "", $params ['areaConstruida'] );
$params ['areaTotal'] = str_replace ( ",", "", $params ['areaTotal'] );
$params ['comprimento'] = str_replace ( ",", "", $params ['comprimento'] );
$params ['largura'] = str_replace ( ",", "", $params ['largura'] );
$params ['edicula'] = str_replace ( ",", "", $params ['edicula'] );
$params ['valorCondominio'] = str_replace ( ",", "", $params ['valorCondominio'] );
$params ['valorIPTU'] = str_replace ( ",", "", $params ['valorIPTU'] );

/**
 * Aqui agora deve chamar o método para salvar os dados do imóvel e retorna o id do imovel
 * cadastrado para ser utilizado para cadastrar as imagens a ele
 */
$wasUpdated = $propertyController->updateProperty ( $params );

/*
 * Não preciso dos códigos comentados abaixo
 */
// $temFoto = false;
//
// for ($i = 0; $i < count($_FILES['property_photo']['name']); $i++){
//
// if (!$_FILES['property_photo']['name'][$i] == NULL){
// $temFoto = true;
// break;
// }
// }

if ($wasUpdated) {
	
	/*
	 * Ao inves de fazer o header location, coloquei o código inline (aqui mesmo)
	 * para evitar futuros problemas de includes
	 */
	// header("Location:".URL_ACTION. "cadPropertyPhoto.action.php?propertyId=" . $propertyId);
	
	$propertyPhotoController = new PropertyPhotoController ();
	
	$validatePhotoController = new ValidatePhotoController ();
	
	// === Upload info.
	$photoFolder = PATH_IMAGES . "property_photo/";
	
	for($i = 0; $i < count ( $_FILES ['property_photo'] ['name'] ); $i ++) {
		
		if ($_FILES ['property_photo'] ['error'] [$i] === 0) {
			
			/*
			 * validar as imagens
			 */
			if (! $validatePhotoController->isValidWidthAndHeight ( 800, 2000, 800, 1800, $_FILES ['property_photo'] ['tmp_name'] [$i] )) {
				header ( "Location:" . URL . "adm_listImovel.php?error=size" );
				exit ();
			}
			
			// Rename the file totimestamp and extension.
			$finalName = basename ( $_FILES ['property_photo'] ['name'] [$i] ) . time () . "." . substr ( $_FILES ['property_photo'] ['name'] [$i], (strlen ( $_FILES ['property_photo'] ['name'] [$i] ) - 3) );
			
			if (move_uploaded_file ( $_FILES ['property_photo'] ['tmp_name'] [$i], $photoFolder . $finalName )) {
				
				$newFileName = $photoFolder . $finalName;
				
				$propertyPhoto = new PropertyPhoto ();
				$propertyPhoto->setPath ( $newFileName );
				$propertyPhoto->setProperty ( $propertyController->getById ( $params ['property_id'] ) );
				
				$propertyPhotoController->insertPropertyPhoto ( $propertyPhoto );
				
				/*
				 * gerar a foto menor
				 */
				gera_thumb ( $newFileName );
			} else {
				echo "Falha ao mover a imagem dentro do servidor. Por favor, tente novamente mais tarde!";
				exit ();
			}
		}
	}
	
	/*
	 * Agora fazer o cadastro das imobiliárias marcados pelo usuário
	 */
	updateStoresInProperty ( $params ['property_id'] );
	
	/*
	 * Agora fazer o cadastro dos detalhes marcados pelo usuário
	 */
	updateDetailsInProperty ( $params ['property_id'] );
	
	/*
	 * Agora fazer o update das proximidades
	 */
	updateNearsInProperty ( $params ['property_id'] );
	
	/*
	 * agora fazer um header location para apresentar a tela de início do admin
	 */
	header ( "location:" . URL_ADMIN_PAGE . "?success=update_property" );
} else {
	
	/*
	 * agora fazer um header location para apresentar a tela de início do admin
	 */
	header ( "location:" . URL_ADMIN_PAGE . "?failure=update_property" );
}

/*
 * Método para atribuir e cadastrar os serviços ao usuário recém-cadastrado
 */
function updateDetailsInProperty($propertyId) {
	
	// Get user for record in user_service.
	if ((! empty ( $propertyId )) && ($propertyId != "")) {
		
		$propertyController = new PropertyController ();
		$propertyDetailController = new PropertyDetailController ();
		
		$property = $propertyController->getById ( $propertyId );
		
		if ($property instanceof Property) {
			// if (isset($_POST['serviceProvider'])) {
			
			if (! is_array ( $_POST ['details'] ) && ! isset ( $_POST ['details'] ))
				$detailsToInsert [] = $_POST ['details'];
			else
				foreach ( $_POST ['details'] as $detailsValue ) {
					$detailsToInsert [] = $detailsValue;
				}
				
				/*
			 * Alterei o if abaixo, ao invés de ficar dando includes e location
			 * essa página pelo que notei apenas apresentava os dados na tela
			 * como não irei querer isso então eu apenas mando a página inicial
			 * PAGINA DO ADMIN
			 */
				// // if (!is_null(propertyHasDetails($property->getidUnico())))
				// deleteAllDetailsInProperty($property->getidUnico());
			if (! is_null ( propertyHasDetails ( $propertyId ) ))
				deleteAllDetailsInProperty ( $propertyId );
			
			if (isset ( $_POST ['details'] ))
				if ($propertyDetailController->createPropertyDetail ( $detailsToInsert, $property )) {
					// echo "retornou aqui";
					// exit();
					// header("Location: confirmRecordedUser.php?user=" . $user->getId());
					// returnToPage("success","cad_upd_user");
				} else {
					// $error['ServiceProviderNotRecorded'] = TRUE;
					
					echo 'Erro! Infelizmente não foi possível cadastrar-se como um provedor
                  de serviços neste momento. Tente mais tarde! Agradecemos sua paciência.';
					exit ();
				}
			
			// } elseif ($_POST['choiceNo'] == 'no') {//caso não tenha informado ser provedor de serviço
			// returnToPage("success","cad_upd_user");
			// // header("Location: confirmRecordedUser.php?user=" . $user->getId());
			// }
		} else {
			// $error['UserNotFound'] = TRUE;
			// header("Content-Type: text/html; charset=UTF-8");
			echo "Erro! O imóvel informado não foi encontrado.";
			exit ();
		}
	} else {
		// $error['DontUserParam'] = TRUE;
		// header("Content-Type: text/html; charset=UTF-8");
		echo 'Erro! Você não informou um imóvel.';
		exit ();
	}
}

/*
 * Function that will see if user has service provider. If has
 * this function return true. Otherwise return false
 */
function propertyHasDetails($idProperty) {
	$propertyHasDetails = new PropertyDetailController ();
	$detailsInProperty = $propertyHasDetails->getPropertyDetail ( $idProperty );
	
	if (! is_null ( $detailsInProperty ))
		return $detailsInProperty;
	
	return NULL;
}

/*
 * function that will remove all service provider to this user
 */
function deleteAllDetailsInProperty($idProperty) {
	$propertyDetailControllerDelete = new PropertyDetailController ();
	$wasDeleted = $propertyDetailControllerDelete->deleteDetailsInProperty ( $idProperty );
	
	return $wasDeleted;
}

/*
 * Método para atribuir e cadastrar os serviços ao usuário recém-cadastrado
 */
function updateNearsInProperty($propertyId) {
	
	// Get user for record in user_service.
	if ((! empty ( $propertyId )) && ($propertyId != "")) {
		
		$propertyController = new PropertyController ();
		$propertyNearController = new PropertyNearController ();
		
		$property = $propertyController->getById ( $propertyId );
		
		if ($property instanceof Property) {
			// if (isset($_POST['serviceProvider'])) {
			
			if (! is_array ( $_POST ['nears'] ) && ! isset ( $_POST ['nears'] ))
				$nearsToInsert [] = $_POST ['nears'];
			else
				foreach ( $_POST ['nears'] as $nearsValue ) {
					$nearsToInsert [] = $nearsValue;
				}
				
				/*
			 * Alterei o if abaixo, ao invés de ficar dando includes e location
			 * essa página pelo que notei apenas apresentava os dados na tela
			 * como não irei querer isso então eu apenas mando a página inicial
			 * PAGINA DO ADMIN
			 */
			if (! is_null ( propertyHasNears ( $propertyId ) ))
				deleteAllNearsInProperty ( $propertyId );
			
			if (isset ( $_POST ['nears'] ))
				
				if ($propertyNearController->createPropertyNear ( $nearsToInsert, $property )) {
					// echo "retornou aqui";
					// exit();
					// header("Location: confirmRecordedUser.php?user=" . $user->getId());
					// returnToPage("success","cad_upd_user");
				} else {
					// $error['ServiceProviderNotRecorded'] = TRUE;
					
					echo 'Erro! Infelizmente não foi possível cadastrar-se como um provedor
                    de serviços neste momento. Tente mais tarde! Agradecemos sua paciência.';
					exit ();
				}
			
			// } elseif ($_POST['choiceNo'] == 'no') {//caso não tenha informado ser provedor de serviço
			// returnToPage("success","cad_upd_user");
			// // header("Location: confirmRecordedUser.php?user=" . $user->getId());
			// }
		} else {
			// $error['UserNotFound'] = TRUE;
			// header("Content-Type: text/html; charset=UTF-8");
			echo "Erro! O imóvel informado não foi encontrado.";
			exit ();
		}
	} else {
		// $error['DontUserParam'] = TRUE;
		// header("Content-Type: text/html; charset=UTF-8");
		echo 'Erro! Você não informou um imóvel.';
		exit ();
	}
}

/*
 * Function that will see if user has service provider. If has
 * this function return true. Otherwise return false
 */
function propertyHasNears($idProperty) {
	$propertyHasNears = new PropertyNearController ();
	$nearsInProperty = $propertyHasNears->getPropertyNear ( $idProperty );
	
	if (! is_null ( $nearsInProperty ))
		return $nearsInProperty;
	
	return NULL;
}

/*
 * function that will remove all service provider to this user
 */
function deleteAllNearsInProperty($idProperty) {
	$propertyNearControllerDelete = new PropertyNearController ();
	$wasDeleted = $propertyNearControllerDelete->deleteNearsInProperty ( $idProperty );
	
	return $wasDeleted;
}

/*
 * Método para atribuir e cadastrar os serviços ao usuário recém-cadastrado
 */
function updateStoresInProperty($propertyId) {
	
	// Get user for record in user_service.
	if ((! empty ( $propertyId )) && ($propertyId != "")) {
		
		$propertyController = new PropertyController ();
		$propertyStoreController = new PropertyStoreController ();
		
		$property = $propertyController->getById ( $propertyId );
		
		if ($property instanceof Property) {
			// if (isset($_POST['serviceProvider'])) {
			
			if (! is_array ( $_POST ['stores'] ) && ! isset ( $_POST ['stores'] )) {
				$storesToInsert [] = $_POST ['stores'];
				$storeReferencia [] = $_POST ['referencia'];
			} else {
				foreach ( $_POST ['referencia'] as $referencia ) {
					if ($referencia != "") {
						$storeReferencia [] = $referencia;
					}
				}
				foreach ( $_POST ['stores'] as $storesValue ) {
					$storesToInsert [] = $storesValue;
				}
			}
			
			/*
			 * Alterei o if abaixo, ao invés de ficar dando includes e location
			 * essa página pelo que notei apenas apresentava os dados na tela
			 * como não irei querer isso então eu apenas mando a página inicial
			 * PAGINA DO ADMIN
			 */
			// if (!is_null(propertyHasStores($propertyId)))
			if (! is_null ( propertyHasStores ( $property->getidUnico () ) ))
				deleteAllStoresInProperty ( $property->getidUnico () );
			
			if (isset ( $_POST ['stores'] ))
				if ($propertyStoreController->createPropertyStore ( $storeReferencia, $storesToInsert, $property )) {
					// echo "retornou aqui";
					// exit();
					// header("Location: confirmRecordedUser.php?user=" . $user->getId());
					// returnToPage("success","cad_upd_user");
				} else {
					// $error['ServiceProviderNotRecorded'] = TRUE;
					
					echo 'Erro! Infelizmente não foi possível cadastrar-se como um provedor
                  de serviços neste momento. Tente mais tarde! Agradecemos sua paciência.';
					exit ();
				}
			
			// } elseif ($_POST['choiceNo'] == 'no') {//caso não tenha informado ser provedor de serviço
			// returnToPage("success","cad_upd_user");
			// // header("Location: confirmRecordedUser.php?user=" . $user->getId());
			// }
		} else {
			// $error['UserNotFound'] = TRUE;
			// header("Content-Type: text/html; charset=UTF-8");
			echo "Erro! O imóvel informado não foi encontrado.";
			exit ();
		}
	} else {
		// $error['DontUserParam'] = TRUE;
		// header("Content-Type: text/html; charset=UTF-8");
		echo 'Erro! Você não informou um imóvel.';
		exit ();
	}
}

/*
 * Function that will see if user has service provider. If has
 * this function return true. Otherwise return false
 */
function propertyHasStores($idProperty) {
	$propertyHasStores = new PropertyStoreController ();
	$storesInProperty = $propertyHasStores->getPropertyStore ( $idProperty );
	
	if (! is_null ( $storesInProperty ))
		return $storesInProperty;
	
	return NULL;
}

/*
 * function that will remove all service provider to this user
 */
function deleteAllStoresInProperty($idProperty) {
	$propertyStoreControllerDelete = new PropertyStoreController ();
	$wasDeleted = $propertyStoreControllerDelete->deleteStoresInProperty ( $idProperty );
	
	return $wasDeleted;
}

/*
 * Função pegada na net que irá gerar o ThumbNail das imagens
 * para abrir de forma mais rápida no server
 */
function gera_thumb($image_path/*, $widthOld, $heightOld*/) {
	
	// Pega onde está a imagem
	// $image_file = str_replace('..', '', $_SERVER['QUERY_STRING']);
	// $image_path = PATH_IMG . '/' . $image_file;
	
	// Carrega a imagem
	$img = null;
	
	$extensao = strtolower ( end ( explode ( '.', $image_path ) ) );
	
	if ($extensao == 'jpg' || $extensao == 'jpeg') {
		$img = @imagecreatefromjpeg ( $image_path );
	} else if ($extensao == 'png') {
		$img = @imagecreatefrompng ( $image_path );
		// Se a versão do GD incluir suporte a GIF, mostra...
	} elseif ($extensao == 'gif') {
		$img = @imagecreatefromgif ( $image_path );
	}
	
	// Se a imagem foi carregada com sucesso, testa o tamanho da mesma
	if ($img) {
		// Pega o tamanho da imagem e proporção de resize
		$width = imagesx ( $img );
		$height = imagesy ( $img );
		$scale = min ( MAX_WIDTH / $width, MAX_HEIGHT / $height );
		
		// echo "WIDTH: ".$widthOld. " HEIGHT: ".$heightOld;
		
		// $rotate = 0;
		// if (floatval($heightOld) < floatval($widthOld))
		// $rotate = 270;
		
		// echo "Rotate:: ". $rotate;
		// exit();
		
		// Se a imagem é maior que o permitido, encolhe ela
		if ($scale < 1) {
			$new_width = floor ( $scale * $width );
			$new_height = floor ( $scale * $height );
			// Cria uma imagem temporária
			$tmp_img = imagecreatetruecolor ( $new_width, $new_height );
			// Copia e resize a imagem velha na nova
			imagecopyresized ( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
			imagedestroy ( $img );
			unlink ( $image_path );
			$img = $tmp_img;
			
			$ext = strtolower ( end ( explode ( '.', $image_path ) ) );
			if ($ext == "jpg" || $ext == "jpeg") {
				imagejpeg ( $img, PATH_PROPERTY_PHOTOS . basename ( $image_path ), IMAGE_QUALITY );
				return true;
			} elseif ($ext == "gif") {
				imagepng ( $img, PATH_PROPERTY_PHOTOS . basename ( $image_path ) );
				return true;
			} elseif ($ext == "png") {
				imagepng ( $img, PATH_PROPERTY_PHOTOS . basename ( $image_path ) );
				return true;
			} else {
				echo ("Formato de destino nao suportado");
				exit ();
			}
		}
	}
	
	// Cria uma imagem de erro se necessário
	if (! $img) {
		$img = imagecreate ( MAX_WIDTH, MAX_HEIGHT );
		imagecolorallocate ( $img, 204, 204, 204 );
		$c = imagecolorallocate ( $img, 153, 153, 153 );
		$c1 = imagecolorallocate ( $img, 0, 0, 0 );
		imageline ( $img, 0, 0, MAX_WIDTH, MAX_HEIGHT, $c );
		imageline ( $img, MAX_WIDTH, 0, 0, MAX_HEIGHT, $c );
		imagestring ( $img, 2, 12, 55, 'erro ao carregar imagem', $c1 );
	}
}

/*
 * Method that will called to delete the property' photo
 */
function deletePhoto($propertyPhotoId) {
	$propertyPhotoController = new PropertyPhotoController ();
	$wasDeleted = $propertyPhotoController->deletePropertyPhoto ( $propertyPhotoId );
}
?>
