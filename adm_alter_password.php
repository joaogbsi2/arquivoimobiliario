<?php

/*
 * INCLUDE SECTOR
 */

// $_SERVER['DOCUMENT_ROOT'] = "/home/arqui937/public_html/";
// define ("PATH", $_SERVER['DOCUMENT_ROOT']);

// include the file of configuration
// require_once '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_INCLUDES_ADMIN . 'session.php';
/*
 * The bellow file was needed because, will be shown all
 * clients cadastred for the root administrator and he
 * will be to set the its status
 */
require_once PATH_CONTROLLER . 'UserController.class.php';
require_once PATH_MODEL_ENTITIES . 'User.class.php';

/*
 * Function to shown the link to all letters
 */
function showLinkOfNames() {
	for($i = 65; $i < 91; $i ++) {
		?>
<a href="<?php echo URL_ADMIN_PAGE . "?letter=". chr($i) ?>"><?php echo chr($i) ?>  |  </a>
<?php
	}
}
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon" href="images/arquiLogo.png">
<title>Arquivo Imobiliário</title>
<link href="style.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">

    /*
     * Create a function that will validate if password and confirm 
     * password is have a same value
     */
    function isSamePassword(){
        
        var password = document.getElementById("password").value;
        var confirmPassword = document.getElementById("confirm_password").value;
        
        if(password == confirmPassword)
            return true;
        else{
            window.alert("Campo Nova Senha e Confirmar Senha devem possuir o mesmo valor!");
            document.getElementById("confirm_password").focus();
            return false;
        }
    }
</script>
</head>

<body>

	<!-- GERAL -->
	<div id="geral">


		<!-- TOPO -->
    <?php
				require_once PATH_INCLUDES_ADMIN . 'header.php';
				?>
    <!-- /TOPO -->


		<!-- MAIN CONTEUDO -->
		<div id="mainConteudo">

			<!-- CONTEUDO -->
			<div id="conteudo">

				<!-- ADMIN COL1 -->
            <?php
												require_once PATH_INCLUDES_ADMIN . 'right_colum.html';
												?>
            <!-- /ADMIN COL1 -->


				<!-- Tabela para ativar e desativar usuários -->
				<table width="75%" border="1" cellspacing="2">
					<tr>
						<th width="35%" align="left">Nome</th>
						<th width="21%" align="left">Telefone</th>
						<th width="24%" align="left">Email</th>
						<th width="20%" colspan="2" align="left">Alterar Senha</th>
					</tr>
                    <?php
																				/*
																				 * Show the list of users to alter its password
																				 */
																				showLinkOfNames ();
																				
																				$userController = new UserController ();
																				$statusAllUsers = $userController->getStatusAllClients ();
																				foreach ( $statusAllUsers as $usersStatus ) {
																					?>
                            <tr>
						<form name="status-users-form" method="post">
							<td width="35%" align="left"><?php echo $usersStatus->getName() ?></td>
							<td width="21%" align="left"><?php echo $usersStatus->getPhone() ?></td>
							<td width="24%" align="left"><?php echo $usersStatus->getEmail() ?></td>
							<td width="20%" align="left"><input type="hidden" name="id_user"
								value="<?php echo $usersStatus->getId(); ?>"> <!--<input type="hidden" name="action" value="">-->
								<input type="submit" value="Alterar Senha"></td>
						</form>

					</tr>
                        <?php
																				}
																				?>
                    </table>
            
                    <?php
																				/*
																				 * If exist a form submit. Then show this form with user`s data
																				 * defined in its fields to alter password
																				 */
																				if (isset ( $_POST ['id_user'] ) && $_POST ['id_user'] != NULL) {
																					
																					$user = $userController->getById ( $_POST ['id_user'] );
																					
																					/*
																					 * If was located a user with ID then shown the form
																					 */
																					if (! is_null ( $user )) {
																						?>
                                
                                <form name="form-alter-password"
					method="post" action="action/adminServices.action.php">

					<table width="700">
						<tr>
							<td width="180" align="right">Usuário:</td>
							<td width="50"><input type="text" class="textfield" name="user"
								disabled="true" value="<?php echo $user->getName(); ?>" /></td>
						</tr>
						<tr>
							<td colspan="2" height="5"></td>
						</tr>
						<tr>
							<td width="180" align="right">Nova Senha:</td>
							<td width="50"><input type="text" class="textfield"
								name="password" id="password" /></td>
						</tr>
						<tr>
							<td colspan="2" height="5"></td>
						</tr>
						<tr>
							<td width="180" align="right">Confirmar Senha:</td>
							<td width="50"><input type="text" class="textfield"
								name="confirm_password" onblur="return isSamePassword();"
								id="confirm_password" /></td>
						</tr>
						<tr>
							<td colspan="2" height="5"></td>
						</tr>

						<tr>
							<td width="180" align="right"></td>
							<input type="hidden" name="action" value="alterPassword" />
							<input type="hidden" name="id_user"
								value="<?php echo $user->getId(); ?>" />
							<td width="50"><input type="submit" value="Alterar"
								onclick="return isSamePassword();" /></td>
						</tr>
						<tr>
							<td colspan="2" height="5"></td>
						</tr>
					</table>
				</form>
                            <?php
																					}
																				}
																				?>
                    
      	</div>
			<!-- CONTEUDO -->

		</div>
		<!-- /MAIN CONTEUDO -->


		<!-- FOOTER -->
    <?php
				require_once PATH_INCLUDES_ADMIN . 'footer.html';
				?>
    <!-- /FOOTER -->



	</div>
	<!-- /GERAL -->


</body>

</html>