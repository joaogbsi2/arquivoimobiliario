<?php

/*
 * INCLUDE SECTOR
 */

// $_SERVER['DOCUMENT_ROOT'] = "/home/arqui937/public_html/";
// define ("PATH", $_SERVER['DOCUMENT_ROOT']);

// include the file of configuration
// require_once '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_INCLUDES_ADMIN . 'session.php';
require_once PATH_MODEL_ENTITIES . 'Decoracao.class.php';
require_once PATH_CONTROLLER . 'DecoracaoController.class.php';

$decoController = new DecoracaoController ();
$decoracao = $decoController->findAll ();

if (is_null ( $decoracao ))
	$decoracao = new Decoracao ();

$pathDecoracaoPhoto = URL_DECORACAO_PHOTOS . basename ( $decoracao->getPhoto () );
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon" href="images/arquiLogo.png">
<title>Arquivo Imobiliário</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>

<body>
    
    <?php
				
				if (isset ( $_GET ['error'] )) {
					if ($_GET ['error'] == "size") {
						?>
                <script type="text/javascript">
                    window.alert("Erro ao tentar cadastrar imagem.\nPor favor, verifique o limite de largura e altura da imagem!");
                </script>
        <?php
					}
				}
				
				?>

<!-- GERAL -->
	<div id="geral">

		<!-- TOPO -->
    <?php
				require_once PATH_INCLUDES_ADMIN . 'header.php';
				?>
	<!-- /TOPO -->


		<!-- MAIN CONTEUDO -->
		<div id="mainConteudo">

			<!-- CONTEUDO -->
			<div id="conteudo">

				<!-- ADMIN COL1 -->
            <?php
												require_once PATH_INCLUDES_ADMIN . 'right_colum.html';
												?>
            <!-- /ADMIN COL1 -->

				<!-- AREA DE CADASTRO -->
				<div id="adm_COL2">
					<!-- FORM -->
					<form class="validate" id="frmCadTech"
						action="action/cadDecoracao.action.php" method="post"
						enctype="multipart/form-data">
						<div id="Tit">Decoração</div>

						<div id="cxInfo">
							<div id="fotoInfo">
								<img src="images/imgDados.jpg" />
							</div>
						</div>

						<div id="Tit">Cadastrar</div>

						<div id="cxBusca">
							<table width="515">
								<tr>
									<td align="right">Título:</td>
									<td><input name="title" type="text" maxlength="255"
										class="campo1" id="user" /></td>
								</tr>
								<tr>
									<td align="right">Sub-Título:</td>
									<td><input name="subTitle" type="text" maxlength="50"
										class="campo1" id="user" /></td>
								</tr>
								<tr>
									<td align="right">Descrição:</td>
									<td><textarea name="description" rows="5" class="campo2"
											id="user"></textarea></td>
								</tr>
								<tr>
									<td align="right">Imagem:</td>
									<td><input name="photo" type="file" /></td>
								</tr>
								<tr>
									<td></td>
									<td align="right"><b>Tamanho da imagem: </b>Mínimo 600x600
										máximo 1200x1200 <input type="hidden" name="action"
										value="cadastre" /> <input type="submit" name="Enviar"
										id="Enviar" value="Cadastrar" /></td>
								</tr>
							</table>
						</div>
					</form>

					<div id="Tit">Última decoração cadastrada</div>

					<div id="cxCont1">
						<div id="tit"><?php echo $decoracao->getTitle(); ?></div>
						<div id="desc">
							<p>
								<img width="400px" height="280px"
									src="<?php echo $pathDecoracaoPhoto ?>" align="left"
									style="padding: 10px 10px 10px 10px" />
							</p>
							<p>&nbsp;</p>
							<p style="font-size: 11pt;"><?php echo nl2br($decoracao->getDescription()); ?></p>
						</div>
					</div>


				</div>
				<!-- /COL2-->

			</div>
			<!-- CONTEUDO -->

		</div>
		<!-- /MAIN CONTEUDO -->


		<!-- FOOTER -->
		<div id="footer">
			<div id="conteudoFooter">
				<div id="ass">2013 © Arquivo Imobiliário. Todos os direitos
					reservados.</div>
			</div>
		</div>
		<!-- /FOOTER -->



	</div>
	<!-- /GERAL -->


</body>

</html>