<?php
$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_INCLUDES_ADMIN . 'session.php';
require_once PATH_MODEL_ENTITIES . 'Store.class.php';
require_once PATH_CONTROLLER . 'StoreController.class.php';

define ( "LIMIT_STORE", 4 );

$storeController = new StoreController ();
$stores = $storeController->findAll ( LIMIT_STORE, "id DESC" );
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="shortcut icon" href="images/arquiLogo.png">
<title>Arquivo Imobiliário</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>

<script type="text/javascript">

    /*
     * create a function that will go ask if the user want really delete the user 
     */
    function doYouWantDelete(){
        
        var answer = window.confirm("Deseja realmente remover essa imobiliária?");
        
        if(answer)
            return true;
        
        return false;
    }
</script>

<body>
    
    <?php
				
				if (isset ( $_GET ['error'] )) {
					if ($_GET ['error'] == "size") {
						?>
                <script type="text/javascript">
                    window.alert("Erro ao tentar cadastrar imagem.\nPor favor, verifique o limite de largura e altura da imagem!");
                </script>
        <?php
					}
				}
				
				?>

<!-- GERAL -->
	<div id="geral">


		<!-- TOPO -->
    <?php
				require_once PATH_INCLUDES_ADMIN . 'header.php';
				?>
    <!-- /TOPO -->


		<!-- MAIN CONTEUDO -->
		<div id="mainConteudo">

			<!-- CONTEUDO -->
			<div id="conteudo">

				<!-- ADMIN COL1 -->
                <?php
																require_once PATH_INCLUDES_ADMIN . 'right_colum.html';
																?>
                <!-- /ADMIN COL1 -->

				<div id="adm_COL2">

					<form class="validate" id="frmCadStore"
						action="action/cadImob.action.php" method="post"
						enctype="multipart/form-data">

						<div id="Tit">Imobiliárias</div>

						<!-- 						<div id="cxInfo"> -->
						<!-- 							<div id="fotoInfo"> -->
						<!-- 								<img src="images/imgDados.jpg" /> -->
						<!-- 							</div> -->
						<!-- 							<div id="txtInfo">Existem 4 noticias</div> -->
						<!-- 							<div id="txtInfo">Exibindo 4 ultimos</div> -->
						<!-- 							<div id="txtInfo">Data do último cadastro: 18/05/2013</div> -->
						<!-- 						</div> -->

						<div id="Tit">Cadastrar</div>

						<div id="cxBusca">
							<table width="515">
								<tr>
									<td align="right">Título:</td>
									<td><input name="name" type="text" class="campo1" id="user"
										maxlength="75" /></td>
								</tr>
								<tr>
									<td align="right">E-mail:</td>
									<td><input name="mail" placeholder="exemplo@exemplo.com.br"
										class="textfield required email" type="text" class="campo1"
										id="user" maxlength="50" width="200" /></td>
								</tr>
								<tr>
									<td align="right">Endereco:</td>
									<td><input name="endereco" type="text" class="campo1" id="user"
										maxlength=300 /></td>
								</tr>
								<tr>
									<td align="right">Telefone:</td>
									<td><input name="telefone" type="text" class="campo1" id="user"
										maxlength="30" /></td>
								</tr>
								<tr>
									<td align="right">Responsavel:</td>
									<td><input name="responsavel" type="text" class="campo1"
										id="user" maxlength="200" /></td>
								</tr>
								<tr>
									<td align="right">Imagem:</td>
									<td><input type="file" class="file" id="photo" name="photo" /></td>
								</tr>
								<tr>
									<td></td>
									<td align="right"><b>Tamanho da imagem: </b>Mínimo 600x600
										máximo 1200x1200 <input type="hidden" name="action"
										value="cadastre" /> <input type="submit" name="Enviar"
										id="Enviar" value="Cadastrar" /></td>
								</tr>
							</table>
						</div>

					</form>
					<div id="Tit">Últimas imobiliárias cadastradas</div>
                
                <?php
																
																if (! is_null ( $stores ))
																	foreach ( $stores as $storeCadastred ) {
																		?>
                        <div id="cxLisImov">
<!-- 						<a href="adm_viewImov.php?id_store= -->
						<?php //echo $storeCadastred->getId(); 
																		?>
<!-- 						"> -->
							<div id="cxCod">
								<img style="margin-top: -18px;" width="120px" height="65px;"
									src="images/store_photo/<?php echo basename($storeCadastred->getPhoto()); ?>" />
							</div>
						</a>
						<div id="cxDesc">
							<table width="600">
								<tr>
									<td width="74"><strong>Titulo:</strong></td>
									<td><?php echo $storeCadastred->getName(); ?></td>
								</tr>
								<tr>
									<td><strong>E-mail:</strong></td>
									<td><?php echo $storeCadastred->getMail(); ?></td>
								</tr>
								<tr>
									<td><strong>Endereco:</strong></td>
									<td><?php echo $storeCadastred->getEndereco(); ?></td>
								</tr>
								<tr>
									<td><strong>Telefone</strong></td>
									<td><?php echo $storeCadastred->getTelefone(); ?></td>
								</tr>
								<tr>
									<td><strong>Responsavel:</strong></td>
									<td><?php echo $storeCadastred->getResponsavel(); ?></td>
								</tr>
								<tr>
									<td>
										<form name="update_store" method="post"
											action="action/adminServices.action.php">
											<input type="hidden" name="action" value="updateStore"> <input
												type="hidden" name="store_id"
												value="<?php echo intval($storeCadastred->getId()); ?>"> <input
												type="submit" name="update" value="Ver mais">
										</form>
									</td>
									<td>
										<form name="delete_store" method="post"
											action="action/adminServices.action.php">
											<input type="hidden" name="action" value="deleteStore"> <input
												type="hidden" name="store_id"
												value="<?php echo intval($storeCadastred->getId()); ?>"> <input
												type="submit" name="delete" value="Excluir Imobiliária"
												onclick="return doYouWantDelete();">
										</form>
									</td>
								</tr>

							</table>
						</div>
					</div>
                    <?php
																	}
																
																?>
                
                
            </div>

			</div>
			<!-- CONTEUDO -->

		</div>
		<!-- /MAIN CONTEUDO -->


		<!-- FOOTER -->
		  <?php
				require_once PATH_INCLUDES_ADMIN . 'footer.html';
				?>
		<!-- /FOOTER -->



	</div>
	<!-- /GERAL -->

	<!-- End conteiner -->
	<!-- JS Content and JSLibary -->
	<script type="text/javascript" src="js/jquery-1.8.3.js"></script>

	<!-- includes for multiple select -->
	<script type="text/javascript" src="js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/jquery.multiselect.min.js"></script>
	<!-- end multiple select include -->

	<!-- includes for maskedinput -->
	<script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>
	<!-- end maskedinput includes -->

	<!-- includes for jquery validate and their messages -->
	<script type="text/javascript" src="js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="js/messages_pt_BR.js"></script>
	<!-- end jquery validate and their messages includes -->


</body>

</html>