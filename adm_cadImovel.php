<?php

/*
 * INCLUDE SECTOR
 */

// $_SERVER['DOCUMENT_ROOT'] = "/Users/edilson/Sites/webmovel2/";
// define ("PATH", $_SERVER['DOCUMENT_ROOT']);

// include the file of configuration
// require_once '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_INCLUDES_ADMIN . 'session.php';
require_once PATH_ACTION . 'cadProperty.action.php';
require_once PATH_MODEL_ENTITIES . 'State.class.php';
require_once PATH_MODEL_ENTITIES . 'City.class.php';
require_once PATH_MODEL_ENTITIES . 'Status.class.php';
require_once PATH_MODEL_ENTITIES . 'PropertyType.class.php';

/*
 * Create a new field. Combobox of Store
 */
require_once PATH_CONTROLLER . 'StoreController.class.php';
require_once PATH_MODEL_ENTITIES . 'Store.class.php';

/*
 * listar os detalhes
 */
require_once PATH_CONTROLLER . 'DetailController.class.php';
require_once PATH_MODEL_ENTITIES . 'Detail.class.php';

/*
 * listar as proximidades
 */
require_once PATH_CONTROLLER . 'NearController.class.php';
require_once PATH_MODEL_ENTITIES . 'Near.class.php';

$storeController = new StoreController ();
$stores = $storeController->findAll ( NULL, " NAME ASC" );

$detailController = new DetailController ();
$details = $detailController->getAll ( NULL, "name ASC" );

$nearControllerList = new NearController ();
$nearsList = $nearControllerList->getAll ( NULL, "name ASC" );

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon" href="images/arquiLogo.png">
<title>Arquivo Imobiliário</title>
<link href="style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/jquery.multiselect.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />

</head>

<body>
    
    <?php
				if (isset ( $_GET ['error'] )) {
					if ($_GET ['error'] == "size") {
						?>
                <script type="text/javascript">
                    window.alert("Erro ao tentar cadastrar imagem.\nPor favor, verifique o limite de largura e altura da imagem!");
                </script>
        <?php
					}
				}
				?>

<!-- GERAL -->
	<div id="geral">


		<!-- TOPO -->
    <?php
				require_once PATH_INCLUDES_ADMIN . 'header.php';
				?>
	<!-- /TOPO -->


		<!-- MAIN CONTEUDO -->
		<div id="mainConteudo">

			<!-- CONTEUDO -->
			<div id="conteudo">

				<!-- ADMIN COL1 -->
            <?php
												require_once PATH_INCLUDES_ADMIN . 'right_colum.html';
												?>
            <!-- /ADMIN COL1 -->

				<!-- AREA DE CADASTRO -->
				<div id="adm_COL2">
					<div id="Tit">Cadastro de Imóvel</div>
					<form method="post" action="action/recebeUpload.php"
						enctype="multipart/form-data" accept-charset="UTF-8">
						<div id="cxTit">Enviar por arquivo</div>
						<div id="cxTex">
							<table width="700">
								<tr>
									<td>
									
									<td width="180" align="right">Escolher arquivo:</td>
									<td><input type="file" name="arquivo" /></td>
									<td><input type="submit" value="Enviar" /></td>
								</tr>
							</table>
						</div>
					</form>

					<form id="frmCadProperty" method="post"
						action="action/cadPropertyForm.action.php"
						enctype="multipart/form-data" accept-charset="UTF-8">


						<div id="cxTit">Tipo de Negócio</div>
						<div id="cxTex">
							<table width="700">
								<!-- ACHO QUE O CAMPO ID JÁ PODERÁ SER USADO COMO O CÓDIGO,
                            NÃO HÁ A NECESSIDADE DE CRIAR UM CAMPO CÓDIGO EU ACHO
                            ALTERAÇÃO FEITA NO DIA 27/10/2013
                        <tr>
                            <td width="180" align="right">Código do Imóvel:</td>
                            <td width="520"><input name="user" type="text" class="campo1" id="user"/></td>
                        </tr>
                        
                        <tr>
                        	<td colspan="2" height="5"></td>
                        </tr>
                        -->

								<!-- INICIO DO FORM DE CADASTRO DE IMOVEL DA VERSÃO NOVA -->
								<tr>
									<td width="180" align="right">* Este imóvel é para: (Operação)</td>
									<td width="520">
										<div id="status">
                                <?php
																																/*
																																 * Essas variáveis são pegas do include no .action
																																 * que cria um novo objeto da classe StatusController
																																 *
																																 */
																																foreach ( $statusController->getAll () as $status ) {
																																	echo '<span>';
																																	echo '<input type="radio" class="checkbox required" name="statusProperty" id="status" value="' . $status->getId () . '" />';
																																	echo $status->getDescription () . "              ";
																																	echo '</span>';
																																}
																																?>
                                </div>
									</td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>

								<tr>
									<td width="180" align="right">* Tipo de Imóvel:</td>
									<td width="520"><select class="required" name="propertyType"
										id="propertyType">
                                <?php
																																foreach ( $propertyTypeList as $propertyType ) {
																																	echo '<option value="' . $propertyType->getId () . '">' . $propertyType . '</option>';
																																}
																																?>
				</select></td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>

								<tr>
									<td width="180" align="right">* Imobiliária:</td>
									<td width="520">
                        <?php
																								if (! is_null ( $stores )) {
																									foreach ( $stores as $iterStoreList ) {
																										?>
                                    <input type="checkbox"
										name="stores[]" value="<?php echo $iterStoreList->getId(); ?>" />
                                                <?php echo $iterStoreList->getName(); ?>
                                    <input class="textfield"
										name="referencia[]" id="referencia" size="10" /> <br />   
                        	<?php
																									}
																								}
																								
																								?>
                            </td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>

								<!-- Campo de exclusividade da imobiliária -->
								<tr>
									<td width="180" align="right">Exclusividade:</td>
									<td width="520"><select name="exclusive_store"
										id="exclusive_store">
											<option value="0">Selecione...</option>
                                <?php
																																foreach ( $stores as $iter ) {
																																	echo '<option value="' . $iter->getId () . '">' . $iter->getName () . '</option>';
																																}
																																?>
				</select></td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>

								<tr>
									<td width="180" align="right">Quantidade de quartos:</td>
									<td width="50"><input class="textfield" name="qtdRooms" /></td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>

								<!--                        <tr>
                            <td width="180" align="right">Destaque?</td>
                            <td width="520"><select name="tipo" class="campo1" id title="cidade">
                          <option value="label" selected="selected">Label</option></select></td>
                        </tr>-->
							</table>
							<div id="temporadaConfig">
								* Dias da semana: <select id="daysOfWeek" name="daysOfWeek[]"
									multiple="multiple">
									<option value='domingo'>Domingo</option>
									<option value='segunda'>Segunda</option>
									<option value='terca'>Terça</option>
									<option value='quarta'>Quarta</option>
									<option value='quinta'>Quinta</option>
									<option value='sexta'>Sexta</option>
									<option value='sabado'>Sábado</option>
								</select> * Meses do ano: <select id="monthOfYear"
									name="monthOfYear[]" multiple="multiple">
									<option value='janeiro'>Janeiro</option>
									<option value='fevereiro'>Fevereiro</option>
									<option value='marco'>Março</option>
									<option value='abril'>Abril</option>
									<option value='maio'>Maio</option>
									<option value='junho'>Junho</option>
									<option value='julho'>Julho</option>
									<option value='agosto'>Agosto</option>
									<option value='setembro'>Setembro</option>
									<option value='outubro'>Outubro</option>
									<option value='novembro'>Novembro</option>
									<option value='dezembro'>Dezembro</option>
								</select>
							</div>

						</div>

						<div id="cxTit">Localização do Imóvel</div>
						<div id="cxTex">
							<table width="700">
								<tr>
									<td width="180" align="right">Endereço:</td>
									<td width="520"><input class="textfield" name="street"
										id="street" /></td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>
								<tr>
									<td width="180" align="right">Número:</td>
									<td width="40"><input type="text" class="textfield"
										name="number" id="number" /></td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>
								<tr>
									<td width="180" align="right">Bairro:</td>
									<td width="300"><input type="text" class="textfield"
										name="neighborhood" id="neighborhood" /></td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>
								<tr>
									<td width="180" align="right">CEP:</td>
									<td width="105"><input type="text" class="textfield" name="cep"
										id="cep" /></td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>
								<tr>
									<td width="180" align="right">* Cidade:</td>
									<td width="520"><select class="required" name="city">
                                    <?php
																																				foreach ( $cityController->getAllCities () as $city ) {
																																					echo '<option name="city" value="' . $city->getId () . '">' . $city->getName () . '</option>\r\n';
																																				}
																																				?>
                                </select></td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>
								<tr>
									<td width="180" align="right">* UF:</td>
									<td width="520"><select name="state" class="required">
                                    <?php
																																				foreach ( $stateController->getAllStates () as $state ) {
																																					echo '<option name="state" value="' . $state->getId () . '" >' . $state->getName () . '</option>\r\n';
																																				}
																																				?>
                                </select></td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>
								<!--                        <tr>
                            <td width="180" align="right">Exibir endereço?:</td>
                            <td width="520">
                                <select name="tipo" class="campo1" id title="cidade">
                                    <option value="1" selected="selected">Sim</option>
                                    <option value="0">Não</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                        	<td colspan="2" height="5"></td>
                        </tr>-->

							</table>
						</div>

						<div id="cxTit">Descrição do Imóvel</div>
						<div id="cxTex">
							<table width="700">
								<tr>
									<td width="180" align="right">Descrição:</td>
									<td width="400"><textarea
											style="width: 400px; heigth: 75px; overflow: auto;"
											class="textfield" name="description" id="description" /></textarea>
									</td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>
								<tr>
									<td width="180" align="right">Email de contato:</td>
									<td width="400"><input style="width: 400px;" type="text"
										placeholder="exemplo@exemplo.com.br" class="textfield email"
										name="contactEmail" id="contactEmail" /></td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>
								<tr>
									<td width="180" align="right">Telefone:</td>
									<td width="400"><input type="text" class="textfield"
										id="contactPhone" name="contactPhone" /></td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>


								<!--                        <tr>
                        	<td width="180" align="right">Dependências:</td>
                            <td width="520"><input name="user" type="text" class="campo2" id="user" value="" /></td>
                        </tr>
                        <tr>
                        	<td colspan="2" height="5"></td>
                        </tr>-->
								<!--                        <tr>
                        	<td width="180" align="right">Dormitórios:</td>
                            <td width="520"><input name="user2" type="text" class="campo1" id="user2"/></td>
                        </tr>
                        <tr>
                        	<td colspan="2" height="5"></td>
                        </tr>-->
								<!--                        <tr>
                        	<td width="180" align="right">Garagem:</td>
                            <td width="520"><input name="garagem" type="text" class="campo1" id="user3"/></td>
                        </tr>
                        <tr>
                        	<td colspan="2" height="5"></td>
                        </tr>-->
								<!--                        <tr>
                        	<td width="180" align="right">Link do vídeo:</td>
                            <td width="520"><input name="user4" type="text" class="campo1" id="user4"/></td>
                        </tr>
                        <tr>
                        	<td colspan="2" height="5"></td>
                        </tr>-->
								<!--                        <tr>
                        	<td width="180" align="right">Cadastrar Link Mapa:</td>
                            <td width="520"><input name="user4" type="text" class="campo1" id="user4"/></td>
                        </tr>-->
							</table>
						</div>

						<div id="cxTit">Características</div>
						<div id="cxTex">
							<table width="700">
								<tr>
									<td width="180" align="right">Garagem:</td>
									<td width="520"><input name="garagem" type="text"
										class="campo1" id="user5" /></td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>
								<tr>
									<td width="180" align="right">Pavimento:</td>
									<td width="520"><input name="pavimento" type="text"
										class="campo1" id="user2" /></td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>
								<!--                        <tr>
                        	<td width="180" align="right">Dependências:</td>
                            <td width="520"><input name="user3" type="text" class="campo1" id="user3"/></td>
                        </tr>
                        <tr>
                        	<td colspan="2" height="5"></td>
                        </tr>-->
								<tr>
									<td width="180" align="right">Posição:</td>
									<td width="520"><input name="posicao" type="text"
										class="campo1" id="user4" /></td>
								</tr>
							</table>
						</div>

						<div id="cxTit">Detalhes</div>
						<div id="cxTex">
                	    <?php
																					if (! is_null ( $details )) {
																						foreach ( $details as $iterDetail ) {
																							?>
                                    <input type="checkbox"
								name="details[]" value="<?php echo $iterDetail->getId(); ?>"
								id="CheckboxGroup1_0" />
                                                <?php echo $iterDetail->getName(); ?>
                                                
                                    <br />   
                        	<?php
																						}
																					}
																					?>
<!--                    	<tr>
                        	<td width="190"><input type="checkbox" name="CheckboxGroup1" value="caixa de seleção" id="CheckboxGroup1_0" />
Caixa de seleção</td>
                            <td width="190"><input type="checkbox" name="CheckboxGroup1_" value="caixa de seleção" id="CheckboxGroup1_" />
Caixa de seleção</td>
                          <td width="190"><input type="checkbox" name="CheckboxGroup1_2" value="caixa de seleção" id="CheckboxGroup1_2" />
Caixa de seleção</td>
                          <td width="190"><input type="checkbox" name="CheckboxGroup1_3" value="caixa de seleção" id="CheckboxGroup1_3" />
Caixa de seleção</td>
                        </tr>
                        <tr>
                        	<td width="190"><input type="checkbox" name="CheckboxGroup1" value="caixa de seleção" id="CheckboxGroup1_0" />
Caixa de seleção</td>
                            <td width="190"><input type="checkbox" name="CheckboxGroup1_" value="caixa de seleção" id="CheckboxGroup1_" />
Caixa de seleção</td>
                          <td width="190"><input type="checkbox" name="CheckboxGroup1_2" value="caixa de seleção" id="CheckboxGroup1_2" />
Caixa de seleção</td>
                          <td width="190"><input type="checkbox" name="CheckboxGroup1_3" value="caixa de seleção" id="CheckboxGroup1_3" />
Caixa de seleção</td>
                        </tr>
                        <tr>
                        	<td width="190"><input type="checkbox" name="CheckboxGroup1" value="caixa de seleção" id="CheckboxGroup1_0" />
Caixa de seleção</td>
                            <td width="190"><input type="checkbox" name="CheckboxGroup1_" value="caixa de seleção" id="CheckboxGroup1_" />
Caixa de seleção</td>
                          <td width="190"><input type="checkbox" name="CheckboxGroup1_2" value="caixa de seleção" id="CheckboxGroup1_2" />
Caixa de seleção</td>
                          <td width="190"><input type="checkbox" name="CheckboxGroup1_3" value="caixa de seleção" id="CheckboxGroup1_3" />
Caixa de seleção</td>
                        </tr>
                        <tr>
                        	<td width="190"><input type="checkbox" name="CheckboxGroup1" value="caixa de seleção" id="CheckboxGroup1_0" />
Caixa de seleção</td>
                            <td width="190"><input type="checkbox" name="CheckboxGroup1_" value="caixa de seleção" id="CheckboxGroup1_" />
Caixa de seleção</td>
                          <td width="190"><input type="checkbox" name="CheckboxGroup1_2" value="caixa de seleção" id="CheckboxGroup1_2" />
Caixa de seleção</td>
                          <td width="190"><input type="checkbox" name="CheckboxGroup1_3" value="caixa de seleção" id="CheckboxGroup1_3" />
Caixa de seleção</td>
                        </tr>-->

							</table>
						</div>

						<div id="cxTit">Proximidades</div>
						<div id="cxTex">
                    
                        <?php
																								if (! is_null ( $nearsList )) {
																									foreach ( $nearsList as $iterNearList ) {
																										?>
                                    <input type="checkbox"
								name="nears[]" value="<?php echo $iterNearList->getId(); ?>"
								id="CheckboxGroup1_0" />
                                                <?php echo $iterNearList->getName(); ?>
                                                
                                    <br />   
                        	<?php
																									}
																								}
																								?>
<!--                	<table width="700">
                    	<tr>
                        	<td width="190"><input type="checkbox" name="CheckboxGroup1" value="caixa de seleção" id="CheckboxGroup1_0" />
Caixa de seleção</td>
                            <td width="190"><input type="checkbox" name="CheckboxGroup1_" value="caixa de seleção" id="CheckboxGroup1_" />
Caixa de seleção</td>
                          <td width="190"><input type="checkbox" name="CheckboxGroup1_2" value="caixa de seleção" id="CheckboxGroup1_2" />
Caixa de seleção</td>
                          <td width="190"><input type="checkbox" name="CheckboxGroup1_3" value="caixa de seleção" id="CheckboxGroup1_3" />
Caixa de seleção</td>
                        </tr>
                        <tr>
                        	<td width="190"><input type="checkbox" name="CheckboxGroup1" value="caixa de seleção" id="CheckboxGroup1_0" />
Caixa de seleção</td>
                            <td width="190"><input type="checkbox" name="CheckboxGroup1_" value="caixa de seleção" id="CheckboxGroup1_" />
Caixa de seleção</td>
                          <td width="190"><input type="checkbox" name="CheckboxGroup1_2" value="caixa de seleção" id="CheckboxGroup1_2" />
Caixa de seleção</td>
                          <td width="190"><input type="checkbox" name="CheckboxGroup1_3" value="caixa de seleção" id="CheckboxGroup1_3" />
Caixa de seleção</td>
                        </tr>
                        <tr>
                        	<td width="190"><input type="checkbox" name="CheckboxGroup1" value="caixa de seleção" id="CheckboxGroup1_0" />
Caixa de seleção</td>
                            <td width="190"><input type="checkbox" name="CheckboxGroup1_" value="caixa de seleção" id="CheckboxGroup1_" />
Caixa de seleção</td>
                          <td width="190"><input type="checkbox" name="CheckboxGroup1_2" value="caixa de seleção" id="CheckboxGroup1_2" />
Caixa de seleção</td>
                          <td width="190"><input type="checkbox" name="CheckboxGroup1_3" value="caixa de seleção" id="CheckboxGroup1_3" />
Caixa de seleção</td>
                        </tr>
                        <tr>
                        	<td width="190"><input type="checkbox" name="CheckboxGroup1" value="caixa de seleção" id="CheckboxGroup1_0" />
Caixa de seleção</td>
                            <td width="190"><input type="checkbox" name="CheckboxGroup1_" value="caixa de seleção" id="CheckboxGroup1_" />
Caixa de seleção</td>
                          <td width="190"><input type="checkbox" name="CheckboxGroup1_2" value="caixa de seleção" id="CheckboxGroup1_2" />
Caixa de seleção</td>
                          <td width="190"><input type="checkbox" name="CheckboxGroup1_3" value="caixa de seleção" id="CheckboxGroup1_3" />
Caixa de seleção</td>
                        </tr>
                       
                    </table>-->
						</div>

						<!--                 <div id="cxTit">Vincular Imobiliárias ao Imóvel</div>
                 <div id="cxTex">
                	<table width="396">
                    	<tr>
                        	<td width="190"><input type="checkbox" name="CheckboxGroup1" value="caixa de seleção" id="CheckboxGroup1_0" />
                        	  Imobiliária 1</td>
                            <td width="190"><input type="checkbox" name="CheckboxGroup1_" value="caixa de seleção" id="CheckboxGroup1_" />
                            Imobiliária 5</td>
                        </tr>
                        <tr>
                        	<td width="190"><input type="checkbox" name="CheckboxGroup1" value="caixa de seleção" id="CheckboxGroup1_0" />
                       	    Imobiliária 2</td>
                            <td width="190"><input type="checkbox" name="CheckboxGroup1_" value="caixa de seleção" id="CheckboxGroup1_" />
                            Imobiliária 6</td>
                        </tr>
                        <tr>
                        	<td width="190"><input type="checkbox" name="CheckboxGroup1" value="caixa de seleção" id="CheckboxGroup1_0" />
                       	    Imobiliária 3</td>
                            <td width="190"><input type="checkbox" name="CheckboxGroup1_" value="caixa de seleção" id="CheckboxGroup1_" />
                            Imobiliária 7</td>
                        </tr>
                        <tr>
                        	<td width="190"><input type="checkbox" name="CheckboxGroup1" value="caixa de seleção" id="CheckboxGroup1_0" />
                       	    Imobiliária 4</td>
                            <td width="190"><input type="checkbox" name="CheckboxGroup1_" value="caixa de seleção" id="CheckboxGroup1_" />
                            Imobiliária 8</td>
                        </tr>
                       
                    </table>
              </div>-->

						<div id="cxTit">Áreas</div>
						<div id="cxTex">
							<table width="700">
								<tr>
									<td width="180" align="right">Área Construída:</td>
									<td width="520"><input class="textfield areaConstruida"
										name="areaConstruida" type="text" id="user5" /></td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>
								<tr>
									<td width="180" align="right">Área Total:</td>
									<td width="520"><input class="textfield areaTotal"
										name="areaTotal" type="text" id="user2" /></td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>
								<tr>
									<td width="180" align="right">Comprimento:</td>
									<td width="520"><input class="textfield comprimento"
										name="comprimento" type="text" id="user2" /></td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>
								<tr>
									<td width="180" align="right">Largura:</td>
									<td width="520"><input class="textfield largura" name="largura"
										type="text" id="user2" /></td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>
								<tr>
									<td width="180" align="right">Edicula:</td>
									<td width="520"><input class="textfield edicula" name="edicula"
										type="text" id="user2" /></td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>
							</table>
						</div>

						<div id="cxTit">Valores</div>
						<div id="cxTex">
							<table width="700">
								<tr>
									<td width="180" align="right">* Preço:</td>
									<td width="50"><input class="textfield required price"
										name="price" /></td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>
								<tr>
									<td width="180" align="right">Valor Condomínio:</td>
									<td width="520"><input class="textfield valorCondominio"
										name="valorCondominio" type="text" id="user2" /></td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>
								<tr>
									<td width="180" align="right">Valor do IPTU:</td>
									<td width="520"><input class="textfield valorIPTU"
										name="valorIPTU" type="text" id="user2" /></td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>

							</table>
						</div>

						<div id="cxTit">Fotos</div>
						<div id="cxTex">
							<table width="700">
								<tr>
									<td width="180" align="right">Foto 1:</td>
									<td width="400"><input type="file" class="submit file"
										name="property_photo[]" /></td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>
								<tr>
									<td width="180" align="right">Foto 2:</td>
									<td width="400"><input type="file" class="submit file"
										name="property_photo[]" /></td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>
								<tr>
									<td width="180" align="right">Foto 3:</td>
									<td width="400"><input type="file" class="submit file"
										name="property_photo[]" /></td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>
								<tr>
									<td width="180" align="right">Foto 4:</td>
									<td width="400"><input type="file" class="submit file"
										name="property_photo[]" /></td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>
								<tr>
									<td width="180" align="right">Foto 5:</td>
									<td width="400"><input type="file" class="submit file"
										name="property_photo[]" /></td>
								</tr>
								<tr>
									<td colspan="2" height="5"><b>Tamanho da imagem:</b> Mínimo
										800x800 máximo 2000x1800</td>
								</tr>

								<tr>
									<td width="180" align="right"><input type="hidden"
										name="updade" value="false" /> <input type="submit"
										name="Enviar" id="Enviar" value="Salvar" /></td>
									<td width="520"><input type="reset" name="Cancelar"
										id="Cancelar" value="Cancelar" /> <input type="button"
										name="cancel" value="Voltar" onclick="history.back()" /></td>
								</tr>
							</table>
						</div>
					</form>
					<!-- /FORM -->
				</div>
			</div>
			<!-- CONTEUDO -->

		</div>
		<!-- /MAIN CONTEUDO -->


		<!-- FOOTER -->
    <?php
				require_once PATH_INCLUDES_ADMIN . 'footer.html';
				?>
    <!-- /FOOTER -->



	</div>
	<!-- /GERAL -->

	<!-- End conteiner -->
	<!-- JS Content and JSLibary -->
	<script type="text/javascript" src="js/jquery-1.8.3.js"></script>

	<!-- includes for multiple select -->
	<script type="text/javascript" src="js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/jquery.multiselect.min.js"></script>
	<!-- end multiple select include -->

	<!-- includes for maskedinput -->
	<script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>
	<!-- end maskedinput includes -->

	<!-- includes for jquery validate and their messages -->
	<script type="text/javascript" src="js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="js/messages_pt_BR.js"></script>
	<!-- end jquery validate and their messages includes -->

	<!-- includes for jquery price -->
	<script type="text/javascript" src="js/jquery.price_format.1.7.min.js"></script>
	<!-- end jquery price include -->

	<script type="text/javascript">
      $(document).ready(function(){
        $('.price').priceFormat({
              prefix: '',
              centsSeparator: '.',
              thousandsSeparator: ','
        });
        $('.areaConstruida').priceFormat({
              prefix: '',
              centsSeparator: '.',
              thousandsSeparator: ','
        });
        $('.areaTotal').priceFormat({
              prefix: '',
              centsSeparator: '.',
              thousandsSeparator: ','
        });
        $('.comprimento').priceFormat({
              prefix: '',
              centsSeparator: '.',
              thousandsSeparator: ','
        });
        $('.largura').priceFormat({
              prefix: '',
              centsSeparator: '.',
              thousandsSeparator: ','
        });
        $('.edicula').priceFormat({
              prefix: '',
              centsSeparator: '.',
              thousandsSeparator: ','
        });
        $('.valorCondominio').priceFormat({
              prefix: '',
              centsSeparator: '.',
              thousandsSeparator: ','
        });
        $('.valorIPTU').priceFormat({
              prefix: '',
              centsSeparator: '.',
              thousandsSeparator: ','
        });
        

        $('input[value=1]', '#status').attr('checked', 'checked');
        $("#cep").mask('99999-999');
        $('#contactPhone').mask('(99) 9999.9999');
        $("#frmCadProperty").validate();
        $('#temporadaConfig').css({'display' : 'none'});
        $("#daysOfWeek").multiselect({
          noneSelectedText: 'Dias da semana',
          selectedText: 'Dias da semana',
          checkAllText: 'Marque todos',
          uncheckAllText: 'Desmarque todos'
        });
        $("#monthOfYear").multiselect({
          noneSelectedText: 'Meses do ano',
          selectedText: 'Meses do ano',
          checkAllText: 'Marque todos',
          uncheckAllText: 'Desmarque todos'
        });
        
        $('input[value=1], input[value=2]', '#status').change(function(){
          if(!$('input[value=3]', '#status').is(':checked')){
            $('#temporadaConfig').css({'display' : 'none'});
          }
        });
        
        $('input[value=3]', '#status').change(function(){
          if($('input[value=3]', '#status').is(':checked')){
            $('#temporadaConfig').css({'display' : 'inline-block'});  
          }
        });
      });
    </script>


</body>

</html>