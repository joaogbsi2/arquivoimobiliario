<?php
$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_ENTITIES . 'State.class.php';
require_once PATH_CONTROLLER . 'StateController.class.php';

// buscar todos os estados cadastrados
$stateController = new StateController ();
$states = $stateController->getAllStates ();

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="shortcut icon" href="images/arquiLogo.png">
<title>Arquivo Imobiliário</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>

<script type="text/javascript">

    /*
     * create a function that will go ask if the user want really delete the user 
     */
    function doYouWantDelete(){
        
        var answer = window.confirm("Deseja realmente remover esse estado?");
        
        if(answer)
            return true;
        
        return false;
    }
</script>
        <body>

	<!-- GERAL -->
	<div id="geral">


		<!-- TOPO -->
    <?php
				require_once PATH_INCLUDES_ADMIN . 'header.php';
				?>
    <!-- /TOPO -->


		<!-- MAIN CONTEUDO -->
		<div id="mainConteudo">

			<!-- CONTEUDO -->
			<div id="conteudo">

				<!-- ADMIN COL1 -->
                <?php
																require_once PATH_INCLUDES_ADMIN . 'right_colum.html';
																?>
                <!-- /ADMIN COL1 -->

				<div id="adm_COL2">
                <?php
																
																/*
																 * Caso tenha feito a pesquisa pelo nome da cidade
																 */
																if (isset ( $_POST ['name_state'] ) && trim ( $_POST ['name_state'] ) != NULL && trim ( $_POST ['name_state'] ) != "") {
																	$states = $stateController->getAllStates ( $_POST ['name_state'], LIMIT_CITY, "id DESC" );
																}
																?>
                <div id="Tit">Estados</div>

					
					<form id="form1" name="form1" method="post" action="">
						<table width="515">
							<tr>
								<td align="right">Estado:</td>
								<td><input name="name_estado" type="text" /> <input type="submit"
									name="Enviar" value="Buscar" /></td>
							</tr>
						</table>
					</form>

					<div id="Tit">Cadastrar novo estado</div>
					<form class="validate" id="frmCadCity"
						action="action/cadState.action.php" method="post"
						enctype="multipart/form-data">

						<div id="cxBusca">
							<table width="527">
								<tr>
									<td width="103" align="right">Estado:</td>
									<td width="400"><input name="state" type="text" class="campo1"
										id="user" /></td>
								</tr>
								<tr><tr>
									<td width="103" align="right">UF:</td>
									<td width="400"><input name="uf" type="text" class="campo1"
										id="user" /></td>
								</tr>
								</tr>
								<tr>
									<td></td>
									<td align="right"><input type="hidden" name="action"
										value="cadastre"> <input type="submit" name="Enviar"
										id="Enviar" value="Cadastrar" /></td>
								</tr>
							</table>
						</div>

					</form>
					<div id="Tit">Estados Cadastradas</div>

					<div id="cxLisImov">
						<table width="733" cellspacing="5">
							<tr>
								<td width="361" align="center"><strong>Estados:</strong></td>
								<td width="360" align="center" colspan="2"><strong>Ações</strong></td>
							</tr>
                        
                        <?php
																								if (! is_null ( $states )) {
																									foreach ( $states as $iterState ) {
																										?>
                                    <tr>
								<td align="left"><?php echo $iterState->getName() ; ?></td>
								<td align="right">
									<form name="update_state" method="post"
										action="action/cadState.action.php">
										<input type="hidden" name="action" value="update"> <input
											type="hidden" name="state_id"
											value="<?php echo intval($iterState->getId()); ?>"> <input
											type="submit" name="update" value="Ver mais">
									</form>
								</td>
								<td align="left">
									<form name="delete_city" method="post"
										action="action/cadState.action.php">
										<input type="hidden" name="action" value="delete"> <input
											type="hidden" name="state_id"
											value="<?php echo intval($iterState->getId()); ?>"> <input
											type="submit" name="delete" value="Excluir"
											onclick="return doYouWantDelete();">
									</form>
								</td>
							</tr>
                        <?php
																									}
																								}
																								?>
                        
                    </table>
					</div>

				</div>

			</div>
			<!-- CONTEUDO -->

		</div>
		<!-- /MAIN CONTEUDO -->


		<!-- FOOTER -->
			<<?php
				require_once PATH_INCLUDES_ADMIN . 'footer.html';
				?>
		<!-- /FOOTER -->



	</div>
	<!-- /GERAL -->


</body>

</html>