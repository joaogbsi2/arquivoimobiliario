<?php

/*
 * INCLUDE SECTOR
 */

// $_SERVER['DOCUMENT_ROOT'] = "/home/arqui937/public_html/";
// define ("PATH", $_SERVER['DOCUMENT_ROOT']);

// include the file of configuration
// require_once '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_INCLUDES_ADMIN . 'session.php';
require_once PATH_CONTROLLER . 'AdminServicesController.class.php';
require_once PATH_MODEL_ENTITIES . 'Advertising.class.php';
require_once PATH_MODEL_ENTITIES . 'MainAdvertising.class.php';
require_once PATH_MODEL_ENTITIES . 'Parceiro.class.php';

// Initialize the controller
$adminServicesController = new AdminServicesController ();

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon" href="images/arquiLogo.png">
<title>Arquivo Imobiliário</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>

<body>
    
    
    <?php
				
				if (isset ( $_GET ['error'] )) {
					if ($_GET ['error'] == "size") {
						?>
                <script type="text/javascript">
                    window.alert("Erro ao tentar cadastrar imagem.\nPor favor, verifique o limite de largura e altura da imagem!");
                </script>
        <?php
					}
				}
				
				?>
    
<!-- GERAL -->
	<div id="geral">


		<!-- TOPO -->
    <?php
				require_once PATH_INCLUDES_ADMIN . 'header.php';
				?>
    <!-- /TOPO -->


		<!-- MAIN CONTEUDO -->
		<div id="mainConteudo">

			<!-- CONTEUDO -->
			<div id="conteudo">

				<!-- ADMIN COL1 -->
            <?php
												require_once PATH_INCLUDES_ADMIN . 'right_colum.html';
												?>
            <!-- /ADMIN COL1 -->

				<!-- ADMIN COL2 -->
				<div id="adm_COL2">

					<!-- Definir as propagandas e suas posições -->
					<!-- Insert the form to cadastre the photos of advertising (propaganda) -->
					<form id="frmCadastrePhoto" method="post"
						action="action/adminServices.action.php"
						enctype="multipart/form-data" accept-charset="UTF-8">
						<div id="Tit">Propagandas menores (da coluna lateral)</div>
                <?php
																$advertisings = $adminServicesController->getAllAdvertisings ();
																foreach ( $advertisings as $iter ) {
																	$photo = $iter->getPhoto ();
																	$path = URL_ADVERTISINGS_PHOTOS . basename ( $photo );
																	?>
                        <div class="advertisingBox">
							<img class="tamanho-foto" src="<?php echo $path; ?>" /> <span>Posição: <?php echo $iter->getPosition(); ?></span>
						</div>
                <?php
																}
																?>
                
                <table width="130">
							<tr>
								<td width="30" align="justify" class="texto">Posição:</td>
								<td width="80"><select name="position">
                            <?php
																												for($i = 1; $i <= 15; $i ++) {
																													?>
                                    <option value="<?php echo $i ?>"><?php echo $i ?></option>
                            <?php
																												}
																												?>
                        </select></td>
								<td width="10"></td>
							</tr>

							<tr>
								<td align="right" class="texto">Foto:</td>
								<td><input name="photo" type="file" class="campo1" id="photo" /></td>
								<!-- This field define what the admin user is making -->
								<input type="hidden" name="action" value="advertising" />
								<td><input type="submit" value="Atualizar" /></td>
							</tr>

						</table>
						<b>Tamanho da imagem:</b> Mínimo: 300x300 e máximo 600x600
					</form>

					<br />

					<!-- Propagandas principais (as que ficarão rotacionando na página inicial) -->
					<!-- Definir as propagandas e suas posições -->
					<!-- Insert the form to cadastre the photos of advertising (propaganda) -->
					<div id="Tit">Propagandas principais</div>
                <?php
																$mainAdvertisings = $adminServicesController->getAllMainAdvertisings ();
																foreach ( $mainAdvertisings as $iter ) {
																	$photo = $iter->getPhoto ();
																	$path = URL_MAIN_ADVERTISINGS_PHOTOS . basename ( $photo );
																	?>
                        <div class="advertisingBoxMainAdvertising">
						<img class="tamanho-foto-main-advertising"
							src="<?php echo $path; ?>" /> <span>Posição: <?php echo $iter->getPosition(); ?></span>
					</div>
                <?php
																}
																?>
            <form id="frmCadastreMainAdvertising" method="post"
						action="action/adminServices.action.php"
						enctype="multipart/form-data" accept-charset="UTF-8">
						<table width="130">
							<tr>
								<td width="30" align="justify" class="texto">Posição:</td>
								<td width="80"><select name="position">
                            <?php
																												for($i = 1; $i <= 5; $i ++) {
																													?>
                                    <option value="<?php echo $i ?>"><?php echo $i ?></option>
                            <?php
																												}
																												?>
                        </select></td>
								<td width="10"></td>
							</tr>
							<tr>
								<td align="right" class="texto">Foto:</td>
								<td><input name="photo" type="file" class="campo1" id="photo" /></td>
							</tr>
							<!-- another fields -->
							<tr>
								<td align="right" class="texto">Link:</td>
								<td><input name="link" type="text" class="campo1" id="link" /></td>
							</tr>
							<tr>
								<td align="right" class="texto">Título:</td>
								<td><input name="title" type="text" maxlength="30"
									class="campo1" id="title" /></td>
							</tr>
							<tr>
								<td align="right" class="texto">Pequena descrição:</td>
								<td><input name="subtitle" type="text" maxlength="80"
									class="campo1" id="subtitle" /></td>
							</tr>
							<tr>
								<!-- This field define what the admin user is making -->
								<input type="hidden" name="action" value="mainAdvertising" />
								<td></td>
								<td><input type="submit" value="Atualizar" /></td>
							</tr>
						</table>
						<b>Tamanho da imagem:</b> Mínimo: 1200x1000 e máximo 2000x1400
					</form>

					<br />

					<!--
            Formulário para cadastro das propagandas laterais à esquerda da página
            Propagandas de parceiros
            -->
					<div id="Tit">Propagandas de parceiros</div>
                <?php
																$parceiros = $adminServicesController->getAllParceiros ();
																foreach ( $parceiros as $iter ) {
																	$photo = $iter->getPhoto ();
																	$path = URL_PARCEIRO_PHOTOS . basename ( $photo );
																	?>
                        <div class="advertisingBox">
						<img class="tamanho-foto" src="<?php echo $path; ?>" /> <span>Posição: <?php echo $iter->getPosition(); ?></span>
					</div>
                <?php
																}
																?>
            
            <form id="frmCadastreParceiros" method="post"
						action="action/adminServices.action.php"
						enctype="multipart/form-data" accept-charset="UTF-8">
						<table width="130">
							<tr>
								<td width="30" align="justify" class="texto">Posição:</td>
								<td width="80"><select name="position">
                            <?php
																												for($i = 1; $i <= 4; $i ++) {
																													?>
                                    <option value="<?php echo $i ?>"><?php echo $i ?></option>
                            <?php
																												}
																												?>
                        </select></td>
								<td width="10"></td>
							</tr>

							<tr>
								<td align="right" class="texto">Foto:</td>
								<td><input name="photo" type="file" class="campo1" id="photo" /></td>
								<!-- This field define what the admin user is making -->
								<input type="hidden" name="action" value="parceiro" />
								<td><input type="submit" value="Atualizar" /></td>
							</tr>

						</table>
						<b>Tamanho da imagem:</b> Mínimo: 300x300 e máximo 500x500
					</form>

				</div>
				<!-- CONTEUDO -->

			</div>
			<!-- /ADMIN COL2 -->

		</div>
		<!-- /MAIN CONTEUDO -->


		<!-- FOOTER -->
    <?php
				require_once PATH_INCLUDES_ADMIN . 'footer.html';
				?>
    <!-- /FOOTER -->



	</div>
	<!-- /GERAL -->


</body>

</html>