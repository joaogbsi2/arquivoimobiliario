<?php

/*
 * INCLUDE SECTOR
 */

// $_SERVER['DOCUMENT_ROOT'] = "/home/arqui937/public_html/";
// define ("PATH", $_SERVER['DOCUMENT_ROOT']);

// include the file of configuration
// require_once '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_INCLUDES_ADMIN . 'session.php';

// require_once PATH_ACTION. 'cadUser.action.php';
require_once PATH_MODEL_ENTITIES . 'User.class.php';
require_once PATH_CONTROLLER . 'ServiceController.class.php';
require_once PATH_MODEL_ENTITIES . 'Service.class.php';

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon" href="images/arquiLogo.png">
<title>Arquivo Imobiliário</title>
<link href="style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/jquery.multiselect.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />


</head>

<body>
    
   <?php
			
			if (isset ( $_GET ['error'] )) {
				if ($_GET ['error'] == "size") {
					?>
                <script type="text/javascript">
                    window.alert("Erro ao tentar cadastrar imagem.\nPor favor, verifique o limite de largura e altura da imagem!");
                </script>
        <?php
				}
			}
			
			?>

<!-- GERAL -->
	<div id="geral">


		<!-- TOPO -->
    <?php
				require_once PATH_INCLUDES_ADMIN . 'header.php';
				?>
	<!-- /TOPO -->


		<!-- MAIN CONTEUDO -->
		<div id="mainConteudo">

			<!-- CONTEUDO -->
			<div id="conteudo">

				<!-- ADMIN COL1 -->
            <?php
												require_once PATH_INCLUDES_ADMIN . 'right_colum.html';
												?>
            <!-- /ADMIN COL1 -->

				<!-- AREA DE CADASTRO -->
				<div id="adm_COL2">
					<form method="post" action="action/recebeServico.php"
						enctype="multipart/form-data" accept-charset="UTF-8">
						<div id="cxTit">Enviar por arquivo</div>
						<div id="cxTex">
							<table width="700">
								<tr>
									<td>
									
									<td width="180" align="right">Escolher arquivo:</td>
									<td><input type="file" name="arquivo" /></td>
									<td><input type="submit" value="Enviar" /></td>
								</tr>
							</table>
						</div>
					</form>
					<!-- FORM -->
					<form class="validate" id="frmCadUser"
						action="action/cadUser.action.php" method="post"
						enctype="multipart/form-data">

						<div id="Tit">Cadastro de Usuário/Serviço</div>
						<div id="cxTex">
							<table width="700">
								<tr>
									<td width="180" align="right">* Nome:</td>
									<td width="50"><input class="textfield" id="name" name="name" /></td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>

								<tr>
									<td width="180" align="right">Celular:</td>
									<td width="50"><input class="textfield" id="mobile_phone"
										name="mobile_phone" /></td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>

								<tr>
									<td width="180" align="right">Telefone:</td>
									<td width="50"><input class="textfield" id="phone" name="phone" /></td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>

								<tr>
									<td width="180" align="right">Foto:</td>
									<td width="50"><input type="file" class="file" id="photo"
										name="photo" /></td>
								</tr>
								<tr>
									<td colspan="2" height="5" align="right"><b>Tamanho da imagem:
									</b>Mínimo 600x600 máximo 1200x1000</td>
								</tr>

								<tr>
									<td width="180" align="right">E-mail:</td>
									<td width="50"><input class="textfield" id="email" name="email" /></td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>

								<!--                            <tr>
                                <td width="180" align="right">* Senha:</td>
                                <td width="50"><input type="password" class="textfield" id="passwd1" name="passwd1" /></td>
                            </tr>
                            <tr>
                                <td colspan="2" height="5"></td>
                            </tr>

                            <tr>
                                <td width="180" align="right">* Repita a senha:</td>
                                <td width="50"><input type="password" class="textfield" id="passwd2" name="passwd2" /></td>
                            </tr>
                            <tr>
                                <td colspan="2" height="5"></td>
                            </tr>-->
							</table>
						</div>
						<div id="cxTit">Prestação de serviço</div>
						<div id="cxTex">
							<table width="700">
								<!-- Edilson Justiniano alterou essa parte aqui. Eu add esse campo aqui nesse formulário
                                     para que o usuário selecione tudo em uma tela apenas e faça esse cadastro dessa 
                                     forma, acredito que assim ficará mais simples para realizar a edição 
                                -->

								<tr>
									<td width="180" align="right">Você é um prestador de serviços?</td>
									<td width="50">SIM<input type="radio"
										name="youAreServiceProvider" class="radio"
										id="iAmServiceProvider" value="yes" /> NÃO<input type="radio"
										name="youAreServiceProvider" class="radio"
										id="iAmServiceProvider" value="no" checked="true" />
									</td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>
							</table>
							<!-- Descobrir o por quê do campo abaixo
                                 Não sei irei usá-lo na edição do usuário, dai 
                                 pego o que o usuário era antes caso tenha alterado
                                 era prestador de serviço e não é mais eu então poderei
                                 saber para remover todos o user_services que esse usuário
                                 possuia anteriormente
                            -->
							<!--<input type="hidden" name="choiceNo" id="choiceNo" value="no" />-->

							<div id="hidden">
                              <?php
																														
																														// Get all services.
																														$serviceController = new ServiceController ();
																														$services = $serviceController->getAllServices ();
																														
																														if (! is_null ( $services )) {
																															foreach ( $services as $service ) {
																																?>
                                      <label
									for="<?php echo $service->getServiceDescription(); ?>"> <input
									type='checkbox' class='checkbox' name='serviceProvider[]'
									id='serviceProvider' value="<?php echo $service->getId(); ?>" /><?php echo $service->getServiceDescription(); ?>
                                          <br />
								</label>
                                  <?php
																																// print "<label for='" . $service->getServiceDescription() . "'>\r\n";
																																// print "<input type='checkbox' class='checkbox' name='serviceProvider[]' id='serviceProvider' value='" . $service->getId() . "' />" . $service->getServiceDescription() . "\r\n";
																																// print "</label>\r\n";
																															}
																														} else {
																															?>
                                  Nenhuma categoria de serviços disponível no momento!
                                <?php
																															echo "<span class='error'>Nenhuma categoria de serviços disponível no momento</span>";
																														}
																														?>
                            </div>

							<table width="700">
								<!-- SUBMIT BUTTONS -->
								<tr>
									<td width="280" align="right"><input type="hidden"
										name="action" value="cadastre" /> <input type="submit"
										name="Enviar" id="Enviar" value="Salvar" /></td>
									<td width="420"><input type="reset" name="Cancelar"
										id="Cancelar" value="Cancelar" /> <input type="button"
										name="voltar" id="Cancelar" value="Voltar"
										onclick="history.back()" /></td>
								</tr>
							</table>
						</div>
				
				</div>
				<!-- /ADM_COL2 -->
				</form>
				<!-- /FORM -->
			</div>
		</div>
		<!-- CONTEUDO -->

	</div>
	<!-- /MAIN CONTEUDO -->


	<!-- FOOTER -->
    <?php
				require_once PATH_INCLUDES_ADMIN . 'footer.html';
				?>
    <!-- /FOOTER -->



	</div>
	<!-- /GERAL -->



	<!-- Plugins JQuery and JS content -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"
		type="text/javascript"></script>
	<script
		src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"
		type="text/javascript"></script>
	<script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
	<script src="js/jquery.jget.js" type="text/javascript"></script>
	<script type="text/javascript">
      $(document).ready(function(){
        // Valida form
        $("#frmCadUser").validate({
          // Define as regras
          rules:{
            name:{
              required: true, minlength: 2
            },
            passwd1:{
              required: true
            },
            passwd2:{
              required: true, equalTo: "#passwd1"
            }
          },
          // Define as mensagens de erro para cada regra
          messages:{
            name:{
              required: "Digite o seu nome",
              minlength: "O seu nome deve conter, no mínimo, 2 caracteres"
            },
            passwd1:{
              required: "Digite uma senha"
            },
            passwd2:{
              required: "Confirme a senha digitada", equalTo: "As senhas não são idênticas."
            }
          }
        });
        //Input mask. 
        $('#phone').mask('(99) 9999.9999');
        $('#mobile_phone').mask('(99) 9999.9999');
        $("#hidden").hide(1000);
        
//        //Case user choice 'no'
//        $(".values").click(function(){
//          if($(":radio[name='youAreServiceProvider']").is(':checked')){
//            if($(":radio[name='youAreServiceProvider']:checked").val() == 'no'){
//              var userId = $.jget['user'];
//              $("#choiceNo").attr('value','no');
//              location.href = "confirmRecordedUser.php?user=" + userId;
//            }
//            else {
//              $("#choiceNo").attr('value','yes');
//            }
//          }
//        });

        //Show the options for register.
        $(":radio[name='youAreServiceProvider']").click(function(){
          if($(this).val() == 'yes'){
            $("#hidden").show(1000);
          }
          else{
            $("#hidden").hide(1000);
          }
        });
      });
    </script>


</body>

</html>