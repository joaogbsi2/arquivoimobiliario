<?php

/*
 * INCLUDE SECTOR
 */

// $_SERVER['DOCUMENT_ROOT'] = "/home/arqui937/public_html/";
// define ("PATH", $_SERVER['DOCUMENT_ROOT']);

// include the file of configuration
// require_once '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_INCLUDES_ADMIN . 'session.php';
require_once PATH_MODEL_ENTITIES . 'State.class.php';
require_once PATH_CONTROLLER . 'StateController.class.php';
require_once PATH_CONTROLLER . 'CityController.class.php';
require_once PATH_MODEL_ENTITIES . 'City.class.php';

define ( "LIMIT_CITY", 20 );

$cityController = new CityController ();
$cities = $cityController->getAllCities ( LIMIT_CITY, "id DESC" );

$totalCities = $cityController->getCount ();

// $storeController = new StoreController();
// $stores =$storeController->findAll(LIMIT_STORE, "id DESC");

// buscar todos os estados cadastrados
$stateController = new StateController ();
$states = $stateController->getAllStates ();

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="shortcut icon" href="images/arquiLogo.png">
<title>Arquivo Imobiliário</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>

<script type="text/javascript">

    /*
     * create a function that will go ask if the user want really delete the user 
     */
    function doYouWantDelete(){
        
        var answer = window.confirm("Deseja realmente remover essa cidade?");
        
        if(answer)
            return true;
        
        return false;
    }
</script>

<body>

	<!-- GERAL -->
	<div id="geral">


		<!-- TOPO -->
    <?php
				require_once PATH_INCLUDES_ADMIN . 'header.php';
				?>
    <!-- /TOPO -->


		<!-- MAIN CONTEUDO -->
		<div id="mainConteudo">

			<!-- CONTEUDO -->
			<div id="conteudo">

				<!-- ADMIN COL1 -->
                <?php
																require_once PATH_INCLUDES_ADMIN . 'right_colum.html';
																?>
                <!-- /ADMIN COL1 -->

				<div id="adm_COL2">
                <?php
																
																/*
																 * Caso tenha feito a pesquisa pelo nome da cidade
																 */
																if (isset ( $_POST ['name_city'] ) && trim ( $_POST ['name_city'] ) != NULL && trim ( $_POST ['name_city'] ) != "") {
																	$cities = $cityController->getCitiesByName ( $_POST ['name_city'], LIMIT_CITY, "id DESC" );
																}
																?>
                <div id="Tit">Cidades</div>

					<div id="cxInfo">
						<div id="fotoInfo">
							<img src="images/imgDados.jpg" />
						</div>
						<!--<div id="txtInfo">Página 1 de 1</div>-->

						<div id="txtInfo">Exibindo <?php echo count($cities); ?> registros de um total de <?php echo $totalCities; ?></div>
						<!--                    <div id="txtInfo">Iniciando em 1, terminando em 2</div>-->
					</div>

					<form id="form1" name="form1" method="post" action="">
						<table width="515">
							<tr>
								<td align="right">Cidade:</td>
								<td><input name="name_city" type="text" /> <input type="submit"
									name="Enviar" value="Buscar" /></td>
							</tr>
						</table>
					</form>

					<div id="Tit">Cadastrar nova cidade</div>
					<form class="validate" id="frmCadCity"
						action="action/cadCity.action.php" method="post"
						enctype="multipart/form-data">

						<div id="cxBusca">
							<table width="527">
								<tr>
									<td width="103" align="right">Cidade:</td>
									<td width="400"><input name="city" type="text" class="campo1"
										id="user" /></td>
								</tr>
								<tr>
									<td width="103" align="right">Estado:</td>
									<td width="400"><select name="state" class="campo1" id
										title="state">
                                    <?php
																																				
																																				foreach ( $states as $iterState ) {
																																					?>
                                    <option
												value="<?php echo $iterState->getId(); ?>"><?php echo $iterState->getName(); ?></option>
                                    <?php
																																				}
																																				?>
                                </select></td>
								</tr>
								<tr>
									<td></td>
									<td align="right"><input type="hidden" name="action"
										value="cadastre"> <input type="submit" name="Enviar"
										id="Enviar" value="Cadastrar" /></td>
								</tr>
							</table>
						</div>

					</form>
					<div id="Tit">Cidades Cadastradas</div>

					<div id="cxLisImov">
						<table width="733" cellspacing="5">
							<tr>
								<td width="361" align="center"><strong>Cidades:</strong></td>
								<td width="360" align="center" colspan="2"><strong>Ações</strong></td>
							</tr>
                        
                        <?php
																								if (! is_null ( $cities )) {
																									foreach ( $cities as $iterCity ) {
																										?>
                                    <tr>
								<td align="left"><?php echo $iterCity->getName() . "  (". $iterCity->getState()->getUf().")"; ?></td>
								<td align="right">
									<form name="update_city" method="post"
										action="action/cadCity.action.php">
										<input type="hidden" name="action" value="update"> <input
											type="hidden" name="city_id"
											value="<?php echo intval($iterCity->getId()); ?>"> <input
											type="submit" name="update" value="Ver mais">
									</form>
								</td>
								<td align="left">
									<form name="delete_city" method="post"
										action="action/cadCity.action.php">
										<input type="hidden" name="action" value="delete"> <input
											type="hidden" name="city_id"
											value="<?php echo intval($iterCity->getId()); ?>"> <input
											type="submit" name="delete" value="Excluir"
											onclick="return doYouWantDelete();">
									</form>
								</td>
							</tr>
                        <?php
																									}
																								}
																								?>
                        
                    </table>
					</div>

				</div>

			</div>
			<!-- CONTEUDO -->

		</div>
		<!-- /MAIN CONTEUDO -->


		<!-- FOOTER -->
			<<?php
				require_once PATH_INCLUDES_ADMIN . 'footer.html';
				?>
		<!-- /FOOTER -->



	</div>
	<!-- /GERAL -->


</body>

</html>