<?php

/*
 * INCLUDE SECTOR
 */

// $_SERVER['DOCUMENT_ROOT'] = "/home/arqui937/public_html/";
// define ("PATH", $_SERVER['DOCUMENT_ROOT']);

// include the file of configuration
// require_once '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_INCLUDES_ADMIN . 'session.php';

require_once PATH_CONTROLLER . 'DetailController.class.php';
require_once PATH_MODEL_ENTITIES . 'Detail.class.php';

define ( "LIMIT_DETAIL", 20 );

$detailController = new DetailController ();
$details = $detailController->getAll ( LIMIT_DETAIL, "id DESC" );

$totalDetails = $detailController->getCount ();

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="shortcut icon" href="images/arquiLogo.png">
<title>Arquivo Imobiliário</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>

<script type="text/javascript">

    /*
     * create a function that will go ask if the user want really delete the user 
     */
    function doYouWantDelete(){
        
        var answer = window.confirm("Deseja realmente remover esse detalhe?");
        
        if(answer)
            return true;
        
        return false;
    }
</script>

<body>

	<!-- GERAL -->
	<div id="geral">


		<!-- TOPO -->
    <?php
				require_once PATH_INCLUDES_ADMIN . 'header.php';
				?>
    <!-- /TOPO -->


		<!-- MAIN CONTEUDO -->
		<div id="mainConteudo">

			<!-- CONTEUDO -->
			<div id="conteudo">

				<!-- ADMIN COL1 -->
                <?php
																require_once PATH_INCLUDES_ADMIN . 'right_colum.html';
																?>
                <!-- /ADMIN COL1 -->

				<div id="adm_COL2">
                <?php
																
																/*
																 * Caso tenha feito a pesquisa pelo nome da cidade
																 */
																if (isset ( $_POST ['detail'] ) && trim ( $_POST ['detail'] ) != NULL && trim ( $_POST ['detail'] ) != "") {
																	$details = $detailController->getDetailsByName ( $_POST ['detail'], LIMIT_DETAIL, "id DESC" );
																}
																?>
                <div id="Tit">Detalhes</div>

					<div id="cxInfo">
						<div id="fotoInfo">
							<img src="images/imgDados.jpg" />
						</div>
						<!--<div id="txtInfo">Página 1 de 1</div>-->

						<div id="txtInfo">Exibindo <?php echo count($details); ?> registros de um total de <?php echo $totalDetails; ?></div>
						<!--                    <div id="txtInfo">Iniciando em 1, terminando em 2</div>-->
					</div>

					<form id="form1" name="form1" method="post" action="">
						<table width="515">
							<tr>
								<td align="right">Detalhe:</td>
								<td><input name="detail" type="text" /> <input type="submit"
									name="Enviar" value="Buscar" /></td>
							</tr>
						</table>
					</form>

					<div id="Tit">Cadastrar novo detalhe</div>
					<form class="validate" id="frmCadDetail"
						action="action/cadDetail.action.php" method="post">

						<div id="cxBusca">
							<table width="527">
								<tr>
									<td width="103" align="right">Detalhe:</td>
									<td width="400"><input name="detail" type="text" class="campo1"
										id="user" /></td>
								</tr>
								<tr>
									<td></td>
									<td align="right"><input type="hidden" name="action"
										value="cadastre"> <input type="submit" name="Enviar"
										id="Enviar" value="Cadastrar" /></td>
								</tr>
							</table>
						</div>

					</form>
					<div id="Tit">Detalhes Cadastrados</div>

					<div id="cxLisImov">
						<table width="733" cellspacing="5">
							<tr>
								<td width="361" align="center"><strong>Detalhes:</strong></td>
								<td width="360" align="center" colspan="2"><strong>Ações</strong></td>
							</tr>
                        
                        <?php
																								if (! is_null ( $details )) {
																									foreach ( $details as $iterDetail ) {
																										?>
                                    <tr>
								<td align="left"><?php echo $iterDetail->getName(); ?></td>
								<td align="right">
									<form name="update_detail" method="post"
										action="action/cadDetail.action.php">
										<input type="hidden" name="action" value="update"> <input
											type="hidden" name="detail_id"
											value="<?php echo intval($iterDetail->getId()); ?>"> <input
											type="submit" name="update" value="Atualizar">
									</form>
								</td>
								<td align="left">
									<form name="delete_detail" method="post"
										action="action/cadDetail.action.php">
										<input type="hidden" name="action" value="delete"> <input
											type="hidden" name="detail_id"
											value="<?php echo intval($iterDetail->getId()); ?>"> <input
											type="submit" name="delete" value="Excluir"
											onclick="return doYouWantDelete();">
									</form>
								</td>
							</tr>
                        <?php
																									}
																								}
																								?>
                        
                    </table>
					</div>

				</div>

			</div>
			<!-- CONTEUDO -->

		</div>
		<!-- /MAIN CONTEUDO -->


		<!-- FOOTER -->
		<div id="footer">
			<div id="conteudoFooter">
				<div id="ass">2013 © Arquivo Imobiliário. Todos os direitos
					reservados.</div>
			</div>
		</div>
		<!-- /FOOTER -->



	</div>
	<!-- /GERAL -->


</body>

</html>