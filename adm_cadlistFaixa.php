﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="images/arquiLogo.png">
<title>Arquivo Imobiliário</title>
<link href="style.css" rel="stylesheet" type="text/css" />

</head>

<body>

<!-- GERAL -->
<div id="geral">

	
    <!-- TOPO -->
    <div id="topo">
       <div id="topoCont">
       		<div id="logo"><img src="images/logotipo.png"/></div>
         <div id="cxAdm">Administrador</div>
            <div id="cxAdm2"><a href="alt_senha.php">Alterar Senha</a> |<a href="index.php"> Sair</a> |</div>
       </div>
    </div>
	<!-- /TOPO -->
    
    
	<!-- MAIN CONTEUDO -->
    <div id="mainConteudo">
    
    	<!-- CONTEUDO -->
        <div id="conteudo">
        
        	<div id="adm_COL1">
           	  <div id="menu">
               	  	<div id="cxTit">Painel de Controle</div>
          			<div id="cxDes"><a href="adm_index.php">-Início</a></div>
                    
                  <div id="cxTit">Imóveis</div>
                	<div id="cxDes"><a href="adm_listImovel.php">-Listagem</a><br />
               	    <a href="adm_cadImovel.php">-Cadastrar Imóveis</a></div>
                    
                    <div id="cxTit">Cadastro Auxiliares</div>
                	<div id="cxDes">
                	  <p><a href="adm_cadlistArea.php">-Áreas</a><br />
                	    <a href="adm_cadlistBairros.php">-Bairros</a><br />
                	    <a href="adm_cadlistCidades.php">-Cidades</a><br />
                	    <a href="adm_cadlistCarac.php">-Características</a><br />
                	    <a href="adm_cadlistDetalhes.php">-Detalhes</a><br />
                	    <a href="adm_cadlistFaixa.php">-Faixa de Valores</a><br />
                	    <a href="adm_cadlistOpera.php">-Operações</a><br />
               	      <a href="adm_cadlistProxi.php">-Proximidades</a></p>
                	  <p><a href="adm_cadlistTipo.php">-Tipos de Imóvel</a></p>
                	</div>
                    
      				<div id="cxTit">Cadastrar Sessões</div>
                	<div id="cxDes">
                	  <p><a href="adm_cadDeco.php">-Decoração</a><br />
                	    <a href="adm_cadDica.php">-Dicas</a><br />
                	    <a href="adm_cadNot.php">-Notícias</a><br />
                	    <a href="adm_cadTec.php">-Tecnologia</a><br />
                	    <a href="adm_cadParc.php">-Parceiros</a><br />
               	      <a href="adm_cadServ.php">-Serviços para sua casa </a><br />
               	      <a href="adm_cadImob.php">-Imobiliárias </a>                	  </p>
</div>
                    
                    <div id="cxTit">Usuário</div>
                	<div id="cxDes">-Alterar Senha<br />
                	  -Cadastrar Novo<br />
                	  -Sair do Sistema
                	</div>
                    
              </div>
            </div>
            
            <div id="adm_COL2">
            	<div id="Tit">Faixas de Valores</div>
                
				<div id="cxInfo">
                	<div id="fotoInfo"><img src="images/imgDados.jpg"/></div>
                    <div id="txtInfo">Página 1 de 1</div>
                    <div id="txtInfo">Exibindo 2 registros de um total de 2</div>
      				<div id="txtInfo">Iniciando em 1, terminando em 2</div>
             	</div>
                
                <div id="Tit">Cadastrar faixa</div>
                
                <div id="cxBusca">
                	<table width="527">
                    	<tr>
                        	<td width="103" align="right">Operação:</td>
                            <td width="400"><select name="tipo" class="campo1" id title="cidade">
                          <option value="label" selected="selected">Label</option></select></td>
                        </tr>
                        <tr>
                        	<td width="103" align="right">:</td>
                            <td width="400"><select name="tipo" class="campo1" id title="cidade">
                          <option value="label" selected="selected">Label</option></select></td>
                        </tr>
                        <tr>
                        	<td width="103" align="right">&nbsp;</td>
                            <td width="400"><input name="user" type="text" class="campo1" id="user"/></td>
                        </tr>
                        <tr>
                        	<td></td>
                            <td align="right"><form id="form1" name="form1" method="post" action="">
                              <input type="submit" name="Enviar" id="Enviar" value="Cadastrar" />
                          </form></td>
                        </tr>
                    </table>
              </div>
                
                <div id="Tit">Faixas cadastradas</div>
                
                <div id="cxLisImov">
                	<table width="733">
                    	<tr>
                        	<td width="155"><strong>Oepração</strong></td>
                       	  	<td width="334" align="center"><strong>Faixa de Valores</strong></td>
                            <td width="228" align="center"><strong>Ações</strong></td>
                        </tr>
                        <tr>
                        	<td>Locação</td>
                        	<td align="left">De 500,00 até 1.000,00</td>
                       	  	<td align="center"> editar | excluir</td>
                        </tr>
                        <tr>
                        	<td>Venda</td>
                        	<td align="left">De 10.000,00 até 100.000,00</td>
                       	  	<td align="center">editar | excluir</td>
                        </tr>
                    </table>
              </div>
                
            </div> 		
            
      	</div>  
        <!-- CONTEUDO -->
        
  	</div>
    <!-- /MAIN CONTEUDO -->	
    
         
    <!-- FOOTER -->
    <div id="footer">
    	<div id="conteudoFooter">
        	<div id="ass">2013 © Arquivo Imobiliário. Todos os direitos reservados.</div>
        </div>	
    </div>
    <!-- /FOOTER -->
     
     
     
</div>
<!-- /GERAL -->

    
</body>

</html>