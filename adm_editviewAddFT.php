﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="images/arquiLogo.png">
<title>Arquivo Imobiliário</title>
<link href="style.css" rel="stylesheet" type="text/css" />

</head>

<body>

<!-- GERAL -->
<div id="geral">

	
    <!-- TOPO -->
    <div id="topo">
       <div id="topoCont">
       		<div id="logo"><img src="images/logotipo.png"/></div>
         <div id="cxAdm">Administrador</div>
            <div id="cxAdm2"><a href="alt_senha.php">Alterar Senha</a> |<a href="index.php"> Sair</a> |</div>
       </div>
    </div>
	<!-- /TOPO -->
    
    
	<!-- MAIN CONTEUDO -->
    <div id="mainConteudo">
    
    	<!-- CONTEUDO -->
        <div id="conteudo">
        
        	<div id="adm_COL1">
           	  <div id="menu">
               	  	<div id="cxTit">Painel de Controle</div>
          			<div id="cxDes"><a href="adm_index.php">-Início</a></div>
                    
                  <div id="cxTit">Imóveis</div>
                	<div id="cxDes"><a href="adm_listImovel.php">-Listagem</a><br />
               	    <a href="adm_cadImovel.php">-Cadastrar Imóveis</a></div>
                    
                    <div id="cxTit">Cadastro Auxiliares</div>
                	<div id="cxDes">
                	  <p><a href="adm_cadlistArea.php">-Áreas</a><br />
                	    <a href="adm_cadlistBairros.php">-Bairros</a><br />
                	    <a href="adm_cadlistCidades.php">-Cidades</a><br />
                	    <a href="adm_cadlistCarac.php">-Características</a><br />
                	    <a href="adm_cadlistDetalhes.php">-Detalhes</a><br />
                	    <a href="adm_cadlistFaixa.php">-Faixa de Valores</a><br />
                	    <a href="adm_cadlistOpera.php">-Operações</a><br />
               	      <a href="adm_cadlistProxi.php">-Proximidades</a></p>
                	  <p><a href="adm_cadlistTipo.php">-Tipos de Imóvel</a></p>
                	</div>
                    
      				<div id="cxTit">Cadastrar Sessões</div>
                	<div id="cxDes">
                	  <p><a href="adm_cadDeco.php">-Decoração</a><br />
                	    <a href="adm_cadDica.php">-Dicas</a><br />
                	    <a href="adm_cadNot.php">-Notícias</a><br />
                	    <a href="adm_cadTec.php">-Tecnologia</a><br />
                	    <a href="adm_cadParc.php">-Parceiros</a><br />
               	      <a href="adm_cadServ.php">-Serviços para sua casa </a><br />
               	      <a href="adm_cadImob.php">-Imobiliárias </a>                	  </p>
				</div>
                    
                    <div id="cxTit">Usuário</div>
                	<div id="cxDes">-Alterar Senha<br />
                	  -Cadastrar Novo<br />
                	  -Sair do Sistema
                	</div>
                    
              </div>
            </div>
            
            <!-- COL 2 -->
            <div id="adm_COL2">
            	<div id="Tit">Fotos do imóvel</div>
                
                <div id="cxviewEdit"> <a href="adm_viewImov.php">&lt; voltar|</a></div>
                                
               	
                    <div id="cxTex">
<table width="700">
                            <tr>
                                <td width="130" align="right"><strong>Código do Imóvel:</strong></td>
                                <td width="3"></td>
                              <td width="556">0008</td>
                            </tr>
                            <tr>
                                <td width="130" align="right"><strong>Operação:</strong></td>
                                <td width="3"></td>
                              <td width="556">Venda</td>
                            </tr>
                            <tr>
                                <td width="130" align="right"><strong>Endereço:</strong></td>
                                <td width="3"></td>
                              <td width="556">(Nadir) GMA (endereço está oculto)</td>
                            </tr>
                            <tr>
                              	<td width="130" align="right"><strong>Bairro:</strong></td>
            <td width="3"></td>
                                <td width="556"> BELO HORIZONTE</td>
                            </tr>
                            <tr>
                                <td width="130" align="right"><strong>Cidade:</strong></td>
                                <td width="3"></td>
                              <td width="556">Pouso Alegre</td>
                            </tr>
                            <tr>
                                <td width="130" align="right"><strong>Visualizações:</strong></td>
                                <td width="3"></td>
                              <td width="556">2</td>
                            </tr>
                            <tr>
                                <td width="130" align="right"><strong>Cadastro:</strong></td>
                                <td width="3"></td>
                              <td width="556">18/05/2013 - 10:05:29</td>
                            </tr>
                        </table>
                  	</div>
                    
                    <div id="cxTit">Adicionar Fotos</div>
                    <div id="cxTex">
                    	<table width="605">
                            <tr>
                                <td width="188">Selecionar o arquivo:</td>
                                <td width="225"><input name="arquivo" type="file" /></td>
                            </tr>
                            <tr>
                                <td width="188" align="center">&nbsp;</td>
                                <td width="225" align="left"><input type="submit" class="btEnviar" value="Enviar" /></td>
                            </tr>
                        </table>
              </div>
                  
                  	<div id="cxTit">Fotos relacionadas</div>
                    <div id="cxTex">
<table>
                            <tr>
                                <td width="160"><img src="images/ftImov1.jpg" width="160" height="120" /></td>
                                <td width="160"><img src="images/ftImov1.jpg" width="160" height="120" /></td>
                                <td width="160"><img src="images/ftImov1.jpg" width="160" height="120" /></td>
                                <td width="160"><img src="images/ftImov1.jpg" width="160" height="120" /></td>
                            </tr>
                            <tr>
                                <td width="160" align="center"><strong>Excluir esta foto</strong></td>
                                <td width="160" align="center"><strong>Excluir esta foto</strong></td>
                                <td width="160" align="center"><strong>Excluir esta foto</strong></td>
                                <td width="160" align="center"><strong>Excluir esta foto</strong></td>
                            </tr>
                        </table>
                  </div>
                  
                       
                
          </div>	
           <!-- /COL 2 -->
           
      	</div>  
        <!-- CONTEUDO -->
        
  	</div>
    <!-- /MAIN CONTEUDO -->	
    
         
    <!-- FOOTER -->
    <div id="footer">
    	<div id="conteudoFooter">
        	<div id="ass">2013 © Arquivo Imobiliário. Todos os direitos reservados.</div>
        </div>	
    </div>
    <!-- /FOOTER -->
     
     
     
</div>
<!-- /GERAL -->

    
</body>

</html>