<?php

/*
 * INCLUDE SECTOR
 */

// include the file of configuration
// require_once '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_INCLUDES_ADMIN . 'session.php';
/*
 * The bellow file was needed because, will be shown all
 * clients cadastred for the root administrator and he
 * will be to set the its status
 */
require_once PATH_CONTROLLER . 'UserController.class.php';
require_once PATH_MODEL_ENTITIES . 'User.class.php';

/*
 * Function to shown the link to all letters
 */
function showLinkOfNames() {
	for($i = 65; $i < 91; $i ++) {
		?>
<a href="<?php echo URL_ADMIN_PAGE . "?letter=". chr($i) ?>"><?php echo chr($i) ?>  |  </a>
<?php
	}
}

if (isset ( $_GET ['success'] ) && $_GET ['success'] != NULL) {
	
	$success = $_GET ['success'];
	$urlRedirect = NULL;
	
	if ($success == "cad_property") {
		$urlRedirect = URL . "adm_cadImovel.php";
		?>
<script type="text/javascript">
                    window.alert("Sucesso ao realizar o cadastro do imóvel!");
                </script>
<?php
	
} else if ($success == "del_property") {
		$urlRedirect = URL . "adm_listImovel.php";
		?>
<script type="text/javascript">
                    window.alert("sucesso ao remover o imóvel");
                </script>
<?php
	
} else if ($success == "update_property") {
		$urlRedirect = URL . "adm_listImovel.php";
		?>
<script type="text/javascript">
                    window.alert("sucesso ao atualizar o imóvel");
                </script>
<?php
	
} else if ($success == "cad_advertising") {
		$urlRedirect = URL . "adm_cadParc.php";
		?>
<script type="text/javascript">
                    window.alert("sucesso ao cadastrar a propaganda");
                </script>
<?php
	
} else if ($success == "update_advertising") {
		$urlRedirect = URL . "adm_cadParc.php";
		?>
<script type="text/javascript">
                    window.alert("sucesso ao atualizar a propaganda");
                </script>
<?php
	
} else if ($success == "active_user") {
		$urlRedirect = URL_ADMIN_PAGE;
		?>
<script type="text/javascript">
                    window.alert("sucesso ao ativar/desativar usuário");
                </script>
<?php
	
} else if ($success == "update_password") {
		$urlRedirect = URL_ADMIN_PAGE;
		?>
<script type="text/javascript">
                    window.alert("sucesso ao atualizar senha do usuário");
                </script>
<?php
	
} else if ($success == "cad_upd_user") {
		$urlRedirect = URL . "adm_cadUser.php";
		?>
<script type="text/javascript">
                    window.alert("sucesso ao realizar o cadastro/edição do cliente!");
                </script>
<?php
	
} else if ($success == "delete_user") {
		$urlRedirect = URL . "adm_listUser.php";
		?>
<script type="text/javascript">
                    window.alert("sucesso ao remover o cliente!");
                </script>
<?php
	
} else if ($success == "update_user") {
		$urlRedirect = URL . "adm_listUser.php";
		?>
<script type="text/javascript">
                    window.alert("sucesso ao realizar a atualização do cliente!");
                </script>
<?php
	
} else if ($success == "cad_store") {
		$urlRedirect = URL . "adm_cadImob.php";
		?>
<script type="text/javascript">
                    window.alert("sucesso ao realizar o cadastro da imobiliária!");
                </script>
<?php
	
} else if ($success == "update_store") {
		$urlRedirect = URL . "adm_cadImob.php";
		?>
<script type="text/javascript">
                    window.alert("sucesso ao realizar a atualização da imobiliária!");
                </script>
<?php
	
} else if ($success == "delete_store") {
		$urlRedirect = URL . "adm_cadImob.php";
		?>
<script type="text/javascript">
                    window.alert("sucesso ao remover a imobiliária!");
                </script>
<?php
	
} else if ($success == "cad_notice") {
		$urlRedirect = URL . "adm_cadNot.php";
		?>
<script type="text/javascript">
                    window.alert("sucesso ao realizar o cadastro da notícia!");
                </script>
<?php
	
} else if ($success == "cad_tec") {
		$urlRedirect = URL . "adm_cadTec.php";
		?>
<script type="text/javascript">
                    window.alert("sucesso ao realizar o cadastro da tecnologia!");
                </script>
<?php
	
} else if ($success == "cad_deco") {
		$urlRedirect = URL . "adm_cadDeco.php";
		?>
<script type="text/javascript">
                    window.alert("sucesso ao realizar o cadastro da decoração!");
                </script>
<?php
	
} else if ($success == "cad_dica") {
		$urlRedirect = URL . "adm_cadDica.php";
		?>
<script type="text/javascript">
                    window.alert("sucesso ao realizar o cadastro da dica!");
                </script>
<?php
	
} else if ($success == "cad_main_advertising") {
		$urlRedirect = URL . "adm_cadParc.php";
		?>
<script type="text/javascript">
                    window.alert("sucesso ao realizar o cadastro da propaganda!");
                </script>
<?php
	
} else if ($success == "update_main_advertising") {
		$urlRedirect = URL . "adm_cadParc.php";
		?>
<script type="text/javascript">
                    window.alert("sucesso ao realizar a atualização da propaganda!");
                </script>
<?php
	
} else if ($success == "cad_city") {
		$urlRedirect = URL . "adm_cadlistCidades.php";
		?>
<script type="text/javascript">
                    window.alert("sucesso ao realizar o cadastro da cidade!");
                </script>
<?php
	
} else if ($success == "del_city") {
		$urlRedirect = URL . "adm_cadlistCidades.php";
		?>
<script type="text/javascript">
                    window.alert("sucesso ao excluir a cidade!");
                </script>
<?php
	
} else if ($success == "update_city") {
		$urlRedirect = URL . "adm_cadlistCidades.php";
		?>
<script type="text/javascript">
                    window.alert("sucesso ao atualizar os dados da cidade!");
                </script>
<?php
	
} else if ($success == "cad_user") {
		$urlRedirect = URL . "adm_cadUser.php";
		?>
<script type="text/javascript">
                    window.alert("sucesso ao cadastrar usúario!");
                </script>
<?php
	
} else if ($success == "cad_detail") {
		$urlRedirect = URL . "adm_cadlistDetalhes.php";
		?>
<script type="text/javascript">
                    window.alert("sucesso ao cadastrar o detalhe!");
                </script>
<?php
	
} else if ($success == "del_detail") {
		$urlRedirect = URL . "adm_cadlistDetalhes.php";
		?>
<script type="text/javascript">
                    window.alert("sucesso ao excluir o detalhe!");
                </script>
<?php
	
} else if ($success == "update_detail") {
		$urlRedirect = URL . "adm_cadlistDetalhes.php";
		?>
<script type="text/javascript">
                    window.alert("sucesso ao atualizar o detalhe!");
                </script>
<?php
	
} else if ($success == "cad_near") {
		$urlRedirect = URL . "adm_cadlistProxi.php";
		?>
<script type="text/javascript">
                    window.alert("sucesso ao cadastrar a proximidade!");
                </script>
<?php
	
} else if ($success == "del_near") {
		$urlRedirect = URL . "adm_cadlistProxi.php";
		?>
<script type="text/javascript">
                    window.alert("sucesso ao excluir a proximidade!");
                </script>
<?php
	
} else if ($success == "update_near") {
		$urlRedirect = URL . "adm_cadlistProxi.php";
		?>
<script type="text/javascript">
                    window.alert("sucesso ao atualizar a proximidade!");
                </script>
<?php
	
} else if ($success == "update_parceiro") {
		$urlRedirect = URL . "adm_cadParc.php";
		?>
<script type="text/javascript">
                    window.alert("sucesso ao atualizar o parceiro!");
                </script>
<?php
	
} else if ($success == "cad_parceiro") {
		$urlRedirect = URL . "adm_cadParc.php";
		?>
<script type="text/javascript">
                    window.alert("sucesso ao cadastrar o parceiro!");
                </script>
<?php
	
}
	/*
	 * Colocar uma var para definir os headers e redirecionar
	 */
	header ( "Location:" . $urlRedirect );
} else {
	
	if (isset ( $_GET ['failure'] ) && $_GET ['failure'] != NULL) {
		
		$failure = $_GET ['failure'];
		$urlRedirect = NULL;
		
		if ($failure == "cad_property") {
			$urlRedirect = URL . "adm_cadImovel.php";
			?>
<script type="text/javascript">
                        window.alert("Falha ao tentar realizar o cadastro do imóvel!");
                    </script>
<?php
		
} else if ($failure == "del_property") {
			$urlRedirect = URL . "adm_listImovel.php";
			?>
<script type="text/javascript">
                        window.alert("falha ao remover o imóvel!\nVerifique se o imóvel não está alugado!");
                    </script>
<?php
		
} else if ($failure == "update_property") {
			$urlRedirect = URL . "adm_listImovel.php";
			?>
<script type="text/javascript">
                        window.alert("falha ao atualizar o imóvel!");
                    </script>
<?php
		
} else if ($failure == "cad_advertising") {
			$urlRedirect = URL . "adm_cadParc.php";
			?>
<script type="text/javascript">
                        window.alert("falha ao cadastrar a propaganda!");
                    </script>
<?php
		
} else if ($failure == "active_user") {
			$urlRedirect = URL_ADMIN_PAGE;
			?>
<script type="text/javascript">
                        window.alert("falha ao ativar/desativar usuário!");
                    </script>
<?php
		
} else if ($failure == "update_password") {
			$urlRedirect = URL . "adm_alter_password.php";
			?>
<script type="text/javascript">
                        window.alert("falha ao atualizar senha do usuário!");
                    </script>
<?php
		
} else if ($failure == "cad_user") {
			$urlRedirect = URL . "adm_cadUser.php";
			?>
<script type="text/javascript">
                        window.alert("falha ao tentar realizar o cadastro do cliente!");
                    </script>
<?php
		
} else if ($failure == "delete_user") {
			$urlRedirect = URL . "adm_listUser.php";
			?>
<script type="text/javascript">
                        window.alert("falha ao remover cliente!");
                    </script>
<?php
		
} else if ($failure == "update_user") {
			$urlRedirect = URL . "adm_listUser.php";
			?>
<script type="text/javascript">
                        window.alert("falha ao realizar a atualização do cliente!");
                    </script>
<?php
		
} else if ($failure == "cad_store") {
			$urlRedirect = URL . "adm_cadImob.php";
			?>
<script type="text/javascript">
                        window.alert("falha ao tentar realizar o cadastro da imobiliária!");
                    </script>
<?php
		
} else if ($failure == "update_store") {
			$urlRedirect = URL . "adm_cadImob.php";
			?>
<script type="text/javascript">
                        window.alert("falha ao tentar realizar a atualização da imobiliária!");
                    </script>
<?php
		
} else if ($failure == "delete_store") {
			$urlRedirect = URL . "adm_cadImob.php";
			?>
<script type="text/javascript">
                        window.alert("falha ao remover imobiliária!\nVerifique se a mesma não possui propriedades vinculadas a ela!");
                    </script>
<?php
		
} else if ($failure == "cad_notice") {
			$urlRedirect = URL . "adm_cadNot.php";
			?>
<script type="text/javascript">
                        window.alert("falha ao tentar realizar o cadastro da notícia!");
                    </script>
<?php
		
} else if ($failure == "cad_tec") {
			$urlRedirect = URL . "adm_cadTec.php";
			?>
<script type="text/javascript">
                        window.alert("falha ao tentar realizar o cadastro da tecnologia!");
                    </script>
<?php
		
} else if ($failure == "cad_deco") {
			$urlRedirect = URL . "adm_cadDeco.php";
			?>
<script type="text/javascript">
                        window.alert("falha ao tentar realizar o cadastro da decoração!");
                    </script>
<?php
		
} else if ($failure == "cad_dica") {
			$urlRedirect = URL . "adm_cadDica.php";
			?>
<script type="text/javascript">
                        window.alert("falha ao tentar realizar o cadastro da dica!");
                    </script>
<?php
		
} else if ($failure == "cad_main_advertising") {
			$urlRedirect = URL . "adm_cadParc.php";
			?>
<script type="text/javascript">
                        window.alert("falha ao tentar realizar o cadastro da propaganda!");
                    </script>
<?php
		
} else if ($failure == "cad_city") {
			$urlRedirect = URL . "adm_cadlistCidades.php";
			?>
<script type="text/javascript">
                        window.alert("falha ao tentar realizar o cadastro da cidade!");
                    </script>
<?php
		
} else if ($failure == "del_city") {
			$urlRedirect = URL . "adm_cadlistCidades.php";
			?>
<script type="text/javascript">
                        window.alert("falha ao tentar excluir a cidade!\nVerifique se não há imóvel nessa cidade.");
                    </script>
<?php
		
} else if ($failure == "update_city") {
			$urlRedirect = URL . "adm_cadlistCidades.phpcadlistCidades.php";
			?>
<script type="text/javascript">
                        window.alert("falha ao tentar atualizar os dados da cidade!");
                    </script>
<?php
		
} else if ($failure == "cad_detail") {
			$urlRedirect = URL . "adm_cadlistDetalhes.php";
			?>
<script type="text/javascript">
                        window.alert("falha ao tentar cadastrar o detalhe!");
                    </script>
<?php
		
} else if ($failure == "del_detail") {
			$urlRedirect = URL . "adm_cadlistDetalhes.php";
			?>
<script type="text/javascript">
                        window.alert("falha ao tentar cadastrar o detalhe!");
                    </script>
<?php
		
} else if ($failure == "update_detail") {
			$urlRedirect = URL . "adm_cadlistDetalhes.php";
			?>
<script type="text/javascript">
                        window.alert("falha ao tentar atualizar o detalhe!");
                    </script>
<?php
		
} else if ($failure == "cad_near") {
			$urlRedirect = URL . "adm_cadlistProxi.php";
			?>
<script type="text/javascript">
                        window.alert("falha ao tentar cadastrar a proximidade!");
                    </script>
<?php
		
} else if ($failure == "del_near") {
			$urlRedirect = URL . "adm_cadlistProxi.php";
			?>
<script type="text/javascript">
                        window.alert("falha ao tentar excluir a proximidade!");
                    </script>
<?php
		
} else if ($failure == "update_near") {
			$urlRedirect = URL . "adm_cadlistProxi.php";
			?>
<script type="text/javascript">
                        window.alert("falha ao tentar atualizar a proximidade!");
                    </script>
<?php
		
} else if ($failure == "cad_parceiro") {
			$urlRedirect = URL . "adm_cadParc.php";
			?>
<script type="text/javascript">
                        window.alert("falha ao tentar cadastrar parceiro!");
                    </script>
<?php
		
}
		
		header ( "Location:" . $urlRedirect );
	}
}

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon" href="images/arquiLogo.png">
<title>Arquivo Imobiliário - Admin</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<!-- GERAL -->
	<div id="geral">


		<!-- TOPO -->
    <?php
				require_once PATH_INCLUDES_ADMIN . 'header.php';
				?>
    <!-- /TOPO -->


		<!-- MAIN CONTEUDO -->
		<div id="mainConteudo">

			<!-- CONTEUDO -->
			<div id="conteudo">

				<!-- ADMIN COL1 -->
            <?php
												require_once PATH_INCLUDES_ADMIN . 'right_colum.html';
												?>
            <!-- /ADMIN COL1 -->

				<div id="adm_COL2">
					<div id="Tit">Painel de Controle</div>
				</div>

				<!-- Tabela para ativar e desativar usuários -->
				<table width="75%" border="1" cellspacing="2">
					<tr>
						<th width="35%" align="left">Nome</th>
						<th width="24%" align="left">Email</th>
						<th width="20%" colspan="2" align="left">Ativo?</th>
					</tr>
                    <?php
																				/*
																				 * Here on bellow lines, where is that I shown the list of data of
																				 * status of user. I yet don't known how to shown this informations
																				 * the Borborema say: try to use the onchange, onclick event and on
																				 * hour that user change the value I create a request for server and
																				 * insert the alteration on database. But How to comunicate javascript
																				 * with php thus? This is a good question! Tomorrow I see.
																				 */
																				
																				showLinkOfNames ();
																				
																				$userController = new UserController ();
																				$statusAllUsers = $userController->getStatusAllClients ();
																				if (! is_null ( $statusAllUsers ))
																					foreach ( $statusAllUsers as $usersStatus ) {
																						?>
                            <tr>
						<form name="status-users-form" method="post"
							action="action/adminServices.action.php">
							<td width="35%" align="left"><?php echo $usersStatus->getName() ?></td>
							<td width="24%" align="left"><?php echo $usersStatus->getEmail() ?></td>
							<td width="13%" align="left"><select name="active">
                                        <?php if (!$usersStatus->getIsActive()) { ?>
                                            <option value="0">Não</option>
									<option value="1">Sim</option>
                                        <?php } else { ?>
                                            <option value="1">Sim</option>
									<option value="0">Não</option>
                                        <?php } ?>
                                        </select></td>
							<td width="7%" align="left"><input type="hidden" name="id_user"
								value="<?php echo $usersStatus->getId(); ?>"> <input
								type="hidden" name="action" value="alterstatus"> <input
								type="submit" value="Atualizar"></td>
						</form>

					</tr>
                        <?php
																					}
																				?>
                    </table>

			</div>
			<!-- CONTEUDO -->

		</div>
		<!-- /MAIN CONTEUDO -->


		<!-- FOOTER -->
    <?php
				require_once PATH_INCLUDES_ADMIN . 'footer.html';
				?>
    <!-- /FOOTER -->



	</div>
	<!-- /GERAL -->


</body>

</html>