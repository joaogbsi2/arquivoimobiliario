<?php

/*
 * INCLUDE SECTOR
 */

// $_SERVER['DOCUMENT_ROOT'] = "/home/arqui937/public_html/";
// define ("PATH", $_SERVER['DOCUMENT_ROOT']);

// include the file of configuration
// require_once '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_INCLUDES_ADMIN . 'session.php';
require_once PATH_CONTROLLER . 'PropertyController.class.php';
require_once PATH_MODEL_ENTITIES . 'Property.class.php';
require_once PATH_MODEL_ENTITIES . 'PropertyPhoto.class.php';
require_once PATH_CONTROLLER . 'PropertyPhotoController.class.php';

define ( "LIMIT_PROPERTIES", 10 );

$propertyController = new PropertyController ();
$properties = $propertyController->findAll ( LIMIT_PROPERTIES, " p.id DESC" );

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon" href="images/arquiLogo.png">
<title>Arquivo Imobiliário</title>
<link href="style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">

    /*
     * create a function that will go ask if the user want really delete the user 
     */
    function doYouWantDelete(){
        
        var answer = window.confirm("Deseja realmente remover esse imóvel?");
        
        if(answer)
            return true;
        
        return false;
    }
</script>

</head>

<body>
    
    <?php
				if (isset ( $_GET ['error'] )) {
					if ($_GET ['error'] == "size") {
						?>
                <script type="text/javascript">
                    window.alert("Erro ao tentar atualizar imagem.\nPor favor, verifique o limite de largura e altura da imagem!");
                </script>
        <?php
					}
				}
				?>
    
<!-- GERAL -->
	<div id="geral">


		<!-- TOPO -->
    <?php
				require_once PATH_INCLUDES_ADMIN . 'header.php';
				?>
	<!-- /TOPO -->


		<!-- MAIN CONTEUDO -->
		<div id="mainConteudo">

			<!-- CONTEUDO -->
			<div id="conteudo">


				<!-- ADMIN COL1 -->
            <?php
												require_once PATH_INCLUDES_ADMIN . 'right_colum.html';
												?>
            <!-- /ADMIN COL1 -->

				<div id="adm_COL2">

					<div id="Tit">Imóveis</div>

					<div id="cxInfo">
						<div id="fotoInfo">
							<img src="images/imgDados.jpg" />
						</div>
						<div id="txtInfo">Existem <?php echo $propertyController->getCount() ?> imóveis cadastrados </div>
						<div id="txtInfo">Exibindo <?php echo LIMIT_PROPERTIES; ?> últimos imóveis cadastrados</div>
					</div>

					<div id="Tit">Buscar imóvel?</div>

					<div id="cxBusca">
						<form id="form1" name="form1" method="post" action="">
							<table width="515">
								<tr>
									<td align="right">Código do Imóvel:</td>
									<td><input name="code_property" type="text" /> <input
										type="submit" name="Enviar" value="Buscar" /></td>
								</tr>
							</table>
						</form>
					</div>

					<div id="Tit">Últimos imóveis cadastrados</div>
                
                <?php
																
																/*
																 * Bom não coloquei um action diferente dessa mesma página visto que
																 * apenas irei buscar o imóvel com esse id e substituir a lista, ao invés
																 * de usar todos os imóveis mostro apenas o selecionado
																 */
																if (isset ( $_POST ['code_property'] ) && $_POST ['code_property'] != NULL) {
																	$property = $propertyController->getById ( $_POST ['code_property'] );
																	
																	// Get unique photo by property
																	$propertyPhotoController = new PropertyPhotoController ();
																	$picture = $propertyPhotoController->getUniquePhotoByProperty ( $property->getId () );
																	
																	if (is_null ( $picture )) {
																		$path = 'images/ft1.jpg';
																	} else {
																		$path = URL_PROPERTY_PHOTOS . basename ( $picture );
																	}
																	
																	?>
                                    <div id="cxLisImov">
                                        Código do Imóvel: <?php echo $property->getId(); ?>
                                        <div id="cxDesc">
							<table width="600">

								<tr>
									<td rowspan="3"><img class="tamanho-foto"
										src="<?php echo $path; ?>" /></td>
									<td>
										<form name="update_property" method="post"
											action="action/adminServices.action.php">
											<input type="hidden" name="action" value="updateProperty"> <input
												type="hidden" name="property_id"
												value="<?php echo $property->getId(); ?>"> <input
												type="submit" name="update" value="Ver mais">
										</form>
									</td>
									<td>
										<form name="delete_property" method="post"
											action="action/adminServices.action.php">
											<input type="hidden" name="action" value="deleteProperty"> <input
												type="hidden" name="property_id"
												value="<?php echo $property->getId(); ?>"> <input
												type="submit" name="delete" value="Excluir Imóvel"
												onclick="return doYouWantDelete();">
										</form>
									</td>

								</tr>

								<tr>
									<td width="74">Endereço:</td>
									<td colspan="3"><?php echo $property->getStreet().", ".$property->getIdentifier(); ?></td>
								</tr>
								<tr>
									<td>Bairro:</td>
									<td width="210">
										<!--<a href="#">-->
                                                            <?php echo $property->getNeighborhood(); ?>
                                                        <!--</a>-->
									</td>
									<td width="55">Cidade:</td>
									<td width="241">
										<!--<a href="#">-->
                                                            <?php echo $property->getCity(); ?>
                                                        <!--</a>-->
									</td>
								</tr>
							</table>
						</div>
					</div>
					<hr />
                            <?php
																} else {
																	
																	// Get unique photo by property
																	$propertyPhotoController = new PropertyPhotoController ();
																	
																	if (! is_null ( $properties ))
																		foreach ( $properties as $property ) {
																			$picture = $propertyPhotoController->getUniquePhotoByProperty ( $property->getId () );
																			if (is_null ( $picture )) {
																				$path = 'images/ft1.jpg';
																			} else {
																				$path = URL_PROPERTY_PHOTOS . basename ( $picture );
																			}
																			?>
                                    <div id="cxLisImov">
                                        Código do Imóvel: <?php echo $property->getId(); ?>
                                        <div id="cxDesc">
							<table width="600">
								<tr>
									<td rowspan="3"><img class="tamanho-foto"
										src="<?php echo $path; ?>" /></td>

									<td>
										<form name="update_property" method="post"
											action="action/adminServices.action.php">
											<input type="hidden" name="action" value="updateProperty"> <input
												type="hidden" name="property_id"
												value="<?php echo $property->getId(); ?>"> <input
												type="submit" name="update" value="Ver mais">
										</form>
									</td>
									<td>
										<form name="delete_property" method="post"
											action="action/adminServices.action.php">
											<input type="hidden" name="action" value="deleteProperty"> <input
												type="hidden" name="property_id"
												value="<?php echo $property->getId(); ?>"> <input
												type="submit" name="delete" value="Excluir Imóvel"
												onclick="return doYouWantDelete();">
										</form>
									</td>

								</tr>
								<tr>
									<td width="74">Endereço:</td>
									<td colspan="3"><?php echo $property->getStreet().", ".$property->getIdentifier(); ?></td>
								</tr>
								<tr>
									<td>Bairro:</td>
									<td width="210">
										<!--<a href="#">-->
                                                            <?php echo $property->getNeighborhood(); ?>
                                                        <!--</a>-->
									</td>
									<td width="55">Cidade:</td>
									<td width="241">
										<!--<a href="#">-->
                                                            <?php echo $property->getCity(); ?>
                                                        <!--</a>-->
									</td>
								</tr>
							</table>
						</div>
					</div>
					<hr />
                    <?php
																		} // foreach
																} // else
																?>
                
              
              
                
            </div>

			</div>
			<!-- CONTEUDO -->

		</div>
		<!-- /MAIN CONTEUDO -->


		<!-- FOOTER -->
    <?php
				require_once PATH_INCLUDES_ADMIN . 'footer.html';
				?>
    <!-- /FOOTER -->



	</div>
	<!-- /GERAL -->


</body>

</html>