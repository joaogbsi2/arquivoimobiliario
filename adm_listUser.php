<?php

/*
 * INCLUDE SECTOR
 */

// $_SERVER['DOCUMENT_ROOT'] = "/home/arqui937/public_html/";
// define ("PATH", $_SERVER['DOCUMENT_ROOT']);

// include the file of configuration
// require_once '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_INCLUDES_ADMIN . 'session.php';
require_once PATH_CONTROLLER . 'UserController.class.php';
require_once PATH_MODEL_ENTITIES . 'User.class.php';

define ( "LIMIT_USER", 10 );

/*
 * Sempre lembrar de usar nomes diferentes nas variáveis e tomar cuidado
 * com conflitos até entre arquivos diferentes
 */
$userController = new UserController ();
$users = $userController->findAll ( LIMIT_USER, " id DESC" );

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon" href="images/arquiLogo.png">
<title>Arquivo Imobiliário</title>
<link href="style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">

    /*
     * create a function that will go ask if the user want really delete the user 
     */
    function doYouWantDelete(){
        
        var answer = window.confirm("Deseja realmente remover esse cliente?");
        
        if(answer)
            return true;
        
        return false;
    }
</script>

</head>

<body>
    
    
   <?php
			
			if (isset ( $_GET ['error'] )) {
				if ($_GET ['error'] == "size") {
					?>
                <script type="text/javascript">
                    window.alert("Erro ao tentar atualizar imagem.\nPor favor, verifique o limite de largura e altura da imagem!");
                </script>
        <?php
				}
			}
			
			?>
    
<!-- GERAL -->
	<div id="geral">


		<!-- TOPO -->
    <?php
				require_once PATH_INCLUDES_ADMIN . 'header.php';
				?>
	<!-- /TOPO -->


		<!-- MAIN CONTEUDO -->
		<div id="mainConteudo">

			<!-- CONTEUDO -->
			<div id="conteudo">


				<!-- ADMIN COL1 -->
            <?php
												require_once PATH_INCLUDES_ADMIN . 'right_colum.html';
												?>
            <!-- /ADMIN COL1 -->

				<div id="adm_COL2">

					<div id="Tit">Usuários/Serviços</div>

					<div id="cxInfo">
						<div id="fotoInfo">
							<img src="images/imgDados.jpg" />
						</div>
						<div id="txtInfo">Existem <?php echo $userController->getCount(); ?> clientes cadastrados </div>
						<div id="txtInfo">Exibindo <?php echo LIMIT_USER; ?> últimos clientes cadastrados</div>
					</div>

					<div id="Tit">Buscar Cliente?</div>

					<div id="cxBusca">
						<form id="form1" name="form1" method="post" action="">
							<table width="515">
								<tr>
									<td align="right">Código do Cliente:</td>
									<td><input name="code_user" type="text" /> <input type="submit"
										name="Enviar" value="Buscar" /></td>
								</tr>
							</table>
						</form>
					</div>

					<div id="Tit">Últimos clientes cadastrados</div>
                
                <?php
																
																/*
																 * Bom não coloquei um action diferente dessa mesma página visto que
																 * apenas irei buscar o imóvel com esse id e substituir a lista, ao invés
																 * de usar todos os imóveis mostro apenas o selecionado
																 */
																if (isset ( $_POST ['code_user'] ) && $_POST ['code_user'] != NULL) {
																	$userSearched = $userController->getById ( $_POST ['code_user'] );
																	?>
                                    <div id="cxLisImov">
                                        Código do Cliente: <?php echo $userSearched->getId(); ?>
                                        <div id="cxDesc"
							style="height: auto;">
							<table width="600">
								<tr>
									<td>
										<form name="update_user" method="post"
											action="action/adminServices.action.php">
											<input type="hidden" name="action" value="updateUser"> <input
												type="hidden" name="user_id"
												value="<?php echo intval($userSearched->getId()); ?>"> <input
												type="submit" name="update" value="Ver mais">
										</form>
									</td>
									<td>
										<form name="delete_user" method="post"
											action="action/adminServices.action.php">
											<input type="hidden" name="action" value="deleteUser"> <input
												type="hidden" name="user_id"
												value="<?php echo intval($userSearched->getId()); ?>"> <input
												type="submit" name="delete" value="Excluir Cliente"
												onclick="return doYouWantDelete();">
										</form>
									</td>

								</tr>

								<!-- Adicionar a foto do usuário como solicitado pelo Henrique dia 31-01-2013 -->


								<tr>
									<td width="75px"><img
										src="<?php echo URL_USER_PHOTOS. basename($userSearched->getUserPhotoPath()); ?>"
										width="70px" heigth="100px"></td>
									<td>
                                                        Cliente: <?php echo $userSearched->getName(); ?><br />
										E-mail: <a href="#"><?php echo $userSearched->getEmail(); ?></a>
									</td>
									<td><br />
                                                        Telefone: <?php echo $userSearched->getPhone(); ?>
                                                    </td>

								</tr>

							</table>
						</div>
					</div>
					<hr />
                            <?php
																} else {
																	
																	foreach ( $users as $iter ) {
																		?>
                                    <div id="cxLisImov">
                                        Código do Cliente: <?php echo $iter->getId(); ?>
                                        <div id="cxDesc"
							style="height: auto;">
							<table width="600">
								<tr>
									<td>
										<form name="update_user" method="post"
											action="action/adminServices.action.php">
											<input type="hidden" name="action" value="updateUser"> <input
												type="hidden" name="user_id"
												value="<?php echo intval($iter->getId()); ?>"> <input
												type="submit" name="update" value="Ver mais">
										</form>
									</td>
									<td>
										<form name="delete_user" method="post"
											action="action/adminServices.action.php">
											<input type="hidden" name="action" value="deleteUser"> <input
												type="hidden" name="user_id"
												value="<?php echo intval($iter->getId()); ?>"> <input
												type="submit" name="delete" value="Excluir Cliente"
												onclick="return doYouWantDelete();">
										</form>
									</td>

								</tr>
								<tr>
									<td width="75px"><img
										src="<?php echo URL_USER_PHOTOS. basename($iter->getUserPhotoPath()); ?>"
										width="70px" heigth="100px"></td>
									<td>
                                                        Cliente: <?php echo $iter->getName(); ?><br />
										E-mail: <a href="#"><?php echo $iter->getEmail(); ?></a>
									</td>
									<td><br />
                                                        Telefone: <?php echo $iter->getPhone(); ?>
                                                    </td>

								</tr>

							</table>
						</div>
					</div>
					<hr />
                    <?php
																	} // foreach
																} // else
																?>
                
              
              
                
            </div>

			</div>
			<!-- CONTEUDO -->

		</div>
		<!-- /MAIN CONTEUDO -->


		<!-- FOOTER -->
    <?php
				require_once PATH_INCLUDES_ADMIN . 'footer.html';
				?>
    <!-- /FOOTER -->



	</div>
	<!-- /GERAL -->


</body>

</html>