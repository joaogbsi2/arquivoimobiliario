<?php

/*
 * INCLUDE SECTOR
 */

// $_SERVER['DOCUMENT_ROOT'] = "/home/arqui937/public_html/";
// define ("PATH", $_SERVER['DOCUMENT_ROOT']);

// include the file of configuration
// require_once '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_INCLUDES_ADMIN . 'session.php';
require_once PATH_MODEL_ENTITIES . 'State.class.php';
require_once PATH_CONTROLLER . 'StateController.class.php';
require_once PATH_CONTROLLER . 'CityController.class.php';
require_once PATH_MODEL_ENTITIES . 'City.class.php';

if (isset ( $_GET ['city_id'] )) {
	
	$cityController = new CityController ();
	$cityToUpdate = $cityController->getById ( $_GET ['city_id'] );
} else {
	header ( "location:" . URL_ADMIN_PAGE );
}

// buscar todos os estados cadastrados
$stateController = new StateController ();
$states = $stateController->getAllStates ();

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="shortcut icon" href="images/arquiLogo.png">
<title>Arquivo Imobiliário</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>

<body>

	<!-- GERAL -->
	<div id="geral">


		<!-- TOPO -->
    <?php
				require_once PATH_INCLUDES_ADMIN . 'header.php';
				?>
    <!-- /TOPO -->


		<!-- MAIN CONTEUDO -->
		<div id="mainConteudo">

			<!-- CONTEUDO -->
			<div id="conteudo">

				<!-- ADMIN COL1 -->
                <?php
																require_once PATH_INCLUDES_ADMIN . 'right_colum.html';
																?>
                <!-- /ADMIN COL1 -->

				<div id="adm_COL2">

					<div id="Tit">Cidades</div>

					<div id="cxInfo">
						<div id="fotoInfo">
							<img src="images/imgDados.jpg" />
						</div>

					</div>

					<div id="Tit">Atualizar cidade</div>
					<form class="validate" id="frmCadCity"
						action="action/cadCity.action.php" method="post"
						enctype="multipart/form-data">

						<div id="cxBusca">
							<table width="527">
								<tr>
									<td width="103" align="right">Cidade:</td>
									<td width="400"><input name="city" type="text" class="campo1"
										id="user" value="<?php echo $cityToUpdate->getName(); ?>" /></td>
								</tr>
								<tr>
									<td width="103" align="right">Estado:</td>
									<td width="400"><select name="state" class="campo1" id
										title="state">
                                    <?php
																																				
																																				foreach ( $states as $iterState ) {
																																					if ($iterState->getId () == $cityToUpdate->getState ()->getId ()) {
																																						?>
                                    <option
												value="<?php echo $iterState->getId(); ?>" selected="true"><?php echo $iterState->getName(); ?></option>

                                       <?php } else { ?>
                                            <option
												value="<?php echo $iterState->getId(); ?>"><?php echo $iterState->getName(); ?></option>
                                       <?php
																																					
}
																																				}
																																				?>
                                </select></td>
								</tr>
								<tr>
									<td></td>
									<td align="right"><input type="hidden" name="action"
										value="performUpdate"> <input type="hidden" name="id"
										value="<?php echo $cityToUpdate->getId(); ?>"> <input
										type="submit" name="Enviar" id="Enviar" value="Atualizar" /> <input
										type="button" name="cancel" value="Cancelar"
										onclick="history.back()" /></td>
								</tr>
							</table>
						</div>

					</form>

				</div>

			</div>
			<!-- CONTEUDO -->

		</div>
		<!-- /MAIN CONTEUDO -->


		<!-- FOOTER -->
			<<?php
				require_once PATH_INCLUDES_ADMIN . 'footer.html';
				?>		<!-- /FOOTER -->



	</div>
	<!-- /GERAL -->


</body>

</html>