<?php

/*
 * INCLUDE SECTOR
 */

// $_SERVER['DOCUMENT_ROOT'] = "/home/arqui937/public_html/";
// define ("PATH", $_SERVER['DOCUMENT_ROOT']);

// include the file of configuration
// require_once '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}
require_once PATH_INCLUDES_ADMIN . 'session.php';
require_once PATH_MODEL_ENTITIES . 'Store.class.php';
require_once PATH_CONTROLLER . 'StoreController.class.php';

/*
 * First verify if user select a store to edit
 * case no then return to admin index page
 */
// if (isset($$_GET['store_id'])){
$storeId = $_GET ['store_id'];
$storeController = new StoreController ();
$storeUpdate = $storeController->getById ( $storeId );
// } else {
// header("location:" .URL_ADMIN_PAGE);
// }

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="shortcut icon" href="images/arquiLogo.png">
<title>Arquivo Imobiliário</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>

<body>

	<!-- GERAL -->
	<div id="geral">


		<!-- TOPO -->
    <?php
				require_once PATH_INCLUDES_ADMIN . 'header.php';
				?>
    <!-- /TOPO -->


		<!-- MAIN CONTEUDO -->
		<div id="mainConteudo">

			<!-- CONTEUDO -->
			<div id="conteudo">

				<!-- ADMIN COL1 -->
                <?php
																require_once PATH_INCLUDES_ADMIN . 'right_colum.html';
																?>
                <!-- /ADMIN COL1 -->

				<div id="adm_COL2">

					<form class="validate" id="frmUpdStore"
						action="action/cadImob.action.php" method="post"
						enctype="multipart/form-data">

						<div id="Tit">Imobiliárias</div>



						<div id="Tit">Atualizar</div>

						<div id="cxBusca">

							<table width="515">
								<tr>
									<td align="right">Título:</td>
									<td><input name="name" type="text" class="campo1" id="user"
										maxlength="75" value="<?php echo $storeUpdate->getName(); ?>" /></td>
								</tr>
								<tr>
									<td align="right">E-mail:</td>
									<td><input name="mail" type="text"
										placeholder="exemplo@exemplo.com.br"
										class="textfield required email" class="campo1" id="user"
										maxlength="75" value="<?php echo $storeUpdate->getMail(); ?>" /></td>
								</tr>
								<tr>
									<td align="right">Endereco:</td>
									<td><input name="endereco" type="text" class="campo1" id="user"
										maxlength="300"
										value="<?php echo $storeUpdate->getEndereco(); ?>" /></td>
								</tr>
								<tr>
									<td align="right">Telefone:</td>
									<td><input name="telefone" type="text" class="campo1" id="user"
										maxlength="30"
										value="<?php echo $storeUpdate->getTelefone(); ?>" /></td>
								</tr>
								<tr>
									<td align="right">Responsavel:</td>
									<td><input name="responsavel" type="text" class="campo1"
										id="user" maxlength="200"
										value="<?php echo $storeUpdate->getResponsavel(); ?>" /></td>
								</tr>
								<tr>
									<td align="right">Imagem:</td>
									<td><input type="file" class="file" id="photo" name="photo" /></td>
								</tr>
								<!-- SUBMIT BUTTONS -->
								<tr>
									<td width="280" align="right"><input type="hidden"
										name="store_id" value="<?php echo $storeUpdate->getId(); ?>" />
										<input type="hidden" name="action" value="update" /> <input
										type="submit" name="Enviar" id="Enviar" value="Atualizar" /></td>
									<td width="420"><input type="button" name="voltar"
										id="Cancelar" value="Voltar" onclick="history.back()" /></td>
								</tr>

							</table>
						</div>

					</form>

				</div>

			</div>
			<!-- CONTEUDO -->

		</div>
		<!-- /MAIN CONTEUDO -->


		<!-- FOOTER -->
		<<?php require_once PATH_INCLUDES_ADMIN . 'footer.html'; ?>
		<!-- /FOOTER -->



	</div>
	<!-- /GERAL -->


</body>

</html>