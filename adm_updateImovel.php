<?php

/*
 * INCLUDE SECTOR
 */

// $_SERVER['DOCUMENT_ROOT'] = "/Users/edilson/Sites/webmovel2/";
// define ("PATH", $_SERVER['DOCUMENT_ROOT']);

// include the file of configuration
// require_once '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_INCLUDES_ADMIN . 'session.php';
require_once PATH_ACTION . 'cadProperty.action.php';
require_once PATH_MODEL_ENTITIES . 'State.class.php';
require_once PATH_MODEL_ENTITIES . 'City.class.php';
require_once PATH_MODEL_ENTITIES . 'Status.class.php';
require_once PATH_MODEL_ENTITIES . 'PropertyType.class.php';

require_once PATH_CONTROLLER . 'DetailController.class.php';
require_once PATH_MODEL_ENTITIES . 'Detail.class.php';

require_once PATH_CONTROLLER . 'NearController.class.php';
require_once PATH_MODEL_ENTITIES . 'Near.class.php';

require_once PATH_CONTROLLER . 'StoreController.class.php';
require_once PATH_MODEL_ENTITIES . 'Store.class.php';

/*
 * require_once para carregar o imóvel a ser editado
 * através do método getById no controller PropertyController
 */
require_once PATH_CONTROLLER . 'PropertyController.class.php';

require_once PATH_CONTROLLER . 'PropertyDetailController.class.php';
require_once PATH_MODEL_ENTITIES . 'PropertyDetail.class.php';

require_once PATH_CONTROLLER . 'PropertyNearController.class.php';
require_once PATH_MODEL_ENTITIES . 'PropertyNear.class.php';

require_once PATH_CONTROLLER . 'PropertyStatusController.class.php';
require_once PATH_MODEL_ENTITIES . 'PropertyStatus.class.php';

require_once PATH_CONTROLLER . 'PropertyStoreController.class.php';
require_once PATH_MODEL_ENTITIES . 'PropertyStore.class.php';

require_once PATH_CONTROLLER . 'PropertyPhotoController.class.php';
require_once PATH_MODEL_ENTITIES . 'PropertyPhoto.class.php';

/*
 * Agora pegar o id da propriedade e buscar o Transfer objeto
 * para preencher os campos com os dados da propriedade selecionada
 * para ser atualizada
 */

/*
 * First verify if user select a property to edit
 * case no then return to admin index page
 */
if (isset ( $_GET ['property_id'] )) {
	
	$propertyController = new PropertyController ();
	$propertyUpdate = $propertyController->getById ( $_GET ['property_id'] );
	
	$propertyStatusController = new PropertyStatusController ();
	$arrayPropertyStatus = $propertyStatusController->getPropertyStatus ( $_GET ['property_id'] );
	
	$arrayStatus = array();
	foreach ($arrayPropertyStatus as $propertyStatus){
		$arrayStatus[] = $propertyStatus->getStatus();
	}
	
	$propertyDetailController = new PropertyDetailController ();
	$detailsInProperty = $propertyDetailController->getPropertyDetail ( $_GET ['property_id'] );
	
	$propertyNearController = new PropertyNearController ();
	$nearsInProperty = $propertyNearController->getPropertyNear ( $_GET ['property_id'] );
	
	$propertyStoreController = new PropertyStoreController ();
	// $storesInProperty = $propertyStoreController->getPropertyStore($_GET['property_id']);
	$storesInProperty = $propertyStoreController->getPropertyStore ( $propertyUpdate->getidUnico () );
	
	$storeController = new StoreController ();
	$stores = $storeController->findAll ( NULL, " NAME ASC" );
	
	if (! is_null ( $propertyUpdate->getExclusiveStore () )) {
		
		if ($propertyUpdate->getExclusiveStore () != 0) {
			$storeController = new StoreController ();
			$exclusiveStore = $storeController->getById ( $propertyUpdate->getExclusiveStore () );
		} else {
			$propertyUpdate->setExclusiveStore ( NULL );
		}
	} else
		$propertyUpdate->setExclusiveStore ( NULL );
	
	$propertyPhotoController = new PropertyPhotoController ();
	$photos = $propertyPhotoController->getPhotosByProperty ( $_GET ['property_id'] );
	$propertyIdToDeletePhoto = $_GET ['property_id'];
} else {
	
	header ( "location:" . URL_ADMIN_PAGE );
}

/*
 * Create a new field. Combobox of Store
 */
require_once PATH_CONTROLLER . 'StoreController.class.php';
require_once PATH_MODEL_ENTITIES . 'Store.class.php';

/*
 * Bom caso, eu esteja tentando atualizar um imóvel
 * que possui status = 3 tenho que carregar
 * os dias e meses que podem ser locados conforme
 * estava no momento do cadastro
 */
require_once PATH_CONTROLLER . 'RentToTheseDaysController.class.php';
require_once PATH_CONTROLLER . 'RentToTheseMonthsController.class.php';
require_once PATH_MODEL_ENTITIES . 'RentToTheseDays.class.php';
require_once PATH_MODEL_ENTITIES . 'RentToTheseMonths.class.php';

$rentToTheseDaysController = new RentToTheseDaysController ();
$rentToTheseMonthsController = new RentToTheseMonthsController ();

$storeController = new StoreController ();
$stores = $storeController->findAll ( NULL, " NAME ASC" );

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon" href="images/arquiLogo.png">
<title>Arquivo Imobiliário</title>
<link href="style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/jquery.multiselect.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />

<script language="javascript">

    function changeType(){
        
        var fieldTypeValue = document.getElementById("status").value;
        if (fieldTypeValue == 3){
            document.getElementById("temporadaConfig").setAttribute("style","display: inline-block;");
            console.log("alterando o style para inline-block");
          
        } else {
            document.getElementById("temporadaConfig").setAttribute("style","display: none;");
            console.log("alterando o style para none");
          
        }
    }
</script>

</head>

<body>

	<!-- GERAL -->
	<div id="geral">


		<!-- TOPO -->
    <?php
				require_once PATH_INCLUDES_ADMIN . 'header.php';
				?>
	<!-- /TOPO -->


		<!-- MAIN CONTEUDO -->
		<div id="mainConteudo">

			<!-- CONTEUDO -->
			<div id="conteudo">

				<!-- ADMIN COL1 -->
                <?php
																require_once PATH_INCLUDES_ADMIN . 'right_colum.html';
																?>
                <!-- /ADMIN COL1 -->

				<!-- AREA DE CADASTRO -->
				<div id="adm_COL2">

					<form id="frmCadProperty" method="post"
						action="action/updatePropertyForm.action.php"
						enctype="multipart/form-data" accept-charset="UTF-8">

						<div id="Tit">Atualização do Imóvel</div>

						<div id="cxTit">Tipo de Negócio</div>
						<div id="cxTex">
							<table width="700">
								<!-- ACHO QUE O CAMPO ID JÁ PODERÁ SER USADO COMO O CÓDIGO,
                                NÃO HÁ A NECESSIDADE DE CRIAR UM CAMPO CÓDIGO EU ACHO
                                ALTERAÇÃO FEITA NO DIA 27/10/2013
                            <tr>
                                <td width="180" align="right">Código do Imóvel:</td>
                            <td width="520"><input name="user" type="text" class="campo1" id="user"/></td>
                        </tr>
                        
                        <tr>
                        	<td colspan="2" height="5"></td>
                        </tr>
                        -->

								<!-- INICIO DO FORM DE CADASTRO DE IMOVEL DA VERSÃO NOVA -->
								<tr>
									<td width="180" align="right">* Este imóvel é para: (Operação)</td>
									<td width="520">
										<div id="status">
                                <?php
																																/*
																																 * Essas variáveis são pegas do include no .action
																																 * que cria um novo objeto da classe StatusController
																																 *
																																 */
																																foreach ( $statusController->getAll () as $status ) {
																																	?>
                                        <span>
                                        <?php if (in_array($status, $arrayStatus)) { ?>
                                            <input type="checkbox"
												class="checkbox required" name="statusProperty" id="status"
												value="<?php echo $status->getId(); ?>" checked="true"><?php echo $status->getDescription(); ?>
                                        <?php } else { ?>
                                            <input type="checkbox"
												class="checkbox required" name="statusProperty" id="status"
												value="<?php echo $status->getId(); ?>"><?php echo $status->getDescription(); ?>
                                        <?php } ?>
                                        </span>
                                    <?php
																																}
																																?>
                                </div>
									</td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>

								<tr>
									<td width="180" align="right">* Tipo de Imóvel:</td>
									<td width="520"><select class="required" name="propertyType"
										id="propertyType">
                                <?php
																																foreach ( $propertyTypeList as $propertyType ) {
																																	
																																	if ($propertyType == $propertyUpdate->getType ()) {
																																		?>
                                        <option
												value="<?php echo $propertyType->getId(); ?>"
												selected="true"><?php echo $propertyType; ?></option>;
                                    <?php } else { ?>
                                            <option
												value="<?php echo $propertyType->getId(); ?>"><?php echo $propertyType; ?></option>;
                                    <?php
																																	}
																																}
																																?>
				</select></td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>

								<!-- Campo de seleção da imobiliária -->
								<tr>
									<td width="180" align="right">* Imobiliária:</td>
									<td width="520">
										<div id="imobiliaria">
                        <?php
																								
																								// buscar todos os detalhes cadastrados
																								$storeController = new StoreController ();
																								$storesList = $storeController->findAll ( NULL, "name ASC" );
																								if (! is_null ( $storesList )) {
																									
																									if (! is_null ( $storesInProperty )) {
																										
																										// varrer todos os detalhes cadastrados e caso algum bata com os selecionados no cadastro, marque-os
																										foreach ( $storesList as $allStores ) {
																											$flag = false; // to validate later
																											foreach ( $storesInProperty as $storesInserted ) {
																												
																												if ($allStores->getId () == $storesInserted->getStore ()->getId ()) {
																													?>
                                        <input type="checkbox"
												name="stores[]" id="stores"
												value="<?php echo $allStores->getId(); ?>" checked="true" /><?php echo $allStores->getName(); ?>
                                                            
                                                            <input
												class="textfield" name="referencia[]" id="referencia"
												size="10"
												value="<?php echo $storesInserted->getReferencia()?>" /> <br />
                                                    <?php
																													$flag = true;
																													break;
																												}
																												?>

                                              
                                            <?php
																											} // foreach serviceProvider (servicos que o usuário possui cadastrados)
																											?>
                                            <?php if (!$flag) { //se estiver false é que o usuário não tem esse serviço cadastrado a ele ?> 
                                                <input type='checkbox'
												class='checkbox' name='stores[]' id='stores'
												value="<?php echo $allStores->getId(); ?>" /><?php echo $allStores->getName(); ?>
											<input class="textfield" name="referencia[]" id="referencia"
												size="10" /></br>
                                            <?php
																											} // if Já add o checkbox
																										}
																									} else {
																										?>

                                    <?php
																										foreach ( $storesList as $iterStore ) {
																											?>
                                            <input type="checkbox"
												name="stores[]" value="<?php echo $iterStore->getId(); ?>"
												id="CheckboxGroup1_0" />
                                                    <?php echo $iterStore->getName(); ?>
                                                    <input
												class="textfield" name="referencia[]" id="referencia"
												size="10" /> <br />


                                    <?php
																										}
																									}
																								} else
																									echo "nao há nenhuma imobiliária cadastrada!";
																								
																								?>
								</div>
									</td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>

								<!-- Campo de exclusividade de imobiliaria -->
								<tr>
									<td width="180" align="right">Exclusividade:</td>
									<td width="520"><select name="exclusive_store"
										id="exclusive_store">
                                <?php
																																if (is_null ( $exclusiveStore )) {
																																	?>
                                        <option value="0"
												selected="true">Selecione...</option>
                                <?php
																																	foreach ( $stores as $iter ) {
																																		echo '<option value="' . $iter->getId () . '">' . $iter->getName () . '</option>';
																																	}
																																} else {
																																	?>
                                        <option value="0">Selecione...</option>
                                <?php
																																	foreach ( $stores as $iter ) {
																																		if ($iter->getId () == $exclusiveStore->getId ())
																																			echo '<option value="' . $iter->getId () . '" selected="true">' . $iter->getName () . '</option>';
																																		else
																																			echo '<option value="' . $iter->getId () . '">' . $iter->getName () . '</option>';
																																	}
																																}
																																?>
				</select></td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>

								<tr>
									<td width="180" align="right">Quantidade de quartos:</td>
									<td width="50"><input class="textfield" name="qtdRooms"
										value="<?php echo $propertyUpdate->getQtdRooms(); ?>" /></td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>

								<!--                        <tr>
                            <td width="180" align="right">Destaque?</td>
                            <td width="520"><select name="tipo" class="campo1" id title="cidade">
                          <option value="label" selected="selected">Label</option></select></td>
                        </tr>-->
							</table>
                   
                    
                            <?php
																												if ($propertyStatus->getStatus ()->getId () == 3) {
																													?>

                                   <div id="temporadaConfig"
								style="display: inline-block;">
                            <?php
																													$days = $rentToTheseDaysController->getRentToTheseDays ( $propertyUpdate->getId () );
																													$months = $rentToTheseMonthsController->getRentToTheseMonths ( $propertyUpdate->getId () );
																													
																													?>
                                * Dias da semana:
                                <select id="daysOfWeek"
									name="daysOfWeek[]" multiple="multiple">
                                    <?php if ($days->getDomingo() == 1)  { ?>
                                        <option value='domingo'
										selected="true">Domingo</option>
                                    <?php } else { ?>
                                        <option value='domingo'>Domingo</option>
                                    <?php } ?>
                                    <?php if ($days->getSegunda() == 1) { ?>  
                                        <option value='segunda'
										selected="true">Segunda</option>
                                    <?php } else { ?>
                                        <option value='segunda'>Segunda</option>
                                    <?php } ?>
                                    <?php if ($days->getTerca() == 1) { ?>
                                        <option value='terca'
										selected="true">Terça</option>
                                    <?php } else { ?>
                                        <option value='terca'>Terça</option>
                                    <?php } ?>
                                    <?php if ($days->getQuarta() == 1) { ?>
                                        <option value='quarta'
										selected="true">Quarta</option>
                                    <?php } else { ?>
                                        <option value='quarta'>Quarta</option>
                                    <?php } ?>
                                    <?php if ($days->getQuinta() == 1) { ?>
                                        <option value='quinta'
										selected="true">Quinta</option>
                                    <?php } else { ?>
                                        <option value='quinta'>Quinta</option>
                                    <?php } ?>
                                    <?php if ($days->getSexta() == 1) { ?>
                                        <option value='sexta'
										selected="true">Sexta</option>
                                    <?php } else { ?>
                                        <option value='sexta'>Sexta</option>
                                    <?php } ?>
                                    <?php if ($days->getSabado() == 1) { ?>
                                        <option value='sabado'
										selected="true">Sábado</option>
                                    <?php } else { ?>
                                        <option value='sabado'>Sábado</option>
                                    <?php } ?>
                                </select> * Meses do ano: <select
									id="monthOfYear" name="monthOfYear[]" multiple="multiple">
                                    <?php if ($months->getJaneiro() == 1) { ?>
                                        <option value='janeiro'
										selected="true">Janeiro</option>
                                    <?php } else { ?>
                                        <option value='janeiro'>Janeiro</option>
                                    <?php } ?>
                                    <?php if ($months->getFevereiro() == 1) { ?>
                                        <option value='fevereiro'
										selected="true">Fevereiro</option>
                                    <?php } else { ?>
                                        <option value='fevereiro'>Fevereiro</option>
                                    <?php } ?>
                                    <?php if ($months->getMarco() == 1) { ?>
                                        <option value='marco'
										selected="true">Março</option>
                                    <?php } else { ?>
                                        <option value='marco'>Março</option>
                                    <?php } ?>
                                    <?php if ($months->getAbril() == 1) { ?>
                                        <option value='abril'
										selected="true">Abril</option>
                                    <?php } else { ?>
                                        <option value='abril'>Abril</option>
                                    <?php } ?>
                                    <?php if ($months->getMaio() == 1) { ?>
                                        <option value='maio'
										selected="true">Maio</option>
                                    <?php } else { ?>
                                        <option value='maio'>Maio</option>
                                    <?php } ?>
                                    <?php if ($months->getJunho() == 1) { ?>
                                        <option value='junho'
										selected="true">Junho</option>
                                    <?php } else { ?>
                                        <option value='junho'>Junho</option>
                                    <?php } ?>
                                    <?php if ($months->getJulho() == 1) { ?>
                                        <option value='julho'
										selected="true">Julho</option>
                                    <?php } else { ?>
                                        <option value='julho'>Julho</option>
                                    <?php } ?>
                                    <?php if ($months->getAgosto() == 1) { ?>
                                        <option value='agosto'
										selected="true">Agosto</option>
                                    <?php } else { ?>
                                        <option value='agosto'>Agosto</option>
                                    <?php } ?>
                                    <?php if ($months->getSetembro() == 1) { ?>
                                        <option value='setembro'
										selected="true">Setembro</option>
                                    <?php } else { ?>
                                        <option value='setembro'>Setembro</option>
                                    <?php } ?>
                                    <?php if ($months->getOutubro() == 1) { ?>
                                        <option value='outubro'
										selected="true">Outubro</option>
                                    <?php } else { ?>
                                        <option value='outubro'>Outubro</option>
                                    <?php } ?>
                                    <?php if ($months->getNovembro() == 1) { ?>
                                        <option value='novembro'
										selected="true">Novembro</option>
                                    <?php } else { ?>
                                        <option value='novembro'>Novembro</option>
                                    <?php } ?>
                                    <?php if ($months->getDezembro() == 1) { ?>
                                        <option value='dezembro'
										selected="true">Dezembro</option>
                                    <?php } else { ?>
                                        <option value='dezembro'>Dezembro</option>
                                    <?php } ?>
                               </select>
                               <?php } else { ?> 
                                        <div id="temporadaConfig"
									style="display: none;">
									* Dias da semana: <select id="daysOfWeek" name="daysOfWeek[]"
										multiple="multiple">
										<option value='domingo'>Domingo</option>
										<option value='segunda'>Segunda</option>
										<option value='terca'>Terça</option>
										<option value='quarta'>Quarta</option>
										<option value='quinta'>Quinta</option>
										<option value='sexta'>Sexta</option>
										<option value='sabado'>Sábado</option>
									</select> * Meses do ano: <select id="monthOfYear"
										name="monthOfYear[]" multiple="multiple">
										<option value='janeiro'>Janeiro</option>
										<option value='fevereiro'>Fevereiro</option>
										<option value='marco'>Março</option>
										<option value='abril'>Abril</option>
										<option value='maio'>Maio</option>
										<option value='junho'>Junho</option>
										<option value='julho'>Julho</option>
										<option value='agosto'>Agosto</option>
										<option value='setembro'>Setembro</option>
										<option value='outubro'>Outubro</option>
										<option value='novembro'>Novembro</option>
										<option value='dezembro'>Dezembro</option>
									</select>
                               <?php } ?> 
                                
                           </div>

							</div>

							<div id="cxTit">Localização do Imóvel</div>
							<div id="cxTex">
								<table width="700">
									<tr>
										<td width="180" align="right">Endereço:</td>
										<td width="520"><input class="textfield" name="street"
											id="street"
											value="<?php echo $propertyUpdate->getStreet(); ?>" /></td>
									</tr>
									<tr>
										<td colspan="2" height="5"></td>
									</tr>
									<tr>
										<td width="180" align="right">Número:</td>
										<td width="40"><input type="text" class="textfield"
											name="number" id="number"
											value="<?php echo $propertyUpdate->getIdentifier(); ?>" /></td>
									</tr>
									<tr>
										<td colspan="2" height="5"></td>
									</tr>
									<tr>
										<td width="180" align="right">Bairro:</td>
										<td width="300"><input type="text" class="textfield"
											name="neighborhood" id="neighborhood"
											value="<?php echo $propertyUpdate->getNeighborHood(); ?>" /></td>
									</tr>
									<tr>
										<td colspan="2" height="5"></td>
									</tr>
									<tr>
										<td width="180" align="right">CEP:</td>
										<td width="105"><input type="text" class="textfield"
											name="cep" id="cep"
											value="<?php echo $propertyUpdate->getCep(); ?>" /></td>
									</tr>
									<tr>
										<td colspan="2" height="5"></td>
									</tr>
									<tr>
										<td width="180" align="right">* Cidade:</td>
										<td width="520"><select class="required" name="city">
                                    <?php
																																				foreach ( $cityController->getAllCities () as $city ) {
																																					?>
                                    <?php if ($propertyUpdate->getCity() == $city) { ?>
                                    <option name="city"
													value="<?php echo $city->getId(); ?>" selected="true"><?php echo $city->getName(); ?></option>
                                    <?php } else { ?>
                                        <option name="city"
													value="<?php echo $city->getId(); ?>"><?php echo $city->getName(); ?></option>
                                    <?php
																																					}
																																				}
																																				?>
                                </select></td>
									</tr>
									<tr>
										<td colspan="2" height="5"></td>
									</tr>
									<tr>
										<td width="180" align="right">* UF:</td>
										<td width="520"><select name="state" class="required">
                                    <?php
																																				foreach ( $stateController->getAllStates () as $state ) {
																																					?>
                                    <?php if ($propertyUpdate->getCity()->getState() == $state) { ?>
                                    <option name="state"
													value="<?php echo $state->getId(); ?>" selected="true"><?php echo $state->getName(); ?></option>
                                    <?php } else { ?>
                                    <option name="state"
													value="<?php echo $state->getId(); ?>"><?php echo $state->getName(); ?></option>
                                    <?php
																																					}
																																				}
																																				?>
                                </select></td>
									</tr>
									<tr>
										<td colspan="2" height="5"></td>
									</tr>
									<!--                        <tr>
                            <td width="180" align="right">Exibir endereço?:</td>
                            <td width="520">
                                <select name="tipo" class="campo1" id title="cidade">
                                    <option value="1" selected="selected">Sim</option>
                                    <option value="0">Não</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                        	<td colspan="2" height="5"></td>
                        </tr>-->

								</table>
							</div>

							<div id="cxTit">Descrição do Imóvel</div>
							<div id="cxTex">
								<table width="700">
									<tr>
										<td width="180" align="right">Descrição:</td>
										<td width="400"><textarea
												style="width: 400px; heigth: 75px; overflow: auto;"
												class="textfield" name="description" id="description" /><?php echo $propertyUpdate->getDescription(); ?></textarea>
										</td>
									</tr>
									<tr>
										<td colspan="2" height="5"></td>
									</tr>
									<tr>
										<td width="180" align="right">Email de contato:</td>
										<td width="400"><input style="width: 400px;" type="text"
											placeholder="exemplo@exemplo.com.br" class="textfield email"
											name="contactEmail" id="contactEmail"
											value="<?php echo $propertyUpdate->getContactEmail(); ?>" />
										</td>
									</tr>
									<tr>
										<td colspan="2" height="5"></td>
									</tr>
									<tr>
										<td width="180" align="right">Telefone:</td>
										<td width="400"><input type="text" class="textfield"
											id="contactPhone" name="contactPhone"
											value="<?php echo $propertyUpdate->getContactPhone(); ?>" />
										</td>
									</tr>
									<tr>
										<td colspan="2" height="5"></td>
									</tr>


									<!--                        <tr>
                        	<td width="180" align="right">Dependências:</td>
                            <td width="520"><input name="user" type="text" class="campo2" id="user" value="" /></td>
                        </tr>
                        <tr>
                        	<td colspan="2" height="5"></td>
                        </tr>
                        <tr>
                        	<td width="180" align="right">Dormitórios:</td>
                            <td width="520"><input name="user2" type="text" class="campo1" id="user2"/></td>
                        </tr>
                        <tr>
                        	<td colspan="2" height="5"></td>
                        </tr>
                        <tr>
                        	<td width="180" align="right">Vagas garagem:</td>
                            <td width="520"><input name="user3" type="text" class="campo1" id="user3"/></td>
                        </tr>
                        <tr>
                        	<td colspan="2" height="5"></td>
                        </tr>
                        <tr>
                        	<td width="180" align="right">Link do vídeo:</td>
                            <td width="520"><input name="user4" type="text" class="campo1" id="user4"/></td>
                        </tr>
                        <tr>
                        	<td colspan="2" height="5"></td>
                        </tr>
                        <tr>
                        	<td width="180" align="right">Cadastrar Link Mapa:</td>
                            <td width="520"><input name="user4" type="text" class="campo1" id="user4"/></td>
                        </tr>-->
								</table>
							</div>

							<div id="cxTit">Características</div>
							<div id="cxTex">
								<table width="700">
									<tr>
										<td width="180" align="right">Garagem:</td>
										<td width="520"><input name="garagem" type="text"
											class="campo1" id="user5"
											value="<?php echo $propertyUpdate->getGaragem(); ?>" /></td>
									</tr>
									<tr>
										<td colspan="2" height="5"></td>
									</tr>
									<tr>
										<td width="180" align="right">Pavimento:</td>
										<td width="520"><input name="pavimento" type="text"
											class="campo1" id="user2"
											value="<?php echo $propertyUpdate->getPavimento(); ?>" /></td>
									</tr>
									<tr>
										<td colspan="2" height="5"></td>
									</tr>
									<!--                        <tr>
                        	<td width="180" align="right">Dependências:</td>
                            <td width="520"><input name="user3" type="text" class="campo1" id="user3"/></td>
                        </tr>
                        <tr>
                        	<td colspan="2" height="5"></td>
                        </tr>-->
									<tr>
										<td width="180" align="right">Posição:</td>
										<td width="520"><input name="posicao" type="text"
											class="campo1" id="user4"
											value="<?php echo $propertyUpdate->getPosicao(); ?>" /></td>
									</tr>
								</table>
							</div>

							<div id="cxTit">Detalhes</div>
							<div id="cxTex">
								<!--<table width="700">-->
                         <?php
																									
																									// buscar todos os detalhes cadastrados
																									$detailController = new DetailController ();
																									$details = $detailController->getAll ( NULL, "name ASC" );
																									
																									if (! is_null ( $details )) {
																										
																										if (! is_null ( $detailsInProperty )) {
																											
																											// varrer todos os detalhes cadastrados e caso algum bata com os selecionados no cadastro, marque-os
																											foreach ( $details as $allDetails ) {
																												$flag = false; // to validate later
																												foreach ( $detailsInProperty as $detailsInserted ) {
																													?>
                                                <?php
																													
																													if ($allDetails->getId () == $detailsInserted->getDetail ()->getId ()) {
																														?>
                                                        <input
									type="checkbox" name="details[]" id="details"
									value="<?php echo $allDetails->getId(); ?>" checked="true" /><?php echo $allDetails->getName(); ?>
                                                        <br />
                                                <?php
																														$flag = true;
																														break;
																													}
																													?>

                                           </label>
                                        <?php
																												} // foreach serviceProvider (servicos que o usuário possui cadastrados)
																												?>
                                        <?php if (!$flag) { //se estiver false é que o usuário não tem esse serviço cadastrado a ele ?> 
                                            <input type='checkbox'
									class='checkbox' name='details[]' id='details'
									value="<?php echo $allDetails->getId(); ?>" /><?php echo $allDetails->getName(); ?>
                                            <br />
                                        <?php
																												} // if Já add o checkbox
																											}
																										} else {
																											?>
                                    
                                <?php
																											foreach ( $details as $iterDetail ) {
																												?>
                                        <input type="checkbox"
									name="details[]" value="<?php echo $iterDetail->getId(); ?>"
									id="CheckboxGroup1_0" />
                                                <?php echo $iterDetail->getName(); ?>
                                        <br />
                                    
                                       
                        	<?php
																											}
																										}
																									} else
																										echo "nao há nenhum detalhe cadastrado!";
																									
																									?>
                            
                            
<!--                    	<tr>
                        	<td width="190"><input type="checkbox" name="CheckboxGroup1" value="caixa de seleção" id="CheckboxGroup1_0" />
Caixa de seleção</td>
                            <td width="190"><input type="checkbox" name="CheckboxGroup1_" value="caixa de seleção" id="CheckboxGroup1_" />
Caixa de seleção</td>
                          <td width="190"><input type="checkbox" name="CheckboxGroup1_2" value="caixa de seleção" id="CheckboxGroup1_2" />
Caixa de seleção</td>
                          <td width="190"><input type="checkbox" name="CheckboxGroup1_3" value="caixa de seleção" id="CheckboxGroup1_3" />
Caixa de seleção</td>
                        </tr>
                        <tr>
                        	<td width="190"><input type="checkbox" name="CheckboxGroup1" value="caixa de seleção" id="CheckboxGroup1_0" />
Caixa de seleção</td>
                            <td width="190"><input type="checkbox" name="CheckboxGroup1_" value="caixa de seleção" id="CheckboxGroup1_" />
Caixa de seleção</td>
                          <td width="190"><input type="checkbox" name="CheckboxGroup1_2" value="caixa de seleção" id="CheckboxGroup1_2" />
Caixa de seleção</td>
                          <td width="190"><input type="checkbox" name="CheckboxGroup1_3" value="caixa de seleção" id="CheckboxGroup1_3" />
Caixa de seleção</td>
                        </tr>
                        <tr>
                        	<td width="190"><input type="checkbox" name="CheckboxGroup1" value="caixa de seleção" id="CheckboxGroup1_0" />
Caixa de seleção</td>
                            <td width="190"><input type="checkbox" name="CheckboxGroup1_" value="caixa de seleção" id="CheckboxGroup1_" />
Caixa de seleção</td>
                          <td width="190"><input type="checkbox" name="CheckboxGroup1_2" value="caixa de seleção" id="CheckboxGroup1_2" />
Caixa de seleção</td>
                          <td width="190"><input type="checkbox" name="CheckboxGroup1_3" value="caixa de seleção" id="CheckboxGroup1_3" />
Caixa de seleção</td>
                        </tr>
                        <tr>
                        	<td width="190"><input type="checkbox" name="CheckboxGroup1" value="caixa de seleção" id="CheckboxGroup1_0" />
Caixa de seleção</td>
                            <td width="190"><input type="checkbox" name="CheckboxGroup1_" value="caixa de seleção" id="CheckboxGroup1_" />
Caixa de seleção</td>
                          <td width="190"><input type="checkbox" name="CheckboxGroup1_2" value="caixa de seleção" id="CheckboxGroup1_2" />
Caixa de seleção</td>
                          <td width="190"><input type="checkbox" name="CheckboxGroup1_3" value="caixa de seleção" id="CheckboxGroup1_3" />
Caixa de seleção</td>
                        </tr>-->

								<!--</table>-->
							</div>

							<div id="cxTit">Proximidades</div>
							<div id="cxTex">
                    
                    <?php
																				
																				// buscar todos os detalhes cadastrados
																				$nearController = new NearController ();
																				$nearsList = $nearController->getAll ( NULL, "name ASC" );
																				
																				if (! is_null ( $nearsList )) {
																					
																					if (! is_null ( $nearsInProperty )) {
																						
																						// varrer todos os detalhes cadastrados e caso algum bata com os selecionados no cadastro, marque-os
																						foreach ( $nearsList as $allNears ) {
																							$flag = false; // to validate later
																							foreach ( $nearsInProperty as $nearsInserted ) {
																								?>
                                                <?php
																								
																								if ($allNears->getId () == $nearsInserted->getNear ()->getId ()) {
																									?>
                                                        <input
									type="checkbox" name="nears[]" id="nears"
									value="<?php echo $allNears->getId(); ?>" checked="true" /><?php echo $allNears->getName(); ?>
                                                        <br />
                                                <?php
																									$flag = true;
																									break;
																								}
																								?>

                                           </label>
                                        <?php
																							} // foreach serviceProvider (servicos que o usuário possui cadastrados)
																							?>
                                        <?php if (!$flag) { //se estiver false é que o usuário não tem esse serviço cadastrado a ele ?> 
                                            <input type='checkbox'
									class='checkbox' name='nears[]' id='nears'
									value="<?php echo $allNears->getId(); ?>" /><?php echo $allNears->getName(); ?>
                                            <br />
                                        <?php
																							} // if Já add o checkbox
																						}
																					} else {
																						?>
                                    
                                <?php
																						foreach ( $nearsList as $iterNear ) {
																							?>
                                        <input type="checkbox"
									name="nears[]" value="<?php echo $iterNear->getId(); ?>"
									id="CheckboxGroup1_0" />
                                                <?php echo $iterNear->getName(); ?>
                                        <br />
                                    
                                       
                        	<?php
																						}
																					}
																				} else
																					echo "nao há nenhuma proximidade cadastrada!";
																				
																				?>
<!--                	<table width="700">
                    	<tr>
                        	<td width="190"><input type="checkbox" name="CheckboxGroup1" value="caixa de seleção" id="CheckboxGroup1_0" />
Caixa de seleção</td>
                            <td width="190"><input type="checkbox" name="CheckboxGroup1_" value="caixa de seleção" id="CheckboxGroup1_" />
Caixa de seleção</td>
                          <td width="190"><input type="checkbox" name="CheckboxGroup1_2" value="caixa de seleção" id="CheckboxGroup1_2" />
Caixa de seleção</td>
                          <td width="190"><input type="checkbox" name="CheckboxGroup1_3" value="caixa de seleção" id="CheckboxGroup1_3" />
Caixa de seleção</td>
                        </tr>
                        <tr>
                        	<td width="190"><input type="checkbox" name="CheckboxGroup1" value="caixa de seleção" id="CheckboxGroup1_0" />
Caixa de seleção</td>
                            <td width="190"><input type="checkbox" name="CheckboxGroup1_" value="caixa de seleção" id="CheckboxGroup1_" />
Caixa de seleção</td>
                          <td width="190"><input type="checkbox" name="CheckboxGroup1_2" value="caixa de seleção" id="CheckboxGroup1_2" />
Caixa de seleção</td>
                          <td width="190"><input type="checkbox" name="CheckboxGroup1_3" value="caixa de seleção" id="CheckboxGroup1_3" />
Caixa de seleção</td>
                        </tr>
                        <tr>
                        	<td width="190"><input type="checkbox" name="CheckboxGroup1" value="caixa de seleção" id="CheckboxGroup1_0" />
Caixa de seleção</td>
                            <td width="190"><input type="checkbox" name="CheckboxGroup1_" value="caixa de seleção" id="CheckboxGroup1_" />
Caixa de seleção</td>
                          <td width="190"><input type="checkbox" name="CheckboxGroup1_2" value="caixa de seleção" id="CheckboxGroup1_2" />
Caixa de seleção</td>
                          <td width="190"><input type="checkbox" name="CheckboxGroup1_3" value="caixa de seleção" id="CheckboxGroup1_3" />
Caixa de seleção</td>
                        </tr>
                        <tr>
                        	<td width="190"><input type="checkbox" name="CheckboxGroup1" value="caixa de seleção" id="CheckboxGroup1_0" />
Caixa de seleção</td>
                            <td width="190"><input type="checkbox" name="CheckboxGroup1_" value="caixa de seleção" id="CheckboxGroup1_" />
Caixa de seleção</td>
                          <td width="190"><input type="checkbox" name="CheckboxGroup1_2" value="caixa de seleção" id="CheckboxGroup1_2" />
Caixa de seleção</td>
                          <td width="190"><input type="checkbox" name="CheckboxGroup1_3" value="caixa de seleção" id="CheckboxGroup1_3" />
Caixa de seleção</td>
                        </tr>
                       
                    </table>-->
							</div>

							<!--                 <div id="cxTit">Vincular Imobiliárias ao Imóvel</div>
                <div id="cxTex">
                	<table width="396">
                    	<tr>
                        	<td width="190"><input type="checkbox" name="CheckboxGroup1" value="caixa de seleção" id="CheckboxGroup1_0" />
                        	  Imobiliária 1</td>
                            <td width="190"><input type="checkbox" name="CheckboxGroup1_" value="caixa de seleção" id="CheckboxGroup1_" />
                            Imobiliária 5</td>
                        </tr>
                        <tr>
                        	<td width="190"><input type="checkbox" name="CheckboxGroup1" value="caixa de seleção" id="CheckboxGroup1_0" />
                       	    Imobiliária 2</td>
                            <td width="190"><input type="checkbox" name="CheckboxGroup1_" value="caixa de seleção" id="CheckboxGroup1_" />
                            Imobiliária 6</td>
                        </tr>
                        <tr>
                        	<td width="190"><input type="checkbox" name="CheckboxGroup1" value="caixa de seleção" id="CheckboxGroup1_0" />
                       	    Imobiliária 3</td>
                            <td width="190"><input type="checkbox" name="CheckboxGroup1_" value="caixa de seleção" id="CheckboxGroup1_" />
                            Imobiliária 7</td>
                        </tr>
                        <tr>
                        	<td width="190"><input type="checkbox" name="CheckboxGroup1" value="caixa de seleção" id="CheckboxGroup1_0" />
                       	    Imobiliária 4</td>
                            <td width="190"><input type="checkbox" name="CheckboxGroup1_" value="caixa de seleção" id="CheckboxGroup1_" />
                            Imobiliária 8</td>
                        </tr>
                       
                    </table>
              </div>-->

							<div id="cxTit">Áreas</div>
							<div id="cxTex">
								<table width="700">
									<tr>
										<td width="180" align="right">Área Construída:</td>
										<td width="520"><input
											class="textfield required areaConstruida"
											name="areaConstruida" type="text" id="user5"
											value="<?php echo number_format($propertyUpdate->getAreaConstruida(), 2, '.', ','); ?>" /></td>
									</tr>
									<tr>
										<td colspan="2" height="5"></td>
									</tr>
									<tr>
										<td width="180" align="right">Área Total:</td>
										<td width="520"><input class="textfield required areaTotal"
											name="areaTotal" type="text" id="user2"
											value="<?php echo number_format($propertyUpdate->getAreaTotal(), 2, '.', ','); ?>" /></td>
									</tr>
									<tr>
										<td width="180" align="right">Comprimento:</td>
										<td width="520"><input class="textfield required comprimento"
											name="comprimento" type="text" id="user2"
											value="<?php echo number_format($propertyUpdate->getComprimento(), 2, '.', ','); ?>" /></td>
									</tr>
									<tr>
										<td colspan="2" height="5"></td>
									</tr>
									<tr>
										<td width="180" align="right">Largura:</td>
										<td width="520"><input class="textfield required largura"
											name="largura" type="text" id="user2"
											value="<?php echo number_format($propertyUpdate->getLargura(), 2, '.', ','); ?>" /></td>
									</tr>
									<tr>
										<td colspan="2" height="5"></td>
									</tr>
									<tr>
										<td width="180" align="right">Edicula:</td>
										<td width="520"><input class="textfield required edicula"
											name="edicula" type="text" id="user2"
											value="<?php echo number_format($propertyUpdate->getEdicula(), 2, '.', ','); ?>" /></td>
									</tr>
									<tr>
										<td colspan="2" height="5"></td>
									</tr>
								</table>
							</div>

							<div id="cxTit">Valores</div>
							<div id="cxTex">
								<table width="700">
									<tr>
										<td width="180" align="right">* Preço:</td>
										<td width="50"><input class="textfield required price"
											name="price"
											value="<?php echo number_format($propertyUpdate->getPrice(), 2, '.', ','); ?>" /></td>
									</tr>
									<tr>
										<td colspan="2" height="5"></td>
									</tr>
									<tr>
										<td width="180" align="right">Valor Condomínio:</td>
										<td width="520"><input
											class="textfield required valorCondominio"
											name="valorCondominio" type="text" id="user2"
											value="<?php echo number_format($propertyUpdate->getValorCondominio(), 2, '.', ','); ?>" /></td>
									</tr>
									<tr>
										<td colspan="2" height="5"></td>
									</tr>
									<tr>
										<td width="180" align="right">Valor do IPTU:</td>
										<td width="520"><input class="textfield required valorIPTU"
											name="valorIPTU" type="text" id="user2"
											value="<?php echo number_format($propertyUpdate->getValorIPTU(), 2, '.', ','); ?>" /></td>
									</tr>
									<tr>
										<td colspan="2" height="5"></td>
									</tr>

								</table>
							</div>

							<div id="cxTit">Fotos</div>
                
                <?php
																
																if (! is_null ( $photos )) {
																	foreach ( $photos as $iter ) {
																		$picture = $iter->getPath ();
																		$path = URL_PROPERTY_PHOTOS . basename ( $picture );
																		?>
                        <div class="photos-of-property">
								<img class="tamanho-foto-property-edit"
									src="<?php echo $path; ?>" /> <a
									href="action/updatePropertyForm.action.php?action=deletePropertyPhoto&idPropertyPhoto=<?php echo $iter->getId(); ?>&idProperty=<?php echo $propertyIdToDeletePhoto; ?>">
									<img src="./images/icon-del.png" width="16px" height="16px"
									title="Deletar Foto" />
								</a>

							</div>
                   <?php
																	}
																}
																?>
                
                <div id="cxTex">
								<table width="700">
									<tr>
										<td width="180" align="right">Foto 1:</td>
										<td width="400"><input type="file" class="submit file"
											name="property_photo[]" /></td>
									</tr>
									<tr>
										<td colspan="2" height="5"></td>
									</tr>
									<tr>
										<td width="180" align="right">Foto 2:</td>
										<td width="400"><input type="file" class="submit file"
											name="property_photo[]" /></td>
									</tr>
									<tr>
										<td colspan="2" height="5"></td>
									</tr>
									<tr>
										<td width="180" align="right">Foto 3:</td>
										<td width="400"><input type="file" class="submit file"
											name="property_photo[]" /></td>
									</tr>
									<tr>
										<td colspan="2" height="5"></td>
									</tr>
									<tr>
										<td width="180" align="right">Foto 4:</td>
										<td width="400"><input type="file" class="submit file"
											name="property_photo[]" /></td>
									</tr>
									<tr>
										<td colspan="2" height="5"></td>
									</tr>
									<tr>
										<td width="180" align="right">Foto 5:</td>
										<td width="400"><input type="file" class="submit file"
											name="property_photo[]" /></td>
									</tr>
									<tr>
										<td colspan="2" height="5"><b>Tamanho da imagem:</b> Mínimo
											800x800 máximo 2000x1800</td>
									</tr>

									<tr>
										<td width="180" align="right"><input type="hidden"
											name="oldStatus"
											value="<?php echo $propertyStatus->getStatus()->getId(); ?>" />
											<input type="hidden" name="property_id"
											value="<?php echo $_GET['property_id']; ?>"> <input
											type="submit" name="Enviar" id="Enviar" value="Atualizar" />
										</td>
										<td width="520"><input type="button" name="voltar"
											id="Cancelar" value="Voltar" onclick="history.back()" /></td>
									</tr>
								</table>
							</div>
					
					</form>
					<!-- /FORM -->
				</div>
			</div>
			<!-- CONTEUDO -->

		</div>
		<!-- /MAIN CONTEUDO -->


		<!-- FOOTER -->
    <?php
				require_once PATH_INCLUDES_ADMIN . 'footer.html';
				?>
    <!-- /FOOTER -->



	</div>
	<!-- /GERAL -->

	<!-- End conteiner -->
	<!-- JS Content and JSLibary -->
	<script type="text/javascript" src="js/jquery-1.8.3.js"></script>

	<!-- includes for multiple select -->
	<script type="text/javascript" src="js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/jquery.multiselect.min.js"></script>
	<!-- end multiple select include -->

	<!-- includes for maskedinput -->
	<script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>
	<!-- end maskedinput includes -->

	<!-- includes for jquery validate and their messages -->
	<script type="text/javascript" src="js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="js/messages_pt_BR.js"></script>
	<!-- end jquery validate and their messages includes -->

	<!-- includes for jquery price -->
	<script type="text/javascript" src="js/jquery.price_format.1.7.min.js"></script>
	<!-- end jquery price include -->

	<script type="text/javascript">
      $(document).ready(function(){
          /*
           * Comentar as linhas abaixo, pois aparecem sempre faltando 00 no final quando
           * termina com 00 então só irei mostrar o float Value deles
           */
        $('.price').priceFormat({
              prefix: '',
              centsSeparator: '.',
              thousandsSeparator: ','
        });
        $('.areaConstruida').priceFormat({
              prefix: '',
              centsSeparator: '.',
              thousandsSeparator: ','
        });
        $('.areaTotal').priceFormat({
              prefix: '',
              centsSeparator: '.',
              thousandsSeparator: ','
        });
        $('.comprimento').priceFormat({
              prefix: '',
              centsSeparator: '.',
              thousandsSeparator: ','
        });
        $('.largura').priceFormat({
              prefix: '',
              centsSeparator: '.',
              thousandsSeparator: ','
        });
        $('.edicula').priceFormat({
              prefix: '',
              centsSeparator: '.',
              thousandsSeparator: ','
        });
        $('.valorCondominio').priceFormat({
              prefix: '',
              centsSeparator: '.',
              thousandsSeparator: ','
        });
        $('.valorIPTU').priceFormat({
              prefix: '',
              centsSeparator: '.',
              thousandsSeparator: ','
        });
        
        /*
         * não usarei, a linha abaixo, pois não quero iniciar a minha
         * página com uma opção marcada e sim deixar marcada a que 
         * já veio do imóvel
         */
//        $('input[value=1]', '#status').attr('checked', 'checked');
        $("#cep").mask('99999-999');
        $('#contactPhone').mask('(99) 9999.9999');
        $("#frmCadProperty").validate();
//        $('#temporadaConfig').css({'display' : 'none'});
        $("#daysOfWeek").multiselect({
          noneSelectedText: 'Dias da semana',
          selectedText: 'Dias da semana',
          checkAllText: 'Marque todos',
          uncheckAllText: 'Desmarque todos'
        });
        $("#monthOfYear").multiselect({
          noneSelectedText: 'Meses do ano',
          selectedText: 'Meses do ano',
          checkAllText: 'Marque todos',
          uncheckAllText: 'Desmarque todos'
        });
        
        $('input[value=1], input[value=2]', '#status').change(function(){
          if(!$('input[value=3]', '#status').is(':checked')){
            $('#temporadaConfig').css({'display' : 'none'});
          }
        });
        
        $('input[value=3]', '#status').change(function(){
          if($('input[value=3]', '#status').is(':checked')){
            $('#temporadaConfig').css({'display' : 'inline-block'});  
          }
        });
      });
    </script>


</body>

</html>