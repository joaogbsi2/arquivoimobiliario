<?php

/*
 * INCLUDE SECTOR
 */

// $_SERVER['DOCUMENT_ROOT'] = "/home/arqui937/public_html/";
// define ("PATH", $_SERVER['DOCUMENT_ROOT']);

// include the file of configuration
// require_once '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_INCLUDES_ADMIN . 'session.php';

require_once PATH_CONTROLLER . 'NearController.class.php';
require_once PATH_MODEL_ENTITIES . 'Near.class.php';

if (isset ( $_GET ['near_id'] )) {
	
	$nearController = new NearController ();
	$nearToUpdate = $nearController->getById ( $_GET ['near_id'] );
} else {
	header ( "location:" . URL_ADMIN_PAGE );
}

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="shortcut icon" href="images/arquiLogo.png">
<title>Arquivo Imobiliário</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>

<script type="text/javascript">

    /*
     * create a function that will go ask if the user want really delete the user 
     */
    function doYouWantDelete(){
        
        var answer = window.confirm("Deseja realmente remover esse detalhe?");
        
        if(answer)
            return true;
        
        return false;
    }
</script>

<body>

	<!-- GERAL -->
	<div id="geral">


		<!-- TOPO -->
    <?php
				require_once PATH_INCLUDES_ADMIN . 'header.php';
				?>
    <!-- /TOPO -->


		<!-- MAIN CONTEUDO -->
		<div id="mainConteudo">

			<!-- CONTEUDO -->
			<div id="conteudo">

				<!-- ADMIN COL1 -->
                <?php
																require_once PATH_INCLUDES_ADMIN . 'right_colum.html';
																?>
                <!-- /ADMIN COL1 -->

				<div id="adm_COL2">

					<div id="Tit">Proximidades</div>

					<div id="cxInfo"></div>


					<div id="Tit">Atualizar proximidade</div>
					<form class="validate" id="frmUpdDetail"
						action="action/cadNear.action.php" method="post">

						<div id="cxBusca">
							<table width="527">
								<tr>
									<td width="103" align="right">Proximidade:</td>
									<td width="400"><input name="near" type="text" class="campo1"
										id="user" value="<?php echo $nearToUpdate->getName(); ?>" /></td>
								</tr>
								<tr>
									<td></td>
									<td align="right"><input type="hidden" name="action"
										value="performUpdate"> <input type="hidden" name="near_id"
										value="<?php echo $nearToUpdate->getId(); ?>" /> <input
										type="submit" name="Enviar" id="Enviar" value="Atualizar" /> <input
										type="button" name="voltar" value="Cancelar"
										onclick="history.back()" /></td>
								</tr>
							</table>
						</div>

					</form>


				</div>

			</div>
			<!-- CONTEUDO -->

		</div>
		<!-- /MAIN CONTEUDO -->


		<!-- FOOTER -->
		<div id="footer">
			<div id="conteudoFooter">
				<div id="ass">2013 © Arquivo Imobiliário. Todos os direitos
					reservados.</div>
			</div>
		</div>
		<!-- /FOOTER -->



	</div>
	<!-- /GERAL -->


</body>

</html>