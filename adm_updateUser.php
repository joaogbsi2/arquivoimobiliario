<?php

/*
 * INCLUDE SECTOR
 */

// $_SERVER['DOCUMENT_ROOT'] = "/home/arqui937/public_html/";
// define ("PATH", $_SERVER['DOCUMENT_ROOT']);

// include the file of configuration
// require_once '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_INCLUDES_ADMIN . 'session.php';
// require_once PATH_ACTION. 'cadUser.action.php';
require_once PATH_MODEL_ENTITIES . 'User.class.php';
require_once PATH_CONTROLLER . 'ServiceController.class.php';
require_once PATH_MODEL_ENTITIES . 'Service.class.php';

require_once PATH_CONTROLLER . 'UserController.class.php';
require_once PATH_CONTROLLER . 'UserServiceController.class.php';
require_once PATH_MODEL_ENTITIES . 'UserService.class.php';

/*
 * Agora pegar o id do user e buscar o Transfer objeto
 * para preencher os campos com os dados do usuário selecionado
 * para ser atualizado
 */

/*
 * First verify if user select a user to edit
 * case no then return to admin index page
 */
if (isset ( $_GET ['user_id'] )) {
	
	$userController = new UserController ();
	$userToUpdate = $userController->getById ( $_GET ['user_id'] );
	
	/*
	 * Set attributes phone and mobilePhone
	 * case the attributes is null or ""
	 */
	if ($userToUpdate->getPhone () == "" || $userToUpdate->getPhone () == NULL)
		$userToUpdate->setPhone ( "(00) 0000.0000" );
	if ($userToUpdate->getMobilePhone () == "" || $userToUpdate->getMobilePhone () == NULL)
		$userToUpdate->setMobilePhone ( "(00) 0000.0000" );
	
	$userServiceController = new UserServiceController ();
	$userServicesProvider = $userServiceController->getServiceProvider ( $_GET ['user_id'] );
} else {
	header ( "location:" . URL_ADMIN_PAGE );
}

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon" href="images/arquiLogo.png">
<title>Arquivo Imobiliário</title>
<link href="style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/jquery.multiselect.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />


</head>

<body>

	<!-- GERAL -->
	<div id="geral">


		<!-- TOPO -->
    <?php
				require_once PATH_INCLUDES_ADMIN . 'header.php';
				?>
	<!-- /TOPO -->


		<!-- MAIN CONTEUDO -->
		<div id="mainConteudo">

			<!-- CONTEUDO -->
			<div id="conteudo">

				<!-- ADMIN COL1 -->
            <?php
												require_once PATH_INCLUDES_ADMIN . 'right_colum.html';
												?>
            <!-- /ADMIN COL1 -->

				<!-- AREA DE CADASTRO -->
				<div id="adm_COL2">
					<!-- FORM -->
					<form class="validate" id="frmUpdUser"
						action="action/cadUser.action.php" method="post"
						enctype="multipart/form-data">

						<div id="Tit">Cadastro de Usuário</div>
						<div id="cxTex">
							<table width="700">
								<tr>
									<td width="180" align="right">* Nome:</td>
									<td width="50"><input class="textfield" id="name" name="name"
										value="<?php echo $userToUpdate->getName(); ?>" /></td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>

								<tr>
									<td width="180" align="right">Celular:</td>
									<td width="50"><input class="textfield" id="mobile_phone"
										name="mobile_phone"
										value="<?php echo $userToUpdate->getMobilePhone(); ?>" /></td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>

								<tr>
									<td width="180" align="right">Telefone:</td>
									<td width="50"><input class="textfield" id="phone" name="phone"
										value="<?php echo $userToUpdate->getPhone(); ?>" /></td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>

								<tr>
									<td width="180" align="right">Foto:</td>
									<td width="50"><input type="file" class="file" id="photo"
										name="photo" /></td>
								</tr>
								<tr>
									<td colspan="2" height="5" align="right"><b>Tamanho da imagem:
									</b>Mínimo 600x600 máximo 1200x1000</td>
								</tr>

								<tr>
									<td width="180" align="right">E-mail:</td>
									<td width="50"><input class="textfield" id="email" name="email"
										value="<?php echo $userToUpdate->getEmail(); ?>" /></td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>

							</table>
						</div>
						<div id="cxTit">Prestação de serviço</div>
						<div id="cxTex">
							<table width="700">
								<!-- Edilson Justiniano alterou essa parte aqui. Eu add esse campo aqui nesse formulário
                                     para que o usuário selecione tudo em uma tela apenas e faça esse cadastro dessa 
                                     forma, acredito que assim ficará mais simples para realizar a edição 
                                -->

								<tr>
									<td width="180" align="right">Você é um prestador de serviços?</td>
									<td width="50">
                                        <?php
																																								
																																								// Get all services.
																																								$serviceController = new ServiceController ();
																																								$services = $serviceController->getAllServices ();
																																								
																																								if (! is_null ( $services )) {
																																									
																																									if (! is_null ( $userServicesProvider )) {
																																										?>
                                                
                                                    SIM<input
										type="radio" name="youAreServiceProvider" class="radio"
										id="iAmServiceProvider" value="yes" checked="true" /> NÃO<input
										type="radio" name="youAreServiceProvider" class="radio"
										id="iAmServiceProvider" value="no" /> <input type="hidden"
										name="wasServiceProvider" id="wasServiceProvider" value="true" />
										<div id="hidden">
                                                    <?php
																																										foreach ( $services as $service ) {
																																											$flag = false;
																																											foreach ( $userServicesProvider as $serviceProvider ) {
																																												?>
                                                            <label
												for="<?php echo $service->getServiceDescription(); ?>">
                                                                <?php
																																												if ($service->getId () == $serviceProvider->getService ()->getId () && (! is_null ( $serviceProvider ))) {
																																													?>
                                                                      <input
												type='checkbox' class='checkbox' name='serviceProvider[]'
												id='serviceProvider'
												value="<?php echo $service->getId(); ?>" checked="true" /><?php echo $service->getServiceDescription(); ?>
                                                                      <br />
                                                                <?php
																																													$flag = true;
																																													break;
																																												}
																																												?>

                                                            </label>
                                                        <?php
																																											} // foreach serviceProvider (servicos que o usuário possui cadastrados)
																																											?>
                                                        <?php if (!$flag) { //se estiver false é que o usuário não tem esse serviço cadastrado a ele ?> 
                                                            <input
												type='checkbox' class='checkbox' name='serviceProvider[]'
												id='serviceProvider'
												value="<?php echo $service->getId(); ?>" /><?php echo $service->getServiceDescription(); ?>
                                                            <br />
                                                        <?php
																																											
} // if Já add o checkbox
																																										} // foreach service (ALL SERVICES)
																																									} else {
																																										?>
                                                    SIM<input
												type="radio" name="youAreServiceProvider" class="radio"
												id="iAmServiceProvider" value="yes" /> NÃO<input
												type="radio" name="youAreServiceProvider" class="radio"
												id="iAmServiceProvider" value="no" checked="true" /> <input
												type="hidden" name="wasServiceProvider"
												id="wasServiceProvider" value="false" />
											<div id="hidden">
                                                    <?php
																																										foreach ( $services as $service ) {
																																											?>
                                                            <label
													for="<?php echo $service->getServiceDescription(); ?>"> <input
													type='checkbox' class='checkbox' name='serviceProvider[]'
													id='serviceProvider'
													value="<?php echo $service->getId(); ?>" /><?php echo $service->getServiceDescription(); ?>
                                                                <br />
												</label>
                                                    <?php
																																										
}
																																									}
																																									?>
                                                
                                          <?php
																																								} else { // Nenhum serviço cadastrado no banco de dados ?>
                                            Nenhuma categoria de serviços disponível no momento!
                                          <?php
																																									echo "<span class='error'>Nenhuma categoria de serviços disponível no momento</span>";
																																								}
																																								
																																								?>
                                      </div>
									
									</td>
								</tr>
								<tr>
									<td colspan="2" height="5"></td>
								</tr>
							</table>

							<table width="700">
								<!-- SUBMIT BUTTONS -->
								<tr>
									<td width="280" align="right"><input type="hidden"
										name="user_id" value="<?php echo $userToUpdate->getId(); ?>" />
										<input type="hidden" name="action" value="update" /> <input
										type="submit" name="Enviar" id="Enviar" value="Atualizar" /></td>
									<td width="420"><input type="button" name="voltar"
										id="Cancelar" value="Voltar" onclick="history.back()" /></td>
								</tr>
							</table>
						</div>
				
				</div>
				<!-- /ADM_COL2 -->
				</form>
				<!-- /FORM -->
			</div>
		</div>
		<!-- CONTEUDO -->

	</div>
	<!-- /MAIN CONTEUDO -->


	<!-- FOOTER -->
    <?php
				require_once PATH_INCLUDES_ADMIN . 'footer.html';
				?>
    <!-- /FOOTER -->



	</div>
	<!-- /GERAL -->



	<!-- Plugins JQuery and JS content -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"
		type="text/javascript"></script>
	<script
		src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"
		type="text/javascript"></script>
	<script src="js/jquery.maskedinput-1.3.min.js" type="text/javascript"></script>
	<script src="js/jquery.jget.js" type="text/javascript"></script>
	<script type="text/javascript">
      $(document).ready(function(){
        // Valida form
        $("#frmUpdUser").validate({
          // Define as regras
          rules:{
            name:{
              required: true, minlength: 2
            },
            passwd1:{
              required: true
            },
            passwd2:{
              required: true, equalTo: "#passwd1"
            }
          },
          // Define as mensagens de erro para cada regra
          messages:{
            name:{
              required: "Digite o seu nome",
              minlength: "O seu nome deve conter, no mínimo, 2 caracteres"
            },
            passwd1:{
              required: "Digite uma senha"
            },
            passwd2:{
              required: "Confirme a senha digitada", equalTo: "As senhas não são idênticas."
            }
          }
        });
        //Input mask. 
        $('#phone').mask('(99) 9999.9999');
        $('#mobile_phone').mask('(99) 9999.9999');
        var wasServiceProvider = $('#wasServiceProvider').val();
        if (wasServiceProvider != "true"){
            $("#hidden").hide(1000);
        }
//        $("input[name='wasServiceProvider']").
//        $("#hidden").hide(1000);
        
//        //Case user choice 'no'
//        $(".values").click(function(){
//          if($(":radio[name='youAreServiceProvider']").is(':checked')){
//            if($(":radio[name='youAreServiceProvider']:checked").val() == 'no'){
//              var userId = $.jget['user'];
//              $("#choiceNo").attr('value','no');
////              location.href = "confirmRecordedUser.php?user=" + userId;
//            }
//            else {
//              $("#choiceNo").attr('value','yes');
//            }
//          }
//        });

        
        
        //Show the options for register.
        $(":radio[name='youAreServiceProvider']").click(function(){
          if($(this).val() == 'yes'){
            $("#hidden").show(1000);
          }
          else{
            $("#hidden").hide(1000);
          }
        });
      });
    </script>


</body>

</html>