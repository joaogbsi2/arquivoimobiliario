﻿<?php
$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

// Necessario para carregar imóvel

require_once PATH_INCLUDES_ADMIN . 'session.php';
require_once PATH_ACTION . 'cadProperty.action.php';
require_once PATH_MODEL_ENTITIES . 'State.class.php';
require_once PATH_MODEL_ENTITIES . 'City.class.php';
require_once PATH_MODEL_ENTITIES . 'Status.class.php';
require_once PATH_MODEL_ENTITIES . 'PropertyType.class.php';

require_once PATH_CONTROLLER . 'DetailController.class.php';
require_once PATH_MODEL_ENTITIES . 'Detail.class.php';

require_once PATH_CONTROLLER . 'NearController.class.php';
require_once PATH_MODEL_ENTITIES . 'Near.class.php';

require_once PATH_CONTROLLER . 'StoreController.class.php';
require_once PATH_MODEL_ENTITIES . 'Store.class.php';

/*
 * require_once para carregar o imóvel a ser editado
 * através do método getById no controller PropertyController
 */
require_once PATH_CONTROLLER . 'PropertyController.class.php';

require_once PATH_CONTROLLER . 'PropertyDetailController.class.php';
require_once PATH_MODEL_ENTITIES . 'PropertyDetail.class.php';

require_once PATH_CONTROLLER . 'PropertyNearController.class.php';
require_once PATH_MODEL_ENTITIES . 'PropertyNear.class.php';

require_once PATH_CONTROLLER . 'PropertyStoreController.class.php';
require_once PATH_MODEL_ENTITIES . 'PropertyStore.class.php';

require_once PATH_CONTROLLER . 'PropertyPhotoController.class.php';
require_once PATH_MODEL_ENTITIES . 'PropertyPhoto.class.php';

/*
 * Agora pegar o id da propriedade e buscar o Transfer objeto
 * para preencher os campos com os dados da propriedade selecionada
 * para ser atualizada
 */

/*
 * First verify if user select a property to edit
 * case no then return to admin index page
 */
if (isset ( $_GET ['property_id'] )) {

	$propertyController = new PropertyController ();
	$propertyUpdate = $propertyController->getById ( $_GET ['property_id'] );

	$propertyDetailController = new PropertyDetailController ();
	$detailsInProperty = $propertyDetailController->getPropertyDetail ( $_GET ['property_id'] );

	$propertyNearController = new PropertyNearController ();
	$nearsInProperty = $propertyNearController->getPropertyNear ( $_GET ['property_id'] );

	$propertyStoreController = new PropertyStoreController ();
	// $storesInProperty = $propertyStoreController->getPropertyStore($_GET['property_id']);
	$storesInProperty = $propertyStoreController->getPropertyStore ( $propertyUpdate->getidUnico () );

	$storeController = new StoreController ();
	$stores = $storeController->findAll ( NULL, " NAME ASC" );

	if (! is_null ( $propertyUpdate->getExclusiveStore () )) {

		if ($propertyUpdate->getExclusiveStore () != 0) {
			$storeController = new StoreController ();
			$exclusiveStore = $storeController->getById ( $propertyUpdate->getExclusiveStore () );
		} else {
			$propertyUpdate->setExclusiveStore ( NULL );
		}
	}

	$propertyPhotoController = new PropertyPhotoController ();
	$photos = $propertyPhotoController->getPhotosByProperty ( $_GET ['property_id'] );
	$propertyIdToDeletePhoto = $_GET ['property_id'];
} else {

	header ( "location:" . URL_ADMIN_PAGE );
}

/*
 * Create a new field. Combobox of Store
 */
require_once PATH_CONTROLLER . 'StoreController.class.php';
require_once PATH_MODEL_ENTITIES . 'Store.class.php';

/*
 * Bom caso, eu esteja tentando atualizar um imóvel
 * que possui status = 3 tenho que carregar
 * os dias e meses que podem ser locados conforme
 * estava no momento do cadastro
 */
require_once PATH_CONTROLLER . 'RentToTheseDaysController.class.php';
require_once PATH_CONTROLLER . 'RentToTheseMonthsController.class.php';
require_once PATH_MODEL_ENTITIES . 'RentToTheseDays.class.php';
require_once PATH_MODEL_ENTITIES . 'RentToTheseMonths.class.php';

$rentToTheseDaysController = new RentToTheseDaysController ();
$rentToTheseMonthsController = new RentToTheseMonthsController ();

$storeController = new StoreController ();
$stores = $storeController->findAll ( NULL, " NAME ASC" );

?>
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="images/arquiLogo.png">
	<title>Arquivo Imobiliário</title>
	<link href="style.css" rel="stylesheet" type="text/css" />

</head>

<body>

	<!-- GERAL -->
	<div id="geral">


		<!-- TOPO -->
		<<?php require_once PATH_INCLUDES_ADMIN . 'header.php'; ?>
		<!-- /TOPO -->


		<!-- MAIN CONTEUDO -->
		<div id="mainConteudo">

			<!-- CONTEUDO -->
			<div id="conteudo">

				<div id="adm_COL1">
					 <?php
						require_once PATH_INCLUDES_ADMIN . 'right_colum.html';
						?>
				</div>

				<!-- COL 2 -->
				<div id="adm_COL2">
					<div id="Tit">Detalhes do imóvel</div>

					<div id="cxviewEdit">
						<a href="adm_editviewAddFT.php">add fotos</a> | <a
							href="adm_editImov.php">editar</a> | <a href="adm_exImov.php">excluir</a>
					</div>


					<div id="cxTex">
						<table width="700">
							<tr>
								<td width="130" align="right"><strong>Código do Imóvel:</strong></td>
								<td width="3"></td>
								<td width="556">0008</td>
							</tr>
							<tr>
								<td width="130" align="right"><strong>Operação:</strong></td>
								<td width="3"></td>
								<td width="556">Venda</td>
							</tr>
							<tr>
								<td width="130" align="right"><strong>Endereço:</strong></td>
								<td width="3"></td>
								<td width="556">(Nadir) GMA (endereço está oculto)</td>
							</tr>
							<tr>
								<td width="130" align="right"><strong>Bairro:</strong></td>
								<td width="3"></td>
								<td width="556">BELO HORIZONTE</td>
							</tr>
							<tr>
								<td width="130" align="right"><strong>Cidade:</strong></td>
								<td width="3"></td>
								<td width="556">Pouso Alegre</td>
							</tr>
							<tr>
								<td width="130" align="right"><strong>Visualizações:</strong></td>
								<td width="3"></td>
								<td width="556">2</td>
							</tr>
							<tr>
								<td width="130" align="right"><strong>Cadastro:</strong></td>
								<td width="3"></td>
								<td width="556">18/05/2013 - 10:05:29</td>
							</tr>
						</table>
					</div>

					<div id="cxTit">Áreas</div>
					<div id="cxTex">
						<table width="700">
							<tr>
								<td width="130" align="right"><strong>Área total:</strong></td>
								<td width="3"></td>
								<td width="558">Aproximadamente 350 m²</td>
							</tr>
							<tr>
								<td width="130" align="right"><strong>Área contruída:</strong></td>
								<td width="3"></td>
								<td width="558">24.200 m²</td>
							</tr>
						</table>
					</div>

					<div id="cxTit">Características</div>
					<div id="cxTex"></div>

					<div id="cxTit">Detalhes</div>
					<div id="cxTex">
						<table width="700">
							<tr>
								<td width="231"><img src="images/bgSetinhaList.png" width="13"
									height="13" />Portão Eletrônico</td>
								<td width="230"><img src="images/bgSetinhaList.png" width="13"
									height="13" />Portão Eletrônico</td>
								<td width="223"><img src="images/bgSetinhaList.png" width="13"
									height="13" />Portão Eletrônico</td>
							</tr>
							<tr>
								<td width="231"><img src="images/bgSetinhaList.png" width="13"
									height="13" />Portão Eletrônico</td>
								<td width="230"><img src="images/bgSetinhaList.png" width="13"
									height="13" />Portão Eletrônico</td>
								<td width="223"><img src="images/bgSetinhaList.png" width="13"
									height="13" />Portão Eletrônico</td>
							</tr>
						</table>
					</div>

					<div id="cxTit">Proximidades</div>
					<div id="cxTex">
						<table width="700">
							<tr>
								<td width="231"><img src="images/bgSetinhaList.png" width="13"
									height="13" />Portão Eletrônico</td>
								<td width="230"><img src="images/bgSetinhaList.png" width="13"
									height="13" />Portão Eletrônico</td>
								<td width="223"><img src="images/bgSetinhaList.png" width="13"
									height="13" />Portão Eletrônico</td>
							</tr>
							<tr>
								<td width="231"><img src="images/bgSetinhaList.png" width="13"
									height="13" />Portão Eletrônico</td>
								<td width="230"><img src="images/bgSetinhaList.png" width="13"
									height="13" />Portão Eletrônico</td>
								<td width="223"><img src="images/bgSetinhaList.png" width="13"
									height="13" />Portão Eletrônico</td>
							</tr>
						</table>
					</div>

				</div>
				<!-- /COL 2 -->

			</div>
			<!-- CONTEUDO -->

		</div>
		<!-- /MAIN CONTEUDO -->


		<!-- FOOTER -->
		<?php
		require_once PATH_INCLUDES_ADMIN . 'footer.html';
		?>
		<!-- /FOOTER -->



	</div>
	<!-- /GERAL -->


</body>

</html>