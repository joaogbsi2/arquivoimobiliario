<?php


$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}
require_once PATH_ACTION . 'index.action.php';
require_once PATH_CONTROLLER . 'StatusController.class.php';
require_once PATH_ACTION . 'adminServices.action.php';
require_once PATH_MODEL_ENTITIES . 'Advertising.class.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="images/arquiLogo.png">
	<title>Arquivo Imobiliário</title> <!-- 		<link href="style.css" rel="stylesheet" type="text/css" />	 -->
	<link href="estilo.css" rel="stylesheet" type="text/css" />
	<!-- jquery ui theme and select multiple theme -->
	<link rel="stylesheet" type="text/css"
		href="css/jquery.multiselect.css" />
	<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
	<!-- Style of Advertisings -->
	<style type="text/css">
<!--
.advertising {
	width: 150px;
	height: 95px;
	margin-left: 3px;
}
-->
</style>

	<!-- Modernizr do Slider em jquery das propagandas principais -->
	<script src="js/modernizr.js"></script>
	<!-- CSS do slider em jquery -->
	<link rel="stylesheet" href="css/flexslider.css" type="text/css"
		media="screen" />

</head>
<body>
	<!-- JS Content -->
	<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>

	<!-- includes for multiple select -->
	<script type="text/javascript" src="js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/jquery.multiselect.min.js"></script>
	<script type="text/javascript" src="js/jquery.validate.min.js"></script>
	<!-- end multiple select include -->

	<!-- includes for jquery price -->
	<script type="text/javascript" src="js/jquery.price_format.1.7.min.js"></script>
	<!-- end jquery price include -->
	<script type="text/javascript">
	    
	    function showVisitMessage(){
	        
	        return alert('Por favor, escolha uma das imobiliarias ao lado!');
	    }   
	</script>
	<!-- Início Geral-->
	<div id="geral">
		<header id="topo">
			<?php
			require_once PATH_INCLUDES_USERS . 'header.php';
			?>
		</header>
		<!-- Inicio Main Conteudo -->
		<div id="mainConteudo">

			<!-- Inicio conteudo -->
			<div id="conteudo">
				<!-- Inicio Conteudo File1 -->
				<div id="home_file1">
					<!-- Inicio Coluna Direita -->
					<div id="hf1_Col1">
					<?php
					require_once PATH_INCLUDES_USERS . 'form-search.php';
					/*Inicio divulgacao*/
					require_once PATH_INCLUDES_USERS . 'window-parceiros.php';
					/*Fim divulgacao*/
					?>
					</div>
					<!-- Fim Coluna direita -->
					<!-- Inicio Coluna esquerda -->
					<!-- Incio Menu -->
					<div id="hf1_Col2">
					<?php
					require_once PATH_INCLUDES_USERS . 'main-menu.php';
					?>
					<!-- Fim Menu -->
					<div id="contQuem">
						<div id="Tit">Anuncie com a gente</div>
						<div id="Desc">
							<div id="lipsum">
								<p class="quemSomosPage">
								Em 2014, o Brasil atingiu 148,2 milhões de pessoas com acesso ao aparelho com conexão a internet. Segundo a Anatel, o país fechou o ano com linhas de 281,1 milhões.O número de lares brasileiros conectados à internet chegou ou 32,3 milhões de domicílios em 2014</p> 

								<p class="quemSomosPage">
								Segundo o Ibope Media, somos 105 milhões de internautas tupiniquins (10/2015), sendo o Brasil o 4º país mais conectado, ultrapassando o Japão.</p>

								<p class="quemSomosPage">
								57,2 milhões de usuários acessam regularmente a Internet. 87% dos internautas brasileiros entram na internet pelo menos uma vez por semana.</p>

								<p class="quemSomosPage">
								A internet se tornou o terceiro veículo de maior alcance no Brasil, atrás apenas de rádio e TV. 87% dos internautas utilizam a rede para pesquisar produtos e serviços. Antes de comprar, 90% dos consumidores ouvem sugestões de pessoas conhecidas, enquanto 70% confiam em opiniões expressas online.</p>

								<p class="quemSomosPage">
								E baseado nessa realidade que acreditamos que o site. arquivoimobiliario.com, é uma excelente fonte de divulgação do seu produto.</p> 

								<p class="quemSomosPage">O valor investido proporcional ao alcance do publico a que se destina é pequeno, pelo beneficio, e a quantidade de pessoas atingidas.</p>

								<p class="quemSomosPage">Confira nossas formatos (banners) para apresentação do seu produto..</p>
								<figure class="fotoLegenda">
									<img src="images/anuncie/1banner.jpg">
									<figcaption>
										<p>Anúncio Banner</p>
									</figcaption>
								</figure>
								
								<figure class="fotoLegenda">
									<img src="images/anuncie/2lateralDireita.jpg">
									<figcaption>
										<p>Anúncio Lateral Direita</p>
									</figcaption>
								</figure>
								
								<figure class="fotoLegenda">
									<img src="images/anuncie/3resultadoBusca.jpg">
									<figcaption>
										<p>Anúncio Resultado Busca</p>
									</figcaption>
								</figure>
								
								<figure class="fotoLegenda">
									<img class="textWrap" src="images/anuncie/4mostra_imovel.jpg">
									<figcaption>
										<p>Anúncio na Página de Exibição do Imóvel</p>
									</figcaption>
								</figure>
								
								<figure class="fotoLegenda">
									<img src="images/anuncie/5cortina.jpg">
									<figcaption>
										<p>Anúncio cortina</p>
									</figcaption>
								</figure>
								
								
							</div>
						</div>
					</div>
					<!-- Fim Anuncie -->
					</div>
					<!-- Incio Anuncie -->
					<!-- Fim Coluna esquerda -->
				</dir>
				<!-- Fim Conteudo File1 -->
			</div>
			<!-- Fim conteudo -->
		</div>
		<!-- Fim Main Conteudo -->
		<!-- Inicio Footer -->
		<footer id="footer">
		<?php
		require_once PATH_INCLUDES_ADMIN . 'footer.html';
		?>
		</footer>
		<!-- Fim Footer -->
	</div>
	<!-- Fim Geral-->
</body>
</html>

<?php
require_once PATH_INCLUDES_USERS . 'functions-js-form-search.html';
?>