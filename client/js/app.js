//URL de acesso de servidor RESTful

SERVER_URL="http://localhost/vendas/sever";


//SERVER_URL = "https://demo-project-c9-danielschmitz.c9.io/angular/livro-angular/sales-server";


var app = angular.module('app', []);

app.config(function($routeProvider, $httpProvider){
	//configura o route
	$routeProvider.
	when('/', {templateUrl:'view/main.html'}).
	when('/clientes', {templateUrl:'view/clientes/main.html', controller:'clientesController'}).
	when('/clientes/new', {templateUrl:'view/clientes/update.html', controller:'clientesController'}).
	when('/cliente/:id', {templateUrl:'view/clientes/update.html', controller:'clientesController'}).
	otherwise({redirectTo:'/'});
	
	//configura o RESPONSE interceptor, usado para exibir o ícone de acesso ao servidor
	//e a exibir uma mensagem de erro coso o servidor retorne algum erro
	
	$httpProvider.responseInterceptors.push(function($q, $rootScope){
		return function(promise){
			$rootScope.hideLoader();
			return promise.then(function(response){
				return (response);
			}, function(response){
				$data = response.data;
				$error = $data.error;
				console.error($data);
				if($error &&$error.text)
					alert("ERRO: "+error.text);
				else{
					if(response.status=404)
						alert(alert("Erro ao acessar servidor. Página não encontrada. Veja 39 rros para maiores detalhes"));
					else
						alert("ERROR! See log console")
				}
				return $q.reject(response);
			});
		}
	});
})

app.run(function($rootScope){
	
	//uma flag que define se o icone de acsso ao servidor deve estar ativo
	$rootScope.showLoaderFlag = false;
	
	//força que o icone de acesso ao servidor seja ativado
	$rootScope.showLoader=function(){
		$rootScope.showLoaderFlag=true;
	}
	
	//força que o icone de acesso ao servidor seja desativado
	$rootScope.hideLoader=function(){
		$rootScope.showLoaderFlag=false;
	}
	
	/*Método que retorna a URL completa de acesso ao servidor
	  Evita usar concatenação no controller*/
	$rootScope.server=function(url){
		return SERVER_URL + url;
	}
	  
});

/*
*/

app.filter('startFrom', function(){
	return function(input, start){
		if(input == null)
			return null;
		start = +start;
		return input.slice(start);
	}
});