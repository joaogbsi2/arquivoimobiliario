

//Clientes Controller
app.controller('clientesController', function($scope,$http, $routeParams, $location){
				
	//lista clientesController
	$scope.rows = null;
	
	//um cliente
	$scope.row = null;
	
	$scope.currentPage = 0;
	$scope.pageSize = 15;
	
	$scope.numberOfPages = function(){
		return Math.ceil($scope.rows.length/$scope.pageSize)
	};
	
	$scope.loadAll = function(){
		$scope.showLoader();
		/* $http.get($scope.server("/costumers")).success(function(data, status){ */
		$http.get($scope.server("/costumers.php")).then(function(response){
			console.log(response.data);
			
			$scope.rows = response.data;
		});
	}
	
	$scope.loadRow = function(){
		if($routeParams.id!=null){
			$scope.showLoader();
			$http.get($scope.server("/costumers/"+$routeParams.id)).success(function(data){
					$scope.row = data;
					$scope.row.isUpdate = true;
				});
		}
		else{
			$scope.row = {}
			$scope.row.costumerId = null;
			$scope.row.isUpdate = false;
		}
	};
	
	$scope.save = function(){
		$scope.showLoader();
		$http.post($scope.server("/costumers/"+$routeParams.id), $scope.row).success(function(data){
			alert("Salvo com sucesso");
			$scope.row.isUpdate = true;
		});
	};
	
	$scope.del = function(){
		if(confirm("Deseja excluir "+$scope.row.costumerId+"?")){
			$http.delete($scope.server("/costumers/"+$routeParams.id)).success(function(s){
				alert("Excluico com sucesso");
				$location.path("/clientes");
			});
		}
	}
})