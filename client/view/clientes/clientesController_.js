//Clientes Controller
$app.controller('clientesController', function($scope,$http, $routParams, $location)){
	//lista clientesController
	$scope.rows = null;
	
	//um cliente
	$scope.row = null;
	
	$scope.currentPage = 0;
	$scope.pageSize = 15;
	
	$scope.numberOfPages = function(){
		return Math.ceil($scope.rows.length/$scope.pageSize)
	};
	
	$scope.loadAll = function(){
		$scope.showLoader();
		$http.get($scope.server("/costumers")).sucess(function(data)){
			$scope.rows = data;
		}
	};
	
	$scope.loadRow = function(){
		if($routParams.id!=null){
			$scope.showLoader();
			$http.get($scope.server("/costumer/"+$routeParams.id)).success(function(data){
					$scope.row = data;
					$scope.row.isUpdate = true;
				});
		}
		else{
			$scope.row = {}
			$scope.row.constumerId = null;
			$scope.row.isUpdate = false;
		}
	};
	
	$scope.save = function(){
		$scope.showLoader();
		$http.post($scope.server("/costumer/"+$routeParans.id), $scope.row).success(function(data){
			alert("Salvo com sucesso");
			$scope.row.isUpdate = true;
		});
	};
	
	$scope.del = function(){
		if(confirm("Deseja excluir "+$scope.row.costumerId+"?")){
			$http.delete($scope.server("/costumer/"+$routeParams.id)).success(function(s){
				alert("Excluico com sucesso");
				$location.path("/clientes");
			});
		}
	}
}