﻿<?php
$pesquisaPorId = false;

/*
 * INCLUDE SECTOR
 */

// $_SERVER['DOCUMENT_ROOT'] = "/home/arqui937/public_html/";
// define ("PATH", $_SERVER['DOCUMENT_ROOT']);

// include the file of configuration
// // require_once '/home/arqui937/public_html/config.php';
// require_once '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_ACTION . 'index.action.php';
require_once PATH_CONTROLLER . 'StatusController.class.php';
require_once PATH_ACTION . 'adminServices.action.php';
require_once PATH_MODEL_ENTITIES . 'Advertising.class.php';
require_once PATH_CONTROLLER . 'SearchPropertyController.class.php';
require_once PATH_MODEL_ENTITIES . 'Property.class.php';
require_once PATH_CONTROLLER . 'PropertyPhotoController.class.php';
require_once PATH_MODEL_ENTITIES . 'PropertyPhoto.class.php';

?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon" href="images/arquiLogo.png">
<title>Arquivo Imobiliário</title>
<link href="style.css" rel="stylesheet" type="text/css" />

<!-- jquery ui theme and select multiple theme -->
<link rel="stylesheet" type="text/css" href="css/jquery.multiselect.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<!-- Style of Advertisings -->
<style type="text/css">
<!--
.advertising {
	width: 150px;
	height: 95px;
	margin-left: 3px;
}

.tamanho-foto {
	padding-top: 3px;
	height: 87px;
	with: 87px;
}
-->
</style>

<!-- Modernizr do Slider em jquery das propagandas principais -->
<script src="js/modernizr.js"></script>
<!-- CSS do slider em jquery -->
<link rel="stylesheet" href="css/flexslider.css" type="text/css"
	media="screen" />

<!-- Class defined by Edilson to limit the width and height of photos of propertys -->

</head>

<body>

	<!-- JS Content -->
	<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>

	<!-- includes for multiple select -->
	<script type="text/javascript" src="js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/jquery.multiselect.min.js"></script>
	<script type="text/javascript" src="js/jquery.validate.min.js"></script>
	<!-- end multiple select include -->

	<!-- includes for jquery price -->
	<script type="text/javascript" src="js/jquery.price_format.1.7.min.js"></script>
	<!-- end jquery price include -->

	<!-- GERAL -->
	<div id="geral">


		<!-- TOPO -->
    <?php
				require_once PATH_INCLUDES_USERS . 'header.php';
				?>
    <!-- /TOPO -->


		<!-- MAIN CONTEUDO -->
		<div id="mainConteudo">

			<!-- CONTEUDO -->
			<div id="conteudo">

				<!-- CONTEUDO COL 1 -->
				<div id="home_COL1">

					<!-- FORM SEARCH -->
                    <?php
																				require_once PATH_INCLUDES_USERS . 'form-search.php';
																				?>
                    <!-- /FORM SEARCH -->

					<!-- CAIXA PARCEIROS -->
                    <?php
																				require_once PATH_INCLUDES_USERS . 'window-parceiros.php';
																				?>
                    <!-- /CAIXA PARCEIROS -->

				</div>
				<!-- /CONTEUDO COL 1 -->

				<!-- CONTEUDO COL 2 -->
				<div id="home_COL2">
					<!-- Menu -->
					<div id="menu">
						<div id="btmenu1">
							<a href="quemsomos.php"><img src="images/btmenu1.png"></a>
						</div>
						<div id="btmenu2">
							<a href="parceiros.php"><img src="images/btmenu2.png"></a>
						</div>
						<div id="btmenu3">
							<a href="comprar.php"><img src="images/btmenu3.png"></a>
						</div>
						<div id="btmenu4">
							<a href="alugar.php"><img src="images/btmenu4.png"></a>
						</div>
						<div id="btmenu5">
							<a href="servicos.php"><img src="images/btmenu5.png"></a>
						</div>
					</div>
					<!-- /Menu -->
                
                <?php
																$adminServicesController = new AdminServicesController ();
																
																$mainAdvertisings = $adminServicesController->getAllMainAdvertisings (); // pegar todas as principais propagandas
																?>
                <div id="bannerImg">
						<div id="container" class="cf" style="margin-top: 53px;">


							<div id="main" role="main" style="width: 664px;">
								<section class="slider">
									<div class="flexslider">
										<ul class="slides">
                       <?php
																							if (! is_null ( $mainAdvertisings ) && is_array ( $mainAdvertisings )) {
																								foreach ( $mainAdvertisings as $iterMainAdvertisings ) {
																									?>

                          <li><img width="670" height="300"
												src="<?php echo URL_MAIN_ADVERTISINGS_PHOTOS. basename($iterMainAdvertisings->getPhoto()); ?>" />
											</li>
                       <?php
																								}
																							}
																							?>

                      </ul>
									</div>
								</section>

							</div>

						</div>

					</div>

					<!-- Conteudo Busca -->
					<div id="Col_Busca">
                    
                    <?php
																				$statusCompra = 2;
																				$limitSearch = 4;
																				
																				$searchPropertyController = new SearchPropertyController ();
																				$result = $searchPropertyController->searchPropertyByStatus ( $statusCompra, $limitSearch );
																				$propertys = $result ['root'];
																				if (count ( $propertys ) == 0) {
																					?>
                            Não há nenhum imóvel para compra cadastrado!
                            
                         <?php
																				}
																				
																				/*
																				 * Function to format the number to better presentation
																				 */
																				function formatNumber($number) {
																					return number_format ( $number, 2, ',', '.' );
																				}
																				
																				/*
																				 * Function for show all found results on the your search
																				 */
																				function showMinProperty($property) {
																					$controller = new PropertyPhotoController ();
																					// echo "ID:: ".$property->getId();
																					$photo = $controller->getUniquePhotoByProperty ( $property->getId () );
																					if (is_null ( $photo )) {
																						$path = 'images/ft1.jpg';
																					} else {
																						$path = URL_PROPERTY_PHOTOS . basename ( $photo );
																					}
																					
																					?>
                                <div id="cxResult">
							<div id="cxTit"><?php echo $property->getCity(); ?></div>
							<div id="cxBgImg">
								<!-- DIV TO SHOW THE PHOTO OF PROPERTY -->
								<div id="imFT">
									<!-- Make a validation before to show the property photo. If not exist
                                                 a photo Then is presented the default image with its default width
                                                 and height. Otherwise, presented a stored photo on database with 
                                                 with and height defined by class "tamanho-foto"
                                            -->
                                            <?php if (is_null($photo)) { ?>
									<!-- Show default photo -->
									<a
										href="imoveisview.php?propertyId=<?php echo $property->getId(); ?>">
										<img src="<?php echo $path; ?>" />
									</a>
                                            <?php } else { ?> <!-- Show Stored Photo -->
									<a
										href="imoveisview.php?propertyId=<?php echo $property->getId(); ?>">
										<img class="tamanho-foto" src="<?php echo $path; ?>" />
									</a>
                                            <?php } ?>
                                        </div>
								<!-- IMAGE OF PROPERTY  -->
							</div>
							<!-- /DIV IMAGE OF PROPERTY -->

							<!--
                                        Agora é tratar o valor para que eu mostre a faixa de 
                                        preço, ao invés do preço em si. Conforme solicitado
                                        pelo Henrique
                                    -->
                                    <?php
																					$faixaValue = NULL;
																					/*
																					 * Correção do bug que agora irá tratar o caso de imóvel ser
																					 * de aluguel ou temporada e setar o valor real neles
																					 * date: 12-02-2014
																					 */
																					if ($property->getStatus () != "Compra")
																						$faixaValue = $property->getPrice ();
																					
																					else if ($property->getPrice () < 150000)
																						$faixaValue = "Até R$ 150 mil";
																					else if ($property->getPrice () < 200000)
																						$faixaValue = "Entre R$ 150 mil e R$ 200 mil";
																					else if ($property->getPrice () < 250000)
																						$faixaValue = "Entre R$ 200 mil e R$ 250 mil";
																					else if ($property->getPrice () < 300000)
																						$faixaValue = "Entre R$ 250 mil e R$ 300 mil";
																					else if ($property->getPrice () < 350000)
																						$faixaValue = "Entre R$ 300 mil e R$ 350 mil";
																					else if ($property->getPrice () < 400000)
																						$faixaValue = "Entre R$ 350 mil e R$ 400 mil";
																					else if ($property->getPrice () < 550000)
																						$faixaValue = "Entre R$ 400 mil e R$ 550 mil";
																					else if ($property->getPrice () < 700000)
																						$faixaValue = "Entre R$ 550 mil e R$ 700 mil";
																					else if ($property->getPrice () < 850000)
																						$faixaValue = "Entre R$ 700 mil e R$ 850 mil";
																					else if ($property->getPrice () < 1000000)
																						$faixaValue = "Entre R$ 850 mil e R$ 1 milhão";
																					else
																						$faixaValue = "Acima de R$ 1 milhão";
																					?>
                                <div id="cxPreco"><?php if ($property->getStatus() != "Compra") echo "R$ ". $faixaValue; else echo $faixaValue; ?></div>
							<br />
							<br />
							<br />
							<div id="cxInfo">
								Quartos:
								<!--<img src="images/imgquartos.png" width="100" height="20" /> -->
                                        <?php echo $property->getQtdRooms(); ?>
                                    </div>
							<div id="cxDesc"><?php echo " ".$property->getType(); ?></div>
							<div id="cxBtmais">
								<a
									href="imoveisview.php?propertyId=<?php echo $property->getId(); ?>"><img
									src="images/bt_mais.png" /></a>
							</div>
						</div>
                    <?php
																				} // eof function showMinProperty
																				
																				if (count ( $propertys ) == 1 && ! is_null ( $propertys ) && $pesquisaPorId)
																					showMinProperty ( $propertys );
																				else
																					for($i = 0; $i < count ( $propertys ); $i ++) {
																						showMinProperty ( $propertys [$i] );
																					}
																				
																				?>
                </div>
					<!-- Conteudo Busca -->

					<!-- Banner Lateral -->
					<div id="Col_Ban">
                	<?php
																	/*
																	 * define a variable limit of advertising here
																	 * and I use this variable on file col-advertising.php
																	 * to limit the advertising which are shown
																	 */
																	$limitAdvertising = 7;
																	require_once PATH_INCLUDES_USERS . 'col-advertising.php';
																	?>
                </div>
					<!-- /Banner Lateral -->

				</div>
				<!-- /CONTEUDO COL 2 -->

			</div>
			<!-- CONTEUDO -->

		</div>
		<!-- /MAIN CONTEUDO -->


		<!-- FOOTER -->
    <?php
				require_once PATH_INCLUDES_ADMIN . 'footer.html';
				?>
    <!-- /FOOTER -->

	</div>
	<!-- /GERAL -->
<?php
require_once PATH_INCLUDES_USERS . 'functions-js-form-search.html';
?>
    
</body>

</html>