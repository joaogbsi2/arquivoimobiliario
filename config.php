<?php

/*
 * File only define some configurations about the path to execution
 */
$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	$_SERVER ['DOCUMENT_ROOT'] = "/home/arqui937/public_html";
	define ( "URL", "http://arquivoimobiliario.com/" ); // define by virtual host created
} else {
	$_SERVER ['DOCUMENT_ROOT'] = "/opt/lampp/htdocs/arquivoImobiliario/";
	define ( "URL", "http://localhost/arquivoImobiliario/" ); // define by virtual host created
}
// define ("URL", $_SERVER['DOCUMENT_ROOT']);
// $_SERVER['DOCUMENT_ROOT'] = "/home/arqui937/public_html";
// $_SERVER ['DOCUMENT_ROOT'] = "/opt/lampp/htdocs/arquivoImobiliario/";
date_default_timezone_set ( 'America/Sao_Paulo' );

define ( "URL_ACTION", URL . '/action/' );
define ( "URL_CONTROLLER", URL . '/controller/' );
define ( "URL_IMAGES", URL . '/images/' );
define ( "URL_PROPERTY_PHOTOS", URL_IMAGES . 'property_photo/' );
define ( "URL_USER_PHOTOS", URL_IMAGES . 'user_photo/' );
define ( "URL_NOTICE_PHOTOS", URL_IMAGES . 'notice_photo/' );
define ( "URL_TECHNOLOGY_PHOTOS", URL_IMAGES . 'technology_photo/' );
define ( "URL_DECORACAO_PHOTOS", URL_IMAGES . 'decoracao_photo/' );
define ( "URL_DICA_PHOTOS", URL_IMAGES . 'dica_photo/' );
define ( "URL_ADVERTISINGS_PHOTOS", URL_IMAGES . 'advertising/' );
define ( "URL_MAIN_ADVERTISINGS_PHOTOS", URL_IMAGES . 'main_advertising/' );
define ( "URL_PARCEIRO_PHOTOS", URL_IMAGES . 'parceiros/' );
define ( "URL_ADMIN_PAGE", URL . 'adm_index.php' );

define ( "PATH", $_SERVER ['DOCUMENT_ROOT'] );
define ( "PATH_ACTION", PATH . '/action/' );
define ( "PATH_CONTROLLER", PATH . '/controller/' );
define ( "PATH_MODEL_BI", PATH . '/model/bi/' );
define ( "PATH_MODEL_DAO", PATH . '/model/dao/' );
define ( "PATH_MODEL_ENTITIES", PATH . '/model/entities/' );
define ( "PATH_IMAGES", PATH . '/images/' );
define ( "PATH_PROPERTY_PHOTOS", PATH_IMAGES . 'property_photo/' );
define ( "PATH_USER_PHOTOS", PATH_IMAGES . 'user_photo/' );
define ( "PATH_ADVERTISING_PHOTOS", PATH_IMAGES . 'advertising/' );
define ( "PATH_MAIN_ADVERTISING_PHOTOS", PATH_IMAGES . 'main_advertising/' );
define ( "PATH_PARCEIRO_PHOTOS", PATH_IMAGES . 'parceiros/' );
define ( "PATH_STORE_PHOTOS", PATH_IMAGES . 'store_photo/' );
define ( "PATH_NOTICE_PHOTOS", PATH_IMAGES . 'notice_photo/' );
define ( "PATH_TECHNOLOGY_PHOTOS", PATH_IMAGES . 'technology_photo/' );
define ( "PATH_DECORACAO_PHOTOS", PATH_IMAGES . 'decoracao_photo/' );
define ( "PATH_DICA_PHOTOS", PATH_IMAGES . 'dica_photo/' );

/*
 * INCLUDES
 */
define ( "PATH_INCLUDES", PATH . "/includes/" );
define ( "PATH_INCLUDES_ADMIN", PATH_INCLUDES . "admin/" );
define ( "PATH_INCLUDES_USERS", PATH_INCLUDES . "users/" );

define ( "PATH_UTIL", PATH . '/Utils/' );
define ( "PATH_UTIL_XML", PATH_UTIL . 'XML/' );
/*
 * MAX WIDTH AND HEIGHT ONE
 */
define ( 'MAX_WIDTH', 380 );
define ( 'MAX_HEIGHT', 300 );
define ( "IMAGE_QUALITY", 95 );

/*
 * EMAIL OF CONTACT
 */
define ( "CONTACT_MAIL", "arquivoimobiliariocontato@gmail.com" );

?>