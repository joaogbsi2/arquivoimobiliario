<?php

/*
 * Date of creating 2014-01-28
 *
 * @author Edilson Justiniano
 */

// include the file of configuration
// require_once './config.php';
// require_once '/home/arqui937/public_html/config.php';
// // require_once '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_CONTROLLER . 'ContactStoreController.class.php';

// receive data
$params = array (
		"name" => trim ( $_POST ['name'] ),
		"mail" => trim ( $_POST ['mail'] ),
		"to" => trim ( $_POST ['to'] ),
		"codeStore" => $_POST ['codeStore'],
		// "phone" => trim($_POST['phone']),
		// "subject" => trim($_POST['subject']),
		"msg" => nl2br ( trim ( $_POST ['msg'] ) ) 
);

$contactStoreController = new ContactStoreController ();
$errorCode = NULL;
if (empty ( $params ["name"] )) {
	$errorCode = 1;
} else if (empty ( $params ["to"] )) {
	$errorCode = 2;
} else if (! (filter_var ( $params ["to"], FILTER_VALIDATE_EMAIL ))) {
	$errorCode = 2;
} else if (empty ( $params ["msg"] )) {
	$errorCode = 3;
}

if (! is_null ( $errorCode ))
	header ( "Location:" . URL . "storeContact.php?code=" . $params ["codeStore"] . "&error=" . $errorCode );
else if ($contactStoreController->sendMail ( $params ))
	header ( "Location:" . URL . "storeContact.php?code=" . $params ["codeStore"] . "&success" );
else
	header ( "Location:" . URL . "storeContact.php?code=" . $params ["codeStore"] . "&error=4" );

?>
