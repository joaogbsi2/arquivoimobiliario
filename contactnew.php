<?php

/*
 * INCLUDE SECTOR
 */

// $_SERVER['DOCUMENT_ROOT'] = "/home/arqui937/public_html/";
// define ("PATH", $_SERVER['DOCUMENT_ROOT']);

// include the file of configuration
// require_once '/home/arqui937/public_html/config.php';
// require_once '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_ACTION . 'index.action.php';
require_once PATH_CONTROLLER . 'StatusController.class.php';
require_once PATH_ACTION . 'adminServices.action.php';
require_once PATH_MODEL_ENTITIES . 'Advertising.class.php';

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon" href="images/arquiLogo.png">
<title>Arquivo Imobiliário</title>
<!-- <link href="style.css" rel="stylesheet" type="text/css" /> -->
<link href="estilo.css" rel="stylesheet" type="text/css" />
<link href="css/formulario.css" rel="stylesheet" type="text/css" />

<!-- jquery ui theme and select multiple theme -->
<link rel="stylesheet" type="text/css" href="css/jquery.multiselect.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />

<!-- Modernizr do Slider em jquery das propagandas principais -->
<script src="js/modernizr.js"></script>
<!-- CSS do slider em jquery -->
<link rel="stylesheet" href="css/flexslider.css" type="text/css"
	media="screen" />

</head>

<body>
	<!-- Inicio Geral -->
	<div id="geral">
		<header id="topo">
			<?php
			require_once PATH_INCLUDES_USERS . 'header.php';
			?>
		</header>
		<!-- Inicio Main Conteudo -->
		<div id="mainConteudo">
			<!-- Inicio Conteudo -->
			<div id="conteudo">
				<!-- Inicio Conteudo File1 -->
				<div id="home_file1">
					<!-- Inicio Coluna esquerda -->
					<div id="hf1_Col1">
						<?php
						require_once PATH_INCLUDES_USERS . 'form-search.php';
						/*Inicio divulgacao*/
						require_once PATH_INCLUDES_USERS . 'window-parceiros.php';
						/*Fim divulgacao*/
						?>
					</div>
					<?php
					if (isset ( $_GET ['success'] )) {
					?>
						 <script type="text/javascript">
                            window.alert("Sua mensagem foi enviada com sucesso!");
                        </script>
					<?php
					}else if (isset ( $_GET ['error'] ) && $_GET ['error'] != NULL) {
						if ($_GET ['error'] == 1) {
						?>
							<script type="text/javascript">
                            	window.alert("Por favor, informe seu nome!");
                        	</script>
						<?php
						}else if ($_GET ['error'] == 2) {
						?>
							<script type="text/javascript">
	                            window.alert("Por favor, informe um e-mail válido!");
	                        </script>
						<?php
						} else if ($_GET ['error'] == 3) {
						?>
							<script type="text/javascript">
	                            window.alert("Por favor, preencha o campo mensagem!");
	                        </script>
						<?php
						} else {
						?>
							 <script type="text/javascript">
	                            window.alert("Erro ao enviar sua mensagem! Tente novamente!");
	                        </script>
						<?php
						}
																		
					}
					?>
					<!-- Fim Coluna Esquerda -->
					<!-- Inicio Coluna direita -->
					<!-- Incio Menu -->
					<div id="hf1_Col2">
					<?php
						require_once PATH_INCLUDES_USERS . 'main-menu.php';
					?>
					<!-- Fim Menu -->
					<!-- Inicio contHm -->
					<div id="contHm">
						<div id="cxCont1">
							<div id="formulario">
								<div id="titulo">Fale Conosco</div>
								<div id="form">
									<form id="frmContact" method="post"
										action="action/contact.action.php" accept-charset="UTF-8">
										<table width="457">
											<tr>
												<td width="65" align="right" class="txt1">* Nome:</td>
												<td width="1"></td>
												<td width="350"><input name="name"
													class="campo1 textfield required" id="name" /></td>
											</tr>
											<tr>
												<td align="right" class="txt1">* E-mail:</td>
												<td width="1"></td>
												<td><input name="mail" placeholder="exemplo@exemplo.com.br"
													class="campo1 textfield required" id="mail" /></td>
											</tr>											
											<tr>
												<td align="right" class="txt1">* Assunto:</td>
												<td width="1"></td>
												<td><select name="subject" class="campo1" id="subject">
														<option value="Imovel">Imóvel</option>
														<option value="Parceria">Parceria</option>
														<option value="Propaganda">Propaganda</option>
														<option value="Imobiliária">Imobiliária</option>
														<option value="Outros">Outros</option>
												</select></td>
											</tr>
											<tr>
												<td align="right" class="txt1">* Mensagem:</td>
												<td width="1"></td>
												<td><textarea name="msg" class="required" id="msg" cols="45"
														rows="8"></textarea></td>
											</tr>
											<tr>
												<td colspan="2"></td>
												<td align="right">
													<!--<a href="#">--> <input type="submit" name="Enviar"
													id="Enviar" value="Enviar" /> <!--<img src="images/bt_enviar.png" width="100" height="30" />-->
													<!--</a>-->
												</td>
											</tr>
										</table>
									</form>
								</div>
							</div>
						</div>
					</div>
					<!-- Fim contHm -->
					</div>
				</div>
				<!-- Fim Conteudo File 1 -->
			</div>
			<!-- Fim Conteudo -->
		</div>		
		<!-- Fim Main Conteudo -->
		<!-- Inicio Footer -->
		<footer id="footer">
		<?php
		require_once PATH_INCLUDES_ADMIN . 'footer.html';
		?>
		</footer>
		<!-- Fim Footer -->
	</div>
	<!-- Fim Geral -->
</body>
</html>

<?php
require_once PATH_INCLUDES_USERS . 'functions-js-form-search.html';
?>