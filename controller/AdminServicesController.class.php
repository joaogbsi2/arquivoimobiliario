<?php

/*
 * date of creating 2013-07-29
 * 
 * @author Edilson Justiano
 */

//include the file of configuration
// require_once  '/home/arqui937/public_html/config.php';
// require_once  '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI .'ConnectionFactoryBI.class.php';
require_once PATH_MODEL_BI .'AdminServicesBI.class.php';

class AdminServicesController {
    
  private $connectionFactoryBI;
  
  
  
  public function cadastreAdvertising(Advertising $advertising){
      
    if(is_null($this->connectionFactoryBI)){
      $this->connectionFactoryBI = new ConnectionFactory();
    }
    
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(FALSE);
    
    $adminServicesBI = new AdminServicesBI($connection);
    
    $advertisingId   = $adminServicesBI->cadastreAdvertising($advertising);
    
    $adminServicesBI->releaseConnection($connection);
    
    return $advertisingId;
  }
  
  
  
  
  public function updateAdvertising(Advertising $advertising){
      
    if(is_null($this->connectionFactoryBI)){
      $this->connectionFactoryBI = new ConnectionFactory();
    }
    
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(FALSE);
    
    $adminServicesBI = new AdminServicesBI($connection);
    
    $advertisingId   = $adminServicesBI->updateAdvertising($advertising);
    
    $adminServicesBI->releaseConnection($connection);
    
    return $advertisingId;
  }
  
  
  public function getByPosition($position) {
    if ($this->connectionFactoryBI == NULL) {
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(FALSE);
    $adminServicesBI = new AdminServicesBI($connection);
    if (!is_null($advertising = $adminServicesBI->getAdvertisingByPosition($position))) {
      $adminServicesBI->releaseConnection($connection);
      return $advertising;
    } else {
      $adminServicesBI->releaseConnection($connection);
      return NULL;
    }
  }
  
  
  public function getAllAdvertisings() {
    if ($this->connectionFactoryBI == NULL) {
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(FALSE);
    $adminServicesBI = new AdminServicesBI($connection);
    if (!is_null($advertisings = $adminServicesBI->getAdvertisings())) {

      $adminServicesBI->releaseConnection($connection);
      return $advertisings;
    }
    $adminServicesBI->releaseConnection($connection);
    return NULL;
  }

  
  /*
   * Mesmo método para verificar se há alguma propaganda 
   * para aquela posiçao caso haja, para que eu posso 
   * deletar ou excluí-la
   */
  public function getMainAdversitingByPosition($position) {
    if ($this->connectionFactoryBI == NULL) {
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(FALSE);
    $adminServicesBI = new AdminServicesBI($connection);
    if (!is_null($mainAdvertisings = $adminServicesBI->getMainAdvertisingByPosition($position))) {
      $adminServicesBI->releaseConnection($connection);
      return $mainAdvertisings;
    } else {
      $adminServicesBI->releaseConnection($connection);
      return NULL;
    }
  }
  
  
  public function updateMainAdvertising(MainAdvertising $mainAdvertising){
      
    if(is_null($this->connectionFactoryBI)){
      $this->connectionFactoryBI = new ConnectionFactory();
    }
    
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(FALSE);
    
    $adminServicesBI = new AdminServicesBI($connection);
    
    $mainAdvertisingId   = $adminServicesBI->updateMainAdvertising($mainAdvertising);
    
    $adminServicesBI->releaseConnection($connection);
    
    return $mainAdvertisingId;
  }
  
  
  public function cadastreMainAdvertising(MainAdvertising $mainAdvertising){
      
    if(is_null($this->connectionFactoryBI)){
      $this->connectionFactoryBI = new ConnectionFactory();
    }
    
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(FALSE);
    
    $adminServicesBI = new AdminServicesBI($connection);
    
    $mainAdvertising   = $adminServicesBI->cadastreMainAdvertising($mainAdvertising);
    
    $adminServicesBI->releaseConnection($connection);
    
    return $mainAdvertising;
  }
  
  
  public function getAllMainAdvertisings() {
    if ($this->connectionFactoryBI == NULL) {
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(FALSE);
    $adminServicesBI = new AdminServicesBI($connection);
    if (!is_null($mainAdvertisings = $adminServicesBI->getMainAdvertisings())) {

      $adminServicesBI->releaseConnection($connection);
      return $mainAdvertisings;
    }
    $adminServicesBI->releaseConnection($connection);
    return NULL;
  }
  
  
  public function getParceiroByPosition($position) {
    if ($this->connectionFactoryBI == NULL) {
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(FALSE);
    $adminServicesBI = new AdminServicesBI($connection);
    if (!is_null($parceiro = $adminServicesBI->getParceiroByPosition($position))) {
      $adminServicesBI->releaseConnection($connection);
      return $parceiro;
    } else {
      $adminServicesBI->releaseConnection($connection);
      return NULL;
    }
  }
  
  public function updateParceiro(Parceiro $parceiro){
      
    if(is_null($this->connectionFactoryBI)){
      $this->connectionFactoryBI = new ConnectionFactory();
    }
    
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(FALSE);
    
    $adminServicesBI = new AdminServicesBI($connection);
    
    $parceirosId   = $adminServicesBI->updateParceiro($parceiro);
    
    $adminServicesBI->releaseConnection($connection);
    
    return $parceirosId;
  }
  
  
  public function cadastreParceiro(Parceiro $parceiro){
      
    if(is_null($this->connectionFactoryBI)){
      $this->connectionFactoryBI = new ConnectionFactory();
    }
    
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(FALSE);
    
    $adminServicesBI = new AdminServicesBI($connection);
    
    $parceiro   = $adminServicesBI->cadastreParceiro($parceiro);
    
    $adminServicesBI->releaseConnection($connection);
    
    return $parceiro;
  }
  
  
  public function getAllParceiros() {
    if ($this->connectionFactoryBI == NULL) {
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(FALSE);
    $adminServicesBI = new AdminServicesBI($connection);
    if (!is_null($parceiros = $adminServicesBI->getParceiros())) {

      $adminServicesBI->releaseConnection($connection);
      return $parceiros;
    }
    $adminServicesBI->releaseConnection($connection);
    return NULL;
  }
}




?>
