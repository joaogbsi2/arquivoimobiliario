<?php

/*
 * INCLUDE: SECTOR
 */

// include the file of configuration
// require_once '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI . 'ConnectionFactoryBI.class.php';
require_once PATH_MODEL_BI . 'CityBI.class.php';
require_once PATH_MODEL_ENTITIES . 'City.class.php';
require_once PATH_CONTROLLER . 'StateController.class.php';

/**
 * Description of CityController
 *
 * @author Daniel
 */
class CityController {
	private $connectionFactoryBI;
	public function getAllCities($limit = null, $orderBy = "") {
		if ($this->connectionFactoryBI == NULL) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( FALSE );
		$cityBI = new CityBI ( $connection );
		if (! is_null ( $cities = $cityBI->getCities ( $limit, $orderBy ) )) {
			
			$cityBI->releaseConnection ( $connection );
			return $cities;
		}
		$cityBI->releaseConnection ( $connection );
		return NULL;
	}
	public function getById($id) {
		if ($this->connectionFactoryBI == NULL) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( FALSE );
		$cityBI = new CityBI ( $connection );
		if (! is_null ( $city = $cityBI->getById ( $id ) )) {
			$cityBI->releaseConnection ( $connection );
			return $city;
		} else {
			$cityBI->releaseConnection ( $connection );
			return NULL;
		}
	}
	public function addCity($params) {
		if (is_null ( $this->connectionFactoryBI )) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( false );
		
		$cityBI = new CityBI ( $connection );
		
		try {
			
			$city = new City ();
			$city->setName ( $params ['name'] );
			
			$stateController = new StateController ();
			if (is_int ( $params ['state'] )) {
				$state = $stateController->getById ( $params ['state'] );
				
				$city->setState ( $state );
			} else
				$city->setState ( $params ['state'] );
			
			$cityBI->addCity ( $city );
			
			$cityBI->releaseConnection ( $connection );
		} catch ( Exception $exc ) {
			$connection->rollBack ();
			$this->connectionFactoryBI ()->releaseConnection ( $connection );
			echo $exc->getTraceAsString ();
		}
		
		$cityBI->releaseConnection ( $connection );
	}
	public function deleteCity($cityId) {
		if (is_null ( $this->connectionFactoryBI )) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( false );
		
		$cityBI = new CityBI ( $connection );
		$wasDeleted = $cityBI->delete ( $cityId );
		$cityBI->releaseConnection ( $connection );
		
		return $wasDeleted;
	}
	public function getCount() {
		if (is_null ( $this->connectionFactoryBI )) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( false );
		
		$cityBI = new CityBI ( $connection );
		$qtde = $cityBI->getCount ();
		$cityBI->releaseConnection ( $connection );
		
		return $qtde;
	}
	public function getCitiesByName($cityName, $limit, $orderBy) {
		if (is_null ( $this->connectionFactoryBI )) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( false );
		
		$cityBI = new CityBI ( $connection );
		$objs = $cityBI->getCitiesByName ( $cityName, $limit, $orderBy );
		$cityBI->releaseConnection ( $connection );
		
		return $objs;
	}
	public function updateCity($params) {
		if (is_null ( $this->connectionFactoryBI )) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( false );
		
		$cityBI = new CityBI ( $connection );
		
		try {
			
			$city = new City ();
			$city->setId ( $params ['id'] );
			$city->setName ( $params ['name'] );
			
			$stateController = new StateController ();
			$state = $stateController->getById ( $params ['state'] );
			
			$city->setState ( $state );
			return $cityBI->updateCity ( $city );
			
			$cityBI->releaseConnection ( $connection );
		} catch ( Exception $exc ) {
			$connection->rollBack ();
			$this->connectionFactoryBI ()->releaseConnection ( $connection );
			echo $exc->getTraceAsString ();
		}
		
		$cityBI->releaseConnection ( $connection );
		
		return false;
	}
}

?>
