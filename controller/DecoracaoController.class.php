<?php



/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once '../config.php';
#require_once PATH .'config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI.         'ConnectionFactoryBI.class.php';
require_once PATH_MODEL_BI.         'DecoracaoBI.class.php';
require_once PATH_MODEL_ENTITIES.   'Decoracao.class.php';

class DecoracaoController{
    
    //the attribute that receives a instance of connection
    private $connectionFactoryBI;
    
    
    public function addDecoracao($params) {

        if (is_null($this->connectionFactoryBI)) {
          $this->connectionFactoryBI = new ConnectionFactoryBI();
        }

        $connection = $this->connectionFactoryBI->createConnectionWithTransaction(false);

        $decoBI = new DecoracaoBI($connection);
        
        try {
            
          $deco = new Decoracao();
          $deco->setTitle($params['title']);
          $deco->setSubTitle($params['subTitle']);
          $deco->setDescription($params['description']);
          $deco->setPhoto($params['photo']);
            
          $decoBI->addDecoracao($deco);

          $decoBI->releaseConnection($connection);
        } catch (Exception $exc) {
          $connection->rollBack();
          $this->connectionFactoryBI()->releaseConnection($connection);
          echo $exc->getTraceAsString();
        }

        $decoBI->releaseConnection($connection);
    }
  
   
  /*
   * Methods add By Edilson Justiniano, on day 12/11/2013. 
   * This methods will be used to find all Notice cadastre
   * on system, or only same.
   */
  public function findAll(){
      if(is_null($this->connectionFactoryBI)){
          $this->connectionFactoryBI = new ConnectionFactoryBI();
      }
      $connection = $this->connectionFactoryBI->createConnectionWithTransaction(false);
      
      $decoBI = new DecoracaoBI($connection);
      $deco = $decoBI->findAll();
      $decoBI->releaseConnection($connection);
      
      return $deco;
    
  }
     
}//eof class UserController

?>
