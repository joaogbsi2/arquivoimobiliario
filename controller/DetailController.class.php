<?php

/*
 *  INCLUDE: SECTOR
 */

//include the file of configuration
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI .        'ConnectionFactoryBI.class.php';
require_once PATH_MODEL_BI .        'DetailBI.class.php';
require_once PATH_MODEL_ENTITIES.   'Detail.class.php';


/**
 * Description of CityController
 *
 * @author Daniel
 */
class DetailController {

  private $connectionFactoryBI;

  public function getAll($limit = null, $orderBy = "") {
    if ($this->connectionFactoryBI == NULL) {
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(FALSE);
    $detailBI = new DetailBI($connection);
    if (!is_null($details = $detailBI->getAll($limit, $orderBy))) {

      $detailBI->releaseConnection($connection);
      return $details;
    }
    $detailBI->releaseConnection($connection);
    return NULL;
  }

  public function getById($id) {
    if ($this->connectionFactoryBI == NULL) {
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(FALSE);
    $detailBI = new DetailBI($connection);
    if (!is_null($detail = $detailBI->getById($id))) {
      $detailBI->releaseConnection($connection);
      return $detail;
    } else {
      $detailBI->releaseConnection($connection);
      return NULL;
    }
  }

  
  public function addDetail($detailName) {

        if (is_null($this->connectionFactoryBI)) {
          $this->connectionFactoryBI = new ConnectionFactoryBI();
        }

        $connection = $this->connectionFactoryBI->createConnectionWithTransaction(false);

        $detailBI = new DetailBI($connection);
        
        try {
            
          $detail = new Detail();
          $detail->setName($detailName);
           
          $detailBI->addDetail($detail);

          $detailBI->releaseConnection($connection);
        } catch (Exception $exc) {
          $connection->rollBack();
          $this->connectionFactoryBI()->releaseConnection($connection);
          echo $exc->getTraceAsString();
        }

        $detailBI->releaseConnection($connection);
    }
    
    
    public function deleteDetail($detailId){
        if(is_null($this->connectionFactoryBI)){
            $this->connectionFactoryBI = new ConnectionFactoryBI();
        }
        $connection = $this->connectionFactoryBI->createConnectionWithTransaction(false);
      
      $detailBI = new DetailBI($connection);
      $wasDeleted = $detailBI->delete($detailId);
      $detailBI->releaseConnection($connection);
      
      return $wasDeleted;
    }
    
    
    public function getCount(){
      
      if(is_null($this->connectionFactoryBI)){
          $this->connectionFactoryBI = new ConnectionFactoryBI();
      }
      $connection = $this->connectionFactoryBI->createConnectionWithTransaction(false);
      
      $detailBI = new DetailBI($connection);
      $qtde = $detailBI->getCount();
      $detailBI->releaseConnection($connection);
      
      return $qtde;
  }
  
  
  
  public function getDetailsByName($detailName, $limit, $orderBy){
      
      if(is_null($this->connectionFactoryBI)){
          $this->connectionFactoryBI = new ConnectionFactoryBI();
      }
      $connection = $this->connectionFactoryBI->createConnectionWithTransaction(false);
      
      $detailBI = new DetailBI($connection);
      $objs = $detailBI->getDetailsByName($detailName, $limit, $orderBy);
      $detailBI->releaseConnection($connection);
      
      return $objs;
  }
  
  
  public function updateDetail($params) {

        if (is_null($this->connectionFactoryBI)) {
          $this->connectionFactoryBI = new ConnectionFactoryBI();
        }

        $connection = $this->connectionFactoryBI->createConnectionWithTransaction(false);

        $detailBI = new DetailBI($connection);
        
        try {
            
          $detail = new Detail();
          $detail->setId($params['id']);
          $detail->setName($params['name']);
          
          return $detailBI->updateDetail($detail);

          $detailBI->releaseConnection($connection);
        } catch (Exception $exc) {
          $connection->rollBack();
          $this->connectionFactoryBI()->releaseConnection($connection);
          echo $exc->getTraceAsString();
        }

        $detailBI->releaseConnection($connection);
        
        return false;
    }
}

?>
