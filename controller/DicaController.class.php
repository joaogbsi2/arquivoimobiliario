<?php



/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once '../config.php';
#require_once PATH .'config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI.         'ConnectionFactoryBI.class.php';
require_once PATH_MODEL_BI.         'DicaBI.class.php';
require_once PATH_MODEL_ENTITIES.   'Dica.class.php';

class DicaController{
    
    //the attribute that receives a instance of connection
    private $connectionFactoryBI;
    
    
    public function addDica($params) {

        if (is_null($this->connectionFactoryBI)) {
          $this->connectionFactoryBI = new ConnectionFactoryBI();
        }

        $connection = $this->connectionFactoryBI->createConnectionWithTransaction(false);

        $dicaBI = new DicaBI($connection);
        
        try {
            
          $dica = new Dica();
          $dica->setTitle($params['title']);
          $dica->setSubTitle($params['subTitle']);
          $dica->setDescription($params['description']);
          $dica->setPhoto($params['photo']);
            
          $dicaBI->addDica($dica);

          $dicaBI->releaseConnection($connection);
        } catch (Exception $exc) {
          $connection->rollBack();
          $this->connectionFactoryBI()->releaseConnection($connection);
          echo $exc->getTraceAsString();
        }

        $dicaBI->releaseConnection($connection);
    }
  
   
  /*
   * Methods add By Edilson Justiniano, on day 12/11/2013. 
   * This methods will be used to find all Notice cadastre
   * on system, or only same.
   */
  public function findAll(){
      if(is_null($this->connectionFactoryBI)){
          $this->connectionFactoryBI = new ConnectionFactoryBI();
      }
      $connection = $this->connectionFactoryBI->createConnectionWithTransaction(false);
      
      $dicaBI = new DicaBI($connection);
      $dica = $dicaBI->findAll();
      $dicaBI->releaseConnection($connection);
      
      return $dica;
    
  }
     
}//eof class UserController

?>
