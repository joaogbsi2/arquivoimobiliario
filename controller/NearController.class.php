<?php

/*
 *  INCLUDE: SECTOR
 */

//include the file of configuration
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI .        'ConnectionFactoryBI.class.php';
require_once PATH_MODEL_BI .        'NearBI.class.php';
require_once PATH_MODEL_ENTITIES.   'Near.class.php';


/**
 * Description of CityController
 *
 * @author Daniel
 */
class NearController {

  private $connectionFactoryBI;

  public function getAll($limit = null, $orderBy = "") {
    if ($this->connectionFactoryBI == NULL) {
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(FALSE);
    $nearBI = new NearBI($connection);
    if (!is_null($nears = $nearBI->getAll($limit, $orderBy))) {

      $nearBI->releaseConnection($connection);
      return $nears;
    }
    $nearBI->releaseConnection($connection);
    return NULL;
  }

  public function getById($id) {
    if ($this->connectionFactoryBI == NULL) {
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(FALSE);
    $nearBI = new NearBI($connection);
    if (!is_null($near = $nearBI->getById($id))) {
      $nearBI->releaseConnection($connection);
      return $near;
    } else {
      $nearBI->releaseConnection($connection);
      return NULL;
    }
  }

  
  public function addNear($nearName) {

        if (is_null($this->connectionFactoryBI)) {
          $this->connectionFactoryBI = new ConnectionFactoryBI();
        }

        $connection = $this->connectionFactoryBI->createConnectionWithTransaction(false);

        $nearBI = new NearBI($connection);
        
        try {
            
          $near = new Near();
          $near->setName($nearName);
           
          $nearBI->addNear($near);

          $nearBI->releaseConnection($connection);
        } catch (Exception $exc) {
          $connection->rollBack();
          $this->connectionFactoryBI()->releaseConnection($connection);
          echo $exc->getTraceAsString();
        }

        $nearBI->releaseConnection($connection);
    }
    
    
    public function deleteNear($nearId){
        if(is_null($this->connectionFactoryBI)){
            $this->connectionFactoryBI = new ConnectionFactoryBI();
        }
        $connection = $this->connectionFactoryBI->createConnectionWithTransaction(false);
      
      $nearBI = new NearBI($connection);
      $wasDeleted = $nearBI->delete($nearId);
      $nearBI->releaseConnection($connection);
      
      return $wasDeleted;
    }
    
    
    public function getCount(){
      
      if(is_null($this->connectionFactoryBI)){
          $this->connectionFactoryBI = new ConnectionFactoryBI();
      }
      $connection = $this->connectionFactoryBI->createConnectionWithTransaction(false);
      
      $nearBI = new NearBI($connection);
      $qtde = $nearBI->getCount();
      $nearBI->releaseConnection($connection);
      
      return $qtde;
  }
  
  
  
  public function getNearsByName($nearName, $limit, $orderBy){
      
      if(is_null($this->connectionFactoryBI)){
          $this->connectionFactoryBI = new ConnectionFactoryBI();
      }
      $connection = $this->connectionFactoryBI->createConnectionWithTransaction(false);
      
      $nearBI = new NearBI($connection);
      $objs = $nearBI->getNearsByName($nearName, $limit, $orderBy);
      $nearBI->releaseConnection($connection);
      
      return $objs;
  }
  
  
  public function updateNear($params) {

        if (is_null($this->connectionFactoryBI)) {
          $this->connectionFactoryBI = new ConnectionFactoryBI();
        }

        $connection = $this->connectionFactoryBI->createConnectionWithTransaction(false);

        $nearBI = new NearBI($connection);
        
        try {
            
          $near = new Near();
          $near->setId($params['id']);
          $near->setName($params['name']);
          
          return $nearBI->updateNear($near);

          $nearBI->releaseConnection($connection);
        } catch (Exception $exc) {
          $connection->rollBack();
          $this->connectionFactoryBI()->releaseConnection($connection);
          echo $exc->getTraceAsString();
        }

        $nearBI->releaseConnection($connection);
        
        return false;
    }
}

?>
