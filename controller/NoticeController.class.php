<?php



/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once '../config.php';
#require_once PATH .'config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI.         'ConnectionFactoryBI.class.php';
require_once PATH_MODEL_BI.         'NoticeBI.class.php';
require_once PATH_MODEL_ENTITIES.   'Notice.class.php';

class NoticeController{
    
    //the attribute that receives a instance of connection
    private $connectionFactoryBI;
    
    
    public function addNotice($params) {

        if (is_null($this->connectionFactoryBI)) {
          $this->connectionFactoryBI = new ConnectionFactoryBI();
        }

        $connection = $this->connectionFactoryBI->createConnectionWithTransaction(false);

        $noticeBI = new NoticeBI($connection);
        
        try {
            
          $notice = new Notice();
          $notice->setTitle($params['title']);
          $notice->setSubTitle($params['subTitle']);
          $notice->setDescription($params['description']);
          $notice->setPhoto($params['photo']);
            
          $noticeBI->addNotice($notice);

          $noticeBI->releaseConnection($connection);
        } catch (Exception $exc) {
          $connection->rollBack();
          $this->connectionFactoryBI()->releaseConnection($connection);
          echo $exc->getTraceAsString();
        }

        $noticeBI->releaseConnection($connection);
    }
  
   
  /*
   * Methods add By Edilson Justiniano, on day 12/11/2013. 
   * This methods will be used to find all Notice cadastre
   * on system, or only same.
   */
  public function findAll(){
      if(is_null($this->connectionFactoryBI)){
          $this->connectionFactoryBI = new ConnectionFactoryBI();
      }
      $connection = $this->connectionFactoryBI->createConnectionWithTransaction(false);
      
      $noticeBI = new NoticeBI($connection);
      $noticias = $noticeBI->findAll();
      $noticeBI->releaseConnection($connection);
      
      return $noticias;
    
  }
     
}//eof class UserController

?>
