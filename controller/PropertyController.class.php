<?php

/*
 * INCLUDE: SECTOR
 */

// include the file of configuration
// require_once '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI . 'ConnectionFactoryBI.class.php';
require_once PATH_MODEL_BI . 'PropertyBI.class.php';
require_once PATH_MODEL_ENTITIES . 'Property.class.php';
require_once PATH_MODEL_ENTITIES . 'RentToTheseDays.class.php';
require_once PATH_MODEL_ENTITIES . 'RentToTheseMonths.class.php';
require_once PATH_CONTROLLER . 'CityController.class.php';
require_once PATH_CONTROLLER . 'StatusController.class.php';
require_once PATH_CONTROLLER . 'PropertyTypeController.class.php';
require_once PATH_CONTROLLER . 'RentToTheseDaysController.class.php';
require_once PATH_CONTROLLER . 'RentToTheseMonthsController.class.php';
require_once PATH_CONTROLLER . 'StoreController.class.php';
require_once PATH_CONTROLLER . 'StreetController.class.php';

$vogais = array (
		"a",
		"á",
		"â",
		"ã",
		"A",
		"Á",
		"Â",
		"Ã",
		"e",
		"é",
		"ê",
		"E",
		"É",
		"Ê",
		"i",
		"í",
		"î",
		"I",
		"Í",
		"Î",
		"o",
		"ó",
		"ô",
		"õ",
		"O",
		"Ó",
		"Ô",
		"Õ",
		"u",
		"ú",
		"û",
		"U",
		"Ú",
		"Û"
);

/**
 * Description of PropertyController
 *
 * @author Daniel
 */
class PropertyController {
	private $connectionFactoryBI;
	private $cityController;
	private $statusController;
	private $propertyTypeController;
	private $propertyBI;
	private $storeController;


	function __construct() {
		$this->cityController = new CityController ();
		$this->statusController = new StatusController ();
		$this->propertyTypeController = new PropertyTypeController ();
		$this->storeController = new StoreController ();
	}
	public function recordProperty($params) {
		if (is_null ( $this->connectionFactoryBI )) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( FALSE );

		$propertyBI = new PropertyBI ( $connection );
		$rentToTheseDaysController = new RentToTheseDaysController ();
		$rentToTheseMonthsController = new RentToTheseMonthsController ();

		$property = new Property ();
		$property->setType ( $this->propertyTypeController->getById ( $params ['propertyType'] ) );
		//		$property->setStatus ( $this->statusController->getById ( $params ['status'] ) );
		//		$property->setPrice ( $params ['price'] );
		$property->setCep ( str_replace ( "-", "", $params ['cep'] ) );
		if (is_int ( $params ['city'] )) {
			$property->setCity ( $this->cityController->getById ( $params ['city'] ) );
		} else {
			$city = $this->cityController->getCitiesByName ( $params ['city'], null, null );
			$property->setCity ($city[0]);
		}
		$property->setNeighborhood ( $params ['neighborhood'] );
		$property->setStreet ( $params ['street'] );
		$property->setIdentifier ( $params ['number'] );
		$property->setidUnico($params['idUnico']);
		$property->setDescription ( $params ['description'] );
		$property->setQtdRooms ( $params ['qtdRooms'] );
		if(is_null($params ['active'])){
			$property->setActive ( 1 );
			
		}
		else{
			$property->setActive ( $params ['active'] );
		}


		/*
		 * Campo imobiliária add por Edilson Justiniano
		 * no dia 26-11-13
		 */
		$property->setExclusiveStore ( $params ['exclusive_store'] );

		/*
		 * Campos novos adicionados na 2ª versão do sistema
		 */
		$property->setCep ( $params ['cep'] );
		$property->setGaragem ( $params ['garagem'] );
		$property->setPavimento ( $params ['pavimento'] );
		$property->setPosicao ( $params ['posicao'] );
		$property->setAreaConstruida ( $params ['areaConstruida'] );
		$property->setAreaTotal ( $params ['areaTotal'] );
		$property->setValorCondominio ( $params ['valorCondominio'] );
		$property->setValorIPTU ( $params ['valorIPTU'] );

		$property->setEdicula ( $params ['edicula'] );
		$property->setComprimento ( $params ['comprimento'] );
		$property->setLargura ( $params ['largura'] );

		$propertyId = $propertyBI->createProperty ( $property );
		if (! empty ( $params ['daysOfWeek'] )) {

			$rentToTheseDays = new RentToTheseDays ();

			foreach ( $params ['daysOfWeek'] as $dayOfWeek ) {
				switch ($dayOfWeek) {
					case "domingo" :
						$rentToTheseDays->setDomingo ( TRUE );
						break;
					case "segunda" :
						$rentToTheseDays->setSegunda ( TRUE );
						break;
					case "terca" :
						$rentToTheseDays->setTerca ( TRUE );
						break;
					case "quarta" :
						$rentToTheseDays->setQuarta ( TRUE );
						break;
					case "quinta" :
						$rentToTheseDays->setQuinta ( TRUE );
						break;
					case "sexta" :
						$rentToTheseDays->setSexta ( TRUE );
						break;
					case "sabado" :
						$rentToTheseDays->setSabado ( TRUE );
						break;
				}
			}
			$rentToTheseDays->setProperty ( $propertyBI->findById ( $propertyId ) );
			$rentToTheseDaysController->insert ( $rentToTheseDays );
		}
		if (! empty ( $params ['monthOfYear'] )) {
			$rentToTheseMonths = new RentToTheseMonths ();
			foreach ( $params ['monthOfYear'] as $monthOfYear ) {
				switch ($monthOfYear) {
					case "janeiro" :
						$rentToTheseMonths->setJaneiro ( TRUE );
						break;
					case "fevereiro" :
						$rentToTheseMonths->setFevereiro ( TRUE );
						break;
					case "marco" :
						$rentToTheseMonths->setMarco ( TRUE );
						break;
					case "abril" :
						$rentToTheseMonths->setAbril ( TRUE );
						break;
					case "maio" :
						$rentToTheseMonths->setMaio ( TRUE );
						break;
					case "junho" :
						$rentToTheseMonths->setJunho ( TRUE );
						break;
					case "julho" :
						$rentToTheseMonths->setJulho ( TRUE );
						break;
					case "agosto" :
						$rentToTheseMonths->setAgosto ( TRUE );
						break;
					case "setembro" :
						$rentToTheseMonths->setSetembro ( TRUE );
						break;
					case "outubro" :
						$rentToTheseMonths->setOutubro ( TRUE );
						break;
					case "novembro" :
						$rentToTheseMonths->setNovembro ( TRUE );
						break;
					case "dezembro" :
						$rentToTheseMonths->setDezembro ( TRUE );
						break;
				}
			}
			$rentToTheseMonths->setProperty ( $propertyBI->findById ( $propertyId ) );
			$rentToTheseMonthsController->insert ( $rentToTheseMonths );
		}
		$propertyBI->releaseConnection ( $connection );
		return $propertyId;
	}
	public function getById($propertyId) {
		if (is_null ( $this->connectionFactoryBI )) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( FALSE );
		$this->propertyBI = new PropertyBI ( $connection );
		$property = $this->propertyBI->findById ( $propertyId );
		$this->propertyBI->releaseConnection ( $connection );
		return $property;
	}
	
	public function getByIdUnico($idUnico) {
		if (is_null ( $this->connectionFactoryBI )) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( FALSE );
		$this->propertyBI = new PropertyBI ( $connection );
		$property = $this->propertyBI->findByIdUnico( $idUnico );
		$this->propertyBI->releaseConnection ( $connection );
		return $property;
	}
	
	public function findProperty_status($propertyId, $statusId){
		if (is_null ( $this->connectionFactoryBI )) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( FALSE );
		$this->propertyBI = new PropertyBI ( $connection );
		$property = $this->propertyBI->findProperty_status($propertyId, $statusId);
		$this->propertyBI->releaseConnection ( $connection );
		return $property;
	}
	public function findNeighborhoodByCity($city) {
		if (is_null ( $this->connectionFactoryBI )) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( false );
		$this->propertyBI = new PropertyBI ( $connection );
		$neigborhood = $this->propertyBI->findNeighborhoodByCity ( $city );
		$this->propertyBI->releaseConnection ( $connection );
		return $neigborhood;
	}
	
	// Add a new parameter code by Edilson Justiniano. This Field was add by new layout on the search
	public function search($cityId, $neighborhood, $type, $min, $max, $qtdMinRooms, $qtdMaxRooms, $status, $limit, $offset, $deDate, $ateDate, $code) {
		if (is_null ( $this->connectionFactoryBI )) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( false );

		$this->propertyBI = new PropertyBI ( $connection );

		// Add a new parameter code by Edilson Justiniano. This Field was add by new layout on the search
		$propertys = $this->propertyBI->search ( $cityId, $neighborhood, $type, $min, $max, $qtdMinRooms, $qtdMaxRooms, $status, $limit, $offset, $deDate, $ateDate, $code );
		// Add a new parameter code by Edilson Justiniano. This Field was add by new layout on the searchSize
		$total = $this->propertyBI->searchSize ( $cityId, $neighborhood, $type, $min, $max, $qtdMinRooms, $qtdMaxRooms, $status, $deDate, $ateDate, $code );
		$this->propertyBI->releaseConnection ( $connection );

		$p = array (
				"total" => $total,
				"root" => $propertys 
		);

		return $p;
	}
	/*
	 * public function searchSize($cityId, $neighborhood,$measure,$type,$min,$max,$qtdeRooms,$status){
	 * if(is_null($this->connectionFactoryBI)){
	 * $this->connectionFactoryBI = new ConnectionFactoryBI();
	 * }
	 * $connection = $this->connectionFactoryBI->createConnectionWithTransaction(false);
	 *
	 * $this->propertyBI = new PropertyBI($connection);
	 * $total = $this->propertyBI->searchSize($cityId, $neighborhood, $measure, $type, $min, $max,$qtdeRooms,$status);
	 * $this->propertyBI->releaseConnection($connection);
	 *
	 * return $total;
	 * }
	 */
	public function enableProperty($propertyId) {
		if (is_null ( $this->connectionFactoryBI )) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( false );

		$this->propertyBI = new PropertyBI ( $connection );
		$this->propertyBI->enableProperty ( $propertyId );
		$this->propertyBI->releaseConnection ( $connection );
	}

	/*
	 * Methods add By Edilson Justiniano, on day 02/11/2013.
	 * This methods will be used to find all Properties cadastre
	 * on system, or only same.
	 */
	public function findAll($limit, $orderBy) {
		if (is_null ( $this->connectionFactoryBI )) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( false );

		$this->propertyBI = new PropertyBI ( $connection );

		$properties = $this->propertyBI->findAll ( $limit, $orderBy );
		$this->propertyBI->releaseConnection ( $connection );

		return $properties;
	}

	/*
	 * Method that will go search and return a qtde of property
	 * stored on system, I can use this information on page of admin
	 * list imoveis
	 */
	public function getCount() {
		if (is_null ( $this->connectionFactoryBI )) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( false );

		$this->propertyBI = new PropertyBI ( $connection );

		$qtde = $this->propertyBI->getCount ();
		$this->propertyBI->releaseConnection ( $connection );

		return $qtde;
	}

	/*
	 * Method that will do delete the selected Property
	 */
	public function deleteProperty($propertyId) {
		if (is_null ( $this->connectionFactoryBI )) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( false );

		$this->propertyBI = new PropertyBI ( $connection );

		$wasDeleted = $this->propertyBI->deleteProperty ( $propertyId );
		$this->propertyBI->releaseConnection ( $connection );

		return $wasDeleted;
	}

	/*
	 * Método para realizar o update de um imóvel selecionado
	 * Lembrando de tomar cuidado, com o esquema de old status
	 */
	public function updateProperty($params) {
		// 	public function updateProperty($property) {
		if (is_null ( $this->connectionFactoryBI )) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( FALSE );

		$propertyBI = new PropertyBI ( $connection );
		$rentToTheseDaysController = new RentToTheseDaysController ();
		$rentToTheseMonthsController = new RentToTheseMonthsController ();


		$propertyNew = new Property ();
		$propertyNew->setType ( $this->propertyTypeController->getById ( $params ['propertyType'] ) );
		// 		$propertyNew->setStatus ( $this->statusController->getById ( $params ['status'] ) );
		// 		$propertyNew->setPrice ( $params ['price'] );
		$propertyNew->setCep ( str_replace ( "-", "", $params ['cep'] ) );
		// 		$propertyNew->setCity ( $this->cityController->getById ( $params ['city'] ) );
		$propertyNew->setCity (  $params ['city']  );
		$propertyNew->setNeighborhood ( $params ['neighborhood'] );
		$propertyNew->setStreet ( $params ['street'] );
		$propertyNew->setIdentifier ( $params ['number'] );
		$propertyNew->setDescription ( $params ['description'] );
		$propertyNew->setQtdRooms ( $params ['qtdRooms'] );
		$propertyNew->setId ( $params ['property_id'] );
		// $property->setActive(0);
		$propertyNew->setActive($params ['active']);

		/*
		 * Campo imobiliária add por Edilson Justiniano
		 * no dia 26-11-13
		 */
		$propertyNew->setExclusiveStore ( $params ['exclusive_store'] );

		/*
		 * Campos novos adicionados na 2ª versão do sistema
		 */
		$propertyNew->setCep ( $params ['cep'] );
		$propertyNew->setGaragem ( $params ['garagem'] );
		$propertyNew->setPavimento ( $params ['pavimento'] );
		$propertyNew->setPosicao ( $params ['posicao'] );
		$propertyNew->setAreaConstruida ( $params ['areaConstruida'] );
		$propertyNew->setAreaTotal ( $params ['areaTotal'] );
		$propertyNew->setValorCondominio ( $params ['valorCondominio'] );
		$propertyNew->setValorIPTU ( $params ['valorIPTU'] );

		$propertyNew->setEdicula ( $params ['edicula'] );
		$propertyNew->setComprimento ( $params ['comprimento'] );
		$propertyNew->setLargura ( $params ['largura'] );
		$propertyNew->setidUnico( $params ['idUnico'] );

		$wasUpdated = $propertyBI->updateProperty ( $propertyNew );

		if (! $wasUpdated) {
			return FALSE;
			// echo "Erro ao atualizar os dados básicos";
			// header("Location:" .URL_ADMIN_PAGE. "?failure=update_property");
		} else {
			/*
			 * Agora chegou a hora de verificar o esquema do old status
			 */
			/*
			 * 1º caso old Status =3 e novo status != 3, ou seja,
			 * tinha dado cadastrado na tabela de dias e meses que podem
			 * ser locado esse imóvel, então devo removê-lo
			 */
			// 			if ($params ['oldStatus'] == 3 && $params ['status'] != 3) {
			// 				/*
			// 				 * Nesse caso deleta, todos os dados da tabela de locações
			// 				 */
			// 				$wasDeleted = $rentToTheseDaysController->deleteByProperty ( $propertyNew->getId () );
			// 				if ($wasDeleted) {
			// 					$wasDeleted = $rentToTheseMonthsController->deleteByProperty ( $propertyNew->getId () );
			// 					if (! $wasDeleted) {
			// 						return FALSE;
			// 						// echo "erro ao excluir todos meses";
			// 					}
			// 				} else {
			// 					return FALSE;
			// 					// echo "erro ao remover todos os dias";
			// 				}
			// 			} else {

			// 				if ($params ['oldStatus'] != 3 && $params ['status'] == 3) {
			// 					/*
			// 					 * Nesse caso, não tem nada cadastrado, apenas insere
			// 					 */
			// 				} else {

			// 					if ($params ['oldStatus'] != 3 && $params ['status'] != 3) {
			// 						/*
			// 						 * Não vai fazer nada, pq não vai cadastrar, não vai atualiza esses dados
			// 						 */
			// 					} else { // $params['oldStatus'] == 3 && $params['status'] == 3
			/*
			* Este é o unico caso onde haverá o update nas tabelas,
			* pois o imovel continua sendo de temporada e pode ter havido
			* atualizações nos meses e dias disponóiveis dele
			*/
			// 						$wasDeleted = $rentToTheseDaysController->deleteByProperty ( $propertyNew->getId () );
			// 						if ($wasDeleted) {
				// 							$wasDeleted = $rentToTheseMonthsController->deleteByProperty ( $propertyNew->getId () );
			// 							if (! $wasDeleted) {
			// 								return FALSE;
			// 								// echo "erro ao excluir todos meses";
			// 							}
			// 						} else {
			// 							return FALSE;
			// 							// echo "erro ao remover todos os meses";
			// 						}
			// 					}
			// 				}
		}
			
		if (! empty ( $params ['daysOfWeek'] ) && $params ['status'] == 3) {

			$rentToTheseDays = new RentToTheseDays ();

			foreach ( $params ['daysOfWeek'] as $dayOfWeek ) {
				switch ($dayOfWeek) {
					case "domingo" :
						$rentToTheseDays->setDomingo ( TRUE );
						break;
					case "segunda" :
						$rentToTheseDays->setSegunda ( TRUE );
						break;
					case "terca" :
						$rentToTheseDays->setTerca ( TRUE );
						break;
					case "quarta" :
						$rentToTheseDays->setQuarta ( TRUE );
						break;
					case "quinta" :
						$rentToTheseDays->setQuinta ( TRUE );
						break;
					case "sexta" :
						$rentToTheseDays->setSexta ( TRUE );
						break;
					case "sabado" :
						$rentToTheseDays->setSabado ( TRUE );
						break;
				}
			}
			$rentToTheseDays->setProperty ( $propertyBI->findById ( $params ['property_id'] ) );
			$rentToTheseDaysController->insert ( $rentToTheseDays );
		}
		if (! empty ( $params ['monthOfYear'] ) && $params ['status'] == 3) {

			$rentToTheseMonths = new RentToTheseMonths ();
			foreach ( $params ['monthOfYear'] as $monthOfYear ) {
				switch ($monthOfYear) {
					case "janeiro" :
						$rentToTheseMonths->setJaneiro ( TRUE );
						break;
					case "fevereiro" :
						$rentToTheseMonths->setFevereiro ( TRUE );
						break;
					case "marco" :
						$rentToTheseMonths->setMarco ( TRUE );
						break;
					case "abril" :
						$rentToTheseMonths->setAbril ( TRUE );
						break;
					case "maio" :
						$rentToTheseMonths->setMaio ( TRUE );
						break;
					case "junho" :
						$rentToTheseMonths->setJunho ( TRUE );
						break;
					case "julho" :
						$rentToTheseMonths->setJulho ( TRUE );
						break;
					case "agosto" :
						$rentToTheseMonths->setAgosto ( TRUE );
						break;
					case "setembro" :
						$rentToTheseMonths->setSetembro ( TRUE );
						break;
					case "outubro" :
						$rentToTheseMonths->setOutubro ( TRUE );
						break;
					case "novembro" :
						$rentToTheseMonths->setNovembro ( TRUE );
						break;
					case "dezembro" :
						$rentToTheseMonths->setDezembro ( TRUE );
						break;
				}
			}
			$rentToTheseMonths->setProperty ( $propertyBI->findById ( $params ['property_id'] ) );
			$rentToTheseMonthsController->insert ( $rentToTheseMonths );
		}
		$propertyBI->releaseConnection ( $connection );
			
		return TRUE;
		// header("Location:" .URL_ADMIN_PAGE. "?success=update_property");
	}


	public function updatePropertyEnable($idUnico){
		if (is_null ( $this->connectionFactoryBI )) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( FALSE );
		$propertyBI = new PropertyBI ( $connection );
		$wasUpdated = $propertyBI->updatePropertyActive($idUnico);
		if (!$wasUpdated)
			return false;
		else return true;
	}
	// 	}

	/*
	 * Método que irá ser usado quando o usuário for fazer a busca pelo imóvel
	 * e informar, ou só o código da propriedade ou todos os campos e mais o
	 * código da propriedade. É que caso tenha informado o código da propriedade
	 * então ele (o usuário) sabe o que ele quer. Ou melhor qual imóvel ele quer
	 * visualizar. Será passado como parâmetro o código informado pelo usuário
	 */
	public function getByIdJSON($propertyId) {
		if (is_null ( $this->connectionFactoryBI )) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( FALSE );
		$this->propertyBI = new PropertyBI ( $connection );
		$property = $this->propertyBI->findById ( $propertyId );
		$this->propertyBI->releaseConnection ( $connection );

		// if (is_null($property)){
		// // echo "error NULL";
		// // exit();
		// } else {
		// echo "OK achou algo";
			// }

			$p = array (
				"total" => 1,
				"root" => $property 
			);

			return $p;
		}

		// 	public function getListProperty(){
		// 		foreach ($this->findAll() as $imovel){
		// 			$lista [$imovel->getid()] = $imovel->getId();
			// 		}
			// 		return $lista;
			// 	}

			// 	public function atualizaListProperty($params){
			// 		$this->recordProperty($params);
			// 		return $this->getListProperty();
			// 	}

			// 	public function updateIdUnico(){
			// 		$streetController = new StreetController();
			// 			$listaRuas = $streetController->getListStreets ();
			// 		foreach ($this->findAll() as $property){
			// 			$aux = explode ( " ", $property->getStreet(), 2 );
				// 			$bairro = str_replace(" ", "", str_replace($vogais,"" , $property->getNeighborhood()));
				// 			if ($listaRuas [$aux [1]] == null) {
				// 				$listaRuas = $streetController->atualizaListStreet ( $aux [1], $property->getNeighborhood(), $property->getCity() );
				// 			}
				// 			// 		mb_strtoupper ( str_replace ( " ", "", str_replace ( $vogais, "", $dados[NEIGHBORHOOD] ) ) ).'*'.$listaRuas[$aux[1]].'*'.$dados[IDENTIFER];
				// 			$property->setidUnico(mb_strtoupper($bairro.'*'.$listaRuas[$aux[1]].'*'.$property->getIdentifier()));
				// 			$propertyController->updateProperty($property);
				// 		}
				// 		return $lista;
				// 	}
}
?>