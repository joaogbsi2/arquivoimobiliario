<?php

/*
 * date of creating 2013-11-12
 * 
 * @author Edilson Justiano
 */

//include the file of configuration
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI .'ConnectionFactoryBI.class.php';
require_once PATH_MODEL_BI .'PropertyDetailBI.class.php';


/**
 * Description of UserServiceController
 *
 * @author Daniel
 */
class PropertyDetailController {

  private $connectionFactoryBI;

  public function createPropertyDetail($detailsSelected, Property $property) {
    $success = FALSE;
    if (is_null($this->connectionFactoryBI)) {
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(TRUE);

    $propertyDetailBI = new PropertyDetailBI($connection);

    try {
      $success = $propertyDetailBI->createNewPropertyDetail($detailsSelected, $property);
      $propertyDetailBI->commitConnection(TRUE);
    } catch (PDOException $exc) {
      echo $exc->getTraceAsString();
      $propertyDetailBI->rollBackConnection(TRUE);
    }
    return $success;
  }

  public function getPropertyDetail($propertyId) {
    if (is_null($this->connectionFactoryBI)) {
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(FALSE);
    
    $propertyDetailBI = new PropertyDetailBI($connection);

    $propertyDetail = $propertyDetailBI->getPropertyDetail($propertyId);

    $propertyDetailBI->releaseConnection($connection);

    return $propertyDetail;
  }
  
  
  /*
   * Method to delete all serviceProvider of this user
   */
  public function deleteDetailsInProperty($propertyId){
      
    if (is_null($this->connectionFactoryBI)) {
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(FALSE);
    
    $propertyDetailBI = new PropertyDetailBI($connection);

    $wasDeleted = $propertyDetailBI->deleteDetailsInProperty($propertyId);

    $propertyDetailBI->releaseConnection($connection);

    return $wasDeleted;
      
  }

  /*
   * Método para mostrar os usuários na tela de serviço
   * é chamado da tela de servicos
   */
  public function getPropertyDetailByDetail($detailId) {
    if (is_null($this->connectionFactoryBI)) {
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(FALSE);
    
    $propertyDetailBI = new PropertyDetailBI($connection);

    $propertyDetail = $propertyDetailBI->getPropertyDetailByDetail($detailId);

    $propertyDetailBI->releaseConnection($connection);

    return $propertyDetail;
  }
}

?>
