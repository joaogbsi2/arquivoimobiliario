<?php

/*
 * date of creating 2013-11-12
 * 
 * @author Edilson Justiano
 */

//include the file of configuration
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI .'ConnectionFactoryBI.class.php';
require_once PATH_MODEL_BI .'PropertyNearBI.class.php';


/**
 * Description of UserServiceController
 *
 * @author Daniel
 */
class PropertyNearController {

  private $connectionFactoryBI;

  public function createPropertyNear($nearssSelected, Property $property) {
    $success = FALSE;
    if (is_null($this->connectionFactoryBI)) {
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(TRUE);

    $propertyNearBI = new PropertyNearBI($connection);

    try {
      $success = $propertyNearBI->createNewPropertyNear($nearssSelected, $property);
      $propertyNearBI->commitConnection(TRUE);
    } catch (PDOException $exc) {
      echo $exc->getTraceAsString();
      $propertyNearBI->rollBackConnection(TRUE);
    }
    return $success;
  }

  public function getPropertyNear($propertyId) {
    if (is_null($this->connectionFactoryBI)) {
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(FALSE);
    
    $propertyNearBI = new PropertyNearBI($connection);

    $propertyNear = $propertyNearBI->getPropertyNear($propertyId);

    $propertyNearBI->releaseConnection($connection);

    return $propertyNear;
  }
  
  
  /*
   * Method to delete all serviceProvider of this user
   */
  public function deleteNearsInProperty($propertyId){
      
    if (is_null($this->connectionFactoryBI)) {
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(FALSE);
    
    $propertyNearBI = new PropertyNearBI($connection);

    $wasDeleted = $propertyNearBI->deleteNearsInProperty($propertyId);

    $propertyNearBI->releaseConnection($connection);

    return $wasDeleted;
      
  }

  /*
   * Método para mostrar os usuários na tela de serviço
   * é chamado da tela de servicos
   */
  public function getPropertyNearByNear($nearId) {
    if (is_null($this->connectionFactoryBI)) {
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(FALSE);
    
    $propertyNearBI = new PropertyNearBI($connection);

    $propertyNear = $propertyNearBI->getPropertyNearByNear($nearId);

    $propertyNearBI->releaseConnection($connection);

    return $propertyNear;
  }
}

?>
