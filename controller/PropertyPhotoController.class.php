<?php

/*
 *  INCLUDE: SECTOR
 */

//include the file of configuration
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI .'ConnectionFactoryBI.class.php';
require_once PATH_MODEL_BI .'PropertyPhotoBI.class.php';

/**
 * Description of PropertyPhotoController
 *
 * @author Daniel Fernandes
 */
class PropertyPhotoController {
  private $connectionFactory;
  private $propertyController;
  
  
  function __construct() {
    $this->propertyController = new PropertyController();
  }

  public function insertPropertyPhoto(PropertyPhoto $propertyPhoto){
    if(is_null($this->connectionFactory)){
      $this->connectionFactory = new ConnectionFactory();
    }
    
    $connection = $this->connectionFactory->createConnection(FALSE);
    
    $propertyPhotoBI = new PropertyPhotoBI($connection);
    
    $propertyPhotoId = $propertyPhotoBI->insertPropertyPhoto($propertyPhoto);
    
    $propertyPhotoBI->releaseConnection($connection);
    
    return $propertyPhotoId;
  }
  
  public function getPhotosByProperty($propertyId){
     if(is_null($this->connectionFactory)){
      $this->connectionFactory = new ConnectionFactory();
    }
    $connection = $this->connectionFactory->createConnection(FALSE);
    
    $propertyPhotoBI = new PropertyPhotoBI($connection);
    
    $propertyPhoto = $propertyPhotoBI->getPhotosByProperty($propertyId);
    
    $propertyPhotoBI->releaseConnection($connection);
    
    return $propertyPhoto;
  }
  
  public function getPhotosByPath($photoPath){
     if(is_null($this->connectionFactory)){
      $this->connectionFactory = new ConnectionFactory();
    }
    $connection = $this->connectionFactory->createConnection(FALSE);
    
    $propertyPhotoBI = new PropertyPhotoBI($connection);
    
    $propertyPhoto = $propertyPhotoBI->getPhotosByPath($photoPath);
    
    $propertyPhotoBI->releaseConnection($connection);
    
    return $propertyPhoto;
  }

  public function getUniquePhotoByProperty($propertyId){
    if(is_null($this->connectionFactory)){
      $this->connectionFactory = new ConnectionFactory();
    }
    $connection = $this->connectionFactory->createConnection(FALSE);
    $propertyPhotoBI = new PropertyPhotoBI($connection);
    $photo = $propertyPhotoBI->findByProperty($propertyId);
    $propertyPhotoBI->releaseConnection($connection);
    return $photo;
  }
  
  
  public function deletePropertyPhoto($propertyPhotoId){
     if(is_null($this->connectionFactory)){
      $this->connectionFactory = new ConnectionFactory();
    }
    $connection = $this->connectionFactory->createConnection(FALSE);
    
    $propertyPhotoBI = new PropertyPhotoBI($connection);
    
    $wasDeleted = $propertyPhotoBI->deletePropertyPhoto($propertyPhotoId);
    
    $propertyPhotoBI->releaseConnection($connection);
    
    return $wasDeleted;
  }
  
}

?>
