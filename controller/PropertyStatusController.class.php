<?php

/*
 * date of creating 2013-11-12
 * 
 * @author Edilson Justiano
 */

//include the file of configuration
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI .'ConnectionFactoryBI.class.php';
require_once PATH_MODEL_BI .'PropertyStatusBI.class.php';


/**
 * Description of UserServiceController
 *
 * @author Daniel
 */
class PropertyStatusController {

  private $connectionFactoryBI;

  public function createPropertyStatus($arrayPrices, $arrayStatus, $arrayStore, Property $property) {
    $success = FALSE;
    if (is_null($this->connectionFactoryBI)) {
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(TRUE);

    $propertyStatusBI = new PropertyStatusBI($connection);

    try {
      $success = $propertyStatusBI->createNewPropertyStatus($arrayPrices, $arrayStatus, $arrayStore, $property);
      $propertyStatusBI->commitConnection(TRUE);
    } catch (PDOException $exc) {
      echo $exc->getTraceAsString();
      $propertyStatusBI->rollBackConnection(TRUE);
    }
    return $success;
  }

  public function getPropertyStatus($propertyId) {
    if (is_null($this->connectionFactoryBI)) {
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(FALSE);
    
    $propertyStatusBI = new PropertyStatusBI($connection);

    $propertyStatus = $propertyStatusBI->getPropertyStatus($propertyId);

    $propertyStatusBI->releaseConnection($connection);

    return $propertyStatus;
  }
  
  
  /*
   * Method to delete all serviceProvider of this user
   */
  public function deleteStoresInProperty($propertyId){
      
    if (is_null($this->connectionFactoryBI)) {
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(FALSE);
    
    $propertyStatus = new PropertyStatusBI($connection);

    $wasDeleted = $propertyStatus->deleteStatusInProperty($propertyId);

    $propertyStatus->releaseConnection($connection);

    return $wasDeleted;
      
  }

  /*
   * Método para mostrar os usuários na tela de serviço
   * é chamado da tela de servicos
   */
  public function getPropertyStatusByStatus($statusId) {
    if (is_null($this->connectionFactoryBI)) {
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(FALSE);
    
    $propertyStatusBI = new PropertyStatusBI($connection);

    $propertyStatus = $propertyStatusBI->getPropertyStatusByStatus($statusId);

    $propertyStatusBI->releaseConnection($connection);

    return $propertyStatus;
  }

  public function getPropertyStatusByStore($propertyId, $statusId, $storeId) {
    if (is_null($this->connectionFactoryBI)) {
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(FALSE);
    
    $propertyStatusBI = new PropertyStatusBI($connection);

    $propertyStatus = $propertyStatusBI->getPropertyStatusByStore($propertyId, $statusId, $storeId);

    $propertyStatusBI->releaseConnection($connection);

    return $propertyStatus;
  }
}

?>
