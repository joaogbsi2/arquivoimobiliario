<?php

/*
 * date of creating 2013-11-12
 * 
 * @author Edilson Justiano
 */

//include the file of configuration
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI .'ConnectionFactoryBI.class.php';
require_once PATH_MODEL_BI .'PropertyStoreBI.class.php';


/**
 * Description of UserServiceController
 *
 * @author Daniel
 */
class PropertyStoreController {

  private $connectionFactoryBI;

  public function createPropertyStore($storeReferencia, $storesSelected, Property $property) {
    $success = FALSE;
    if (is_null($this->connectionFactoryBI)) {
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(TRUE);

    $propertyStoreBI = new PropertyStoreBI($connection);

    try {
      $success = $propertyStoreBI->createNewPropertyStore($storeReferencia, $storesSelected, $property);
      $propertyStoreBI->commitConnection(TRUE);
    } catch (PDOException $exc) {
      echo $exc->getTraceAsString();
      $propertyStoreBI->rollBackConnection(TRUE);
    }
    return $success;
  }

  public function getPropertyStore($propertyId) {
    if (is_null($this->connectionFactoryBI)) {
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(FALSE);
    
    $propertyStoreBI = new PropertyStoreBI($connection);

    $propertyStore = $propertyStoreBI->getPropertyStore($propertyId);

    $propertyStoreBI->releaseConnection($connection);

    return $propertyStore;
  }
  
  
  /*
   * Method to delete all serviceProvider of this user
   */
  public function deleteStoresInProperty($propertyId){
      
    if (is_null($this->connectionFactoryBI)) {
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(FALSE);
    
    $propertyStoreBI = new PropertyStoreBI($connection);

    $wasDeleted = $propertyStoreBI->deleteStoresInProperty($propertyId);

    $propertyStoreBI->releaseConnection($connection);

    return $wasDeleted;
      
  }

  /*
   * Método para mostrar os usuários na tela de serviço
   * é chamado da tela de servicos
   */
  public function getPropertyStoreByStore($propertyDetail) {
    if (is_null($this->connectionFactoryBI)) {
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(FALSE);
    
    $propertyStoreBI = new PropertyStoreBI($connection);

    $propertyStore = $propertyStoreBI->getPropertyStoreByStore($propertyDetail);

    $propertyStoreBI->releaseConnection($connection);

    return $propertyStore;
  }
  
  public function getVetorPropertyStore($propertyDetail){
    if (is_null($this->connectionFactoryBI)) {
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(FALSE);
    
    $propertyStoreBI = new PropertyStoreBI($connection);

    $vPropertyStore = $propertyStoreBI->getVetorPropertyStore($propertyDetail);
    

    $propertyStoreBI->releaseConnection($connection);
    
//     $vPropertyStore = array();
//     foreach ($propertyStore as $aux){
// //     	$vPropertyStore = array ($aux->getProperty()->getIdUnico() => $aux->getReferencia());
//     	array_push($vPropertyStore, $aux->getProperty()->getIdUnico());
//     }

    return $vPropertyStore;
  	
  }
}

?>
