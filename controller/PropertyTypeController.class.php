<?php

/*
 *  INCLUDE: SECTOR
 */

//include the file of configuration
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI .'ConnectionFactoryBI.class.php';
require_once PATH_MODEL_BI .'PropertyTypeBI.class.php';
require_once PATH_MODEL_ENTITIES .'PropertyType.class.php';
        
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class PropertyTypeController{

    private $connectionFactoryBI;
    
    public function getById($propertyTypeId) {
      if(is_null($this->connectionFactoryBI)){
        $this->connectionFactoryBI = new ConnectionFactoryBI();
      }
      $connection = $this->connectionFactoryBI->createConnectionWithTransaction(FALSE);
      $propertyTypeBI = new PropertyTypeBI($connection);
      $propertyType = $propertyTypeBI->findById($propertyTypeId);
      $propertyTypeBI->releaseConnection($connection);
      
      return $propertyType;
    }
    
    public function findAll(){
        if(is_null($this->connectionFactoryBI)){
            $this->connectionFactoryBI = new ConnectionFactoryBI();
        }
        $connection = $this->connectionFactoryBI->createConnectionWithTransaction(false);
        $propertyTypeBI = new PropertyTypeBI($connection);
        $propertyType = $propertyTypeBI->findAll();
        $propertyTypeBI->releaseConnection($connection);
        
        return $propertyType;
    }
    
}

?>
