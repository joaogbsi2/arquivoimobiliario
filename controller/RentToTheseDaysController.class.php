<?php

/*
 *  INCLUDE: SECTOR
 */

//include the file of configuration
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI .'ConnectionFactoryBI.class.php';
require_once PATH_MODEL_BI .'RentToTheseDaysBI.class.php';
require_once PATH_MODEL_ENTITIES .'RentToTheseDays.class.php';

/**
 * Description of RentToTheseDaysController
 *
 * @author Daniel Fernandes
 */
class RentToTheseDaysController {

  private $connectionFactory;

  public function insert(RentToTheseDays $rentToTheseDays) {
    if (is_null($this->connectionFactory)) {
      $this->connectionFactory = new ConnectionFactory();
    }
    $connection = $this->connectionFactory->createConnection(FALSE);

    $rentToTheseDaysBI = new RentToTheseDaysBI($connection);
    $rentToTheseDaysBI->insert($rentToTheseDays);
    $rentToTheseDaysBI->releaseConnection($connection);
  }
  
  
  
  /*
   * Método que irá pegar os detalhes de imoveis que possuem o status = 3
   * temporada. Ou seja, quero pegar os dias que o imovel estará disponível
   */
   public function getRentToTheseDays($propertyId){
  
        if (is_null($this->connectionFactory)) {
            $this->connectionFactory = new ConnectionFactory();
        }
        $connection = $this->connectionFactory->createConnection(FALSE);

        $rentToTheseDaysBI = new RentToTheseDaysBI($connection);
        $days = $rentToTheseDaysBI->getRentToTheseDays($propertyId);
        $rentToTheseDaysBI->releaseConnection($connection);
        
        return $days;
  }
  
  
  /*
   * Método que irá deletar todos os registros da tabela rent_to_these_days
   * de um determinado imovel que será passado por parâmetroß
   */
  public function deleteByProperty($propertyId){
      
       if (is_null($this->connectionFactory)) {
            $this->connectionFactory = new ConnectionFactory();
        }
        $connection = $this->connectionFactory->createConnection(FALSE);

        $rentToTheseDaysBI = new RentToTheseDaysBI($connection);
        $wasDeleted = $rentToTheseDaysBI->deleteByProperty($propertyId);
        $rentToTheseDaysBI->releaseConnection($connection);
        
        return $wasDeleted;
  }

}

?>
