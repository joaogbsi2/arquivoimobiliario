<?php

/*
 *  INCLUDE: SECTOR
 */

//include the file of configuration
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI .'ConnectionFactoryBI.class.php';
require_once PATH_MODEL_BI .'RentToTheseMonthsBI.class.php';
require_once PATH_MODEL_ENTITIES .'RentToTheseMonths.class.php';

/**
 * Description of RentToTheseMonthsController
 *
 * @author Daniel Fernandes
 */
class RentToTheseMonthsController {

  private $connectionFactory;

  public function insert(RentToTheseMonths $rentToTheseMonths) {
    if (is_null($this->connectionFactory)) {
      $this->connectionFactory = new ConnectionFactory();
    }
    $connection = $this->connectionFactory->createConnection(FALSE);

    $rentToTheseMonthsBI = new RentToTheseMonthsBI($connection);
    $rentToTheseMonthsBI->insert($rentToTheseMonths);
    $rentToTheseMonthsBI->releaseConnection($connection);
  }
  
  
  
  /*
   * Método que irá pegar os detalhes de imoveis que possuem o status = 3
   * temporada. Ou seja, quero pegar os meses que ele também estará 
   * disponível para locação
   */
  public function getRentToTheseMonths($propertyId){
      
    if (is_null($this->connectionFactory)) {
      $this->connectionFactory = new ConnectionFactory();
    }
    $connection = $this->connectionFactory->createConnection(FALSE);

    $rentToTheseMonthsBI = new RentToTheseMonthsBI($connection);
    $months = $rentToTheseMonthsBI->getRentToTheseMonths($propertyId);
    $rentToTheseMonthsBI->releaseConnection($connection);
    
    return $months;
  }
  
  
  
  /*
   * Método que irá deletar todos os registros da tabela rent_to_these_days
   * de um determinado imovel que será passado por parâmetroß
   */
  public function deleteByProperty($propertyId){
    
    if (is_null($this->connectionFactory)) {
      $this->connectionFactory = new ConnectionFactory();
    }
    $connection = $this->connectionFactory->createConnection(FALSE);

    $rentToTheseMonthsBI = new RentToTheseMonthsBI($connection);
    $wasDeleted = $rentToTheseMonthsBI->deleteByProperty($propertyId);
    $rentToTheseMonthsBI->releaseConnection($connection);
    
    return $wasDeleted;
  }
}

?>
