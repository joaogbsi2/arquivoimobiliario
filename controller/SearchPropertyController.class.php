<?php

/*
 *  INCLUDE: SECTOR
 */

//include the file of configuration
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI .'ConnectionFactoryBI.class.php';
require_once PATH_MODEL_BI .'PropertyBI.class.php';
require_once PATH_MODEL_ENTITIES .'Property.class.php';
require_once PATH_MODEL_ENTITIES .'RentToTheseDays.class.php';
require_once PATH_MODEL_ENTITIES .'RentToTheseMonths.class.php';
require_once PATH_CONTROLLER .'CityController.class.php';
require_once PATH_CONTROLLER .'StatusController.class.php';
require_once PATH_CONTROLLER .'PropertyTypeController.class.php';
require_once PATH_CONTROLLER .'RentToTheseDaysController.class.php';
require_once PATH_CONTROLLER .'RentToTheseMonthsController.class.php';
require_once PATH_CONTROLLER .'StoreController.class.php';


/**
 * Description of PropertyController
 *
 * @author Daniel
 */
class SearchPropertyController {

  private $connectionFactoryBI;
  private $cityController;
  private $statusController;
  private $propertyTypeController;
  private $propertyBI;
  private $storeController;

  function __construct() {
    $this->cityController = new CityController();
    $this->statusController = new StatusController();
    $this->propertyTypeController = new PropertyTypeController();
    $this->storeController = new StoreController();
  }


  //Add a new parameter code by Edilson Justiniano. This Field was add by new layout on the search
  public function search($cityId, $neighborhood,$type,$min,$max,$qtdMinRooms,$qtdMaxRooms,$status,
                    $limit,$offset,$deDate,$ateDate,$code){
      if(is_null($this->connectionFactoryBI)){
          $this->connectionFactoryBI = new ConnectionFactoryBI();
      }
      $connection = $this->connectionFactoryBI->createConnectionWithTransaction(false);

      $this->propertyBI = new PropertyBI($connection);
      
      //Add a new parameter code by Edilson Justiniano. This Field was add by new layout on the search
      $propertys =  $this->propertyBI->search($cityId, $neighborhood, $type, $min, $max,$qtdMinRooms, $qtdMaxRooms, $status,$limit,$offset,$deDate,$ateDate,$code);
      //Add a new parameter code by Edilson Justiniano. This Field was add by new layout on the searchSize
      $total = $this->propertyBI->searchSize($cityId, $neighborhood, $type, $min, $max, $qtdMinRooms, $qtdMaxRooms, $status,$deDate,$ateDate,$code);
      $this->propertyBI->releaseConnection($connection);

      $p = array("total"=>$total, "root"=>$propertys);

      return $p;
  }
  
  
  
  //Add a new parameter code by Edilson Justiniano. This Field was add by new layout on the search
  public function searchPropertyByStatus($status, $limit){
      if(is_null($this->connectionFactoryBI)){
          $this->connectionFactoryBI = new ConnectionFactoryBI();
      }
      $connection = $this->connectionFactoryBI->createConnectionWithTransaction(false);

      $this->propertyBI = new PropertyBI($connection);
      
      //Add a new parameter code by Edilson Justiniano. This Field was add by new layout on the search
      $propertys =  $this->propertyBI->searchPropertyByStatus($status,$limit);
      //Add a new parameter code by Edilson Justiniano. This Field was add by new layout on the searchSize
      $total = $this->propertyBI->searchSizeByStatus($status);
      $this->propertyBI->releaseConnection($connection);

      $p = array("total"=>$total, "root"=>$propertys);

      return $p;
  }
  
  
  
  /*
   * Method that will go search and return a qtde of property
   * stored on system, I can use this information on page of admin
   * list imoveis 
   */
  public function getCount(){
      
      if(is_null($this->connectionFactoryBI)){
          $this->connectionFactoryBI = new ConnectionFactoryBI();
      }
      $connection = $this->connectionFactoryBI->createConnectionWithTransaction(false);
      
      $this->propertyBI = new PropertyBI($connection);
      
      $qtde = $this->propertyBI->getCount();
      $this->propertyBI->releaseConnection($connection);
      
      return $qtde;
  }
  
      
  
  
  /*
   * Método que irá ser usado quando o usuário for fazer a busca pelo imóvel
   * e informar, ou só o código da propriedade ou todos os campos e mais o 
   * código da propriedade. É que caso tenha informado o código da propriedade
   * então ele (o usuário) sabe o que ele quer. Ou melhor qual imóvel ele quer
   * visualizar. Será passado como parâmetro o código informado pelo usuário
   */
  public function getByIdJSON($propertyId){
    if(is_null($this->connectionFactoryBI)){
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(FALSE);
    $this->propertyBI = new PropertyBI($connection);
    $property = $this->propertyBI->findById($propertyId);
    $this->propertyBI->releaseConnection($connection);
    
//    if (is_null($property)){
////        echo "error NULL";
////        exit();
//    } else {
//        echo "OK achou algo";
//    }
    
    $p = array("total"=> 1, "root"=>$property);
    
    return $p;
  }
}
?>