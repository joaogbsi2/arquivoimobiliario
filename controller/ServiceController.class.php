<?php

/*
 * date of creating 2013-11-12
 * 
 * @author Edilson Justiano
 */

//include the file of configuration
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI .'ConnectionFactoryBI.class.php';
require_once PATH_MODEL_BI .'ServiceBI.class.php';


/**
 * Description of ServiceProviderController
 *
 * @author Daniel
 */
class ServiceController {

  private $connectionFactoryBI;

  public function getAllServices() {
    if (is_null($this->connectionFactoryBI)) {
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(false);
    $serviceBI = new ServiceBI($connection);
    $services = $serviceBI->getAll();
    $serviceBI->releaseConnection($connection);

    return $services;
  }

  public function getById($id) {
    if (is_null($this->connectionFactoryBI)) {
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(false);

    $serviceBI = new ServiceBI($connection);
    $service = $serviceBI->getById($id);
    $serviceBI->releaseConnection($connection);

    return $service;
  }
  
  
  /*
   * Método para buscar todos os serviços que iniciarem com 
   * uma determinada letra, passada por parâmetro
   */
  public function getServicesByCriteria($letter) {
    if (is_null($this->connectionFactoryBI)) {
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(false);
    $serviceBI = new ServiceBI($connection);
    $services = $serviceBI->getServicesByCriteria($letter);
    $serviceBI->releaseConnection($connection);

    return $services;
  }

}

?>
