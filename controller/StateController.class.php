<?php

/*
 * INCLUDE SECTOR
 */

// include the file of configuration
// require_once '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI . 'ConnectionFactoryBI.class.php';
require_once PATH_MODEL_BI . 'StateBI.class.php';
class StateController {
	private $connectionFactoryBI;
	public function addState($params) {
		if (is_null ( $this->connectionFactoryBI )) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( FALSE );
		$stateBI = new StateBI ( $connection );
		
		try {
			$state = new State();
			$state->setName($params['name']);
			$state->setUf($params['uf']);
			
			$stateBI->addState($state);
		} catch (Exception $e) {
			$connection->rollBack();
			$this->connectionFactoryBI()->releaseConnection($connection);
			echo $exc->getTraceAsString();
		}
		$stateBI->releaseConnection($connection);
	}
	
	public function getAllStates() {
		if (is_null ( $this->connectionFactoryBI )) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( FALSE );
		$stateBI = new StateBI ( $connection );
		
		if ($states = $stateBI->getAll ()) {
			
			$stateBI->releaseConnection ( $connection );
			
			return $states;
		}
		$stateBI->releaseConnection ( $connection );
		return NULL;
	}
	
	public function deleteState($stateId){
		if(is_null($this->connectionFactoryBI)){
			$this->connectionFactoryBI = new ConnectionFactoryBI();
		}
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction(false);
	
		$stateBI = new StateBI($connection);
		$wasDeleted = $stateBI->delete($stateId);
		$stateBI->releaseConnection($connection);
	
		return $wasDeleted;
	}
	
	public function getById($id) {
		if ($this->connectionFactoryBI == NULL) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( FALSE );
		$stateBI = new StateBI ( $connection );
		if (! is_null ( $state = $stateBI->getById ( $id ) )) {
			$stateBI->releaseConnection ( $connection );
			return $state;
		} else {
			$stateBI->releaseConnection ( $connection );
			return NULL;
		}
	}
	
	public function getPorUf($uf){
		if ($this->connectionFactoryBI == NULL) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( FALSE );
		$stateBI = new StateBI ( $connection );
		if (! is_null ( $state = $stateBI->getPoruf($uf) )) {
			$stateBI->releaseConnection ( $connection );
			return $state;
		} else {
			$stateBI->releaseConnection ( $connection );
			return NULL;
		}
	}
	
	public function updateState($params) {
	
		if (is_null($this->connectionFactoryBI)) {
			$this->connectionFactoryBI = new ConnectionFactoryBI();
		}
	
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction(false);
	
		$stateBI = new StateBI($connection);
	
		try {
	
			$state = new State();
			$state->setId($params['id']);
			$state->setName($params['name']);
			$state->setUf($params['uf']);
	
			return $stateBI->updateState($state);
	
			$stateBI->releaseConnection($connection);
		} catch (Exception $exc) {
			$connection->rollBack();
			$this->connectionFactoryBI()->releaseConnection($connection);
			echo $exc->getTraceAsString();
		}
	
		$stateBI->releaseConnection($connection);
	
		return false;
	}
}

?>
