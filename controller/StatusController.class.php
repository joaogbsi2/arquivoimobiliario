<?php

/*
 * This class is responsible to validate the information about the login
 * of users.
 * 
 * @author Daniel / updated by Edilson Justiniano
 */


/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once './config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI .'ConnectionFactoryBI.class.php';
require_once PATH_MODEL_BI .'StatusBI.class.php';


class StatusController {

  private $connectionFactory;
  private $statusBI;

  function __construct() {
    $this->connectionFactory = new ConnectionFactoryBI();
  }

  public function getById($id) {
    $connection = $this->connectionFactory->createConnectionWithTransaction(FALSE);
    if (is_null($this->statusBI)) {
      $this->statusBI = new StatusBI($connection);
    }
    return $this->statusBI->getById($id);
  }

  public function getAll() {
    $connection = $this->connectionFactory->createConnectionWithTransaction(FALSE);
    if (is_null($this->statusBI)) {
      $this->statusBI = new StatusBI($connection);
    }
    return $this->statusBI->getAll();
  }

}

?>
