<?php

/*
 * This class is responsible to validate the information about the login
 * of users.
 */

/*
 * INCLUDE SECTOR
 */

// include the file of configuration
// require_once '../config.php';
// require_once PATH .'config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI . 'ConnectionFactoryBI.class.php';
require_once PATH_MODEL_BI . 'StoreBI.class.php';
require_once PATH_MODEL_ENTITIES . 'Store.class.php';
class StoreController {
	
	// the attribute that receives a instance of connection
	private $connectionFactoryBI;
	private $store;
	public function addStore($params) {
		if (is_null ( $this->connectionFactoryBI )) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( false );
		
		$storeBI = new StoreBI ( $connection );
		
		try {
			
			$store = new Store ();
			$store->setName ( $params ['name'] );
			// $store->setDescription($params['description']);
			$store->setMail ( $params ['mail'] );
			$store->setPhoto ( $params ['photo'] );
			$store->setEndereco ( $params ['endereco'] );
			$store->setTelefone ( $params ['telefone'] );
			$store->setResponsavel ( $params ['responsavel'] );
			
			$storeBI->addStore ( $store );
			
			$storeBI->releaseConnection ( $connection );
		} catch ( Exception $exc ) {
			$connection->rollBack ();
			$this->connectionFactoryBI ()->releaseConnection ( $connection );
			echo $exc->getTraceAsString ();
		}
		
		$storeBI->releaseConnection ( $connection );
	}
	
	/*
	 * This method was ad by Edilson Justiniano to find the all data of a client
	 * cadastred on system. This information will be used on root administrator page
	 * code add on day 2013-11-09 REFACTOR.
	 */
	public function getById($id) {
		if ($this->connectionFactoryBI == NULL) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( FALSE );
		$storeBI = new StoreBI ( $connection );
		if (! is_null ( $store = $storeBI->getById ( $id ) )) {
			
			$storeBI->releaseConnection ( $connection );
			return $store;
		}
		$storeBI->releaseConnection ( $connection );
		return NULL;
	}
	
	/*
	 * Methods add By Edilson Justiniano, on day 12/11/2013.
	 * This methods will be used to find all Users cadastre
	 * on system, or only same.
	 */
	public function findAll($limit, $orderBy) {
		if (is_null ( $this->connectionFactoryBI )) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( false );
		
		$storeBI = new StoreBI ( $connection );
		$stores = $storeBI->findAll ( $limit, $orderBy );
		$storeBI->releaseConnection ( $connection );
		
		return $stores;
	}
	
	/*
	 * Method that will go search and return a qtde of users
	 * stored on system, I can use this information on page of admin
	 * list users
	 */
	public function getCount() {
		if (is_null ( $this->connectionFactoryBI )) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( false );
		
		$storeBI = new StoreBI ( $connection );
		$qtde = $storeBI->getCount ();
		$storeBI->releaseConnection ( $connection );
		
		return $qtde;
	}
	
	/*
	 * Method that will remove a user from database
	 */
	public function delete($idStore) {
		if (is_null ( $this->connectionFactoryBI )) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( false );
		
		$storeBI = new StoreBI ( $connection );
		$wasDeleted = $storeBI->delete ( $idStore );
		$storeBI->releaseConnection ( $connection );
		
		return $wasDeleted;
	}
	
	/*
	 * Method that will go to make the update of user
	 */
	public function updateStore($params) {
		if (is_null ( $this->connectionFactoryBI )) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( false );
		
		$storeBI = new StoreBI ( $connection );
		
		try {
			
			$store = new Store ();
			$store->setId($params['id']);
			$store->setName ( $params ['name'] );
			// $store->setDescription($params['description']);
			$store->setMail ( $params ['mail'] );
			$store->setPhoto ( $params ['photo'] );
			$store->setEndereco ( $params ['endereco'] );
			$store->setTelefone ( $params ['telefone'] );
			$store->setResponsavel ( $params ['responsavel'] );
			
			$wasUpdated = $storeBI->updateStore ( $store );
			
			$storeBI->releaseConnection ( $connection );
		} catch ( Exception $exc ) {
			$connection->rollBack ();
			$this->connectionFactoryBI ()->releaseConnection ( $connection );
			echo $exc->getTraceAsString ();
		}
		
		return $wasUpdated;
	}
} // eof class UserController

?>
