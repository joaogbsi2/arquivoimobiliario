<?php

// require_once '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI . 'ConnectionFactoryBI.class.php';
require_once PATH_MODEL_BI . 'StreetBI.class.php';
require_once PATH_MODEL_ENTITIES . 'Street.class.php';
require_once PATH_CONTROLLER . 'CityController.class.php';
class StreetController {
	private $connectionFactoryBI;
	public function getAllStreets($limit = null, $orderBy = "") {
		if ($this->connectionFactoryBI == NULL) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( FALSE );
		$streetBI = new StreetBI ( $connection );
		
		if (! is_null ( $streets = $streetBI->getStreets ( $limit, $orderBy ) )) {
			$streetBI->releaseConnection ( $connection );
			return $streets;
		}
		
		$streetBI->releaseConnection ( $connection );
		return NULL;
	}
	public function getById($id) {
		if ($this->connectionFactoryBI == NULL) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( FALSE );
		$streetBI = new StreetBI ( $connection );
		
		if (! is_null ( $street = $streetBI->getById ( $id ) )) {
			$streetBI->releaseConnection ( $connection );
			return $street;
		}
		$streetBI->releaseConnection ( $connection );
		return NULL;
	}
	public function addStreet($params) {
		if ($this->connectionFactoryBI == NULL) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( FALSE );
		$streetBI = new StreetBI ( $connection );
		
		try {
			
			$street = new Street ();
			$aux = explode ( " ", $params ['street'], 2 );
			$street->setRua ( $aux [1] );
			$street->setBairro ( $params ['neighborhood'] );
			
			$cityController = new CityController ();
			
			if (is_int ( $params ['city'] )) {
				$city = $cityController->getById ( $params ['city'] );
			} else {
				$city = $cityController->getCitiesByName ( $params ['city'], null, null );
			}
			
			if (!is_array($city))
				$street->setCidade ( $city->getId () );
			else
				$street->setCidade ( $city [0]->getId () );
			$streetBI->addStreet ( $street );
			
			$streetBI->releaseConnection ( $connection );
		} catch ( Exception $e ) {
			$connection->rollBack ();
			$this->connectionFactoryBI ()->releaseConnection ( $connection );
			echo $e->getTraceAsString ();
		}
		$streetBI->releaseConnection ( $connection );
	}
	public function getCount() {
		if (is_null ( $this->connectionFactoryBI )) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( FALSE );
		
		$streetBI = new StreetBI ( $connection );
		$qtde = $streetBI->getCount ();
		$streetBI->releaseConnection ( $connection );
		
		return $qtde;
	}
	public function getStreetsByName($streetName, $limit, $orderBy) {
		if (is_null ( $this->connectionFactoryBI )) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( FALSE );
		
		$streetBI = new StreetBI ( $connection );
		$objs = $streetBI->getStreetsByName ( $streetName, $limit, $orderBy );
		$streetBI->releaseConnection ( $connection );
		
		return $objs;
	}
	public function getStreetsByBairroName($bairro, $streetName, $limit = NULL, $orderBy = "") {
		if (is_null ( $this->connectionFactoryBI )) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( FALSE );
		
		$streetBI = new StreetBI ( $connection );
		$objs = $streetBI->getStreetsByBairroName ( $bairro, $streetName, null, null );
		$streetBI->releaseConnection ( $connection );
		
		return $objs;
	}
	public function updateStreet($params) {
		if (is_null ( $this->connectionFactoryBI )) {
			$this->connectionFactoryBI = new ConnectionFactoryBI ();
		}
		
		$connection = $this->connectionFactoryBI->createConnectionWithTransaction ( FALSE );
		
		$streetBI = new StreetBI ( $connection );
		
		try {
			$street = new Street ();
			$aux = explode ( " ", $params ['street'], 2 );
			$street->setRua ( $aux [1] );
			$street->setBairro ( $params ['neighborhood'] );
			
			$cityController = new CityController ();
			$city = $cityController->getById ( $params ['city'] );
			
			$street->setCidade ( $city );
			return $streetBI->updateStreet ( $street );
			
			$streetBI->releaseConnection ( $connection );
		} catch ( Exception $e ) {
			$connection->rollBack ();
			$this->connectionFactoryBI ()->releaseConnection ( $connection );
			echo $e->getTraceAsString ();
		}
		
		$streetBI->releaseConnection ( $connection );
		
		return false;
	}
	public function getListStreets() {
		foreach ( $this->getAllStreets () as $street ) {
			$lista [$street->getRua ()] = $street->getId ();
		}
		return $lista;
	}
	public function atualizaListStreet($rua, $bairro, $cidade) {
		$params ['rua'] = $rua;
		$params ['bairro'] = $bairro;
		$params ['city'] = $cidade;
		$this->addStreet ( $params );
		return $this->getListStreets ();
		// $lista = $this->getListStreets();
		// return $lista;
	}
}