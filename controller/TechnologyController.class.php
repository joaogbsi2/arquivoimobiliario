<?php



/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once '../config.php';
#require_once PATH .'config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI.         'ConnectionFactoryBI.class.php';
require_once PATH_MODEL_BI.         'TechnologyBI.class.php';
require_once PATH_MODEL_ENTITIES.   'Technology.class.php';

class TechnologyController{
    
    //the attribute that receives a instance of connection
    private $connectionFactoryBI;
    
    
    public function addTechnology($params) {

        if (is_null($this->connectionFactoryBI)) {
          $this->connectionFactoryBI = new ConnectionFactoryBI();
        }

        $connection = $this->connectionFactoryBI->createConnectionWithTransaction(false);

        $tecBI = new TechnologyBI($connection);
        
        try {
            
          $tec = new Technology();
          $tec->setTitle($params['title']);
          $tec->setSubTitle($params['subTitle']);
          $tec->setDescription($params['description']);
          $tec->setPhoto($params['photo']);
            
          $tecBI->addTechnology($tec);

          $tecBI->releaseConnection($connection);
        } catch (Exception $exc) {
          $connection->rollBack();
          $this->connectionFactoryBI()->releaseConnection($connection);
          echo $exc->getTraceAsString();
        }

        $tecBI->releaseConnection($connection);
    }
  
   
  /*
   * Methods add By Edilson Justiniano, on day 12/11/2013. 
   * This methods will be used to find all Notice cadastre
   * on system, or only same.
   */
  public function findAll(){
      if(is_null($this->connectionFactoryBI)){
          $this->connectionFactoryBI = new ConnectionFactoryBI();
      }
      $connection = $this->connectionFactoryBI->createConnectionWithTransaction(false);
      
      $tecBI = new TechnologyBI($connection);
      $tec = $tecBI->findAll();
      $tecBI->releaseConnection($connection);
      
      return $tec;
    
  }
     
}//eof class UserController

?>
