<?php

/*
 * date of creating 2013-11-12
 * 
 * @author Edilson Justiano
 */

//include the file of configuration
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI .'ConnectionFactoryBI.class.php';
require_once PATH_MODEL_BI .'UserServiceBI.class.php';


/**
 * Description of UserServiceController
 *
 * @author Daniel
 */
class UserServiceController {

  private $connectionFactoryBI;

  public function createServiceProvider($userChoice, User $user) {
    $success = FALSE;
    if (is_null($this->connectionFactoryBI)) {
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(TRUE);

    $userServiceBI = new UserServiceBI($connection);

    try {
      $success = $userServiceBI->createNewServiceProvider($userChoice, $user);
      $userServiceBI->commitConnection(TRUE);
    } catch (PDOException $exc) {
      echo $exc->getTraceAsString();
      $userServiceBI->rollBackConnection(TRUE);
    }
    return $success;
  }

  public function getServiceProvider($userId) {
    if (is_null($this->connectionFactoryBI)) {
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(FALSE);
    
    $userServiceBI = new UserServiceBI($connection);

    $userService = $userServiceBI->getUserService($userId);

    $userServiceBI->releaseConnection($connection);

    return $userService;
  }
  
  
  /*
   * Method to delete all serviceProvider of this user
   */
  public function deleteServiceProvider($userId){
      
    if (is_null($this->connectionFactoryBI)) {
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(FALSE);
    
    $userServiceBI = new UserServiceBI($connection);

    $wasDeleted = $userServiceBI->deleteServiceProvider($userId);

    $userServiceBI->releaseConnection($connection);

    return $wasDeleted;
      
  }

  /*
   * Método para mostrar os usuários na tela de serviço
   * é chamado da tela de servicos
   */
  public function getUserServiceProvider($serviceId) {
    if (is_null($this->connectionFactoryBI)) {
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(FALSE);
    
    $userServiceBI = new UserServiceBI($connection);

    $userService = $userServiceBI->getUserServiceByService($serviceId);

    $userServiceBI->releaseConnection($connection);

    return $userService;
  }
  
  public function findServiceProviderByUser($serviceId, $userId){
    if (is_null($this->connectionFactoryBI)) {
      $this->connectionFactoryBI = new ConnectionFactoryBI();
    }
    $connection = $this->connectionFactoryBI->createConnectionWithTransaction(FALSE);
    
    $userServiceBI = new UserServiceBI($connection);

    $userService = $userServiceBI->findServiceProviderByUser($serviceId, $userId);

    $userServiceBI->releaseConnection($connection);

    return $userService;
  	
  }
}

?>
