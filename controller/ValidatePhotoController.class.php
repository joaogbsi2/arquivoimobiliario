<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * created by Edilson Justiniano
 * day 2014-02-15
 */

/*
 * Class to validate the any configurations about
 * pictureas like as width and height
 */
class ValidatePhotoController{
    
    
    /*
     * Validate Width and Height Picture
     */
    public function isValidWidthAndHeight($minWidth,$maxWidth,$minHeight,$maxHeight,$picture){
        
        /*
         * Tentar pegar a largura e altura de uma imagem
         */
        if (is_null($picture))
            return true;
        
        $info = getimagesize($picture);
        //largura (width) = info[0]
        //altura (height) = info[1]
        
        //validar largura da imagem
        if ($info[0] < $minWidth || $info[0] > $maxWidth)
            return false;
        
        //validar a altura da imagem
        if ($info[1] < $minHeight || $info[1] > $maxHeight)
            return false;
        
        return true;
       
    }
}


?>
