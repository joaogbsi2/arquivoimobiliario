-- MySQL dump 10.13  Distrib 5.6.12, for osx10.7 (x86_64)
--
-- Host: localhost    Database: webmovel
-- ------------------------------------------------------
-- Server version	5.6.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `advertising`
--

DROP TABLE IF EXISTS `advertising`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `advertising` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `position` int(11) NOT NULL,
  `photo` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `advertising`
--

LOCK TABLES `advertising` WRITE;
/*!40000 ALTER TABLE `advertising` DISABLE KEYS */;
INSERT INTO `advertising` VALUES (1,1,'/home/arqui937/public_html/images/advertising/IMG_0484.JPG1375142858.JPG'),(2,2,'/home/arqui937/public_html/images/advertising/IMG_0484.JPG1375142912.JPG'),(3,3,'/home/arqui937/public_html/images/advertising/IMG_1158.JPG1375142987.JPG'),(4,4,'/home/arqui937/public_html/images/advertising/IMG_1158.JPG1375143057.JPG'),(5,5,'/home/arqui937/public_html/images/advertising/IMG_1172.JPG1375143529.JPG'),(6,6,'/home/arqui937/public_html/images/advertising/IMG_1170.JPG1375145522.JPG');
/*!40000 ALTER TABLE `advertising` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) DEFAULT NULL,
  `state` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_city_state` (`state`),
  CONSTRAINT `fk_city_state` FOREIGN KEY (`state`) REFERENCES `state` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city`
--

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
INSERT INTO `city` VALUES (1,'Pouso Alegre',1);
/*!40000 ALTER TABLE `city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `favorite_property`
--

DROP TABLE IF EXISTS `favorite_property`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `favorite_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_property_user` int(11) NOT NULL,
  `user_property_property` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_favorite_property_property` (`user_property_property`),
  KEY `fk_favorite_property_user` (`user_property_user`),
  CONSTRAINT `fk_favorite_property_property` FOREIGN KEY (`user_property_property`) REFERENCES `property` (`id`),
  CONSTRAINT `fk_favorite_property_user` FOREIGN KEY (`user_property_user`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `favorite_property`
--

LOCK TABLES `favorite_property` WRITE;
/*!40000 ALTER TABLE `favorite_property` DISABLE KEYS */;
/*!40000 ALTER TABLE `favorite_property` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property`
--

DROP TABLE IF EXISTS `property`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `measure` varchar(45) DEFAULT NULL,
  `street` varchar(45) DEFAULT NULL,
  `identifier` varchar(45) DEFAULT NULL,
  `neighborhood` varchar(45) DEFAULT NULL,
  `latitude` varchar(45) DEFAULT NULL,
  `longitude` varchar(45) DEFAULT NULL,
  `city` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `price` double NOT NULL,
  `description` text,
  `qtd_rooms` int(11) DEFAULT NULL,
  `contact_phone` varchar(14) DEFAULT NULL,
  `contact_email` varchar(45) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_property_city` (`city`),
  KEY `fk_property_status` (`status`),
  KEY `fk_property_type` (`type`),
  CONSTRAINT `fk_property_city` FOREIGN KEY (`city`) REFERENCES `city` (`id`),
  CONSTRAINT `fk_property_status` FOREIGN KEY (`status`) REFERENCES `status` (`id`),
  CONSTRAINT `fk_property_type` FOREIGN KEY (`type`) REFERENCES `property_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property`
--

LOCK TABLES `property` WRITE;
/*!40000 ALTER TABLE `property` DISABLE KEYS */;
INSERT INTO `property` VALUES (1,NULL,'Av. Antonio Scodeller','1000','Faisqueira',NULL,NULL,1,2,1,1000,'Ótima Caaá',2,'','edilsonjustiniano@gmail.com',1),(2,NULL,'Av. Antonio Scodeller','1000','Faisqueira',NULL,NULL,1,1,1,1000,'Ótima Casa áááéõ',1,'','edilsonjustiniano@gmail.com',0),(3,NULL,'Av. Antonio Scodeller','1000','Faisqueira',NULL,NULL,1,1,1,1000,'Ótima Casa áááéõ',1,'','edilsonjustiniano@gmail.com',1),(4,NULL,'Av. Duque de Caxias','212','Centro',NULL,NULL,1,2,1,10,'Ótima casa',3,'','edilsonjustiniano@gmail.com',1),(5,NULL,'Rua Clemente Scodeller','108','Faisqueira',NULL,NULL,1,2,1,2000,'Bela casa. Levá-la!',3,'','edilsonjustiniano@gmail.com',1),(6,NULL,'Rua Adalberto Ferraz','200','Centro',NULL,NULL,1,2,1,3500,'Casa linda',2,'','edilsonjustiniano@gmail.com',1),(7,NULL,'Rua Oswaldo Bernardes','34','Faisqueira',NULL,NULL,1,2,1,33000,'Tal e tal e tal',4,'','ads@ads.com',1),(8,NULL,'Rua Bom Jesus','116','Centro',NULL,NULL,1,2,1,100,'oká',2,'','aaa@aaa.com',1),(9,NULL,'Rua Três Corações','30','São João',NULL,NULL,1,2,1,30,'',1,'','aaa@aaa.com',1),(10,NULL,'Av. Dr. Lisboa','396','Centro',NULL,NULL,1,2,1,44.44,'Blz',4,'','aaa@aaa.com',1),(11,NULL,'Rua Silviano Brandão','272','Centro',NULL,NULL,1,2,1,220,'',2,'','aaa@aaa.com',1),(12,NULL,'Rua Bom Jesus','400','Centro',NULL,NULL,1,2,1,20,'',3,'','aaa@aaa.com',1),(13,NULL,'Rua Silviano Brandão','130','Centro',NULL,NULL,1,2,1,10,'',3,'','aaa@aaa.com',1),(14,NULL,'Av. Antonio Scodeller','108','Faisqueira',NULL,NULL,1,2,1,100,'',3,'','aaa@aaa.com',1),(15,NULL,'Rua Bom Jesus','675','Centro',NULL,NULL,1,2,1,20,'',2,'','aaa@aaa.com',1),(16,NULL,'Rua Adalberto Ferraz','308','Centro',NULL,NULL,1,1,3,400,'ótimo Apto!',2,'','aa@aa.com',1),(17,NULL,'Rua Bom Jesus','309','Centro',NULL,NULL,1,3,1,4000,'',5,'','aa@aa.com',0),(18,NULL,'Av. Dr. Lisboa','345','Centro',NULL,NULL,1,3,1,10,'hahahaáá',3,'','aa@aa.com',1);
/*!40000 ALTER TABLE `property` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property_photo`
--

DROP TABLE IF EXISTS `property_photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` text NOT NULL,
  `property` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property_photo`
--

LOCK TABLES `property_photo` WRITE;
/*!40000 ALTER TABLE `property_photo` DISABLE KEYS */;
INSERT INTO `property_photo` VALUES (1,'/Users/edilson/Sites/webmovel/MysqlMigration/TM_Project1/images/property_photo/Foto0198.jpg1373729958.jpg',6),(2,'/Users/edilson/Sites/webmovel/MysqlMigration/TM_Project1/images/property_photo/IMG_1170.JPG1373735099.JPG',7),(3,'/Users/edilson/Sites/webmovel/MysqlMigration/TM_Project1/images/property_photo/IMG_1203.JPG1373735099.JPG',7),(4,'/Users/edilson/Sites/webmovel/MysqlMigration/TM_Project1/images/property_photo/IMG_1158.JPG1373735099.JPG',7),(5,'/Users/edilson/Sites/webmovel/MysqlMigration/TM_Project1/images/property_photo/IMG_1159.JPG1373798464.JPG',8),(6,'/Users/edilson/Sites/webmovel/MysqlMigration/TM_Project1/images/property_photo/IMG_1200.JPG1373798464.JPG',8),(7,'/Users/edilson/Sites/webmovel/MysqlMigration/TM_Project1/images/property_photo/IMG_1199.JPG1373798464.JPG',8),(8,'/Users/edilson/Sites/webmovel/MysqlMigration/TM_Project1/images/property_photo/IMG_1169.JPG1373798464.JPG',8),(9,'/Users/edilson/Sites/webmovel/MysqlMigration/TM_Project1/images/property_photo/IMG_1204.JPG1373798464.JPG',8),(10,'/Users/edilson/Sites/webmovel/MysqlMigration/TM_Project1/images/property_photo/IMG_1159.JPG1373798545.JPG',9),(11,'/Users/edilson/Sites/webmovel/MysqlMigration/TM_Project1/images/property_photo/IMG_1202.JPG1373798545.JPG',9),(12,'/Users/edilson/Sites/webmovel/MysqlMigration/TM_Project1/images/property_photo/IMG_1170.JPG1373798642.JPG',10),(13,'/Users/edilson/Sites/webmovel/MysqlMigration/TM_Project1/images/property_photo/IMG_1159.JPG1373798642.JPG',10),(14,'/Users/edilson/Sites/webmovel/MysqlMigration/TM_Project1/images/property_photo/IMG_1168.JPG1373798878.JPG',11),(15,'/Users/edilson/Sites/webmovel/MysqlMigration/TM_Project1/images/property_photo/IMG_1198.JPG1373798946.JPG',12),(16,'/Users/edilson/Sites/webmovel/MysqlMigration/TM_Project1/images/property_photo/IMG_1169.JPG1373799096.JPG',13),(17,'/Users/edilson/Sites/webmovel/MysqlMigration/TM_Project1/images/property_photo/IMG_1171.JPG1373801065.JPG',14),(18,'/Users/edilson/Sites/webmovel/MysqlMigration/TM_Project1/images/property_photo/IMG_1168.JPG1373801118.JPG',15),(19,'/Users/edilson/Sites/webmovel/MysqlMigration/TM_Project1/images/property_photo/IMG_1174.JPG1373803047.JPG',16),(20,'/Users/edilson/Sites/webmovel/MysqlMigration/TM_Project1/images/property_photo/IMG_1171.JPG1373804397.JPG',17),(21,'/Users/edilson/Sites/webmovel/MysqlMigration/TM_Project1/images/property_photo/IMG_1159.JPG1373931912.JPG',18);
/*!40000 ALTER TABLE `property_photo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property_type`
--

DROP TABLE IF EXISTS `property_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property_type`
--

LOCK TABLES `property_type` WRITE;
/*!40000 ALTER TABLE `property_type` DISABLE KEYS */;
INSERT INTO `property_type` VALUES (1,'casa'),(2,'casa/fundos'),(3,'apartamento'),(4,'flat'),(5,'kitnet'),(6,'comércio'),(7,'galpão');
/*!40000 ALTER TABLE `property_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rent_to_these_days`
--

DROP TABLE IF EXISTS `rent_to_these_days`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rent_to_these_days` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domingo` tinyint(1) DEFAULT NULL,
  `segunda` tinyint(1) DEFAULT NULL,
  `terca` tinyint(1) DEFAULT NULL,
  `quarta` tinyint(1) DEFAULT NULL,
  `quinta` tinyint(1) DEFAULT NULL,
  `sexta` tinyint(1) DEFAULT NULL,
  `sabado` tinyint(1) DEFAULT NULL,
  `property` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `property_rent_to_these_days_fk` (`property`),
  CONSTRAINT `property_rent_to_these_days_fk` FOREIGN KEY (`property`) REFERENCES `property` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rent_to_these_days`
--

LOCK TABLES `rent_to_these_days` WRITE;
/*!40000 ALTER TABLE `rent_to_these_days` DISABLE KEYS */;
INSERT INTO `rent_to_these_days` VALUES (1,1,1,1,1,1,1,1,17),(2,1,1,1,1,1,1,1,18);
/*!40000 ALTER TABLE `rent_to_these_days` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rent_to_these_months`
--

DROP TABLE IF EXISTS `rent_to_these_months`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rent_to_these_months` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `janeiro` tinyint(1) DEFAULT NULL,
  `fevereiro` tinyint(1) DEFAULT NULL,
  `marco` tinyint(1) DEFAULT NULL,
  `abril` tinyint(1) DEFAULT NULL,
  `maio` tinyint(1) DEFAULT NULL,
  `junho` tinyint(1) DEFAULT NULL,
  `julho` tinyint(1) DEFAULT NULL,
  `agosto` tinyint(1) DEFAULT NULL,
  `setembro` tinyint(1) DEFAULT NULL,
  `outubro` tinyint(1) DEFAULT NULL,
  `novembro` tinyint(1) DEFAULT NULL,
  `dezembro` tinyint(1) DEFAULT NULL,
  `property` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `property_rent_to_these_months_fk` (`property`),
  CONSTRAINT `property_rent_to_these_months_fk` FOREIGN KEY (`property`) REFERENCES `property` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rent_to_these_months`
--

LOCK TABLES `rent_to_these_months` WRITE;
/*!40000 ALTER TABLE `rent_to_these_months` DISABLE KEYS */;
INSERT INTO `rent_to_these_months` VALUES (1,NULL,NULL,NULL,NULL,NULL,1,1,NULL,1,1,1,1,17),(2,1,1,1,1,1,1,1,NULL,1,1,1,1,18);
/*!40000 ALTER TABLE `rent_to_these_months` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'root_admin'),(2,'cliente'),(3,'administrador');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms`
--

DROP TABLE IF EXISTS `rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms`
--

LOCK TABLES `rooms` WRITE;
/*!40000 ALTER TABLE `rooms` DISABLE KEYS */;
INSERT INTO `rooms` VALUES (1,'Suite'),(2,'Quarto'),(3,'Sala'),(4,'Copa'),(5,'Cozinha'),(6,'Banheiro'),(7,'Área de serviço');
/*!40000 ALTER TABLE `rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms_property`
--

DROP TABLE IF EXISTS `rooms_property`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rooms_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rooms` int(11) NOT NULL,
  `property` int(11) NOT NULL,
  `measure` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_rooms_property_property` (`property`),
  KEY `fk_rooms_property_rooms` (`rooms`),
  CONSTRAINT `fk_rooms_property_property` FOREIGN KEY (`property`) REFERENCES `property` (`id`),
  CONSTRAINT `fk_rooms_property_rooms` FOREIGN KEY (`rooms`) REFERENCES `rooms` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms_property`
--

LOCK TABLES `rooms_property` WRITE;
/*!40000 ALTER TABLE `rooms_property` DISABLE KEYS */;
/*!40000 ALTER TABLE `rooms_property` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_description` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service`
--

LOCK TABLES `service` WRITE;
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
INSERT INTO `service` VALUES (1,'Téc. de enfermagem'),(2,'Téc. de informática'),(3,'Cuidador(ª)'),(4,'Babá'),(5,'Pedreiro (alvenaria)'),(6,'Pedreiro (acabamento)'),(7,'Pintor'),(8,'Encanador'),(9,'Eletricista predial'),(10,'Eletricista residencial'),(11,'Carpinteiro'),(12,'Marceneiro');
/*!40000 ALTER TABLE `service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `state`
--

DROP TABLE IF EXISTS `state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `uf` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `state`
--

LOCK TABLES `state` WRITE;
/*!40000 ALTER TABLE `state` DISABLE KEYS */;
INSERT INTO `state` VALUES (1,'Minas Gerais','MG');
/*!40000 ALTER TABLE `state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS `status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status`
--

LOCK TABLES `status` WRITE;
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
INSERT INTO `status` VALUES (1,'Aluguel'),(2,'Compra'),(3,'Temporada');
/*!40000 ALTER TABLE `status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_property`
--

DROP TABLE IF EXISTS `user_property`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_property_user` int(11) NOT NULL,
  `user_property_property` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_property_property` (`user_property_property`),
  KEY `fk_user_property_user` (`user_property_user`),
  CONSTRAINT `fk_user_property_property` FOREIGN KEY (`user_property_property`) REFERENCES `property` (`id`),
  CONSTRAINT `fk_user_property_user` FOREIGN KEY (`user_property_user`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_property`
--

LOCK TABLES `user_property` WRITE;
/*!40000 ALTER TABLE `user_property` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_property` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_service`
--

DROP TABLE IF EXISTS `user_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_service` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_role_service` (`id_service`),
  KEY `fk_role_user` (`id_user`),
  CONSTRAINT `fk_role_service` FOREIGN KEY (`id_service`) REFERENCES `service` (`id`),
  CONSTRAINT `fk_role_user` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_service`
--

LOCK TABLES `user_service` WRITE;
/*!40000 ALTER TABLE `user_service` DISABLE KEYS */;
INSERT INTO `user_service` VALUES (1,1,1);
/*!40000 ALTER TABLE `user_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `mobile_phone` varchar(14) DEFAULT NULL,
  `phone` varchar(14) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `user_photo_path` varchar(150) DEFAULT NULL,
  `role` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_role` (`role`),
  CONSTRAINT `fk_user_role` FOREIGN KEY (`role`) REFERENCES `role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'ÉdiÁõ','f970e2767d0cfe75876ea857f92e319b','','','ed@aa.com','',2),(2,'aaá','c20ad4d76fe97759aa27a0c99bff6710','','','aa@aa.com','',2),(3,'edil\'ssss\'sá','c4ca4238a0b923820dcc509a6f75849b','','','teste@test.com','',2),(4,'lalal','c4ca4238a0b923820dcc509a6f75849b','','','lala@lala.com','',2),(5,'xaxaxa','81dc9bdb52d04dc20036dbd8313ed055','','','xax@xaxa.com','',2),(6,'admin','81dc9bdb52d04dc20036dbd8313ed055',NULL,NULL,'admin',NULL,3);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-08-01 20:59:49
