# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.6.12)
# Database: webmovel
# Generation Time: 2013-11-18 22:55:17 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table advertising
# ------------------------------------------------------------

DROP TABLE IF EXISTS `advertising`;

CREATE TABLE `advertising` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `position` int(11) NOT NULL,
  `photo` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table city
# ------------------------------------------------------------

DROP TABLE IF EXISTS `city`;

CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) DEFAULT NULL,
  `state` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_city_state` (`state`),
  CONSTRAINT `fk_city_state` FOREIGN KEY (`state`) REFERENCES `state` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table favorite_property
# ------------------------------------------------------------

DROP TABLE IF EXISTS `favorite_property`;

CREATE TABLE `favorite_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_property_user` int(11) NOT NULL,
  `user_property_property` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_favorite_property_property` (`user_property_property`),
  KEY `fk_favorite_property_user` (`user_property_user`),
  CONSTRAINT `fk_favorite_property_property` FOREIGN KEY (`user_property_property`) REFERENCES `property` (`id`),
  CONSTRAINT `fk_favorite_property_user` FOREIGN KEY (`user_property_user`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table property
# ------------------------------------------------------------

DROP TABLE IF EXISTS `property`;

CREATE TABLE `property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `measure` varchar(45) DEFAULT NULL,
  `street` varchar(45) DEFAULT NULL,
  `identifier` varchar(45) DEFAULT NULL,
  `neighborhood` varchar(45) DEFAULT NULL,
  `latitude` varchar(45) DEFAULT NULL,
  `longitude` varchar(45) DEFAULT NULL,
  `city` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `price` double NOT NULL,
  `description` text,
  `qtd_rooms` int(11) DEFAULT NULL,
  `contact_phone` varchar(14) DEFAULT NULL,
  `contact_email` varchar(45) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_property_city` (`city`),
  KEY `fk_property_status` (`status`),
  KEY `fk_property_type` (`type`),
  CONSTRAINT `fk_property_city` FOREIGN KEY (`city`) REFERENCES `city` (`id`),
  CONSTRAINT `fk_property_status` FOREIGN KEY (`status`) REFERENCES `status` (`id`),
  CONSTRAINT `fk_property_type` FOREIGN KEY (`type`) REFERENCES `property_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table property_photo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `property_photo`;

CREATE TABLE `property_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` text NOT NULL,
  `property` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table property_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `property_type`;

CREATE TABLE `property_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table rent_to_these_days
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rent_to_these_days`;

CREATE TABLE `rent_to_these_days` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domingo` tinyint(1) DEFAULT NULL,
  `segunda` tinyint(1) DEFAULT NULL,
  `terca` tinyint(1) DEFAULT NULL,
  `quarta` tinyint(1) DEFAULT NULL,
  `quinta` tinyint(1) DEFAULT NULL,
  `sexta` tinyint(1) DEFAULT NULL,
  `sabado` tinyint(1) DEFAULT NULL,
  `property` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `property_rent_to_these_days_fk` (`property`),
  CONSTRAINT `property_rent_to_these_days_fk` FOREIGN KEY (`property`) REFERENCES `property` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table rent_to_these_months
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rent_to_these_months`;

CREATE TABLE `rent_to_these_months` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `janeiro` tinyint(1) DEFAULT NULL,
  `fevereiro` tinyint(1) DEFAULT NULL,
  `marco` tinyint(1) DEFAULT NULL,
  `abril` tinyint(1) DEFAULT NULL,
  `maio` tinyint(1) DEFAULT NULL,
  `junho` tinyint(1) DEFAULT NULL,
  `julho` tinyint(1) DEFAULT NULL,
  `agosto` tinyint(1) DEFAULT NULL,
  `setembro` tinyint(1) DEFAULT NULL,
  `outubro` tinyint(1) DEFAULT NULL,
  `novembro` tinyint(1) DEFAULT NULL,
  `dezembro` tinyint(1) DEFAULT NULL,
  `property` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `property_rent_to_these_months_fk` (`property`),
  CONSTRAINT `property_rent_to_these_months_fk` FOREIGN KEY (`property`) REFERENCES `property` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table rooms
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rooms`;

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table rooms_property
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rooms_property`;

CREATE TABLE `rooms_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rooms` int(11) NOT NULL,
  `property` int(11) NOT NULL,
  `measure` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_rooms_property_property` (`property`),
  KEY `fk_rooms_property_rooms` (`rooms`),
  CONSTRAINT `fk_rooms_property_property` FOREIGN KEY (`property`) REFERENCES `property` (`id`),
  CONSTRAINT `fk_rooms_property_rooms` FOREIGN KEY (`rooms`) REFERENCES `rooms` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table service
# ------------------------------------------------------------

DROP TABLE IF EXISTS `service`;

CREATE TABLE `service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_description` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table state
# ------------------------------------------------------------

DROP TABLE IF EXISTS `state`;

CREATE TABLE `state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `uf` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table status
# ------------------------------------------------------------

DROP TABLE IF EXISTS `status`;

CREATE TABLE `status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table user_property
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_property`;

CREATE TABLE `user_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_property_user` int(11) NOT NULL,
  `user_property_property` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_property_property` (`user_property_property`),
  KEY `fk_user_property_user` (`user_property_user`),
  CONSTRAINT `fk_user_property_property` FOREIGN KEY (`user_property_property`) REFERENCES `property` (`id`),
  CONSTRAINT `fk_user_property_user` FOREIGN KEY (`user_property_user`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table user_service
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_service`;

CREATE TABLE `user_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_service` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_role_service` (`id_service`),
  KEY `fk_role_user` (`id_user`),
  CONSTRAINT `fk_role_service` FOREIGN KEY (`id_service`) REFERENCES `service` (`id`),
  CONSTRAINT `fk_role_user` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `mobile_phone` varchar(14) DEFAULT NULL,
  `phone` varchar(14) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `user_photo_path` varchar(150) DEFAULT NULL,
  `role` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_user_role` (`role`),
  CONSTRAINT `fk_user_role` FOREIGN KEY (`role`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
