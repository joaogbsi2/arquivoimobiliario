﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cadastro Empresa</title>
</head>
<style type="text/css">
#formulario {
	width: 500px;
	height: 300px;
	float: left;
	background: #F7F7F7;
}

#formulario #titulo {
	width: 500px;
	font-size: 18px;
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	color: #FFF;
	margin: 10px 0 0 0;
	color: #333;
	text-align: center;
	float: left;
}

#formulario #form {
	width: 460px;
	float: left;
	margin: 20px 0 0 20px;
}

#formulario #form .txt1 {
	font-family: 'GothanMedium';
	font-size: 14px;
	color: #0D547F;
}

#formulario #form .campo1 {
	width: 350px;
	height: 30px;
	float: left;
	font-size: 14px;
	color: #333;
}

#formulario #form .campo2 {
	width: 350px;
	height: 80px;
	float: left;
	font-size: 14px;
	color: #333;
}
</style>
<body>

	<div id="formulario">
		<div id="titulo">Agendar uma visita</div>
		<div id="form">
			<table width="457">
				<tr>
					<td width="65" align="right" class="txt1">Nome:</td>
					<td width="1"></td>
					<td width="350"><input name="user" type="text" class="campo1"
						id="user" /></td>
				</tr>
				<tr>
					<td align="right" class="txt1">E-mail:</td>
					<td width="1"></td>
					<td><input name="user" type="text" class="campo1" id="user" /></td>
				</tr>
				<tr>
					<td align="right" class="txt1">Telefone:</td>
					<td width="1"></td>
					<td><input name="user" type="text" class="campo1" id="user" /></td>
				</tr>
				<tr>
					<td align="right" class="txt1">Escolha Imobiliária:</td>
					<td width="1"></td>
					<td><select name="quartos" class="campo1" id title="bairro">
							<option value="label">Label</option>
					</select></td>
				</tr>
				<tr>
					<td colspan="2"></td>
					<td align="right"><a href="#"><img src="images/bt_enviar.png"
							width="100" height="30" /></a></td>
				</tr>
			</table>
		</div>
	</div>

</body>
</html>