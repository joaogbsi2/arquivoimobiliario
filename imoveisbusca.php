﻿<?php

$pesquisaPorId = false;

/*
 * INCLUDE SECTOR
 */

// $_SERVER['DOCUMENT_ROOT'] = "/home/arqui937/public_html/";
// define ("PATH", $_SERVER['DOCUMENT_ROOT']);

// include the file of configuration
// // require_once '/home/arqui937/public_html/config.php';
// require_once '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_ACTION . 'search.action.php';
require_once PATH_ACTION . 'index.action.php';
require_once PATH_CONTROLLER . 'StatusController.class.php';
require_once PATH_ACTION . 'adminServices.action.php';
require_once PATH_MODEL_ENTITIES . 'Advertising.class.php';
require_once PATH_UTIL . 'FaixaPrecos.php';


?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon" href="images/arquiLogo.png">
<title>Arquivo Imobiliário</title>
<link href="estilo.css" rel="stylesheet" type="text/css" />
<!-- <link href="estilo.css" rel="stylesheet" type="text/css" /> -->

<!-- jquery ui theme and select multiple theme -->
<link rel="stylesheet" type="text/css" href="css/jquery.multiselect.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<!-- Modernizr do Slider em jquery das propagandas principais -->
<script src="js/modernizr.js"></script>
<!-- CSS do slider em jquery -->
<link rel="stylesheet" href="css/flexslider.css" type="text/css"
	media="screen" />
</head>
<body>
	<!-- JS Content -->
	<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>

	<!-- includes for multiple select -->
	<script type="text/javascript" src="js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/jquery.multiselect.min.js"></script>
	<script type="text/javascript" src="js/jquery.validate.min.js"></script>
	<!-- end multiple select include -->

	<!-- includes for jquery price -->
	<script type="text/javascript" src="js/jquery.price_format.1.7.min.js"></script>
	<!-- end jquery price include -->
	<!-- Inicio Geral -->
	<div id="geral">
		<header id="topo">
			<?php 
			require_once PATH_INCLUDES_USERS . 'header.php';
			?>
		</header>

		<!-- Inicio Main Conteudo -->
			<div id="mainConteudo">
				<!-- Inicio Conteudo -->
				<div id="conteudo">
					<!-- Inicio Conteudo File 1 -->
					<div id="home_file1">
						<!-- Inicio Coluna esquerda -->
						<div id="hf1_Col1">
							<?php
							/* Menu de Busca */
							require_once PATH_INCLUDES_USERS . 'form-search.php';
							/*divulgacao*/
							require_once PATH_INCLUDES_USERS . 'window-parceiros.php';
							?>
						</div>
						<!-- fim Coluna esquerda -->
						<!-- Inicio Coluna Esquerda -->
						<div id="hf1_Col2">
							<?php
							/* Menu navegação + banner */
							require_once PATH_INCLUDES_USERS . 'main-menu.php';
							?>
						</div>
						<!-- Fim Coluna Esquerda -->
					</div>
					<!-- Fim Conteudo File 1 -->
					<!-- Inicio Conteudo File 2 -->
					<div id="home_file2">
						<!-- Inicio Conteudo Busca -->
						<div id="Col_Busca">
							<?php
							if (count ( $propertys ) == 0) {
							?>
								<script language="javascript"> 
                                window.alert("Sua busca não localizou nenhum resultado!"); 
                            	</script>
                            	Sua busca não localizou nenhum resultado! Tente alterar algumas informações da busca!
                            <?php
                        	}
                        	function formatNumber($number){
                        		return number_format ( $number, 2, ',', '.' );
                        	}
                        	function showMinProperty($property){
                        		$controller = new PropertyPhotoController ();
                        		$photo = $controller->getUniquePhotoByProperty ( $property->getId () );
                        		if (is_null ( $photo )){
                        			$path = 'images/ft1.jpg';
                        		}else{
                        			$path = URL_PROPERTY_PHOTOS . basename ( $photo );
                        		}
                        		?>
                        	<!--Inicio Resultado  -->
                        	<div id="cxResult">
                        		<div id="cxTit"><?php echo $property->getCity(); ?></div>
                        		<!-- Inicio Imagem -->
                        		<div id="cxBgImg">
                        			<div id="imFT">
                        				<?php if (is_null($photo)){ ?>
                        					<a href="imoveisview.php?propertyId=<?php echo $property->getId(); ?>&statusProperty=<?php echo $property->getStatus()->getId();?>">
											<img src="<?php echo $path; ?>" /></a>
                        				<?php
                        				}else{?>
                        					<a href="imoveisview.php?propertyId=<?php echo $property->getId(); ?>&statusProperty=<?php echo $property->getStatus()->getId();?>">
											<img class="tamanho-foto" src="<?php echo $path; ?>" /> </a>
                        				<?php }?>                        					
                        			</div>
                        		</div>
                        		<!-- Fim Imagem -->
                        		<?php
                        		$faixaValue = NULL;

                        		if ($property->getStatus () != "Compra" )
									$faixaValue = $property->getPrice ();
								else{
									$faixaValue = FaixaPrecos::buscaFaixa($property->getPrice ());
								}
								
                        		?>
                        		<div id="cxPreco"><?php if ($property->getStatus() != "Compra") echo "R$ ". $faixaValue; else echo $faixaValue; ?></div>
                        		<div id="cxInfo">Quartos: <?php echo $property->getQtdRooms(); ?></div>
                        		<div id="cxDesc"><?php echo " ".$property->getType(); ?></div>
                        		<div id="cxBtmais">
									<a href="imoveisview.php?propertyId=<?php echo $property->getId(); ?>&statusProperty=<?php echo $property->getStatus()->getId();?>">
									<img src="images/bt_mais.png" /></a>
								</div>
                        	</div>
                        	<!-- Fim Resultado -->
                        	<?php	
                        	}// fim da function showMinProperty
                        	if (count ( $propertys ) == 1 && ! is_null ( $propertys ) && $pesquisaPorId)
                        		showMinProperty ( $propertys );
                        	else
                        		for($i = 0; $i < count ( $propertys ); $i ++) {
									showMinProperty ( $propertys [$i] );
								}
                            ?>								
						</div>						
						<!-- fim Conteudo Busca -->
						<!-- Inicio Coluna Esquerda -->
						<div id="hf2_Col2">
							<?php
							$limitAdvertising = 4;
							require_once PATH_INCLUDES_USERS . 'col-advertising.php';
							?>
						</div>
					<!-- Fim Coluna Esquerda -->
					</div>
					<!-- Fim Conteudo File 2 -->
					<!-- Inicio Conteudo File 3 -->
					<div id="home_file3">
						<div id="hf_pag"><?php paginate(); ?></div>
					</div>

					<!-- Fim Conteudo File 3 -->
				</div>
				<!-- Fim Conteudo -->
			</div>
		<!-- fim Main Conteudo -->
		<footer id="footer">
		<?php
		require_once PATH_INCLUDES_ADMIN . 'footer.html';
		?>
		</footer>
	</div>
	<!-- Fim Geral -->
</body>
</html>
<?php
require_once PATH_INCLUDES_USERS . 'functions-js-form-search.html';
?>