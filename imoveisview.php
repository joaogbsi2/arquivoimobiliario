
<?php

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_INCLUDES_ADMIN . 'session.php';
require_once PATH_ACTION . 'detailsProperty.action.php';
require_once PATH_ACTION . 'index.action.php';
require_once PATH_CONTROLLER . 'StatusController.class.php';

require_once PATH_CONTROLLER . 'PropertyDetailController.class.php';
require_once PATH_MODEL_ENTITIES . 'Detail.class.php';

require_once PATH_CONTROLLER . 'PropertyNearController.class.php';
require_once PATH_MODEL_ENTITIES . 'Near.class.php';
require_once PATH_UTIL . 'FaixaPrecos.php';

/*
 *
 * Add 21-01
 */
require_once PATH_CONTROLLER . 'PropertyStoreController.class.php';
require_once PATH_CONTROLLER . 'PropertyStatusController.class.php';
// require_once PATH_MODEL_ENTITIES. 'Store.class.php';

/*
 * Function to format the number to better presentation
 */
function formatNumber($number) {
	return number_format ( $number, 2, ',', '.' );
}

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon" href="images/arquiLogo.png" />
<title>Arquivo Imobiliário</title>
<link href="estilo.css" rel="stylesheet" type="text/css" />

<!-- jquery ui theme and select multiple theme -->
<link rel="stylesheet" type="text/css" href="css/jquery.multiselect.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />

<style type="text/css">
<!--
#photos {
	border: 1px solid #ccc;
	display: block;
	float: left;
	margin: 25px 0 0 5px;
	width: 450px;
	padding: 5px;
	min-height: 410px;
	max-height: 500px;
}

.thumbnail {
	border: none;
	height: 50px;
	width: auto;
	margin: 5px;
	cursor: pointer;
}

#imgMignified {
	/*width: 320px;*/
	height: auto;
	max-height: 370px;
	width: 100%;
	border-style: solid;
}
-->
</style>

</head>

<body>

	<!-- JS Content -->
	<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>

	<!-- includes for multiple select -->
	<script type="text/javascript" src="js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/jquery.multiselect.min.js"></script>
	<script type="text/javascript" src="js/jquery.validate.min.js"></script>
	<!-- end multiple select include -->

	<!-- includes for jquery price -->
	<script type="text/javascript" src="js/jquery.price_format.1.7.min.js"></script>
	<!-- end jquery price include -->
	<script type="text/javascript">
	    
	    function showVisitMessage(){
	        
	        return alert('Por favor, escolha uma das imobiliarias ao lado!');
	    }   
	</script>

	<!-- Inicio Geral -->
	<div id="geral">
		<header id="topo">
			<?php 
			require_once PATH_INCLUDES_USERS . 'header.php';
			?>
		</header>
		<!-- Inicio Main Conteudo -->
		<div id="mainConteudo">
			<!-- Início Conteudo -->
			<div id="conteudo">
				<!-- Início Conteudo file 1 -->
				<div id="home_file1">
					<!-- Início Coluna Esquerda -->
					<div id="hf1_Col1">
						<?php
						/* Menu de Busca */
						require_once PATH_INCLUDES_USERS . 'form-search.php';
						/*divulgacao*/
						require_once PATH_INCLUDES_USERS . 'window-parceiros.php';
						?>
					</div>
					<!-- Fim Coluna Esquerda -->
					<!-- Início Coluna Direita -->
					<div id="hf1_Col2">
						<?php
						/* Menu navegação + banner */
						require_once PATH_INCLUDES_USERS . 'main-menu-without-logo.php';
						?>
					</div>
					<!-- Fim Coluna Direita -->
				</div>
				<!-- Fim Conteudo file 1 -->
				<!-- Inicio Conteudo file 4 -->
				<div id="home_file4">
					<?php
					
					$faixaValue = NULL;
					/*
					 * Correção do bug que agora irá tratar o caso de imóvel ser
					 * de aluguel ou temporada e setar o valor real neles
					 * date: 12-02-2014
					 */
					if ($property->getStatus () != "Compra")
						$faixaValue = $property->getPrice ();
					else{
						$faixaValue = FaixaPrecos::buscaFaixa($property->getPrice ());
					}
				
					?>
					<!-- Inicio Conteudo Imovel -->
					<div id="contView">
						<!-- Inicio Descrição Imovel -->
						<div id="vwCol1">
							<div id="cxTit"><?php if ($property->getStatus() != "Compra") echo "R$ ". $faixaValue; else echo $faixaValue; ?></div>
							<span id="status" hidden><?php echo $property->getStatus();?></span>
							<?php if($property->getStatus() != "Compra"){?>
								<div id="cxTxt1">Endereço:</div> 
	 							<div id="cxTxt2"> 
	 								<span id="street"> 
	                                <?php 
	                                	echo $property->getStreet() 
	                                ?> 
	                              	</span> <span id="identifier"> 
	                                <?php 
	                                	echo $property->getIdentifier()
									?>
	                              	</span> 
	
	 							</div> 
							<?php };?>
							<div id="cxTxt1">Tipo :</div>
							<div id="cxTxt2"><span id="tipo"><?php echo $property->getType() ?></span></div>
							<div id="cxTxt1">Bairro:</div>
							<div id="cxTxt2"><span id="bairro"><?php echo $property->getNeighborhood() ?></span></div>
							<div id="cxTxt1">Cidade:</div>
							<div id="cxTxt2">
								<span id="city"><?php echo $property->getCity() ?></span>
							</div>
							<div id="cxTit2">CARACTERÍSTICAS:</div>
							<div id="cxTxt1">Posição:</div>
							<div id="cxTxt2"><?php echo $property->getPosicao(); ?></div>
							<div id="cxTxt1">Garagem:</div>
							<div id="cxTxt2"><?php echo $property->getGaragem(); ?></div>
							<div id="cxTxt1">Pavimento:</div>
							<div id="cxTxt2"><?php echo $property->getPavimento(); ?></div>

							<div id="cxTit2">DEPENDÊNCIAS:</div>
							<div id="cxDesc">
								<?php echo $property->getQtdRooms(); ?> Quartos, 
								<?php echo $property->getDescription()?>
							</div>

							<div id="cxTit2">ÁREA:</div>
							<div id="cxTxt1">Construída:</div>
							<div id="cxTxt2"><?php echo $property->getAreaConstruida(); ?></div>
							<?php if(!intval($property->getEdicula() == "0")) { ?>
								<div id="cxTxt1">Edícula:</div>
								<div id="cxTxt2"><?php echo $property->getEdicula(); ?></div>
							<?php } ?>
							<div id="cxTxt1">Terreno:</div>
							<div id="cxTxt2"><?php echo $property->getAreaTotal(); ?></div>
							<div id="cxTxt1">Comprimento:</div>
							<div id="cxTxt2"><?php echo $property->getComprimento(); ?></div>
							<div id="cxTxt1">Largura:</div>
							<div id="cxTxt2"><?php echo $property->getLargura(); ?></div>

							<!-- <div id="cxTit2">DETALHES:</div> -->
							<?php
								$propertyDetailsControllerList = new PropertyDetailController ();
								$detailsInPropertyList = $propertyDetailsControllerList->getPropertyDetail ( $property->getId () );
								if (! is_null ( $detailsInPropertyList )) {?>
									<div id="cxTit2">DETALHES:</div>
									<?php
									foreach ( $detailsInPropertyList as $iterDetails ) {?>
										<div id="cxTxt3"><?php echo $iterDetails->getDetail()->getName(); ?></div>
										<?php 
									}
								}								
							?>
							<!-- <div id="cxTit2">O QUE TEM PERTO:</div> -->
							<?php
								$propertyNearsControllerList = new PropertyNearController ();
								$nearsInPropertyList = $propertyNearsControllerList->getPropertyNear ( $property->getId () );
								if (! is_null ( $nearsInPropertyList )) {?>
									<div id="cxTit2">O QUE TEM PERTO:</div>
									<?php
									foreach ( $nearsInPropertyList as $iterNears ) {?>
										<div id="cxTxt3"><?php echo $iterNears->getNear()->getName(); ?></div>
										<?php
									}
								}
							?>
							<div id="cxTit2">IMOBILIÁRIAS:</div>
							<?php
								$propertyStoresControllerList = new PropertyStoreController ();
								$propertyStatusController = new PropertyStatusController();
								$storesInPropertyList = $propertyStoresControllerList->getPropertyStore ( $property->getidUnico () );

								$lengthStoresInProperty = count ( $storesInPropertyList );

								if (! is_null ( $storesInPropertyList )) {
									if (is_null ( $property->getExclusiveStore () ) || $property->getExclusiveStore () == 0) {
										foreach ( $storesInPropertyList as $iterStores ) {
											if(!is_null($propertyStatusController->getPropertyStatusByStore($_GET['propertyId'], $_GET['statusProperty'], $iterStores->getStore()->getId()))){?>
												<div id="cxImob">
													<a href="storeContact.php?code=<?php echo $iterStores->getStore()->getId(); ?>" target="_blank">
													<img width="120px" height="65px"
													src="images/store_photo/<?php echo basename($iterStores->getStore()->getPhoto()); ?>" /></a>
												</div>
											<?php
											}else 
												continue;
										}
									}else{
										foreach ( $storesInPropertyList as $iterStores ) {
											if ($iterStores->getStore ()->getId () == $property->getExclusiveStore ()) {?>
												<div id="cxImob">
													<a href="storeContact.php?code=<?php echo $iterStores->getStore()->getId(); ?>" target="_blank">
													<img width="150px" height="40px" src="images/store_photo/<?php echo basename($iterStores->getStore()->getPhoto()); ?>" />
													</a>
												</div>
											<?php
											}
										}
									}
								}else{
									echo "Não há imobiliária vinculada à este imóvel!<br>";
								}
							?>
						</div>
						<!-- Fim Descrição Imovel -->
						<!-- Inicio Fotos Imovel -->
						<div id="vwCol2">
							<!-- Inicio Seção de Fotos -->
							<div id="photo">
								<!-- Inicio Fotos Pequenas -->
								<div id="thumbnails">
									<?php
									if(!$propertyPhoto){
										echo "Nenhuma foto cadastrada.";
									}else{
										foreach ($propertyPhoto as $photo ) {
											echo '<img class="thumbnail" src="images/property_photo/' . basename ( $photo->getPath () ) . '" />';
										}
									}
									?>
								</div>
								<!-- Fim Fotos Pequenas -->
								<!-- Inicio Foto Grande -->
								<div id="magnified">
									<img id="imgMignified" />
								</div>
								<!-- Fim Foto Grande -->
								<!-- Inicio google Maps -->
								<div id="cxMapa"></div>
								<!-- Fim google Maps -->
								<!-- Inicio Menu Resultado -->
								<div id="cxValor">
									<table width="325">
										<tr>
											<td colspan="3" align="center">Valor</td>
										</tr>
										<tr>
											<td width="148"></td>
											<td width="12"></td>
											<td width="149"></td>
										</tr>
										<tr>
											<td colspan="3" align="center"><span class="faixaValue"><?php echo $faixaValue; ?></span></td>
										</tr>
									</table>
								</div>
								<?php
								if ($lengthStoresInProperty == 1) {?>
									<div class="cxBts">
										<a
										href="storeContact.php?code=<?php echo $storesInPropertyList[0]->getStore()->getId(); ?>" target="_blank">
										<img src="images/bt_solicitarN.png" /></a>
									</div>

								<?php 
								}else {?>
									 <div class="cxBts">
										<a href="#" onclick="return showVisitMessage();">
										<img src="images/bt_solicitarN.png" /></a>
									</div>
								<?php
								}
								?>
								<div class="cxBts">
									<a href="#"><img src="images/bt_indicarN.png" /></a>
								</div>
								<div class="cxBts">
									<a href="#"><img src="images/bt_printN.png" /></a>
								</div>
								
								
								<!-- Fim Menu Resultado -->
							</div>
							<!-- Fim Seção de Fotos -->

						</div>
						<!-- Fim Fotos Imovel -->
					</div>
					<!-- Fim Conteudo Imovel -->

					<!-- teste -->
				</div>
				<!-- Fim Conteudo file 4 -->
			</div>
			<!-- Fim Conteudo -->
		</div>
		<!-- Fim Main Conteudo -->
	</div>
	<!-- Fim Geral -->

	<!-- Script GoogleMaps -->
	<script type="text/javascript"
		src="https://maps.google.com/maps/api/js?sensor=true"></script>
	<script type="text/javascript">
  		$(document).ready(function(){
	    	if(document.querySelector(".thumbnail") != null){
	      		var imgMignified = document.getElementById('imgMignified');
	      		imgMignified.src = document.querySelector(".thumbnail").src;
	      		$(".thumbnail").click(function(){
	        	imgMignified.src = $(this).attr('src');
	      	});
    	}
    
    
	    var status = document.getElementById('status').innerHTML,
	    	city = document.getElementById('city').innerHTML,
	    	neighborhood = document.getElementById('bairro').innerHTML,
	        state = "MG",
	        country = "BR",
	    	street,
	    	identifier,
	        addressParam;
	    if(status.trim() != 'Compra'){
	    	street = document.getElementById('street').innerHTML,
	    	identifier = document.getElementById('identifier').innerHTML,
	    	addressParam = identifier + "+" + street + ",+" + city + ",+" + state +
	                       ",+" + country;
	    }else{
	    	addressParam =  neighborhood + ",+" + city + ",+" + state +
	        ",+" + country;
	    }
//        addressParam = identifier + "+" + street + ",+" + city + ",+" + state +
//                       ",+" + country;
//    if (street.trim() != '') { //Don't show the maps when the address is empty
//    if (street.trim() != '') { //Don't show the maps when the address is empty
        $.get("http://maps.google.com/maps/api/geocode/json",
              {address : addressParam,
               sensor : "true"
                   },
              function(data){
                var lat = data.results[0].geometry.location.lat,
                    lng = data.results[0].geometry.location.lng,
                    mapArea = document.getElementById('cxMapa');

                initialize(lat, lng, mapArea);
              }
        );
//    }
 });
  
// Get google maps location
		function initialize(lat, lng, mapArea) {
		  var latlng = new google.maps.LatLng(lat, lng),
		      myLatlng = new google.maps.LatLng(lat,lng),
		        myOptions = {
		        zoom: 18,
		        center: latlng,
		        mapTypeId: google.maps.MapTypeId.ROADMAP
		      },
		      map = new google.maps.Map(mapArea,myOptions),
		      marker = new google.maps.Marker({
		      position: myLatlng,
		      title:"Este pode ser seu novo imóvel!"
		      });
		      
		  marker.setMap(map);
		}
	</script>
	<!-- Fim Script GoogleMaps -->
</body>

</html>

<?php
require_once PATH_INCLUDES_USERS . 'functions-js-form-search.html';
?>