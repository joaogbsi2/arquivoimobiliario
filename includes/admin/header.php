<?php

/*
 * INCLUDE SECTOR
 */

//include the file of configuration
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_INCLUDES_ADMIN.   'session.php';

if (isset($_GET['logout'])){
    
    /*
     * fazendo isso, pois a funcão está na classe UserController
     * assim, caso o usuário clique no link sair, irá 
     * ocorrer essas instruções abaixo
     */
    require_once PATH_CONTROLLER. 'UserController.class.php';

    $userController = new UserController();
    $userController->logout();
}

if (!isset($_SESSION['id_user']) && (!isset($_SESSION['admin']) || $_SESSION['admin'] != true) ){
    header("Location:" .URL. "index.php");
}
?>
<!-- TOPO -->
<!--     <div id="topo"> -->
       <div id="topoCont">
       		<div id="logo"><img src="images/logotipo.png"/></div>
         <div id="cxAdm">Administrador</div>
            <div id="cxAdm2"><a href="adm_alter_password.php">Alterar Senha</a> |<a href="?logout">Sair</a> |</div>
       </div>
<!--     </div> -->
	<!-- /TOPO -->