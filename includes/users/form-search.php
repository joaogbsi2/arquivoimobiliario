<?php
    $dataInitialDefault = date("d/m/Y");
    $dataFinalDefault = date("d/m/Y");
?>
<div id="menuBus">
        <div id="label">
        <!-- Necessita disso aqui para que funcione a listagem de bairros pois ele usa como base
             o id "itens" e na linha abaixo foi uma gambiarra para não aparecer nada lá no layout
             mais que usasse o esquema do link interno-->
             <ul id="itens" class="itens">
                <b><a class="current" href="#search"></a></b>
                <!--<li><a class="current" href="#search">Busque seu imóvel</a></li> -->
             </ul>
             <form style="margin-top: 0px;" id="frmSearchHome" method="get" action="imoveisbusca.php">
                <input type="hidden" value="0" name="offset" />
                <table width="275">
                    <tr>
                        <td colspan="3" align="center">
                        <?php
                            $statusController = new StatusController();
                            foreach ($statusController->getAll() as $status) {
                                echo '<input type="radio" class="checkbox required" name="statusProperty" id="statusImovel" value="' . $status->getId() . '" />';
                                echo $status->getDescription();
                            }
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" height="0"></td>
                    </tr>
                    <tr>
                        <td width="93" align="right" class="texto">Tipo:</td>
                        <td></td>
                        <td>
                            <!-- Edilson atualizou esse campo no dia 27-11-2013 inseri o atributo multiple=true
                                 para que o usuário conseguisse filtrar por vários tipos também na busca
                            -->
                            <select style="width: 20px !important" name="property_type[]" id="type" multiple="true">
                            <?php
                                foreach ($propertyTypeList as $propertyType){
                                    echo '<option value="'.$propertyType->getId().'">'.$propertyType.'</option>';
                                }
                            ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" height="1"></td>
                    </tr>
                    <!--
                    <tr>
                        <td align="right" class="texto">Estado:</td>
                        <td></td>
                        <td><select name="estado" class="campo" id title="bairro">
                            <option value="label">Label</option></select></td>
                    </tr>
                    -->
                    <tr>
                        <td colspan="3" height="1"></td>
                    </tr>
                    <tr>
                        <td align="right" class="texto">Cidade:</td>
                        <td width="10"></td>
                        <td width="150">
                            <select name="city" class="campo">
                                <?php
                                    foreach ($cityList as $city){
                                        echo '<option value="'.$city->getId().'">'.$city.'</option>';
                                    }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" height="1"></td>
                    </tr>
                    <tr>
                        <td align="right" class="texto">Localidade:</td>
                        <td width="10"></td>
                        <td width="150">
                            <select class="campo" name="neighborhood[]" id="neighborhood" multiple="multiple">
                            <!-- <option value="label">Label</option> -->
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" height="1"></td>
                    </tr>
                <!--     <tr>
                         <td align="right" class="texto">Valor Mín:</td>
                        <td><input type="range" name="rangeInput" min="100000" max="3000000" step="25000" onchange="updateTextInputMin(this.value);"> </td>

                    </tr> -->
                    <tr>
                        <td align="right" class="texto">Valor Mín:</td>
                        <!-- <td><input type="range" name="rangeInput" min="100000" max="3000000" step="25000" onchange="updateTextInput(this.value);"> <br /> </td> -->
                        <td></td>
                        <td><input name="min_price" class="campo textfield numType price" id="textInputMin">
                    </tr>
                    <tr>
                        <td colspan="3" height="1"></td>
                    </tr>
                   <!--  <tr>
                        <td align="right" class="texto">Valor Máx:</td>
                        <td><input type="range" name="rangeInput" min="100000" max="3000000" step="25000" onchange="updateTextInputMax(this.value);"> <br />   </td>    
                    </tr> -->
                    <tr>
                        <td align="right" class="texto">Valor Máx:</td>
                        <!-- <td><input type="range" name="rangeInput" min="100000" max="3000000" step="25000" onchange="updateTextInput(this.value);"> <br />   </td> -->
                        <td></td>
                        <td><input name="max_price" class="campo textfield numType price" id="textInputMax">
                    </tr>
                    <tr>
                        <td colspan="3" height="1"></td>
                    </tr>
                    <tr>
                        <td align="right" class="texto">Quartos:</td>
                   	<td></td>
                        <td>
                            <input type="number" class="textfield numType" min="0" name="qtdMaxRooms" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" height="1"></td>
                    </tr>
                    <!-- Remove This field because on the old version this field not exist, and will
                         insert the fields de / ate  -->
                    <tr>
                        <td colspan="2" align="right" class="texto2">Código:</td>
                   	<td><input name="codigo" type="text" class="campo2" id="codigo"/></td>
                    </tr>
                    <tr>
                        <td colspan="3" height="1"></td>
                    </tr>
                    <tr>
                        <td align="right" class="texto">&nbsp;</td>
                        <td colspan="2" align="right" style="margin-top: -20px;"><input type="submit" class="submit" value="Buscar" /></td>
                        
                    </tr>                   
                    <tr id="period" style="visibility: hidden;">
                        <td align="right" class="texto">De:</td>
                        <td></td>
                        <td>
                            <input type="text" id="de" name="de" maxlength="10" size="12" value="<?php echo $dataInitialDefault ?>" />
                        </td>
                    </tr>
                    <tr id="period-ate" style="visibility: hidden;">
                        <td align="right" class="texto">Até:</td>
                        <td></td>
                        <td>
                            <input type="text" id="ate" name="ate" maxlength="10" size="12" value="<?php echo $dataFinalDefault ?>" />
                        </td>
                        <!--
                        <td align="right" class="texto">Até:
                            <input type="text" id="ate" name="ate" />
                        </td>
                        -->
                    </tr>
                </table>
                </form> 
          </div>
     </div>
                    
<!--  Edilson Justiniano comentou essa linha no dia 15/11/2013
      porque os usuários comuns não terão acesso à área administrativa
      que é onde eu cadastro os clientes, imóveis ... até essa primeira
      versão será isso.
    <div id="menuDown">
        <div id="cx1"><a href="#"><img src="images/bt_cadImov.png" /></a></div>
        <div id="cx2"><a href="#"><img src="images/bt_crieUser.png" /></a></div>
     </div>
-->

 <script type="text/javascript">
    function updateTextInputMin(val) {
      document.getElementById('textInputMin').value=val; 
      // document.getElementById('textInput').value=val; 
    }function updateTextInputMax(val) {
      document.getElementById('textInputMax').value=val; 
      // document.getElementById('textInput').value=val; 
    }
  </script>