<?php
    
    $adminServicesController = new AdminServicesController();
    
    $mainAdvertisings = $adminServicesController->getAllMainAdvertisings();//pegar todas as principais propagandas
    
?>

    <nav id="menu">
        <ul>
          <li><a href="quemsomos.php">Quem somos</a></li>
          <li><a href="anuncie.php">Anuncie</a></li>
          <li><a href="contact.php">Fale conosco</a></li>
         <!--  <li><a href="imoveisbusca.php?offset=0&statusProperty=1&city=1&min_price=&max_price=&qtdMaxRooms=&codigo=&de=25%2F01%2F2016&ate=25%2F01%2F2016">Alugar</a></li> -->
          <li><a href="servicos.php">Serviços</a></li>
        </ul>
        <!-- <div id="btmenu1"><a href="quemsomos.php"><img src="images/btmenu1.png"></a></div>
        <div id="btmenu2"><a href="parceiros.php"><img src="images/btmenu2.png"></a></div>
        <div id="btmenu3"><a href="comprar.php"><img src="images/btmenu3.png"></a></div>
        <div id="btmenu4"><a href="alugar.php"><img src="images/btmenu4.png"></a></div>
        <div id="btmenu5"><a href="servicos.php"><img src="images/btmenu5.png"></a></div> -->
<!--         <div class="btmenu1"><a href="quemsomos.php">Quem somos</a></div>
        <div class="btmenu1"><a href="anuncie.php">Anuncie</a></div>
        <div class="btmenu1"><a href="imoveisbusca.php?offset=0&statusProperty=2&city=1&min_price=&max_price=&qtdMaxRooms=&codigo=&de=25%2F01%2F2016&ate=25%2F01%2F2016">Compra</a></div>
        <div class="btmenu1"><a href="imoveisbusca.php?offset=0&statusProperty=1&city=1&min_price=&max_price=&qtdMaxRooms=&codigo=&de=25%2F01%2F2016&ate=25%2F01%2F2016">Alugar</a></div>
        <div class="btmenu1"><a href="servicos.php">Serviços</a></div> -->
    </nav>
                    
    <div id="bannerImg">
        <div id="container" class="cf" style="position: relative;">
    

          <div id="main" role="main" style="width:600px;">
          <section class="slider">
            <div class="flexslider">
              <ul class="slides">
               <?php
               if (!is_null($mainAdvertisings) && is_array($mainAdvertisings)){ 
                   foreach ($mainAdvertisings as $iterMainAdvertisings) { ?>
               
                  <li>
                      <a href="<?php echo $iterMainAdvertisings->getLink();?>" title="<?php echo $iterMainAdvertisings->getTitle();?>" target="_blank">
                      <img  width="600" height="300" src="<?php echo URL_MAIN_ADVERTISINGS_PHOTOS. basename($iterMainAdvertisings->getPhoto()); ?>" />
                      </a>
                  </li>
               <?php 
                   }     
               } 
               ?>
                
              </ul>
            </div>
          </section>

        </div>

        </div>
        
   </div>