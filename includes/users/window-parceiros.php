<div id="cxParceiros" style="min-height: 400px; margin-top: 30px;" >
    <div id="cxTit">
        <p>Divulgação</p>
    </div>
    <div id="cxLog">
        
        <?php
            
            require_once PATH_MODEL_ENTITIES.       'Parceiro.class.php';
            require_once PATH_CONTROLLER.           'AdminServicesController.class.php';
            
            $adminServController = new AdminServicesController();
            $parcs = $adminServController->getAllParceiros();
            
            if (!is_null($parcs))
                foreach ($parcs as $iterParcs) {
                    $pathPhoto = URL_PARCEIRO_PHOTOS . basename($iterParcs->getPhoto());
                ?>
                    <img src="<?php echo $pathPhoto; ?>" width="110px" height="84px"/>
                
                <?php
                }
            else
                echo "Não há parceiros cadastrados!";
            
        
        ?>
    </div>
</div>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery-scrolltofixed.js"></script>
<script type="text/javascript">
    $('#cxParceiros').scrollToFixed({dontSetWidth:true});
</script>