<?php

/*
 * Date of creating 2013-07-29
 *
 * @author Edilson Justiniano
 */
?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon" href="images/arquiLogo.png">
<title>Arquivo Imobiliário - Admin</title>
<link href="style.css" rel="stylesheet" type="text/css" />

<!-- jquery ui theme and select multiple theme -->
<link rel="stylesheet" type="text/css" href="css/jquery.multiselect.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />

</head>

<body onload="showMessage('Bem vindo Administrador');">

	<!-- JS Content -->
	<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>

	<!-- includes for multiple select -->
	<script type="text/javascript" src="js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/jquery.multiselect.min.js"></script>
	<script type="text/javascript" src="js/jquery.validate.min.js"></script>
	<!-- end multiple select include -->

	<!-- includes for jquery price -->
	<script type="text/javascript" src="js/jquery.price_format.1.7.min.js"></script>
	<!-- end jquery price include -->


	<!-- Function add by Edilson to shown the message of success or failure of login -->
	<script language="javascript">
    
        function showMessage(msg){
            
            return (window.alert(msg));
            
        }//eof function
        
    </script>
    
    
    <?php
				
				/*
				 * INCLUDE SECTOR
				 */
				
				// $_SERVER['DOCUMENT_ROOT'] = "/home/arqui937/public_html/";
				// define ("PATH", $_SERVER['DOCUMENT_ROOT']);
				
				// include the file of configuration
				// require_once '/home/arqui937/public_html/config.php';
				
				require_once PATH_ACTION . 'index.action.php';
				require_once PATH_CONTROLLER . 'StatusController.class.php';
				
				/*
				 * The bellow file was needed because, will be shown all
				 * clients cadastred for the root administrator and he
				 * will be to set the its status
				 */
				require_once PATH_CONTROLLER . 'UserController.class.php';
				require_once PATH_MODEL_ENTITIES . 'User.class.php';
				
				/*
				 * Function to shown the link to all letters
				 */
				function showLinkOfNames() {
					for($i = 65; $i < 91; $i ++) {
						?>
                <a
		href="<?php echo URL_ADMIN_PAGE . "?letter=". chr($i) ?>"><?php echo chr($i) ?>  |  </a>
            <?php
					}
				}
				
				/*
				 * Verify if receive the $_GET[`success`] to shown the message of failure or not
				 */
				$success = isset ( $_GET ['success'] ) ? $_GET ['success'] : NULL;
				if ($success != NULL) {
					if ($success == 1) {
						?>
                <script language="javascript">
                    showMessage("Sucesso ao atualizar o status do cliente!");
                </script>
        <?php
					} else {
						?>
                <script language="javascript">
                    showMessage("Falha ao atualizar o status do cliente!");
                </script>
        <?php
					}
				}
				?>    
        
            
   

<!-- GERAL -->
	<div id="geral">


		<!-- TOPO -->
		<div id="topo">
			<div id="topoCont">
				<div id="logo">
					<img src="images/logotipo.png" />
				</div>

				<form name="form_login" method="post"
					action="action/login.action.php">
					<div id="cxUser">
						<table width="130">
							<tr>
								<td width="30" align="justify" class="texto">Login</td>
								<td width="80"><input name="user" type="text" class="campo1"
									id="user" /></td>
								<td width="10"></td>
							</tr>
							<tr>
								<td align="right" class="texto">Senha</td>
								<td><input name="senha" type="password" class="campo1"
									id="senha" /></td>
								<td><input type="submit" /></a></td>
							</tr>
						</table>
					</div>
				</form>
				<!--  /FORM ADD BY EDILSON -->

				<div id="cxSocial">
            	<?php include 'menu-principal.php'; ?>
         	</div>
			</div>
		</div>
		<!-- /TOPO -->


		<!-- MAIN CONTEUDO -->
		<div id="mainConteudo">

			<!-- CONTEUDO -->
			<div id="conteudo">

				<!-- CONTEUDO FILE 1 -->
				<div id="home_file1">

					<div id="hf1_Col1">

						<div id="menuBus">
							<div id="label">
								<!-- Necessita disso aqui para que funcione a listagem de bairros pois ele usa como base
                            o id "itens" e na linha abaixo foi uma gambiarra para não aparecer nada lá no layout
                            mais que usasse o esquema do link interno-->
								<ul id="itens" class="itens">
									<b><a class="current" href="#search"></a></b>
									<!--<li><a class="current" href="#search">Busque seu imóvel</a></li> -->
								</ul>
								<form style="margin-top: -20px;" id="frmSearchHome" method="get"
									action="imoveisbusca.php">
									<input type="hidden" value="0" name="offset" />

									<table width="275">
										<tr>
											<td colspan="3" align="center">
                                    <?php
																																				$statusController = new StatusController ();
																																				foreach ( $statusController->getAll () as $status ) {
																																					echo '<input type="radio" class="checkbox required" name="statusProperty" id="statusImovel" value="' . $status->getId () . '" />';
																																					echo $status->getDescription ();
																																				}
																																				?>
                                    </td>
										</tr>
										<tr>
											<td colspan="3" height="0"></td>
										</tr>
										<tr>
											<td width="93" align="right" class="texto">Tipo:</td>
											<td></td>
											<td><select style="width: 20px !important"
												name="property_type[]" id="type">
						<?php
						foreach ( $propertyTypeList as $propertyType ) {
							echo '<option value="' . $propertyType->getId () . '">' . $propertyType . '</option>';
						}
						?>
					</select></td>
										</tr>
										<tr>
											<td colspan="3" height="1"></td>
										</tr>
										<!--
                                <tr>
                                    
                                    <td align="right" class="texto">Estado:</td>
                                  	<td></td>
                                    <td><select name="estado" class="campo" id title="bairro">
                                    <option value="label">Label</option></select></td>
                                </tr>
                                -->
										<tr>
											<td colspan="3" height="1"></td>
										</tr>
										<tr>
											<td align="right" class="texto">Cidade:</td>
											<td width="10"></td>
											<td width="150"><select name="city" class="campo">
                                        <?php
																																								foreach ( $cityList as $city ) {
																																									echo '<option value="' . $city->getId () . '">' . $city . '</option>';
																																								}
																																								?>
                                        </select></td>
										</tr>
										<tr>
											<td colspan="3" height="1"></td>
										</tr>
										<tr>
											<td align="right" class="texto">Localidade:</td>
											<td width="10"></td>
											<td width="150"><select class="campo" name="neighborhood[]"
												id="neighborhood" multiple="multiple">
													<!-- <option value="label">Label</option> -->
											</select></td>
										</tr>
										<tr>
											<td colspan="3" height="1"></td>
										</tr>
										<tr>
											<td align="right" class="texto">Valor Mín:</td>
											<td></td>
											<td><input name="min_price"
												class="campo textfield numType price">
										
										</tr>
										<tr>
											<td colspan="3" height="1"></td>
										</tr>
										<tr>
											<td align="right" class="texto">Valor Máx:</td>
											<td></td>
											<td><input name="max_price"
												class="campo textfield numType price">
										
										</tr>
										<tr>
											<td colspan="3" height="1"></td>
										</tr>
										<tr>
											<td align="right" class="texto">Quartos:</td>
											<td></td>
											<td><input type="number" class="textfield numType" min="0"
												name="qtdMaxRooms" /></td>
										</tr>
										<tr>
											<td colspan="3" height="1"></td>
										</tr>

										<!-- Remove This field because on the old version this field not exist, and will
                                     insert the fields de / ate  -->
										<tr id="period" style="visibility: hidden;">
											<td align="right" class="texto">De:</td>
											<td></td>
											<td><input type="text" id="de" name="de" maxlength="10"
												size="12" /></td>
										</tr>
										<tr id="period-ate" style="visibility: hidden;">
											<td align="right" class="texto">Até:</td>
											<td></td>
											<td><input type="text" id="ate" name="ate" maxlength="10"
												size="12" /></td>


											<!--
                                    <td align="right" class="texto">Até:
                                        <input type="text" id="ate" name="ate" />
                                    </td>
                                    -->
										</tr>

										<tr>
											<td colspan="2" align="right" class="texto2">Entre com
												código:</td>
											<td><input name="codigo" type="text" class="campo2"
												id="codigo" /></td>
										</tr>
										<tr>
											<td colspan="3" height="1"></td>
										</tr>
										<tr>
											<td align="right" class="texto">&nbsp;</td>
											<td></td>
											<td align="right">
												<!--<a href="imoveisbusca.php"><img src="images/bt_send.png" name="bt_send" id="bt_send"/></a> -->
												<input type="submit" class="submit" value="buscar" />
											</td>
										</tr>
									</table>
								</form>
							</div>
						</div>

						<div id="menuDown">
							<div id="cx1">
								<a href="#"><img src="images/bt_cadImov.png" /></a>
							</div>
							<div id="cx2">
								<a href="#"><img src="images/bt_crieUser.png" /></a>
							</div>
						</div>
					</div>

					<div id="hf1_Col2">

						<div id="menu">
							<div id="btmenu1">
								<a href="quemsomos.php"><img src="images/btmenu1.png"></a>
							</div>
							<div id="btmenu2">
								<a href="parceiros.php"><img src="images/btmenu2.png"></a>
							</div>
							<div id="btmenu3">
								<a href="#"><img src="images/btmenu3.png"></a>
							</div>
							<div id="btmenu4">
								<a href="#"><img src="images/btmenu4.png"></a>
							</div>
							<div id="btmenu5">
								<a href="servicos.php"><img src="images/btmenu5.png"></a>
							</div>
						</div>

						<!-- Insert the form to cadastre the photos of advertising (propaganda) -->
						<form id="frmCadastrePhoto" method="post"
							action="action/adminServices.action.php"
							enctype="multipart/form-data" accept-charset="UTF-8">
							<table width="130">
								<tr>
									<td width="30" align="justify" class="texto">Posição:</td>
									<td width="80"><select name="position">
                                    <?php
																																				for($i = 1; $i <= 7; $i ++) {
																																					?>
                                            <option
												value="<?php echo $i ?>"><?php echo $i ?></option>
                                    <?php
																																				}
																																				?>
                                </select></td>
									<td width="10"></td>
								</tr>


								<tr>
									<td align="right" class="texto">Foto:</td>
									<td><input name="photo" type="file" class="campo1" id="photo" /></td>
									<!-- This field define what the admin user is making -->
									<input type="hidden" name="action" value="advertising" />
									<td><input type="submit" /></td>
								</tr>

							</table>
						</form>

						<table width="100%" border="1" cellspacing="2">
							<tr>
								<th width="35%" align="left">Nome</th>
								<th width="21%" align="left">Telefone</th>
								<th width="24%" align="left">Email</th>
								<th width="20%" colspan="2" align="left">Ativo?</th>
							</tr>
                    <?php
																				/*
																				 * Here on bellow lines, where is that I shown the list of data of
																				 * status of user. I yet don't known how to shown this informations
																				 * the Borborema say: try to use the onchange, onclick event and on
																				 * hour that user change the value I create a request for server and
																				 * insert the alteration on database. But How to comunicate javascript
																				 * with php thus? This is a good question! Tomorrow I see.
																				 */
																				showLinkOfNames ();
																				
																				$userController = new UserController ();
																				$statusAllUsers = $userController->getStatusAllClients ();
																				foreach ( $statusAllUsers as $usersStatus ) {
																					?>
                            <tr>
								<form name="status-users-form" method="post"
									action="action/adminServices.action.php">
									<td width="35%" align="left"><?php echo $usersStatus->getName() ?></td>
									<td width="21%" align="left"><?php echo $usersStatus->getPhone() ?></td>
									<td width="24%" align="left"><?php echo $usersStatus->getEmail() ?></td>
									<td width="13%" align="left"><select name="active">
                                        <?php if (!$usersStatus->getIsActive()) { ?>
                                            <option value="0">Não</option>
											<option value="1">Sim</option>
                                        <?php } else { ?>
                                            <option value="1">Sim</option>
											<option value="0">Não</option>
                                        <?php } ?>
                                        </select></td>
									<td width="7%" align="left"><input type="hidden" name="id_user"
										value="<?php echo $usersStatus->getId(); ?>"> <input
										type="hidden" name="action" value="alterstatus"> <input
										type="submit" value="Atualizar"></td>
								</form>

							</tr>
                        <?php
																				}
																				?>
                    </table>
						<!--
                    <div id="bannerImg">
                    	<div id="imgRot"><img src="images/bannerTop.jpg" /></div>
                  </div>
                    -->
					</div>

				</div>
				<!-- /CONTEUDO FILE 1 -->

				<!-- CONTEUDO FILE 2 -->
				<div id="home_file2">
					<div id="hf2_Col1">

						<div id="hmInfo1">
							<div id="imgFT">
								<img src="images/fotoinfo1.jpg" />
							</div>
							<div id="inf_Tit">NOTÍCIAS</div>
							<div id="inf_Subti">LOREM IPSUM</div>
							<div id="inf_Text">Lorem ipsum dolor sit amet, consectetuer
								adipiscing elit, sed diam nonummy nibh euismod tincidunt.</div>
							<div id="inf_Btmais">
								<a href="noticias.php"><img src="images/bt_mais.png" /></a>
							</div>
						</div>

						<div id="hmInfo2">
							<div id="imgFT">
								<img src="images/fotoinfo2.jpg" />
							</div>
							<div id="inf_Tit">DECORAÇÃO</div>
							<div id="inf_Subti">LOREM IPSUM</div>
							<div id="inf_Text">Lorem ipsum dolor sit amet, consectetuer
								adipiscing elit, sed diam nonummy nibh euismod tincidunt.</div>
							<div id="inf_Btmais">
								<a href="decoracao.php"><img src="images/bt_mais.png" /></a>
							</div>
						</div>

						<div id="hmInfo3">
							<div id="imgFT">
								<img src="images/fotoinfo3.jpg" />
							</div>
							<div id="inf_Tit">TECNOLOGIA</div>
							<div id="inf_Subti">LOREM IPSUM</div>
							<div id="inf_Text">Lorem ipsum dolor sit amet, consectetuer
								adipiscing elit, sed diam nonummy nibh euismod tincidunt.</div>
							<div id="inf_Btmais">
								<a href="tecnologia.php"><img src="images/bt_mais.png" /></a>
							</div>
						</div>

						<div id="hmInfo4">
							<div id="imgFT">
								<img src="images/fotoinfo4.jpg" />
							</div>
							<div id="inf_Tit">DICAS</div>
							<div id="inf_Subti">LOREM IPSUM</div>
							<div id="inf_Text">Lorem ipsum dolor sit amet, consectetuer
								adipiscing elit, sed diam nonummy nibh euismod tincidunt.</div>
							<div id="inf_Btmais">
								<a href="dicas.php"><img src="images/bt_mais.png" /></a>
							</div>
						</div>

					</div>

					<div id="hf2_Col2">
						<div id="bannerPQ">
							<div id="imgB">
								<img src="images/banner1.jpg" />
							</div>
						</div>
						<div id="bannerPQ">
							<div id="imgB">
								<img src="images/banner2.jpg" />
							</div>
						</div>
						<div id="bannerPQ">
							<div id="imgB">
								<img src="images/banner3.jpg" />
							</div>
						</div>
						<div id="bannerPQ">
							<div id="imgB">
								<img src="images/banner4.jpg" />
							</div>
						</div>
					</div>
				</div>
				<!-- /CONTEUDO FILE 2 -->

				<!-- CONTEUDO FILE 3 -->
				<div id="home_file3">
					<div id="hf_pag">
						<a href="#"><img src="images/pgBolinha.png" /></a> <a href="#"><img
							src="images/pgBolinha.png" /></a> <a href="#"><img
							src="images/pgBolinha.png" /></a>
					</div>
				</div>
				<!-- CONTEUDO FILE 3 -->

			</div>
			<!-- CONTEUDO -->

		</div>
		<!-- /MAIN CONTEUDO -->


		<!-- FOOTER -->
		<div id="footer">
			<div id="conteudoFooter">
				<div id="ass">2013 © Arquivo Imobiliário. Todos os direitos
					reservados.</div>
			</div>
		</div>
		<!-- /FOOTER -->



	</div>
	<!-- /GERAL -->



	<!-- JS Content -->
	<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>

	<!-- includes for multiple select -->
	<script type="text/javascript" src="js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/jquery.multiselect.min.js"></script>
	<script type="text/javascript" src="js/jquery.validate.min.js"></script>
	<!-- end multiple select include -->

	<!-- includes for jquery price -->
	<script type="text/javascript" src="js/jquery.price_format.1.7.min.js"></script>
	<!-- end jquery price include -->

	<script type="text/javascript">

				$(document).ready(function(){
					$('.price').priceFormat({
					    prefix: '',
					    centsSeparator: '.',
					    thousandsSeparator: ''
					});
					//muti select in propertyType:
					$("#type").multiselect({
						noneSelectedText: 'Tipo',
						selectedText: 'Tipo',
						selectedList: 4,
						checkAllText: 'Marque todos',
						uncheckAllText: 'Desmarque todos',
                                                minWidth:   180
					});
					$("#neighborhood").multiselect({
						header: "Bairros",
						noneSelectedText: 'Bairros',
						selectedList: 4,
						checkAllText: 'Marque todos',
						uncheckAllText: 'Desmarque todos',
                                                minWidth:   180
					});

					var itens = document.getElementById('itens').getElementsByTagName('li');
					for(var i =0; i < itens.length; i++){
						itens[i].firstChild.addEventListener('click', changeStyle);
					}

					function changeStyle(event){
						for(var i =0; i < itens.length; i++){
							itens[i].firstChild.className = "";
						}
						event.target.className = "current";
					}

					$('#de').datepicker(
					{
						dateFormat: 'dd/mm/yy',
						dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'
						],
						dayNamesMin: [
						'D','S','T','Q','Q','S','S','D'
						],
						dayNamesShort: [
						'Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'
						],
						monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio',
						'Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro']
					}
					);
					$('#ate').datepicker(
					{
						dateFormat: 'dd/mm/yy',
						dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'
						],
						dayNamesMin: [
						'D','S','T','Q','Q','S','S','D'
						],
						dayNamesShort: [
						'Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'
						],
						monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio',
						'Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro']
					}
					);

				//combodinamico

				$('input[name=statusProperty][value=1], input[name=statusProperty][value=2]').change(function(){
					if(!$('input[name=statusProperty][value=3]', '#status').is(':checked')){
						$('#period').css({'visibility' : 'hidden'});
						$('#period-ate').css({'visibility' : 'hidden'});
						$('#content #content_property').css({'height':'410px'})
					}
				});

				$('input[name=statusProperty][value=3]').change(function(){
					if($('input[name=statusProperty][value=3]').is(':checked')){
						$('#period').css({'visibility' : 'visible'});
						$('#period-ate').css({'visibility' : 'visible'});                                                
                                                $('#content #content_property').css({'height':'580px'})
					}
				});
				// Call the neighborhood for the first time.
				getCitiesByNeighborhood($('select[name="city"]').val());
				// Bind change event on the cities select to change neightborhood when
				// city is change.
				$('select[name="city"]').change(function(){
					$('select[name="neighborhood[]"]').html("<option value=0>Carregando...</option>");
					getCitiesByNeighborhood($(this).val());
				});
});

function getCitiesByNeighborhood(city){
	$.getJSON("action/district.action.php",
		{city:city},
		function(valor){
			$("#neighborhood").html('');
			if(valor!=null){
				$.each(valor, function(k, v){
					var opt = new Option(v,v);
					$("#neighborhood").append(opt);
				});
			}
			$("#neighborhood").multiselect("refresh");
		}
		);
}
</script>


</body>

</html>
