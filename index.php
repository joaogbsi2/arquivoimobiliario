<?php
$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_ACTION . 'index.action.php';
require_once PATH_CONTROLLER . 'StatusController.class.php';
require_once PATH_ACTION . 'adminServices.action.php';
require_once PATH_MODEL_ENTITIES . 'Advertising.class.php';

require_once PATH_CONTROLLER . 'NoticeController.class.php';
require_once PATH_MODEL_ENTITIES . 'Notice.class.php';

require_once PATH_CONTROLLER . 'TechnologyController.class.php';
require_once PATH_MODEL_ENTITIES . 'Technology.class.php';

require_once PATH_CONTROLLER . 'DecoracaoController.class.php';
require_once PATH_MODEL_ENTITIES . 'Decoracao.class.php';

require_once PATH_CONTROLLER . 'DicaController.class.php';
require_once PATH_MODEL_ENTITIES . 'Dica.class.php';

/*
 * Buscar a Notícia cadastrada
 */
$noticeController = new NoticeController ();
$notice = $noticeController->findAll ();

if (is_null ( $notice ))
	$notice = new Notice ();

$pathNoticePhoto = URL_NOTICE_PHOTOS . basename ( $notice->getPhoto () );

$tecController = new TechnologyController ();
$tec = $tecController->findAll ();
if (is_null ( $tec ))
	$tec = new Technology ();

$pathTechnologyPhoto = URL_TECHNOLOGY_PHOTOS . basename ( $tec->getPhoto () );

$decoController = new DecoracaoController ();
$deco = $decoController->findAll ();

if (is_null ( $deco ))
	$deco = new Decoracao ();

$pathDecoracaoPhoto = URL_DECORACAO_PHOTOS . basename ( $deco->getPhoto () );

$dicaController = new DicaController ();
$dica = $dicaController->findAll ();
if (is_null ( $dica ))
	$dica = new Dica ();

$pathDicaPhoto = URL_DICA_PHOTOS . basename ( $dica->getPhoto () );

?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" >
	
	<link rel="shortcut icon" href="images/arquiLogo.png">
	<title>Arquivo Imobiliário</title>
	<link href="estilo.css" rel="stylesheet" type="text/css" />
	
	<!-- jquery ui theme and select multiple theme -->
	<link rel="stylesheet" type="text/css" href="css/jquery.multiselect.css" />
	<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
	<!-- Modernizr do Slider em jquery das propagandas principais -->
	<script src="js/modernizr.js"></script>
	<!-- CSS do slider em jquery -->
	<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />

</head>
<body>
	<!-- JS Content -->
	<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>

	<!-- includes for multiple select -->
	<script type="text/javascript" src="js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/jquery.multiselect.min.js"></script>
	<script type="text/javascript" src="js/jquery.validate.min.js"></script>
	<!-- end multiple select include -->

	<!-- includes for jquery price -->
	<script type="text/javascript" src="js/jquery.price_format.1.7.min.js"></script>
	<!-- end jquery price include -->

	<!-- Function add by Edilson to shown the message of success or failure of login -->
	<script language="javascript">
    
        function showMessage(msg){
            
            return (window.alert(msg));
            
        }//eof function
        
    </script>


	<!-- Incio Geral -->
	<div id="geral">
	<?php
	if (isset ( $_GET ['error'] ) && $_GET ['error'] != "") {
		if ($_GET ['error'] == 0) { // error inform Email
			?>
			<script language="javascript">showMessage("Por favor informe um email!");</script>
		<?php
		} else if ($_GET ['error'] == 1) { // error inform password
			?>
			<script language="javascript">showMessage("Por favor informe uma senha!");</script>
		<?php
		} else if ($_GET ['error'] == 2) { // error invalid Login
			?>
            <script language="javascript">showMessage("Login inválido!")</script>
    	<?php
		} else if ($_GET ['error'] == 3) { // error user is not active
			?>
        <script language="javascript">showMessage("Cliente bloqueado!\nPor Favor, entre em contato com o administrador")</script>
        <?php
		}
	} else {
		if (isset ( $_GET ['success'] )) {
			?>
			<script language="javascript">showMessage("Login realizado com sucesso!");</script>
		<?php
		}
	}
	?>
	
		<header id="topo">
			<?php
			require_once PATH_INCLUDES_USERS . 'header.php';
			?>
		</header>

		<!-- Inicio Main Conteudo -->
		<div id="mainConteudo">
			<!-- Inicio Conteudo -->
			<div id="conteudo">
				<!-- Inicio Conteudo File 1 -->
				<div id=home_file1>
					<!-- Inicio Coluna Direita -->
					<div id="hf1_Col1">
						<?php
						/* Menu de Busca */
						require_once PATH_INCLUDES_USERS . 'form-search.php';
						?>
					</div>
					<!-- Fim Coluna Direita -->
					<!-- Inicio Coluna esquerda -->
					<div id="hf1_Col2">
						<?php
						/* Menu navegação + banner */
						require_once PATH_INCLUDES_USERS . 'main-menu.php';
						?>
					</div>
					<!-- Fim Coluna esquerda -->
				</div>
				<!-- Fim Conteudo File 1 -->
				<!-- Inicio Conteudo File2 -->
				<div id="home_file2">
					<!-- Inicio Coluna Direita -->
					<div id="hf2_Col1">
						<div class="hmInfo">
							<div class="imgFT" 
								<img src="<?php echo $pathNoticePhoto; ?>" />
							</div>
							<div class="inf_Tit">NOTÍCIAS</div>
							<div class="inf_Subti"><?php echo $notice->getTitle(); ?></div>
							<div class="inf_Text"><?php echo $notice->getSubTitle(); ?></div>
							<div class="inf_Btmais">
								<a href="noticias.php"><img src="images/bt_mais.png" /></a>
							</div>
						</div>
						<div class="hmInfo">
							<div class="imgFT">
								<img src="<?php echo $pathDecoracaoPhoto; ?>" />
							</div>
							<div class="inf_Tit"><a href="decoracao.php">DECORAÇÃO</a></div>
							<div class="inf_Subti"><a href="decoracao.php"><?php echo $deco->getTitle(); ?></a></div>
							<div class="inf_Text"><a href="decoracao.php"><?php echo $deco->getSubTitle(); ?></a></div>
							<div class="inf_Btmais">
								<a href="decoracao.php"><img src="images/bt_mais.png" /></a>
							</div>
						</div>
						<div class="hmInfo">
							<div class="imgFT">
								<img src="<?php echo $pathTechnologyPhoto; ?>" />
							</div>
							<div class="inf_Tit"><a href="tecnologia.php">TECNOLOGIA</a></div>
							<div class="inf_Subti"><a href="tecnologia.php"><?php echo $tec->getTitle(); ?></a></div>
							<div class="inf_Text"><a href="tecnologia.php"><?php echo $tec->getSubTitle(); ?></a></div>
							<div class="inf_Btmais">
								<a href="tecnologia.php"><img src="images/bt_mais.png" /></a>
							</div>
						</div>
						<div class="hmInfo">
							<div class="imgFT">
								<img src="<?php echo $pathDicaPhoto; ?>" />
							</div>
							<div class="inf_Tit"><a href="dicas.php">DICA</a></div>
							<div class="inf_Subti"><a href="dicas.php"><?php echo $dica->getTitle(); ?></a></div>
							<div class="inf_Text"><a href="dicas.php"><?php echo $dica->getSubTitle(); ?></a></div>
							<div class="inf_Btmais">
								<a href="dicas.php"><img src="images/bt_mais.png" /></a>
							</div>
						</div>
					</div>
					<!-- Fim Coluna Direita -->
					<!-- Inicio Coluna Esquerda -->
					<div id="hf2_Col2">
						<?php
						$limitAdvertising = 4;
						require_once PATH_INCLUDES_USERS . 'col-advertising.php';
						?>
					</div>
					<!-- Fim Coluna Esquerda -->
				</div>
				<!-- Fim Conteudo File2 -->
			</div>
			<!-- Fim Conteudo -->
		</div>
		<!-- Fim Main Conteudo -->
		<footer id="footer">
		<?php
		require_once PATH_INCLUDES_ADMIN . 'footer.html';
		?>
		</footer>
	</div>
	<!-- Fim Geral -->

</body>
</html>
<?php
require_once PATH_INCLUDES_USERS . 'functions-js-form-search.html';
?>