<?php

/*
 * date of creating 2013-07-29
 * 
 * @author Edilson Justiano
 */

//include the file of configuration
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI .'GenericBI.class.php';
require_once PATH_MODEL_DAO .'AdminServicesDAO.class.php';
require_once PATH_MODEL_ENTITIES .'Advertising.class.php';
require_once PATH_MODEL_ENTITIES.   'MainAdvertising.class.php';



class AdminServicesBI extends GenericBI {

  private $adminServicesDAO;
  
  public function __construct($connection) {
    parent::__construct($connection);
  }
  
   public function cadastreAdvertising(Advertising $advertising){
       
    if(is_null($this->adminServicesDAO)){
      $this->adminServicesDAO = new AdminServicesDAO($this->connection);
    }
    
    $advertisingId = $this->adminServicesDAO->insert($advertising);
    
    return $advertisingId;
  }
  
  
  
  public function updateAdvertising(Advertising $advertising){
       
    if(is_null($this->adminServicesDAO)){
      $this->adminServicesDAO = new AdminServicesDAO($this->connection);
    }
    
    $advertisingId = $this->adminServicesDAO->update($advertising);
    
    return $advertisingId;
  }
  
  /*
  public function findByProperty($advertising){
    if(is_null($this->adminServicesDAO)){
      $this->adminServicesDAO = new PropertyPhotoDAO($this->connection);
    }
    $propertyPhoto = $this->adminServicesDAO->findByProperty($advertising);
    return $propertyPhoto;
  }
  */
  public function getAdvertisingByPosition($position){
    if(is_null($this->adminServicesDAO)){
      $this->adminServicesDAO = new AdminServicesDAO($this->connection);
    }
    
    $criteriaString = "WHERE position = " . $position . ";";
    
    $advertising = $this->adminServicesDAO->findByCriteria($criteriaString);
    
    return $advertising;
  }
  
  
  
  public function getAdvertisings() {
      
    if(is_null($this->adminServicesDAO)){
      $this->adminServicesDAO = new AdminServicesDAO($this->connection);
    }
    
    $advertisings = $this->adminServicesDAO->getAll();

    return $advertisings;
  }
  
  
  /*
   * Mesmo método para verificar se há alguma propaganda 
   * para aquela posiçao caso haja, para que eu posso 
   * deletar ou excluí-la
   */
  public function getMainAdvertisingByPosition($position){
    if(is_null($this->adminServicesDAO)){
      $this->adminServicesDAO = new AdminServicesDAO($this->connection);
    }
    
    $criteriaString = "WHERE position = " . $position . ";";
    
    $mainAdvertising = $this->adminServicesDAO->findMainAdvertisingByCriteria($criteriaString);
    
    return $mainAdvertising;
  }
  
  
  
  public function updateMainAdvertising(MainAdvertising $mainAdvertising){
       
    if(is_null($this->adminServicesDAO)){
      $this->adminServicesDAO = new AdminServicesDAO($this->connection);
    }
    
    $mainAdvertisingId = $this->adminServicesDAO->updateMainAdvertising($mainAdvertising);
    
    return $mainAdvertisingId;
  }
  
  
  
  public function cadastreMainAdvertising(MainAdvertising $mainAdvertising){
       
    if(is_null($this->adminServicesDAO)){
      $this->adminServicesDAO = new AdminServicesDAO($this->connection);
    }
    
    $mainAdvertisingId = $this->adminServicesDAO->insertMainAdvertising($mainAdvertising);
    
    return $mainAdvertisingId;
  }
  
  
  
  public function getMainAdvertisings() {
      
    if(is_null($this->adminServicesDAO)){
      $this->adminServicesDAO = new AdminServicesDAO($this->connection);
    }
    
    $mainAdvertisings = $this->adminServicesDAO->getAllMainAdvertisings();

    return $mainAdvertisings;
  }
  
  
  
  public function getParceiroByPosition($position){
    if(is_null($this->adminServicesDAO)){
      $this->adminServicesDAO = new AdminServicesDAO($this->connection);
    }
    
    $criteriaString = "WHERE position = " . $position . ";";
    
    $mainAdvertising = $this->adminServicesDAO->findParceiroByCriteria($criteriaString);
    
    return $mainAdvertising;
  }
  
  
  public function updateParceiro(Parceiro $parceiro){
       
    if(is_null($this->adminServicesDAO)){
      $this->adminServicesDAO = new AdminServicesDAO($this->connection);
    }
    
    $parceirosId = $this->adminServicesDAO->updateParceiro($parceiro);
    
    return $parceirosId;
  }
  
  
  public function cadastreParceiro(Parceiro $parceiro){
       
    if(is_null($this->adminServicesDAO)){
      $this->adminServicesDAO = new AdminServicesDAO($this->connection);
    }
    
    $parceirosId = $this->adminServicesDAO->insertParceiro($parceiro);
    
    return $parceirosId;
  }
  
  
  public function getParceiros() {
      
    if(is_null($this->adminServicesDAO)){
      $this->adminServicesDAO = new AdminServicesDAO($this->connection);
    }
    
    $parceiros = $this->adminServicesDAO->getAllParceiros();

    return $parceiros;
  }
}

?>
