<?php

/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once './config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI .'GenericBI.class.php';
require_once PATH_MODEL_DAO .'CityDAO.class.php';


/**
 * Description of CityBI
 *
 * @author Daniel / updated by Edilson Justiniano
 */
class CityBI extends GenericBI {

  private $cityDAO;

  public function __construct($connection) {
    parent::__construct($connection);
    $this->cityDAO = new CityDAO($connection);
  }

  public function getCities($limit = null, $orderBy = "") {
    
    $cities = $this->cityDAO->getAll($limit, $orderBy);

    return $cities;
  }
  
  public function getById($id){
    
    $city = $this->cityDAO->findById($id);
    
    return $city;
  }
  
  
  public function addCity($city) {
    if (is_null($this->cityDAO)) {
      $this->cityDAO = new CityDAO($this->connection);
    }

    $this->cityDAO->insert($city);
  }

  
  public function delete($cityId){
      
    if(is_null($this->cityDAO)){
       $this->cityDAO = new CityDAO($this->connection);
    }
    
    return $this->cityDAO->delete(intval($cityId));
  }
  
  
  public function getCount(){
      
    if(is_null($this->cityDAO)){
       $this->cityDAO = new CityDAO($this->connection);
    }
    
    return $this->cityDAO->getCount();
  }
  
  
  public function getCitiesByName($cityName, $limit, $orderBy){
      
    if(is_null($this->cityDAO)){
        $this->cityDAO = new CityDAO($this->connection);
    }
    
    return $this->cityDAO->getCitiesByName($cityName, $limit, $orderBy);
  }
  
  
  
  
  public function updateCity($city) {
    if (is_null($this->cityDAO)) {
      $this->cityDAO = new CityDAO($this->connection);
    }

    return $this->cityDAO->update($city);
  }
}

?>
