<?php

/**
 * Description of ConnectionFactoryBI
 *
 * @author Daniel / updated by Edilson Justiniano
 */


/*
 * INCLUDE SECTOR
 */

//include the file of configure
#require_once '../config.php';
#require_once PATH .'config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_DAO. 'ConnectionFactory.class.php';


class ConnectionFactoryBI {
  
  public function createConnectionWithTransaction($startTransaction){
    return ConnectionFactory::getInstance()->createConnection($startTransaction);
  }
  
  public function commitConnection($connection, $releaseConnection){
    ConnectionFactory::getInstance()->commitConnection($connection, $releaseConnection);
  }
  
  public function rollbackConnection($connection, $releaseConnection){
    ConnectionFactory::getInstance()->rollbackConnection($connection, $releaseConnection);
  }
  
}

?>
