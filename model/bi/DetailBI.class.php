<?php

/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once './config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI  .'GenericBI.class.php';
require_once PATH_MODEL_DAO .'DetailDAO.class.php';


/**
 * Description of CityBI
 *
 * @author Daniel / updated by Edilson Justiniano
 */
class DetailBI extends GenericBI {

  private $detailDAO;

  public function __construct($connection) {
    parent::__construct($connection);
    $this->detailDAO = new DetailDAO($connection);
  }

  public function getAll($limit = null, $orderBy = "") {
    if (is_null($this->detailDAO)) {
      $this->detailDAO = new DetailDAO($this->connection);
    }
    $details = $this->detailDAO->getAll($limit, $orderBy);

    return $details;
  }
  
  public function getById($id){
    if (is_null($this->detailDAO)) {
      $this->detailDAO = new DetailDAO($this->connection);
    }
    $detail = $this->detailDAO->findById($id);
    
    return $detail;
  }
  
  
  public function addDetail($detail) {
    if (is_null($this->detailDAO)) {
      $this->detailDAO = new DetailDAO($this->connection);
    }

    $this->detailDAO->insert($detail);
  }

  
  public function delete($detailId){
      
    if(is_null($this->detailDAO)){
       $this->detailDAO = new DetailDAO($this->connection);
    }
    
    return $this->detailDAO->delete(intval($detailId));
  }
  
  
  public function getCount(){
      
    if(is_null($this->detailDAO)){
       $this->detailDAO = new DetailDAO($this->connection);
    }
    
    return $this->detailDAO->getCount();
  }
  
  
  public function getDetailsByName($detailName, $limit, $orderBy){
      
    if(is_null($this->detailDAO)){
        $this->detailDAO = new DetailDAO($this->connection);
    }
    
    return $this->detailDAO->getDetailsByName($detailName, $limit, $orderBy);
  }
  
  
  
  
  public function updateDetail($detail) {
    if (is_null($this->detailDAO)) {
      $this->detailDAO = new DetailDAO($this->connection);
    }

    return $this->detailDAO->update($detail);
  }
}

?>
