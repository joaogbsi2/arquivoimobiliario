<?php

/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once './config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI  .'GenericBI.class.php';
require_once PATH_MODEL_DAO .'NearDAO.class.php';


/**
 * Description of CityBI
 *
 * @author Daniel / updated by Edilson Justiniano
 */
class NearBI extends GenericBI {

  private $nearDAO;

  public function __construct($connection) {
    parent::__construct($connection);
    $this->nearDAO = new NearDAO($connection);
  }

  public function getAll($limit = null, $orderBy = "") {
    if (is_null($this->nearDAO)) {
      $this->nearDAO = new NearDAO($this->connection);
    }
    $nears = $this->nearDAO->getAll($limit, $orderBy);

    return $nears;
  }
  
  public function getById($id){
    if (is_null($this->nearDAO)) {
      $this->nearDAO = new NearDAO($this->connection);
    }
    $near = $this->nearDAO->findById($id);
    
    return $near;
  }
  
  
  public function addNear($near) {
    if (is_null($this->nearDAO)) {
      $this->nearDAO = new NearDAO($this->connection);
    }

    $this->nearDAO->insert($near);
  }

  
  public function delete($nearId){
      
    if(is_null($this->nearDAO)){
       $this->nearDAO = new NearDAO($this->connection);
    }
    
    return $this->nearDAO->delete(intval($nearId));
  }
  
  
  public function getCount(){
      
    if(is_null($this->nearDAO)){
       $this->nearDAO = new NearDAO($this->connection);
    }
    
    return $this->nearDAO->getCount();
  }
  
  
  public function getNearsByName($nearName, $limit, $orderBy){
      
    if(is_null($this->nearDAO)){
        $this->nearDAO = new NearDAO($this->connection);
    }
    
    return $this->nearDAO->getNearsByName($nearName, $limit, $orderBy);
  }
  
  
  
  
  public function updateNear($near) {
    if (is_null($this->nearDAO)) {
      $this->nearDAO = new NearDAO($this->connection);
    }

    return $this->nearDAO->update($near);
  }
}

?>
