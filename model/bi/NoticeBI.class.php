<?php

/*
 * This class is responsible by call the methods on class of package
 * dao to insert, search, delete or update data on the database 
 * 
 * @author Edilson Justiniano
 */


/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once '../config.php';
#require_once PATH .'config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_DAO.        'NoticeDAO.class.php';
require_once PATH_MODEL_BI.         'GenericBI.class.php';
require_once PATH_MODEL_ENTITIES.   'Notice.class.php';


class NoticeBI extends GenericBI {

  private $noticeDAO;

  public function __construct($connection) {
    parent::__construct($connection);
  }

  public function addNotice($notice) {
    if (is_null($this->noticeDAO)) {
      $this->noticeDAO = new NoticeDAO($this->connection);
    }

    $this->noticeDAO->insert($notice);
  }
  
  
  /*
   * Methods add By Edilson Justiniano, on day 12/11/2013. 
   * This methods will be used to find all Users cadastre
   * on system, or only same.
   */
  public function findAll(){
      
    if(is_null($this->noticeDAO)){
       $this->noticeDAO = new NoticeDAO($this->connection);
    }
    
    return $this->noticeDAO->findAll();
  }
  
}//eof class

?>
