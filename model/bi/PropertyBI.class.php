<?php

/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once './config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI .'GenericBI.class.php';
require_once PATH_MODEL_DAO .'PropertyDAO.class.php';
require_once PATH_MODEL_ENTITIES .'Property.class.php';


/**
 * Description of PropertyBI
 *
 * @author Daniel /updated by Edilson Justiniano
 */
class PropertyBI extends GenericBI {

  private $propertyDAO;
  function __construct($connection) {
    parent::__construct($connection);
  }

  public function createProperty(Property $property){
    if(is_null($this->propertyDAO)){
      $this->propertyDAO = new PropertyDAO($this->connection);
    }

    $propertyId = $this->propertyDAO->insertProperty($property);

    return $propertyId;
  }

  public function findById($propertyId){
    if(is_null($this->propertyDAO)){
      $this->propertyDAO = new PropertyDAO($this->connection);
    }
    
//    echo "antes de chamar o dao";
    $property = $this->propertyDAO->findById($propertyId);
    
//    echo "retornou do DAO";
//    exit();
    return $property;
  }

  public function findByIdUnico($idUnico){
    if(is_null($this->propertyDAO)){
      $this->propertyDAO = new PropertyDAO($this->connection);
    }
    
//    echo "antes de chamar o dao";
    $property = $this->propertyDAO->findByIdUnico($idUnico);
    
//    echo "retornou do DAO";
//    exit();
    return $property;
  }

  public function findNeighborhoodByCity($city){
    if(is_null($this->propertyDAO)){
      $this->propertyDAO = new PropertyDAO($this->connection);
    }
    return $this->propertyDAO->findNeighborhoodByCity($city);
  }

  public function findProperty_status($propertyId, $statusId){
    if(is_null($this->propertyDAO)){
      $this->propertyDAO = new PropertyDAO($this->connection);
    }
    $argumentos = "ps.price as price, ps.status_id as status, p.*";
    $tabelas= "property as p INNER JOIN property_status as ps ";
    $criteriaString =  "p.id = ".$propertyId." AND ps.status_id = ".$statusId." AND p.id = ps.property_id";
    
    return $this->propertyDAO->findByCriteria($argumentos, $tabelas, $criteriaString);
  }

  //Add a new parameter code by Edilson Justiniano. This Field was add by new layout on the search
  public function search($cityId, $neighborhood, $type,$min,$max,$qtdMinRooms, $qtdMaxRooms,$status,
                    $limit,$offset,$deDate,$ateDate,$code){
    if(is_null($this->propertyDAO)){
      $this->propertyDAO = new PropertyDAO($this->connection);
    }
    $daysOfWeek = null;
    $monthsOfYear = null;
    if($status == 3){
      $daysOfWeek = array(
        0=>0,
        1=>0,

        2=>0,
        3=>0,
        4=>0,
        5=>0,
        6=>0
        );
      $monthsOfYear = array(
        1=>0,
        2=>0,
        3=>0,
        4=>0,
        5=>0,
        6=>0,
        7=>0,
        8=>0,
        9=>0,
        10=>0,
        11=>0,
        12=>0);
      while($deDate<=$ateDate){
        $dayOfWeek = date('w',$deDate);
        $daysOfWeek[$dayOfWeek] = true;
        $monthOfYear = date('n',$deDate);
        $monthsOfYear[$monthOfYear] = true;
        $cont++;
        $deDate = strtotime("+1 day", $deDate);
      }
    }
    
    //Add a new parameter code by Edilson Justiniano. This Field was add by new layout on the search
    $propertyResultList = $this->propertyDAO->search($cityId, $neighborhood, $type, $min, $max,$qtdMinRooms,$qtdMaxRooms,$status,$limit,$offset,$daysOfWeek,$monthsOfYear,$code);

    return $propertyResultList;

  }
  
  
  public function searchSize($cityId, $neighborhood,$type,$min,$max,$qtdMinRooms, $qtdMaxRooms,$status,$deDate,$ateDate,$code){
    $daysOfWeek = null;
    $monthsOfYear = null;
    if(is_null($this->propertyDAO)){
      $this->propertyDAO = new PropertyDAO($this->connection);
    
    }if($status == 3){
      $daysOfWeek = array(
        0=>0,
        1=>0,

        2=>0,
        3=>0,
        4=>0,
        5=>0,
        6=>0
        );
      $monthsOfYear = array(
        1=>0,
        2=>0,
        3=>0,
        4=>0,
        5=>0,
        6=>0,
        7=>0,
        8=>0,
        9=>0,
        10=>0,
        11=>0,
        12=>0);
      while($deDate<=$ateDate){
        $dayOfWeek = date('w',$deDate);
        $daysOfWeek[$dayOfWeek] = true;
        $monthOfYear = date('n',$deDate);
        $monthsOfYear[$monthOfYear] = true;
        $cont++;
        $deDate = strtotime("+1 day", $deDate);
      }
    }

    
      //$length = $this->propertyDAO->countPropertys($cityId,$neighborhood, $type,$min,$max,$qtdeRooms,$status);
    //Add a new parameter code by Edilson Justiniano. This Field was add by new layout on the search
    $total = $this->propertyDAO->searchSize($cityId, $neighborhood, $type, $min, $max,$qtdMinRooms,$qtdMaxRooms,$status,$daysOfWeek,$monthsOfYear,$code);

    return $total;

  }
  
  /*
   * Method added by Edilson Justiniano to fill the properties on page comprar and alugar.
   * This method will receive only the status and limit
   */
  public function searchPropertyByStatus($status, $limit) {
    if(is_null($this->propertyDAO)){
      $this->propertyDAO = new PropertyDAO($this->connection);
    }
    //Add a new parameter code by Edilson Justiniano. This Field was add by new layout on the search
    $propertyResultList = $this->propertyDAO->searchPropertyByStatus($status,$limit);

    return $propertyResultList;       
  }
  
  public function searchSizeByStatus($status) {
    if(is_null($this->propertyDAO)){
      $this->propertyDAO = new PropertyDAO($this->connection);
    }
    $total = $this->propertyDAO->searchSizeByStatus($status);

    return $total;
  }
  

  public function enableProperty($propertyId){
    if(is_null($this->propertyDAO)){
      $this->propertyDAO = new PropertyDAO($this->connection);
    }
    $this->propertyDAO->setToActive($propertyId);
  }

 
  
  
  /*
   * Methods add By Edilson Justiniano, on day 02/11/2013. 
   * This methods will be used to find all Properties cadastre
   * on system, or only same.
   */
  public function findAll($limit,$orderBy){
      
    if(is_null($this->propertyDAO)){
      $this->propertyDAO = new PropertyDAO($this->connection);
    }
    
    return $this->propertyDAO->findAll($limit, $orderBy);
  }

  
  /*
   * Method that will go search and return a qtde of property
   * stored on system, I can use this information on page of admin
   * list imoveis 
   */
  public function getCount(){
  
    if(is_null($this->propertyDAO)){
      $this->propertyDAO = new PropertyDAO($this->connection);
    }
    
    return $this->propertyDAO->getCount();

  }
  
  
  
  /*
   * Method that will do delete the selected Property 
   */
  public function deleteProperty($propertyId){
  
      if(is_null($this->propertyDAO)){
      $this->propertyDAO = new PropertyDAO($this->connection);
    }
    
    return $this->propertyDAO->deleteProperty($propertyId);
      
  }
  
  
  /*
   * Método que irá mandar realizar o update dos dados básicos
   * do imovel, semelhante ao método
   */
  public function updateProperty(Property $property){
    if(is_null($this->propertyDAO)){
      $this->propertyDAO = new PropertyDAO($this->connection);
    }

    $wasUpdated = $this->propertyDAO->updateProperty($property);

    return $wasUpdated;
  }
  public function updatePropertyActive($idUnico){
    if(is_null($this->propertyDAO)){
      $this->propertyDAO = new PropertyDAO($this->connection);
    }

    $wasUpdated = $this->propertyDAO->updatePropertyActive(0, $idUnico);

    return $wasUpdated;
  }
}
