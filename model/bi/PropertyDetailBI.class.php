<?php

/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once './config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI .        'DetailBI.class.php';
require_once PATH_MODEL_DAO .       'PropertyDetailDAO.class.php';
require_once PATH_MODEL_ENTITIES .  'Property.class.php';

/**
 * Description of UserServiceBI
 *
 * @author Daniel
 */
class PropertyDetailBI extends GenericBI {

  private $propertyDetailDAO;

  function __construct($connection) {
    parent::__construct($connection);
  }

  public function createNewPropertyDetail($detailsSelected, Property $property) {
    if (is_null($this->propertyDetailDAO)) {
      $this->propertyDetailDAO = new PropertyDetailDAO($this->connection);
    }
    $detailBI = new DetailBI($this->connection);
    $succes = FALSE;
    try {
      foreach ($detailsSelected as $value) {
        $succes = $this->propertyDetailDAO->addDetailinProperty($detailBI->getById($value), $property);
      }
    } catch (Exception $exc) {
      echo $exc->getTraceAsString();
    }
    return $succes;
  }

  public function getPropertyDetail($propertyId) {
    if (is_null($this->propertyDetailDAO)) {
      $this->propertyDetailDAO = new PropertyDetailDAO($this->connection);
    }
    $criteriaString = "WHERE id_property = '" . $propertyId."'";
    $propertyDetail = $this->propertyDetailDAO->findByCriteria($criteriaString);

    return $propertyDetail;
  }

  
  
  /*
   * Method to delete all serviceProvider of this user
   */
  public function deleteDetailsInProperty($propertyId){
      
   if (is_null($this->propertyDetailDAO)) {
      $this->propertyDetailDAO = new PropertyDetailDAO($this->connection);
    }
    $wasDeleted = $this->propertyDetailDAO->deleteDetaislInProperty($propertyId);

    return $wasDeleted;
      
  }
  
  
  /*
   * Método para mostrar os usuários na tela de serviço
   * é chamado da tela de servicos
   */
  public function getPropertyDetailByDetail($detailId) {
    if (is_null($this->propertyDetailDAO)) {
      $this->propertyDetailDAO = new PropertyDetailDAO($this->connection);
    }
    $criteriaString = "WHERE id_detail = " . $detailId. " GROUP BY id_property";
    $userService = $this->propertyDetailDAO->findByCriteria($criteriaString);

    return $userService;
  }
}

?>
