<?php

/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once './config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI .        'NearBI.class.php';
require_once PATH_MODEL_DAO .       'PropertyNearDAO.class.php';
require_once PATH_MODEL_ENTITIES .  'Property.class.php';

/**
 * Description of UserServiceBI
 *
 * @author Daniel
 */
class PropertyNearBI extends GenericBI {

  private $propertyNearDAO;

  function __construct($connection) {
    parent::__construct($connection);
  }

  public function createNewPropertyNear($nearsSelected, Property $property) {
    if (is_null($this->propertyNearDAO)) {
      $this->propertyNearDAO = new PropertyNearDAO($this->connection);
    }
    $nearBI = new NearBI($this->connection);
    $succes = FALSE;
    try {
      foreach ($nearsSelected as $value) {
        $succes = $this->propertyNearDAO->addNearInProperty($nearBI->getById($value), $property);
      }
    } catch (Exception $exc) {
      echo $exc->getTraceAsString();
    }
    return $succes;
  }

  public function getPropertyNear($propertyId) {
    if (is_null($this->propertyNearDAO)) {
      $this->propertyNearDAO = new PropertyNearDAO($this->connection);
    }
    $criteriaString = "WHERE id_property = " . $propertyId;
    $propertyNear = $this->propertyNearDAO->findByCriteria($criteriaString);

    return $propertyNear;
  }

  
  
  /*
   * Method to delete all serviceProvider of this user
   */
  public function deleteNearsInProperty($propertyId){
      
   if (is_null($this->propertyNearDAO)) {
      $this->propertyNearDAO = new PropertyNearDAO($this->connection);
    }
    $wasDeleted = $this->propertyNearDAO->deleteNearsInProperty($propertyId);

    return $wasDeleted;
      
  }
  
  
  /*
   * Método para mostrar os usuários na tela de serviço
   * é chamado da tela de servicos
   */
  public function getPropertyNearByNear($nearId) {
    if (is_null($this->propertyNearDAO)) {
      $this->propertyNearDAO = new PropertyNearDAO($this->connection);
    }
    $criteriaString = "WHERE id_near = " . $nearId. " GROUP BY id_property";
    $propertyNear = $this->propertyNearDAO->findByCriteria($criteriaString);

    return $propertyNear;
  }
}

?>
