<?php

/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once './config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI .'GenericBI.class.php';
require_once PATH_MODEL_DAO .'PropertyPhotoDAO.class.php';
require_once PATH_MODEL_ENTITIES .'PropertyPhoto.class.php';


/**
 * Description of PropertyPhotoBI
 *
 * @author Daniel Fernandes
 */
class PropertyPhotoBI extends GenericBI {

  private $propertyPhotoDAO;
  
  public function __construct($connection) {
    parent::__construct($connection);
  }
  
   public function insertPropertyPhoto(PropertyPhoto $propertyPhoto){
    if(is_null($this->propertyPhotoDAO)){
      $this->propertyPhotoDAO = new PropertyPhotoDAO($this->connection);
    }
    $propertyPhotoId = $this->propertyPhotoDAO->insert($propertyPhoto);
    
    return $propertyPhotoId;
  }
  
  public function findByProperty($property){
    if(is_null($this->propertyPhotoDAO)){
      $this->propertyPhotoDAO = new PropertyPhotoDAO($this->connection);
    }
    $propertyPhoto = $this->propertyPhotoDAO->findByProperty($property);
    return $propertyPhoto;
  }
  
  public function getPhotosByProperty($propertyId){
    if(is_null($this->propertyPhotoDAO)){
      $this->propertyPhotoDAO = new PropertyPhotoDAO($this->connection);
    }
    
    $criteriaString = "WHERE property = " . $propertyId . ";";
    
    $propertyPhoto = $this->propertyPhotoDAO->findByCriteria($criteriaString);
    
    return $propertyPhoto;
  }

  public function getPhotosByPath($propertyPath){
    if(is_null($this->propertyPhotoDAO)){
      $this->propertyPhotoDAO = new PropertyPhotoDAO($this->connection);
    }
    
    $criteriaString = "WHERE path LIKE  '" . $propertyPath . ".jpg';";
    
    $propertyPhoto = $this->propertyPhotoDAO->findByCriteria($criteriaString);
    
    return $propertyPhoto;
  }
  
  
  public function deletePropertyPhoto($propertyPhotoId){
    if(is_null($this->propertyPhotoDAO)){
      $this->propertyPhotoDAO = new PropertyPhotoDAO($this->connection);
    }
    $wasDeleted = $this->propertyPhotoDAO->deletePropertyPhoto($propertyPhotoId);
    return $wasDeleted;
  }
  
}

?>
