<?php

/*
 * INCLUDE SECTOR
 */

// include the file of configuration
// require_once './config.php';
// require_once '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI . 'StatusBI.class.php';
require_once PATH_MODEL_DAO . 'PropertyStatusDAO.class.php';
require_once PATH_MODEL_ENTITIES . 'Property.class.php';

/**
 * Description of UserServiceBI
 *
 * @author Daniel
 */
class PropertyStatusBI extends GenericBI {
	private $propertyStatusDAO;
	function __construct($connection) {
		parent::__construct ( $connection );
	}
	public function createNewPropertyStatus($arrayPrecos, $arrayStatus, $arrayStore, Property $property) {
		if (is_null ( $this->propertyStatusDAO )) {
			$this->propertyStatusDAO = new PropertyStatusDAO( $this->connection );
		}
		$statusBI = new StatusBI( $this->connection );
		$storeBI = new StoreBI( $this->connection );
		$succes = FALSE;
		try {
			$count = 0;
			foreach ( $arrayStatus as $value ) {
				if (! $this->propertyStatusDAO->estaNoBanco ( $property->getid (), $value, $arrayStore[$count] )) {
					$succes = $this->propertyStatusDAO->addStatusinProperty($arrayPrecos[$count], $statusBI->getById($value), $storeBI->getById($arrayStore[$count]), $property);
// 					$succes = $this->propertyStatusDAO->addStatusinProperty($arrayPrecos [$count], $statusBI->getById ( $value ), $arrayStore[$count], $property );
				} else {
					$succes = $this->propertyStatusDAO->updateStatusinProperty($arrayPrecos [$count], $statusBI->getById ( $value ), $storeBI->getById($arrayStore[$count]), $property );
				}
				
				$count ++;
			}
		} catch ( Exception $exc ) {
			echo $exc->getTraceAsString ();
		}
		return $succes;
	}
	public function getPropertyStatus($propertyId) {
		if (is_null ( $this->propertyStatusDAO )) {
			$this->propertyStatusDAO = new PropertyStatusDAO( $this->connection );
		}
		$criteriaString = "WHERE property_id = '" . $propertyId . "'";
		$propertyStatus = $this->propertyStatusDAO->findByCriteria ( $criteriaString );
		
		return $propertyStatus;
	}
	
	public function getPropertyStatusStore($propertyId, $storeId){
		if (is_null ( $this->propertyStatusDAO )) {
			$this->propertyStatusDAO = new PropertyStatusDAO( $this->connection );
		}
		$criteriaString = "WHERE property_id = '" . $propertyId . "'";
		$criteriaString .= "AND store_id = '" . $storeId . "'";
		$propertyStatus = $this->propertyStatusDAO->findByCriteria ( $criteriaString );
		
		return $propertyStatus;
	}
	
	/*
	 * Method to delete all serviceProvider of this user
	 */
	public function deleteStatusInProperty($propertyId) {
		if (is_null ( $this->propertyStatusDAO )) {
			$this->propertyStatusDAO = new PropertyStatusDAO( $this->connection );
		}
		$wasDeleted = $this->propertyStatusDAO->deleteStatuslInProperty($propertyId);
		
		return $wasDeleted;
	}
	
	/*
	 * Método para mostrar os usuários na tela de serviço
	 * é chamado da tela de servicos
	 */
	public function getPropertyStatusByStatus($statusId) {
		if (is_null ( $this->propertyStatusDAO )) {
			$this->propertyStatusDAO = new PropertyStatusDAO ( $this->connection );
		}
		$criteriaString = "WHERE status_id = '" . $statusId . "' GROUP BY property_id";
		$propertyStatus = $this->propertyStatusDAO->findByCriteria ( $criteriaString );
		
		return $propertyStatus;
	}

	public function getPropertyStatusByStore($propertyId, $statusId, $storeId) {
		if (is_null ( $this->propertyStatusDAO )) {
			$this->propertyStatusDAO = new PropertyStatusDAO ( $this->connection );
		}
		$criteriaString = "WHERE property_id = '".$propertyId."' AND status_id = '" . $statusId . "' AND store_id = '". $storeId ."' GROUP BY property_id";
		$propertyStatus = $this->propertyStatusDAO->findByCriteria ( $criteriaString );
		
		return $propertyStatus;
	}
}

?>
