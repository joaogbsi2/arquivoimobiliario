<?php

/*
 * INCLUDE SECTOR
 */

// include the file of configuration
// require_once './config.php';
// require_once '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI . 'StoreBI.class.php';
require_once PATH_MODEL_DAO . 'PropertyStoreDAO.class.php';
require_once PATH_MODEL_ENTITIES . 'Property.class.php';

/**
 * Description of UserServiceBI
 *
 * @author Daniel
 */
class PropertyStoreBI extends GenericBI {
	private $propertyStoreDAO;
	function __construct($connection) {
		parent::__construct ( $connection );
	}
	public function createNewPropertyStore($storeReferencia, $storesSelected, Property $property) {
		if (is_null ( $this->propertyStoreDAO )) {
			$this->propertyStoreDAO = new PropertyStoreDAO ( $this->connection );
		}
		$storeBI = new StoreBI ( $this->connection );
		$succes = FALSE;
		try {
			$count = 0;
			foreach ( $storesSelected as $value ) {
				if (! $this->propertyStoreDAO->estaNoBanco ( $property->getidUnico (), $value )) {
					$succes = $this->propertyStoreDAO->addStoreinProperty ( $storeReferencia [$count], $storeBI->getById ( $value ), $property );
				} else {
					$succes = true;
				}
				
				$count ++;
			}
		} catch ( Exception $exc ) {
			echo $exc->getTraceAsString ();
		}
		return $succes;
	}
	public function getPropertyStore($propertyId) {
		if (is_null ( $this->propertyStoreDAO )) {
			$this->propertyStoreDAO = new PropertyStoreDAO ( $this->connection );
		}
		$criteriaString = "WHERE id_property = '" . $propertyId . "'";
		$propertyStore = $this->propertyStoreDAO->findByCriteria ( $criteriaString );
		
		return $propertyStore;
	}
	
	/*
	 * Method to delete all serviceProvider of this user
	 */
	public function deleteStoresInProperty($propertyId) {
		if (is_null ( $this->propertyStoreDAO )) {
			$this->propertyStoreDAO = new PropertyStoreDAO ( $this->connection );
		}
		$wasDeleted = $this->propertyStoreDAO->deleteStoreslInProperty ( $propertyId );
		
		return $wasDeleted;
	}
	
	/*
	 * Método para mostrar os usuários na tela de serviço
	 * é chamado da tela de servicos
	 */
	public function getPropertyStoreByStore($storeId) {
		if (is_null ( $this->propertyStoreDAO )) {
			$this->propertyStoreDAO = new PropertyStoreDAO ( $this->connection );
		}
		$criteriaString = "WHERE id_store = '" . $storeId . "' GROUP BY id_property";
		$propertyStore = $this->propertyStoreDAO->findByCriteria ( $criteriaString );
		
		return $propertyStore;
	}

	public function getVetorPropertyStore($propertyDetail){
		if (is_null ( $this->propertyStoreDAO )) {
			$this->propertyStoreDAO = new PropertyStoreDAO ( $this->connection );
		}
		$criteriaString = "WHERE id_store = '" . $propertyDetail . "' GROUP BY id_property";
		$propertyStore = $this->propertyStoreDAO->getVetorPropertyStore ( $criteriaString );
		
		return $propertyStore;
	}
}

?>
