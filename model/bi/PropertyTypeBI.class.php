<?php

/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once './config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_DAO .'PropertyTypeDAO.class.php';
require_once PATH_MODEL_BI .'GenericBI.class.php';

/*
 * @author: joao
 */

class PropertyTypeBI extends GenericBI{
    private $propertyTypeDAO;
    
    public function __construct($connection) {
        parent::__construct($connection);
        $this->propertyTypeDAO = new PropertyTypeDAO($connection);
    }
    
    public function findById($propertyTypeId){
      return $this->propertyTypeDAO->findById($propertyTypeId);
    }
    
    public function findAll(){
       return $this->propertyTypeDAO->findAll();
    }
    
}
?>
