<?php

/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once './config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI .'GenericBI.class.php';
require_once PATH_MODEL_DAO .'RentToTheseDaysDAO.class.php';
require_once PATH_MODEL_ENTITIES .'RentToTheseDays.class.php';


/**
 * Description of RentToTheseMonthsBI
 *
 * @author Daniel Fernandes
 */
class RentToTheseDaysBI extends GenericBI {

  private $rentToTheseDaysDAO;

  function __construct($connection) {
    parent::__construct($connection);
  }

  public function insert($rentToTheseDaysDAO) {
    if (is_null($this->rentToTheseDaysDAO)) {
      $this->rentToTheseDaysDAO = new RentToTheseDaysDAO($this->connection);
    }

    $this->rentToTheseDaysDAO->insert($rentToTheseDaysDAO);
  }
  
  
  /*
   * Método que irá pegar os detalhes de imoveis que possuem o status = 3
   * temporada. Ou seja, quero pegar os dias que o imovel estará disponível
   */
   public function getRentToTheseDays($propertyId){
  
        if (is_null($this->rentToTheseDaysDAO)) {
          $this->rentToTheseDaysDAO = new RentToTheseDaysDAO($this->connection);
        }

        $dia = $this->rentToTheseDaysDAO->getRentToTheseDays($propertyId);
        return $dia;
    }
    
    
  /*
   * Método que irá deletar todos os registros da tabela rent_to_these_days
   * de um determinado imovel que será passado por parâmetroß
   */
  public function deleteByProperty($propertyId){
      
      if (is_null($this->rentToTheseDaysDAO)) {
          $this->rentToTheseDaysDAO = new RentToTheseDaysDAO($this->connection);
      }

      $wasDeleted = $this->rentToTheseDaysDAO->deleteByProperty($propertyId);
      return $wasDeleted;
  }
    

}

?>
