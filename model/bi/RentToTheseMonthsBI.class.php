<?php

/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once './config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI .'GenericBI.class.php';
require_once PATH_MODEL_DAO .'RentToTheseMonthsDAO.class.php';
require_once PATH_MODEL_ENTITIES .'RentToTheseMonths.class.php';

/**
 * Description of RentToTheseMonthsBI
 *
 * @author Daniel Fernandes
 */
class RentToTheseMonthsBI extends GenericBI {

  private $rentToTheseMonthsDAO;

  function __construct($connection) {
    parent::__construct($connection);
  }

  public function insert($rentToTheseMonthsDAO) {
    if (is_null($this->rentToTheseMonthsDAO)) {
      $this->rentToTheseMonthsDAO = new RentToTheseMonthsDAO($this->connection);
    }

    $this->rentToTheseMonthsDAO->insert($rentToTheseMonthsDAO);
  }
  
  
  
  
  /*
   * Método que irá pegar os detalhes de imoveis que possuem o status = 3
   * temporada. Ou seja, quero pegar os meses que ele também estará 
   * disponível para locação
   */
  public function getRentToTheseMonths($propertyId){
 
     if (is_null($this->rentToTheseMonthsDAO)) {
        $this->rentToTheseMonthsDAO = new RentToTheseMonthsDAO($this->connection);
     }

     $months = $this->rentToTheseMonthsDAO->getRentToTheseMonths($propertyId);
     return $months;
      
  }
  
  
  
  /*
   * Método que irá deletar todos os registros da tabela rent_to_these_days
   * de um determinado imovel que será passado por parâmetroß
   */
  public function deleteByProperty($propertyId){
  
     if (is_null($this->rentToTheseMonthsDAO)) {
        $this->rentToTheseMonthsDAO = new RentToTheseMonthsDAO($this->connection);
     }

     $wasDeleted = $this->rentToTheseMonthsDAO->deleteByProperty($propertyId);
     return $wasDeleted;
      
  }

}

?>
