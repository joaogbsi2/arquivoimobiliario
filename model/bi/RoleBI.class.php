<?php

/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once './config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI .'GenericBI.class.php';
require_once PATH_MODEL_DAO .'RoleDAO.class.php';

/**
 * Description of RoleBI
 *
 * @author Daniel
 */
class RoleBI extends GenericBI {
  private $roleDAO;
  
  public function __construct($connection) {
    parent::__construct($connection);
  }

  public function getRoleByName($name) {
    if(is_null($this->roleDAO)){
      $this->roleDAO = new RoleDAO($this->connection);
    }
    $criteriaString = "WHERE UPPER(role_name) = UPPER(" . $name . ");";

    $role = $this->roleDAO->getUniqueByCriteria($criteriaString);

    return $role;
  }

}

?>
