<?php

/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once './config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI .'GenericBI.class.php';
require_once PATH_MODEL_DAO .'ServiceDAO.class.php';

/**
 * Description of ServiceBI
 *
 * @author Daniel
 */
class ServiceBI extends GenericBI {

  private $serviceDAO;

  function __construct($connection) {
    parent::__construct($connection);
  }

  public function getAll() {
    if (is_null($this->serviceDAO)) {
      $this->serviceDAO = new ServiceDAO($this->connection);
    }
    $services = $this->serviceDAO->findAll();

    return $services;
  }

  public function getById($id) {
    if (is_null($this->serviceDAO)) {
      $this->serviceDAO = new ServiceDAO($this->connection);
    }

    return $this->serviceDAO->findById($id);
  }
  
  
  /*
   * Método para buscar todos os serviços que iniciarem com 
   * uma determinada letra, passada por parâmetro
   */
  public function getServicesByCriteria($letter) {
    if (is_null($this->serviceDAO)) {
      $this->serviceDAO = new ServiceDAO($this->connection);
    }
    $criteriaString = "WHERE UPPER(service_description) LIKE '".$letter."%'";
    $services = $this->serviceDAO->getServicesByCriteria($criteriaString);

    return $services;
  }

}

?>
