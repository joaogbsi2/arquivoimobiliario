<?php

/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once './config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI .'GenericBI.class.php';
require_once PATH_MODEL_DAO .'StateDAO.class.php';


/**
 * Description of StateBI
 *
 * @author Daniel /Edilson Justiniano
 */
class StateBI extends GenericBI {

  private $stateDAO;

  function __construct($connection) {
    parent::__construct($connection);
  }

  public function getById($id) {
    try {
      if (is_null($this->stateDAO)) {
        $this->stateDAO = new StateDAO($this->connection);
      }
      $state = $this->stateDAO->findById($id);

      return $state;
    } catch (Exception $exc) {
      echo $exc->getTraceAsString();
      exit();
    }
  }

  public function getAll() {
    if (is_null($this->stateDAO)) {
      $this->stateDAO = new StateDAO($this->connection);
    }

    $states = $this->stateDAO->findAll();

    return $states;
  }
  
  public function addState($state) {
  	if (is_null($this->stateDAO)) {
  		$this->stateDAO = new StateDAO($this->connection);
  	}
  
  	$this->stateDAO->insert($state);
  }
  
  public function delete($stateId){
  
  	if(is_null($this->stateDAO)){
  		$this->stateDAO = new StateDAO($this->connection);
  	}
  
  	return $this->stateDAO->delete(intval($stateId));
  }

  public function getPoruf($uf) {
    try {
      if (is_null($this->stateDAO)) {
        $this->stateDAO = new StateDAO($this->connection);
      }

      $stringCriteria = "WHERE uf LIKE '" . $uf."'";

      $state = $this->stateDAO->findUniqueByCriteria($stringCriteria);

      return $state;
    } catch (Exception $exc) {
      echo $exc->getTraceAsString();
      exit();
    }
  }
  
  public function getByName($stateName) {
    try {
      if (is_null($this->stateDAO)) {
        $this->stateDAO = new StateDAO($this->connection);
      }

      $stringCriteria = "WHERE name = " . $stateName;

      $state = $this->stateDAO->findUniqueByCriteria($stringCriteria);

      return $state;
    } catch (Exception $exc) {
      echo $exc->getTraceAsString();
      exit();
    }
  }
  
  public function updateState($state) {
  	if (is_null($this->stateDAO)) {
  		$this->stateDAO = new StateDAO($this->connection);
  	}
  
  	return $this->stateDAO->update($state);
  }

}

?>
