<?php

/*
 * This class is responsible by call the methods on class of package
 * dao to search of propertys data on the database 
 * 
 * @author Edilson Justiniano
 */


/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once './config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_DAO. 'StatusDAO.class.php';
require_once PATH_MODEL_BI. 'GenericBI.class.php';

/**
 * Description of StatusBI
 *
 * @author Daniel /updated by Edilson Justiniano
 */
class StatusBI extends GenericBI {
  private $statusDAO;
  
  function __construct($connection) {
    parent::__construct($connection);
    $this->statusDAO = new StatusDAO($connection);
  }
  
  public function getById($id){
    return $this->statusDAO->finById($id);
  }
  
  public function getAll() {
      return $this->statusDAO->findAll();
  }
}

?>
