<?php

/*
 * This class is responsible by call the methods on class of package
 * dao to insert, search, delete or update data on the database 
 * 
 * @author Edilson Justiniano
 */


/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once '../config.php';
#require_once PATH .'config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_DAO.        'StoreDAO.class.php';
require_once PATH_MODEL_BI.         'GenericBI.class.php';
require_once PATH_MODEL_ENTITIES.   'Store.class.php';




class StoreBI extends GenericBI {

  private $storeDAO;

  public function __construct($connection) {
    parent::__construct($connection);
  }

  public function addStore($store) {
    if (is_null($this->storeDAO)) {
      $this->storeDAO = new StoreDAO($this->connection);
    }

    $this->storeDAO->insert($store);
  }

  public function getById($id) {
    if (is_null($this->storeDAO)) {
       $this->storeDAO = new StoreDAO($this->connection);
    }
    
    $store = $this->storeDAO->findById(intval($id));
    return $store;
  }

  
  
  /*
   * Methods add By Edilson Justiniano, on day 12/11/2013. 
   * This methods will be used to find all Users cadastre
   * on system, or only same.
   */
  public function findAll($limit,$orderBy){
      
    if(is_null($this->storeDAO)){
       $this->storeDAO = new StoreDAO($this->connection);
    }
    
    return $this->storeDAO->findAll($limit, $orderBy);
  }
  
  
  
  /*
   * Method that will go search and return a qtde of users
   * stored on system, I can use this information on page of admin
   * list users 
   */
  public function getCount(){
      
    if(is_null($this->storeDAO)){
       $this->storeDAO = new StoreDAO($this->connection);
    }
    
    return $this->storeDAO->getCount();
  }
  
  
  
  /*
   * Method that will remove a user from database
   */
  public function delete($idStore){
      
    if(is_null($this->storeDAO)){
       $this->storeDAO = new StoreDAO($this->connection);
    }
    
    return $this->storeDAO->delete(intval($idStore));
  }
  
  
  
  /*
   * Method that will go to make a update of user
   */
  public function updateStore($store) {
    if (is_null($this->storeDAO)) {
       $this->storeDAO = new StoreDAO($this->connection);
    }

  return $this->storeDAO->update($store);
  }
  
}//eof class

?>
