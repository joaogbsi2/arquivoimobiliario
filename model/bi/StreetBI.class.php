<?php


// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI .'GenericBI.class.php';
require_once PATH_MODEL_DAO .'StreetDAO.class.php';

class StreetBI extends GenericBI{
	
	private $streetDAO;
	
	public function __construct($connection){
		parent::__construct($connection);
		$this->streetDAO = new StreetDAO($connection);
	}
	
	public function getStreets($limit = null, $orderBy =""){
		$streets = $this->streetDAO->getAll($limit, $orderBy);

		return $streets;
	}

	public function getById($id){
		
		$street = $this->streetDAO->findById($id);

		return $street;
	}
	
	public function addStreet($street){
		if (is_null($this->streetDAO)){
			$this->streetDAO = new StreetDAO($connection);
		}
		
		$this->streetDAO->insert($street);
	}
	
	public function getCount(){
		if (is_null($this->streetDAO)){
			$this->streetDAO = new StreetDAO($connection);
		}
		
		return $this->streetDAO->getCount();
		
	}
	
	public function getStreetsByName($streetName, $limit, $orderBy){
		if (is_null($this->streetDAO)){
			$this->streetDAO = new StreetDAO($connection);
		}
		
		return $this->streetDAO->getStreetByName($streetName, $limit, $orderBy);
	}
	public function getStreetsByBairroName($bairro, $streetName, $limit, $orderBy){
		if (is_null($this->streetDAO)){
			$this->streetDAO = new StreetDAO($connection);
		}
		
		return $this->streetDAO->getStreetByBairroName($bairro, $streetName, $limit, $orderBy);
	}
	
	public function updateStreet($street){
		if (is_null($this->streetDAO)){
			$this->streetDAO = new StreetDAO($connection);
		}
		
		return $this->streetDAO->update($street);
	}
}