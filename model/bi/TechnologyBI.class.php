<?php

/*
 * This class is responsible by call the methods on class of package
 * dao to insert, search, delete or update data on the database 
 * 
 * @author Edilson Justiniano
 */


/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once '../config.php';
#require_once PATH .'config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_DAO.        'TechnologyDAO.class.php';
require_once PATH_MODEL_BI.         'GenericBI.class.php';
require_once PATH_MODEL_ENTITIES.   'Technology.class.php';


class TechnologyBI extends GenericBI {

  private $technologyDAO;

  public function __construct($connection) {
    parent::__construct($connection);
  }

  public function addTechnology($notice) {
    if (is_null($this->technologyDAO)) {
      $this->technologyDAO = new TechnologyDAO($this->connection);
    }

    $this->technologyDAO->insert($notice);
  }
  
  
  /*
   * Methods add By Edilson Justiniano, on day 12/11/2013. 
   * This methods will be used to find all Users cadastre
   * on system, or only same.
   */
  public function findAll(){
      
    if(is_null($this->technologyDAO)){
       $this->technologyDAO = new TechnologyDAO($this->connection);
    }
    
    return $this->technologyDAO->findAll();
  }
  
}//eof class

?>
