<?php

/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once './config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_BI .'ServiceBI.class.php';
require_once PATH_MODEL_DAO .'UserServiceDAO.class.php';
require_once PATH_MODEL_ENTITIES .'User.class.php';

/**
 * Description of UserServiceBI
 *
 * @author Daniel
 */
class UserServiceBI extends GenericBI {

  private $userServiceDAO;

  function __construct($connection) {
    parent::__construct($connection);
  }

  public function createNewServiceProvider($userChoice, User $user) {
    if (is_null($this->userServiceDAO)) {
      $this->userServiceDAO = new UserServiceDAO($this->connection);
    }
    $serviceBI = new ServiceBI($this->connection);
    $succes = FALSE;
    try {
      foreach ($userChoice as $value) {
        $succes = $this->userServiceDAO->addServiceProvider($serviceBI->getById($value), $user);
      }
    } catch (Exception $exc) {
      echo $exc->getTraceAsString();
    }
    return $succes;
  }

  public function getUserService($userId) {
    if (is_null($this->userServiceDAO)) {
      $this->userServiceDAO = new UserServiceDAO($this->connection);
    }
    $criteriaString = "WHERE id_user = " . $userId;
    $userService = $this->userServiceDAO->findByCriteria($criteriaString);

    return $userService;
  }

  
  
  /*
   * Method to delete all serviceProvider of this user
   */
  public function deleteServiceProvider($userId){
      
   if (is_null($this->userServiceDAO)) {
      $this->userServiceDAO = new UserServiceDAO($this->connection);
    }
    $wasDeleted = $this->userServiceDAO->deleteServiceProvider($userId);

    return $wasDeleted;
      
  }
  
  
  /*
   * Método para mostrar os usuários na tela de serviço
   * é chamado da tela de servicos
   */
  public function getUserServiceByService($serviceId) {
    if (is_null($this->userServiceDAO)) {
      $this->userServiceDAO = new UserServiceDAO($this->connection);
    }
    $criteriaString = "WHERE id_service = " . $serviceId. " GROUP BY id_user";
    $userService = $this->userServiceDAO->findByCriteria($criteriaString);

    return $userService;
  }
  
  public function findServiceProviderByUser($serviceId, $userId){
  	if (is_null($this->userServiceDAO)) {
  		$this->userServiceDAO = new UserServiceDAO($this->connection);
  	}

    // SELECT * FROM user_service WHERE id_service = 51 AND id_user = 186
  	$criteriaString = "WHERE id_service = " . $serviceId. " AND id_user = ".$userId;
  	$userService = $this->userServiceDAO->findByCriteria($criteriaString);
  	
  	return $userService;
  }
}

?>
