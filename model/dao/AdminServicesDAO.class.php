<?php

/*
 * date of creating 2013-07-29
 * 
 * @author Edilson Justiano
 */

//include the file of configuration
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_ENTITIES.   'Advertising.class.php';
require_once PATH_MODEL_ENTITIES.   'User.class.php';
require_once PATH_MODEL_ENTITIES.   'MainAdvertising.class.php';
require_once PATH_MODEL_ENTITIES.   'Parceiro.class.php';


class AdminServicesDAO {

  private $connection;

  function __construct($connection) {
    $this->connection = $connection;
    $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  }
  
  
  public function insert(Advertising $advertising) {
    try {
      $sql = "INSERT INTO  advertising(
            position, photo)VALUES (:position, :photo);";

      $stmt = $this->connection->prepare($sql);

      $params = array(
          ":position" => $advertising->getPosition(),
          ":photo" => $advertising->getPhoto()
      );

      $stmt = $this->connection->prepare($sql);
      if ($stmt->execute($params)) {
        return 0;  
        //return $this->connection->lastInsertId('property_photo_id_seq');
      } else {
        return NULL;
      }
    } catch (PDOException $exc) {
      echo $exc->getTraceAsString();
      print_r($stmt->errorInfo());
      exit();
    }
  }

  
  
  
  public function update(Advertising $advertising) {
    try {
      $sql = "UPDATE advertising set photo = :photo where position = :position;";

      $stmt = $this->connection->prepare($sql);

      $params = array(
          ":position" => $advertising->getPosition(),
          ":photo" => $advertising->getPhoto()
      );

      $stmt = $this->connection->prepare($sql);
      if ($stmt->execute($params)) {
        return 0;  
        //return $this->connection->lastInsertId('property_photo_id_seq');
      } else {
        return NULL;
      }
    } catch (PDOException $exc) {
      echo $exc->getTraceAsString();
      print_r($stmt->errorInfo());
      exit();
    }
  }


  public function getAll() {
    try {
      $sql = "SELECT * from  advertising";

      $stmt = $this->connection->prepare($sql);
      if ($stmt->execute()) {
        while ($row = $stmt->fetch()) {
          $advertisings[] = $this->row2Admin($row);
        }
        return $advertisings;
      }
      return NULL;
    } catch (PDOException $exc) {
      print_r($stmt->errorInfo());
      echo "<br />";
      echo $exc->getTraceAsString();
      exit();
    }
  }

  public function findById($id){
    try {
      $sql = "SELECT * FROM  advertising WHERE id=" . $id;
      $stmt = $this->connection->prepare($sql);
      if($stmt->execute()){
        $city = $this->row2Admin($stmt->fetch());
        return $city;
      } else {
        return NULL;
      }
    } catch (PDOException $exc) {
      echo $exc->getTraceAsString();
      print_r($stmt->errorInfo());
      exit();
    }
    }
  
    
    public function findByCriteria($criteriaString) {
     try {
      $sql = "SELECT *
              FROM  advertising " . $criteriaString;

      $stmt = $this->connection->prepare($sql);
      if ($stmt->execute()) {
        while ($row = $stmt->fetch()) {
          $advertisings[] = $this->row2Admin($row);
        }
        return $advertisings;
      } else {
        return NULL;
      }
    } catch (PDOException $exc) {
      var_dump($stmt->errorInfo());
      echo "<br /><br />";
      echo $exc->getTraceAsString();
      exit();
    }
  }
  
  private function row2Admin($row) {
    
      $advertising = new Advertising();
      
      $advertising->setId($row['id']);
      $advertising->setPosition($row['position']);
      $advertising->setPhoto($row['photo']);
      
      return $advertising;
  }

  
  /*
   * Mesmo método para verificar se há alguma propaganda 
   * para aquela posiçao caso haja, para que eu posso 
   * deletar ou excluí-la
   */
  public function findMainAdvertisingByCriteria($criteriaString) {
     try {
      $sql = "SELECT *
              FROM  main_advertising " . $criteriaString;

      $stmt = $this->connection->prepare($sql);
      if ($stmt->execute()) {
        while ($row = $stmt->fetch()) {
          $mainAdvertisings[] = $this->row2MainAdvertising($row);
        }
        return $mainAdvertisings;
      } else {
        return NULL;
      }
    } catch (PDOException $exc) {
      var_dump($stmt->errorInfo());
      echo "<br /><br />";
      echo $exc->getTraceAsString();
      exit();
    }
  }
  
  
  
  public function updateMainAdvertising(MainAdvertising $mainAdvertising) {
    try {
      $sql = "UPDATE main_advertising set photo = :photo, link= :link, title = :title, subtitle = :subtitle where position = :position;";

      $stmt = $this->connection->prepare($sql);

      $params = array(
          ":position" => $mainAdvertising->getPosition(),
          ":link" => $mainAdvertising->getLink(),
          ":title" => $mainAdvertising->getTitle(),
          ":subtitle" => $mainAdvertising->getSubTitle(),
          ":photo" => $mainAdvertising->getPhoto()
      );

      $stmt = $this->connection->prepare($sql);
      if ($stmt->execute($params)) {
        return 0;  
        //return $this->connection->lastInsertId('property_photo_id_seq');
      } else {
        return NULL;
      }
    } catch (PDOException $exc) {
      echo $exc->getTraceAsString();
      print_r($stmt->errorInfo());
      exit();
    }
  }
  
  
  
  
  public function insertMainAdvertising(MainAdvertising $mainAdvertising) {
    try {
      $sql = "INSERT INTO  main_advertising(
            position, link, title, subtitle, photo)VALUES (:position, :link, :title, :subtitle, :photo);";

      $stmt = $this->connection->prepare($sql);

      $params = array(
          ":position" => $mainAdvertising->getPosition(),
          ":link" => $mainAdvertising->getLink(),
          ":title" => $mainAdvertising->getTitle(),
          ":subtitle" => $mainAdvertising->getSubTitle(),
          ":photo" => $mainAdvertising->getPhoto()
      );

      $stmt = $this->connection->prepare($sql);
      if ($stmt->execute($params)) {
        return 0;  
        //return $this->connection->lastInsertId('property_photo_id_seq');
      } else {
        return NULL;
      }
    } catch (PDOException $exc) {
      echo $exc->getTraceAsString();
      print_r($stmt->errorInfo());
      exit();
    }
  }
  
  
  
  public function getAllMainAdvertisings() {
    try {
      $sql = "SELECT * from  main_advertising";

      $stmt = $this->connection->prepare($sql);
      if ($stmt->execute()) {
        while ($row = $stmt->fetch()) {
          $mainAdvertisings[] = $this->row2MainAdvertising($row);
        }
        return $mainAdvertisings;
      }
      return NULL;
    } catch (PDOException $exc) {
      print_r($stmt->errorInfo());
      echo "<br />";
      echo $exc->getTraceAsString();
      exit();
    }
  }
  
  private function row2MainAdvertising($row) {
    
      $mainAdvertising = new MainAdvertising();
      
      $mainAdvertising->setId($row['id']);
      $mainAdvertising->setPosition($row['position']);
      $mainAdvertising->setPhoto($row['photo']);
      $mainAdvertising->setLink($row['link']);
      $mainAdvertising->setTitle($row['title']);
      $mainAdvertising->setSubTitle($row['subtitle']);
      
      return $mainAdvertising;
  }
  
  
  
  
  
  /*
   * Mesmo método para verificar se há alguma propaganda 
   * para aquela posiçao caso haja, para que eu posso 
   * deletar ou excluí-la
   */
  public function findParceiroByCriteria($criteriaString) {
     try {
      $sql = "SELECT *
              FROM  parceiros " . $criteriaString;

      $stmt = $this->connection->prepare($sql);
      if ($stmt->execute()) {
        while ($row = $stmt->fetch()) {
          $parceiros[] = $this->row2Parceiro($row);
        }
        return $parceiros;
      } else {
        return NULL;
      }
    } catch (PDOException $exc) {
      var_dump($stmt->errorInfo());
      echo "<br /><br />";
      echo $exc->getTraceAsString();
      exit();
    }
  }
  
  
  public function updateParceiro(Parceiro $parceiro) {
    try {
      $sql = "UPDATE parceiros set photo = :photo where position = :position;";

      $stmt = $this->connection->prepare($sql);

      $params = array(
          ":position" => $parceiro->getPosition(),
          ":photo" => $parceiro->getPhoto()
      );

      $stmt = $this->connection->prepare($sql);
      if ($stmt->execute($params)) {
        return 0;  
        //return $this->connection->lastInsertId('property_photo_id_seq');
      } else {
        return NULL;
      }
    } catch (PDOException $exc) {
      echo $exc->getTraceAsString();
      print_r($stmt->errorInfo());
      exit();
    }
  }
  
  
  public function insertParceiro(Parceiro $parceiro) {
    try {
      $sql = "INSERT INTO  parceiros(
            position, photo)VALUES (:position, :photo);";

      $stmt = $this->connection->prepare($sql);

      $params = array(
          ":position" => $parceiro->getPosition(),
          ":photo" => $parceiro->getPhoto()
      );

      $stmt = $this->connection->prepare($sql);
      if ($stmt->execute($params)) {
        return 0;  
        //return $this->connection->lastInsertId('property_photo_id_seq');
      } else {
        return NULL;
      }
    } catch (PDOException $exc) {
      echo $exc->getTraceAsString();
      print_r($stmt->errorInfo());
      exit();
    }
  }
  
  
  public function getAllParceiros() {
    try {
      $sql = "SELECT * from  parceiros";

      $stmt = $this->connection->prepare($sql);
      if ($stmt->execute()) {
        while ($row = $stmt->fetch()) {
          $parceiros[] = $this->row2Parceiro($row);
        }
        return $parceiros;
      }
      return NULL;
    } catch (PDOException $exc) {
      print_r($stmt->errorInfo());
      echo "<br />";
      echo $exc->getTraceAsString();
      exit();
    }
  }
  
  private function row2Parceiro($row) {
    
      $parceiro = new Parceiro();
      
      $parceiro->setId($row['id']);
      $parceiro->setPosition($row['position']);
      $parceiro->setPhoto($row['photo']);
      
      return $parceiro;
  }

}
?>
