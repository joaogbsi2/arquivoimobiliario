<?php

/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once './config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_ENTITIES .'City.class.php';
require_once PATH_MODEL_DAO .'StateDAO.class.php';


/**
 * Description of CityDAO
 *
 * @author Daniel
 */
class CityDAO {

  private $connection;

  function __construct($connection) {
    $this->connection = $connection;
    $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  }

  public function getAll($limit = NULL, $orderBy = "") {
    try {
      $sql = "SELECT * from  city ";
      
      if ($orderBy != "")
          $sql .= " ORDER BY ". $orderBy;
      
      if ($limit != NULL)
          $sql .= " LIMIT ".$limit;
      
      $stmt = $this->connection->prepare($sql);
      if ($stmt->execute()) {
        while ($row = $stmt->fetch()) {
          $cities[] = $this->row2city($row);
        }
        return $cities;
      }
      return NULL;
    } catch (PDOException $exc) {
      print_r($stmt->errorInfo());
      echo "<br />";
      echo $exc->getTraceAsString();
      exit();
    }
  }

  public function findById($id){
    try {
      $sql = "SELECT * FROM  city WHERE id=" . $id;
      $stmt = $this->connection->prepare($sql);
      if($stmt->execute()){
        $city = $this->row2city($stmt->fetch());
        return $city;
      } else {
        return NULL;
      }
    } catch (PDOException $exc) {
      echo $exc->getTraceAsString();
      print_r($stmt->errorInfo());
      exit();
    }
    }
  
  private function row2city($row) {
    $city = new City();
    $stateDAO = new StateDAO($this->connection);

    $city->setId($row['id']);
    $city->setName($row['name']);
    $city->setState($stateDAO->findById($row['state']));

    return $city;
  }
  
  
  public function insert($city){
      try {
      $sql = "INSERT INTO  city (
                name,
                state)
              VALUES (
                :name,
                :state)";
      $stmt = $this->connection->prepare($sql);

      $params = array(
          "name" => $city->getName(),
          "state" => $city->getState()->getId()
      );

      $stmt->execute($params);
    } catch (PDOException $exc) {
      var_dump($stmt->errorInfo());
      echo "<br />";
      echo $exc->getTraceAsString();
      exit();
    }
  }

  
  public function delete($id) {
    
    try {
      $sql = "SELECT count(p.id) as qtde FROM  property p WHERE city = :city;";

      $stmt = $this->connection->prepare($sql);

      if ($stmt->execute(array('city' => $id))) {
            $row = $stmt->fetch();
            if (($row['qtde']) > 0)
                return false;    
      } 
    } catch (PDOException $exc) {
      var_dump($stmt->errorInfo());
      echo "<br />";
      echo $exc->getTraceAsString();
      exit();
    }
      
    try {
      $sql = "DELETE FROM city WHERE id = :id";

      $stmt = $this->connection->prepare($sql);
      if ($stmt->execute(array(":id" => $id)))
          return true;
      
      return false;
    } catch (PDOException $exc) {
      var_dump($stmt->errorInfo());
      echo "<br />";
      echo $exc->getTraceAsString();
      exit();
    }
    
  }
  
  
  public function getCount(){
      
    try {
        $sql = "SELECT COUNT(c.id) as qtde FROM city AS c";
        
        $stmt = $this->connection->prepare($sql);
        
        if ($stmt->execute()) {
            
            $row = $stmt->fetch();
            return $row['qtde'];
            
        } else {
            return NULL;
        }
        
    } catch (PDOException $exc) {
      echo $exc->getTraceAsString();
      print_r($stmt->errorInfo());
      exit();
    }
  }
  
  
  public function getCitiesByName($cityName, $limit = NULL, $orderBy = ""){
      
      try {
      $sql = "SELECT * from  city WHERE UPPER(name) LIKE '%". strtoupper($cityName) . "%'";
      
      
      if ($orderBy != "")
          $sql .= " ORDER BY ". $orderBy;
      
      if ($limit != NULL)
          $sql .= " LIMIT ".$limit;
      
      $stmt = $this->connection->prepare($sql);
      if ($stmt->execute()) {
        while ($row = $stmt->fetch()) {
          $cities[] = $this->row2city($row);
        }
        return $cities;
      }
      return NULL;
    } catch (PDOException $exc) {
      print_r($stmt->errorInfo());
      echo "<br />";
      echo $exc->getTraceAsString();
      exit();
    }
  }
  
  
  public function update($city){
      try {
      $sql = "UPDATE city set name = :name, state =:state WHERE id = :id";
      $stmt = $this->connection->prepare($sql);

      $params = array(
          "name" => $city->getName(),
          "state" => $city->getState()->getId(),
          "id" => $city->getId()
      );

      if ($stmt->execute($params))
          return true;
      
      return false;
    } catch (PDOException $exc) {
      var_dump($stmt->errorInfo());
      echo "<br />";
      echo $exc->getTraceAsString();
      exit();
    }
  }
}

?>
