<?php

/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once './config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_ENTITIES    .'Detail.class.php';


/**
 * Description of CityDAO
 *
 * @author Daniel
 */
class DetailDAO {

  private $connection;

  function __construct($connection) {
    $this->connection = $connection;
    $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  }

  public function getAll($limit = NULL, $orderBy = "") {
    try {
      $sql = "SELECT * from  detail";
      
      if ($orderBy != "")
          $sql .= " ORDER BY ". $orderBy;
      
      if ($limit != NULL)
          $sql .= " LIMIT ".$limit;
      
      $stmt = $this->connection->prepare($sql);
      if ($stmt->execute()) {
        while ($row = $stmt->fetch()) {
          $details[] = $this->row2detail($row);
        }
        return $details;
      }
      return NULL;
    } catch (PDOException $exc) {
      print_r($stmt->errorInfo());
      echo "<br />";
      echo $exc->getTraceAsString();
      exit();
    }
  }

  public function findById($id){
    try {
      $sql = "SELECT * FROM  detail WHERE id=" . $id;
      $stmt = $this->connection->prepare($sql);
      if($stmt->execute()){
        $detail = $this->row2detail($stmt->fetch());
        return $detail;
      } else {
        return NULL;
      }
    } catch (PDOException $exc) {
      echo $exc->getTraceAsString();
      print_r($stmt->errorInfo());
      exit();
    }
    }
  
  private function row2detail($row) {
    $detail = new Detail();
    
    $detail->setId($row['id']);
    $detail->setName($row['name']);
    
    return $detail;
  }
  
  
  public function insert($detail){
      try {
      $sql = "INSERT INTO  detail (
                name)
              VALUES (
                :name)";
      $stmt = $this->connection->prepare($sql);

      $params = array(
          "name" => $detail->getName()
      );

      $stmt->execute($params);
    } catch (PDOException $exc) {
      var_dump($stmt->errorInfo());
      echo "<br />";
      echo $exc->getTraceAsString();
      exit();
    }
  }

  
  public function delete($idDetail) {
    
//    try {
//      $sql = "SELECT count(pd.id_detail) as qtde FROM  property_detail pd WHERE id_detail = :detail_id;";
//
//      $stmt = $this->connection->prepare($sql);
//
//      if ($stmt->execute(array('detail_id' => $idDetail))) {
//            $row = $stmt->fetch();
//            if (($row['qtde']) > 0)
//                return false;    
//      } 
//    } catch (PDOException $exc) {
//      var_dump($stmt->errorInfo());
//      echo "<br />";
//      echo $exc->getTraceAsString();
//      exit();
//    }
      
    try {
      $sql = "DELETE FROM detail WHERE id = :id";

      $stmt = $this->connection->prepare($sql);
      if ($stmt->execute(array(":id" => $idDetail)))
          return true;
      
      return false;
    } catch (PDOException $exc) {
      var_dump($stmt->errorInfo());
      echo "<br />";
      echo $exc->getTraceAsString();
      exit();
    }
    
  }
  
  
  public function getCount(){
      
    try {
        $sql = "SELECT COUNT(d.id) as qtde FROM detail AS d";
        
        $stmt = $this->connection->prepare($sql);
        
        if ($stmt->execute()) {
            
            $row = $stmt->fetch();
            return $row['qtde'];
            
        } else {
            return NULL;
        }
        
    } catch (PDOException $exc) {
      echo $exc->getTraceAsString();
      print_r($stmt->errorInfo());
      exit();
    }
  }
  
  
  public function getDetailsByName($detailName, $limit = NULL, $orderBy = ""){
      
      try {
      $sql = "SELECT * from  detail WHERE UPPER(name) LIKE '%". strtoupper($detailName) . "%'";
      
      
      if ($orderBy != "")
          $sql .= " ORDER BY ". $orderBy;
      
      if ($limit != NULL)
          $sql .= " LIMIT ".$limit;
      
      $stmt = $this->connection->prepare($sql);
      if ($stmt->execute()) {
        while ($row = $stmt->fetch()) {
          $details[] = $this->row2detail($row);
        }
        return $details;
      }
      return NULL;
    } catch (PDOException $exc) {
      print_r($stmt->errorInfo());
      echo "<br />";
      echo $exc->getTraceAsString();
      exit();
    }
  }
  
  
  public function update($detail){
      try {
      $sql = "UPDATE detail set name = :name WHERE id = :id";
      $stmt = $this->connection->prepare($sql);

      $params = array(
          "name" => $detail->getName(),
          "id" => $detail->getId()
      );

      if ($stmt->execute($params))
          return true;
      
      return false;
    } catch (PDOException $exc) {
      var_dump($stmt->errorInfo());
      echo "<br />";
      echo $exc->getTraceAsString();
      exit();
    }
  }
}

?>
