<?php

/**
 * Description of UserDAO
 * This class is responsible about every events performed by users.i.e Login,
 * cadastre of clients...
 *
 * @author Edilson Justiniano
 */


/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once '../config.php';
#require_once PATH .'config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_ENTITIES. 'Dica.class.php';



class DicaDAO {

  private $connection;

  function __construct($connection) {
    $this->connection = $connection;
    $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  }

  public function insert(Dica $dica) {
      
    if ($this->thereIsDica()){
//        echo "aqui";
//        exit();
        $this->delete ();
    }
    try {
      $sql = "INSERT INTO  dica (
                title,
                subtitle,
                description,
                photo)
              VALUES (
                :title,
                :subTitle,
                :description,
                :photo)";
      $stmt = $this->connection->prepare($sql);

      $params = array(
          "title" => $dica->getTitle(),
          "subTitle" => $dica->getSubTitle(),
          "description" => $dica->getDescription(),
          "photo" => $dica->getPhoto()
      );
      
      $stmt->execute($params);
    } catch (PDOException $exc) {
      var_dump($stmt->errorInfo());
      echo "<br />";
      echo $exc->getTraceAsString();
      exit();
    }
  }

  
  public function delete() {
    
    try {
      $sql = "DELETE FROM dica;";
      
      
      $stmt = $this->connection->prepare($sql);
      if ($stmt->execute()){
        return true;
      }
      return false;
    } catch (PDOException $exc) {
      var_dump($stmt->errorInfo());
      echo "<br />";
      echo $exc->getTraceAsString();
      exit();
    }
    
  }

  public function findAll() {
    try {
      //$users[] = array();
      $sql = "SELECT * FROM  dica;";
      $stmt = $this->connection->prepare($sql);
      if ($stmt->execute()) {
          $flag = false;
        
          while ($row = $stmt->fetch()) {
            $tecObj = $this->row2dica($row);
            $flag = TRUE;
          }
          
          if ($flag){
            return $tecObj;
          }
      } else {
        return NULL;
      }
    } catch (PDOException $exc) {
      var_dump($stmt->errorInfo());
      echo "<br />";
      echo $exc->getTraceAsString();
      exit();
    }
  }

  

 public function row2dica($row) {
    $dica = new Dica();

    $dica->setId($row['id']);
    $dica->setTitle($row['title']);
    $dica->setSubTitle($row['subtitle']);
    $dica->setDescription($row['description']);
    $dica->setPhoto($row['photo']);
    
    return $dica;
  }
  
  
  
  /*
   * Verificar se há propriedade linkada a essa imobiliaria
   */
  public function thereIsDica() {
    try {
      $sql = "SELECT count(id) as qtde FROM dica;";

      $stmt = $this->connection->prepare($sql);

      if ($stmt->execute()) {
          
        $row = $stmt->fetch();
        if(intval($row['qtde']) > 0)
            return true;
        
        return false;
      }
      return false;
    } catch (PDOException $exc) {
      var_dump($stmt->errorInfo());
      echo "<br />";
      echo $exc->getTraceAsString();
      exit();
    }
  }
}

?>
