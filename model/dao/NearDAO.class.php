<?php

/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once './config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_ENTITIES    .'Near.class.php';


/**
 * Description of CityDAO
 *
 * @author Daniel
 */
class NearDAO {

  private $connection;

  function __construct($connection) {
    $this->connection = $connection;
    $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  }

  public function getAll($limit = NULL, $orderBy = "") {
    try {
      $sql = "SELECT * from  near";
      
      if ($orderBy != "")
          $sql .= " ORDER BY ". $orderBy;
      
      if ($limit != NULL)
          $sql .= " LIMIT ".$limit;
      
      $stmt = $this->connection->prepare($sql);
      if ($stmt->execute()) {
        while ($row = $stmt->fetch()) {
          $nears[] = $this->row2near($row);
        }
        return $nears;
      }
      return NULL;
    } catch (PDOException $exc) {
      print_r($stmt->errorInfo());
      echo "<br />";
      echo $exc->getTraceAsString();
      exit();
    }
  }

  public function findById($id){
    try {
      $sql = "SELECT * FROM near WHERE id=" . $id;
      $stmt = $this->connection->prepare($sql);
      if($stmt->execute()){
        $near = $this->row2near($stmt->fetch());
        return $near;
      } else {
        return NULL;
      }
    } catch (PDOException $exc) {
      echo $exc->getTraceAsString();
      print_r($stmt->errorInfo());
      exit();
    }
    }
  
  private function row2near($row) {
    $near = new Near();
    
    $near->setId($row['id']);
    $near->setName($row['name']);
    
    return $near;
  }
  
  
  public function insert($near){
      try {
      $sql = "INSERT INTO  near (
                name)
              VALUES (
                :name)";
      $stmt = $this->connection->prepare($sql);

      $params = array(
          "name" => $near->getName()
      );

      $stmt->execute($params);
    } catch (PDOException $exc) {
      var_dump($stmt->errorInfo());
      echo "<br />";
      echo $exc->getTraceAsString();
      exit();
    }
  }

  
  public function delete($nearId) {
    
//    try {
//      $sql = "SELECT count(pd.id_detail) as qtde FROM  property_detail pd WHERE id_detail = :detail_id;";
//
//      $stmt = $this->connection->prepare($sql);
//
//      if ($stmt->execute(array('detail_id' => $idDetail))) {
//            $row = $stmt->fetch();
//            if (($row['qtde']) > 0)
//                return false;    
//      } 
//    } catch (PDOException $exc) {
//      var_dump($stmt->errorInfo());
//      echo "<br />";
//      echo $exc->getTraceAsString();
//      exit();
//    }
      
    try {
      $sql = "DELETE FROM near WHERE id = :id";

      $stmt = $this->connection->prepare($sql);
      if ($stmt->execute(array(":id" => $nearId)))
          return true;
      
      return false;
    } catch (PDOException $exc) {
      var_dump($stmt->errorInfo());
      echo "<br />";
      echo $exc->getTraceAsString();
      exit();
    }
    
  }
  
  
  public function getCount(){
      
    try {
        $sql = "SELECT COUNT(n.id) as qtde FROM near AS n";
        
        $stmt = $this->connection->prepare($sql);
        
        if ($stmt->execute()) {
            
            $row = $stmt->fetch();
            return $row['qtde'];
            
        } else {
            return NULL;
        }
        
    } catch (PDOException $exc) {
      echo $exc->getTraceAsString();
      print_r($stmt->errorInfo());
      exit();
    }
  }
  
  
  public function getNearsByName($nearName, $limit = NULL, $orderBy = ""){
      
      try {
      $sql = "SELECT * from near WHERE UPPER(name) LIKE '%". strtoupper($nearName) . "%'";
      
      
      if ($orderBy != "")
          $sql .= " ORDER BY ". $orderBy;
      
      if ($limit != NULL)
          $sql .= " LIMIT ".$limit;
      
      $stmt = $this->connection->prepare($sql);
      if ($stmt->execute()) {
        while ($row = $stmt->fetch()) {
          $nears[] = $this->row2near($row);
        }
        return $nears;
      }
      return NULL;
    } catch (PDOException $exc) {
      print_r($stmt->errorInfo());
      echo "<br />";
      echo $exc->getTraceAsString();
      exit();
    }
  }
  
  
  public function update($near){
      try {
      $sql = "UPDATE near set name = :name WHERE id = :id";
      $stmt = $this->connection->prepare($sql);

      $params = array(
          "name" => $near->getName(),
          "id" => $near->getId()
      );

      if ($stmt->execute($params))
          return true;
      
      return false;
    } catch (PDOException $exc) {
      var_dump($stmt->errorInfo());
      echo "<br />";
      echo $exc->getTraceAsString();
      exit();
    }
  }
}

?>
