<?php

/*
 * INCLUDE SECTOR
 */

// include the file of configuration
// require_once './config.php';
// require_once '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_ENTITIES . 'Property.class.php';
require_once PATH_MODEL_DAO . 'CityDAO.class.php';
require_once PATH_MODEL_DAO . 'PropertyTypeDAO.class.php';
require_once PATH_MODEL_ENTITIES . 'City.class.php';
require_once PATH_MODEL_DAO . 'StoreDAO.class.php';
require_once PATH_MODEL_ENTITIES . 'Store.class.php';

/**
 * Description of PropertyDAO
 *
 * @author Daniel /updated by Edilson Justiniano
 */
class PropertyDAO {
	private $connection;
	private $cityDAO;
	private $statusDAO;
	private $propertyTypeDAO;
	private $storeDAO;
	function __construct($connection) {
		$this->connection = $connection;
		$this->connection->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		
		$this->cityDAO = new CityDAO ( $connection );
		$this->statusDAO = new StatusDAO ( $connection );
		$this->propertyTypeDAO = new PropertyTypeDAO ( $connection );
		$this->storeDAO = new StoreDAO ( $connection );
	}
	public function insertProperty(Property $property) {
// 		echo '';
		try {
			// status,
			// price,
			$sql = "INSERT IGNORE INTO property(
              street,
              identifier,
              neighborhood,
              latitude,
              longitude,
              city,
              
              type,
              
              description,
              qtd_rooms,
              active,
              cep,
              garagem,
              pavimento,
              posicao,
              area_construida,
              area_total,
              valor_condominio,
              valor_iptu,
              edicula,
              comprimento,
              largura,
              exclusive_store,
      		  idUnico
      		)
            VALUES(
              :street,
              :identifier,
              :neighborhood,
              :latitude,
              :longitude,
              :city,
              
              :type,
              
              :description,
              :qtdRooms,
              :active,
              :cep,
              :garagem,
              :pavimento,
              :posicao,
              :areaConstruida,
              :areaTotal,
              :valorCondominio,
              :valorIPTU,
              :edicula,
              :comprimento,
              :largura,
              :exclusive_store, 
      		  :idUnico
      		)";
			
			// :status,
			// :price,
			if (is_null ( $property->getAreaConstruida () ) || $property->getAreaConstruida () == "")
				$property->setAreaConstruida ( 'NULL' );
			
			$params = array (
					":street" => $property->getStreet (),
					":identifier" => $property->getIdentifier (),
					":neighborhood" => $property->getNeighborhood (),
					":latitude" => $property->getLatitude (),
					":longitude" => $property->getLongitude (),
					":city" => $property->getCity ()->getId (),
					// ":status" => $property->getStatus ()->getId (),
					":type" => $property->getType ()->getId (),
					// ":price" => $property->getPrice (),
					":description" => $property->getDescription (),
					":qtdRooms" => $property->getQtdRooms (),
					":active" => $property->getActive (),
					":cep" => $property->getCep (),
					":garagem" => $property->getGaragem (),
					":pavimento" => $property->getPavimento (),
					":posicao" => $property->getPosicao (),
					":areaConstruida" => $property->getAreaConstruida (),
					":areaTotal" => $property->getAreaTotal (),
					":valorCondominio" => $property->getValorCondominio (),
					":valorIPTU" => $property->getValorIPTU (),
					":edicula" => $property->getEdicula (),
					":comprimento" => $property->getComprimento (),
					":largura" => $property->getLargura (),
					":exclusive_store" => $property->getExclusiveStore (),
					":idUnico" => $property->getidUnico () 
			);
			
			$stmt = $this->connection->prepare ( $sql );
			if ($stmt->execute ( $params )) {
				return $this->connection->lastInsertId ( 'property_id_seq' );
			} else {
				return NULL;
			}
		} catch ( PDOException $exc ) {
			echo $exc->getTraceAsString ();
			print_r ( $stmt->errorInfo () );
			exit ();
		}
	}
	public function findById($propertyId) {
		try {
			$sql = "SELECT * FROM property WHERE id = :propertyId";
			
			$stmt = $this->connection->prepare ( $sql );
			if ($stmt->execute ( array (
					":propertyId" => $propertyId 
			) )) {
				// echo "antes de tentar gerar o TO";
				// if (is_null($stmt->fetch()) || $stmt->fetch() == "" || count($stmt->fetch()) == 0){
				// echo "retorna NULL";
				// exit();
				// } else {
				// echo "encontrou algo chama o TO e o retorna";
				// // exit();
				// }
				$property = $this->row2property ( $stmt->fetch ( PDO::FETCH_NAMED ) );
				
				// if (is_null($property)){
				// echo "retornou NULL do TO pronto";
				// } else {
				// echo "retornou um OBJ pronto";
				// }
				
				// echo "retornando algo :: ". $property->getId();
				// exit();
				return $property;
			} else {
				// echo "aqui retorna NULL";
				return NULL;
			}
		} catch ( PDOException $exc ) {
			echo $exc->getTraceAsString ();
			print_r ( $stmt->errorInfo () );
			exit ();
		}
	}
	public function findByCriteria($argumentos, $tabelas, $criteriaString) {
// 		echo '';
		try {
			$sql = "SELECT " . $argumentos . " FROM " . $tabelas . " WHERE " . $criteriaString;
			
			$stmt = $this->connection->prepare ( $sql );
			if ($stmt->execute ()) {
				// echo "antes de tentar gerar o TO";
				// if (is_null($stmt->fetch()) || $stmt->fetch() == "" || count($stmt->fetch()) == 0){
				// echo "retorna NULL";
				// exit();
				// } else {
				// echo "encontrou algo chama o TO e o retorna";
				// // exit();
				// }
				$property = $this->row2property ( $stmt->fetch ( PDO::FETCH_NAMED ) );
				
				// if (is_null($property)){
				// echo "retornou NULL do TO pronto";
				// } else {
				// echo "retornou um OBJ pronto";
				// }
				
				// echo "retornando algo :: ". $property->getId();
				// exit();
				return $property;
			} else {
				// echo "aqui retorna NULL";
				return NULL;
			}
		} catch ( PDOException $exc ) {
			echo $exc->getTraceAsString ();
			print_r ( $stmt->errorInfo () );
			exit ();
		}
	}
	public function findByIdUnico($idUnico) {
		echo '';
		try {
			$sql = "SELECT * ";
			$sql .= "FROM property ";
			$sql .= "WHERE idUnico = :idUnico";
			
			$stmt = $this->connection->prepare ( $sql );
			if ($stmt->execute ( array (
					":idUnico" => $idUnico 
			) )) {
				// echo "antes de tentar gerar o TO";
				// if (is_null($stmt->fetch()) || $stmt->fetch() == "" || count($stmt->fetch()) == 0){
				// echo "retorna NULL";
				// exit();
				// } else {
				// echo "encontrou algo chama o TO e o retorna";
				// // exit();
				// }
				$property = new Property ();
				$property = $this->row2property ( $stmt->fetch ( PDO::FETCH_NAMED ) );
				
				// if (is_null($property)){
				// echo "retornou NULL do TO pronto";
				// } else {
				// echo "retornou um OBJ pronto";
				// }
				
				// echo "retornando algo :: ". $property->getId();
				// exit();
				return $property;
			} else {
				// echo "aqui retorna NULL";
				return NULL;
			}
		} catch ( PDOException $exc ) {
			echo $exc->getTraceAsString ();
			print_r ( $stmt->errorInfo () );
			exit ();
		}
	}
	public function findNeighborhoodByCity($city) {
		try {
			$sql = "SELECT DISTINCT p.neighborhood as n FROM property as p WHERE 
          p.city = :city ORDER BY p.neighborhood";
			$stmt = $this->connection->prepare ( $sql );
			if ($stmt->execute ( array (
					"city" => $city->getId () 
			) )) {
				$districtList = NULL;
				while ( $row = $stmt->fetch () ) {
					$districtList [] = $row ['n'];
				}
				return $districtList;
			} else {
				return NULL;
			}
		} catch ( PDOException $exc ) {
			echo $exc->getTraceAsString ();
			print_r ( $stmt->errorInfo () );
			exit ();
		}
	}
	public function searchSize($cityId, $neighborhood, $type, $minPrice, $maxPrice, $qtdMinRooms, $qtdMaxRooms, $status, $days, $months, $code) {
		echo '';
		try {
			$sql = "SELECT COUNT(*) as total FROM property AS p ";
			if ($status == 3) { // temporada
				$sql .= "join rent_to_these_days as d
                on d.property = p.id
                join rent_to_these_months as m
                on m.property = p.id ";
			}
			$sql .= "INNER JOIN property_status as ps";
			$sql .= " where p.id = ps.property_id";
			
			// //Add this condition because the new field on search codigo
			// if (!is_null($code) && $code != "") {
			// $params = array('code' => $code);
			// $sql .= "p.id = :code ";
			// } else {
			
			$sql .= ' AND p.city = :city AND ';
			$sql .= 'ps.status_id = :status';
			
			$params = array (
					'city' => $cityId,
					'status' => $status 
			);
			
			if (count ( $neighborhood ) != 0) {
				$sql .= " AND ";
				$sql .= 'p.neighborhood IN (';
				for($i = 0; $i < count ( $neighborhood ); $i ++) {
					$sql .= ':n' . $i;
					if ($i < count ( $neighborhood ) - 1) {
						$sql .= ',';
					} else {
						$sql .= ') ';
					}
				}
			}
			if ($qtdMinRooms != NULL) {
				$params ['min_rooms'] = $qtdMinRooms;
				$sql .= " AND ";
				$sql .= "p.qtd_rooms > :min_rooms";
			}
			if ($qtdMaxRooms != NULL) {
				$sql .= " AND ";
				$params ['max_rooms'] = $qtdMaxRooms;
				$sql .= "p.qtd_rooms < :max_rooms";
			}
			if ($minPrice != NULL) {
				$sql .= " AND ";
				$params ['min_price'] = $minPrice;
				$sql .= "ps.price > :min_price";
			}
			if ($maxPrice != NULL) {
				$sql .= " AND ";
				$params ['max_price'] = $maxPrice;
				$sql .= "ps.price < :max_price";
			}
			if (count ( $type ) != 0) {
				$sql .= " AND ";
				$sql .= 'p.type IN (';
				for($i = 0; $i < count ( $type ); $i ++) {
					$sql .= ':t' . $i;
					if ($i < count ( $type ) - 1) {
						$sql .= ',';
					} else {
						$sql .= ')';
					}
				}
			}
			
			if ($status == 3) {
				$sql .= $this->buildQueryMonths ( $months );
				$sql .= $this->buildQueryDays ( $days );
			}
			
			// }
			$sql .= " AND ";
			$sql .= "p.active = true";
			$stmt = $this->connection->prepare ( $sql );
			
			for($i = 0; $i < count ( $neighborhood ); $i ++) {
				$key = 'n' . $i;
				$params [$key] = $neighborhood [$i];
			}
			for($i = 0; $i < count ( $type ); $i ++) {
				$key = 't' . $i;
				$params [$key] = $type [$i];
			}
			
			if ($stmt->execute ( $params )) {
				$line = $stmt->fetch ();
				$total = $line ['total'];
				return $total;
			} else {
				return NULL;
			}
		} catch ( PDOException $exc ) {
			echo $exc->getTraceAsString ();
			print_r ( $stmt->errorInfo () );
			exit ();
		}
	}
	
	// Add a new parameter code by Edilson Justiniano. This Field was add by new layout on the search
	public function search($cityId, $neighborhood, $type, $minPrice, $maxPrice, $qtdMinRooms, $qtdMaxRooms, $status, $limit, $offset, $days, $months, $code) {
		try {
			$sql = "SELECT ps.price as price, ps.status_id as status, p.*, p.id as property_id FROM property AS p INNER JOIN property_status as ps ";
			if ($status == 3) {
				$sql .= "join rent_to_these_days as d
                on d.property = p.id
                join rent_to_these_months as m
                on m.property = p.id ";
			}
			$sql .= "where ";
			
			// Add this condition because the new field on search codigo
			// if (!is_null($code) && $code != "") {
			// $params = array('code' => $code);
			// $sql .= "p.id = :code ";
			// } else {
			
			$sql .= 'p.city = :city AND ';
			// $sql .= 'p.city = :'.$cityId. ' AND ';
			$sql .= 'ps.status_id = :status';
			// $sql .= 'p.status = :' .$status;
			
			$params = array (
					'city' => $cityId,
					'status' => $status 
			);
			
			if (count ( $neighborhood ) != 0) {
				$sql .= " AND ";
				$sql .= 'p.neighborhood IN (';
				for($i = 0; $i < count ( $neighborhood ); $i ++) {
					$params ["n" . $i] = $neighborhood [$i];
					$sql .= ':n' . $i;
					// $sql .= $i;
					if ($i < count ( $neighborhood ) - 1) {
						$sql .= ',';
					} else {
						$sql .= ') ';
					}
				}
			}
			if ($qtdMinRooms != NULL) {
				$params ['min_rooms'] = $qtdMinRooms;
				$sql .= " AND ";
				$sql .= "p.qtd_rooms > :min_rooms";
				// $sql .= "p.qtd_rooms > ". $qtdMinRooms;
			}
			if ($qtdMaxRooms != NULL) {
				$sql .= " AND ";
				$params ['max_rooms'] = $qtdMaxRooms;
				$sql .= "p.qtd_rooms = :max_rooms";
				// $sql .= "p.qtd_rooms < ". $qtdMaxRooms;
			}
			if ($minPrice != NULL) {
				$sql .= " AND ";
				$params ['min_price'] = $minPrice;
				$sql .= "ps.price > :min_price";
				// $sql .= "p.price > ". $minPrice;
			}
			if ($maxPrice != NULL) {
				$sql .= " AND ";
				$params ['max_price'] = $maxPrice;
				$sql .= "ps.price < :max_price";
				// $sql .= "p.price < ". $maxPrice;
			}
			if (count ( $type ) != 0) {
				$sql .= " AND ";
				$sql .= 'p.type IN (';
				for($i = 0; $i < count ( $type ); $i ++) {
					$params ["t" . $i] = $type [$i];
					$sql .= ':t' . $i;
					// $sql .= $i;
					if ($i < count ( $type ) - 1) {
						$sql .= ',';
					} else {
						$sql .= ')';
					}
				}
			}
			
			if ($status == 3) {
				$sql .= $this->buildQueryMonths ( $months );
				$sql .= $this->buildQueryDays ( $days );
			}
			
			// }//else new field CODE add on new Layout condition add by Edilson Justiniano
			
			$sql .= " AND ps.property_id = p.id";
			$sql .= " AND ";
			/*
			 * Updated the follow line by Edilson Justiniano because I insert
			 * the Limit and Offset condition to limit the search on 10 results
			 */
			$sql .= "p.active = true LIMIT $limit OFFSET $offset";
			
			/*
			 * Codigo abaixo monta o array para ser executado na query. Note que ele
			 * monta os bairros e tipos de propriedades, porém eu alterei para
			 * fazer isso nas verificações que possuem acima (código desnecessário)
			 */
			
			/*
			 * for($i=0;$i<count($neighborhood);$i++){
			 * $key = 'n' . $i;
			 * $params[$key] = $neighborhood[$i];
			 * }
			 * for($i=0;$i<count($type);$i++){
			 * $key = 't' . $i;
			 * $params[$key] = $type[$i];
			 * }
			 *
			 */
			
			// echo $sql;
			//
			// foreach ($params as $keys=>$value){
			// echo $key ."=>".$value."<br />";
			// }
			$stmt = $this->connection->prepare ( $sql );
			
			if ($stmt->execute ( $params )) {
				$flag = false;
				
				while ( $row = $stmt->fetch ( PDO::FETCH_NAMED ) ) {
					$propertys [] = $this->row2propertyBusca ( $row );
					$flag = true;
				}
				
				if ($flag) {
					return $propertys;
				}
			} else {
				
				return NULL;
			}
		} catch ( PDOException $exc ) {
			echo $exc->getTraceAsString ();
			print_r ( $stmt->errorInfo () );
			exit ();
		}
	}
	public function searchSizeByStatus($status) {
		try {
			$sql = "SELECT COUNT(*) as total FROM property AS p where ";
			$sql .= 'p.status = :status';
			
			$params = array (
					'status' => $status 
			);
			
			$sql .= " AND ";
			$sql .= "p.active = true";
			$stmt = $this->connection->prepare ( $sql );
			
			if ($stmt->execute ( $params )) {
				$line = $stmt->fetch ();
				$total = $line ['total'];
				return $total;
			} else {
				return NULL;
			}
		} catch ( PDOException $exc ) {
			echo $exc->getTraceAsString ();
			print_r ( $stmt->errorInfo () );
			exit ();
		}
	}
	public function searchPropertyByStatus($status, $limit) {
		try {
			$sql = "SELECT *, p.id as property_id FROM property AS p where ";
			$sql .= 'p.status = :status';
			
			$params = array (
					'status' => $status 
			);
			$offset = 0;
			
			$sql .= " AND ";
			/*
			 * Updated the follow line by Edilson Justiniano because I insert
			 * the Limit and Offset condition to limit the search on 10 results
			 */
			$sql .= "p.active = true LIMIT $limit OFFSET $offset";
			
			$stmt = $this->connection->prepare ( $sql );
			
			if ($stmt->execute ( $params )) {
				$flag = false;
				
				while ( $row = $stmt->fetch ( PDO::FETCH_NAMED ) ) {
					$propertys [] = $this->row2propertyBusca ( $row );
					$flag = true;
				}
				
				if ($flag) {
					return $propertys;
				}
			} else {
				
				return NULL;
			}
		} catch ( PDOException $exc ) {
			echo $exc->getTraceAsString ();
			print_r ( $stmt->errorInfo () );
			exit ();
		}
	}
	public function setToActive($propertyId) {
		try {
			$sql = "UPDATE property
              SET active=true
              WHERE id=:id";
			
			$stmt = $this->connection->prepare ( $sql );
			$stmt->execute ( array (
					"id" => $propertyId 
			) );
		} catch ( PDOException $exc ) {
			echo $exc->getTraceAsString ();
			print_r ( $stmt->errorInfo () );
			exit ();
		}
	}
	private function row2propertyBusca($row) {
		$property = new Property ();
		
		$property->setId ( $row ['property_id'] );
		$property->setStreet ( $row ['street'] );
		$property->setIdentifier ( $row ['identifier'] );
		$property->setNeighborhood ( $row ['neighborhood'] );
		$property->setLatitude ( $row ['latitude'] );
		$property->setLongitude ( $row ['longitude'] );
		$property->setDescription ( $row ['description'] );
		$property->setCity ( $this->cityDAO->findById ( $row ['city'] ) );
		if (! is_null ( $row ['status'] [0] ))
			$property->setStatus ( $this->statusDAO->finById ( $row ['status'] [0] ) );
		$property->setType ( $this->propertyTypeDAO->findById ( $row ['type'] ) );
		$property->setDescription ( $row ['description'] );
		if (! is_null ( $row ['price'] [0] ))
			$property->setPrice ( $row ['price'] [0] );
		$property->setQtdRooms ( $row ['qtd_rooms'] );
		
		/*
		 * Define the store to this property
		 */
		$property->setExclusiveStore ( $row ['exclusive_store'] );
		
		/*
		 * Add the new fields added by version 2
		 */
		$property->setPosicao ( $row ['posicao'] );
		$property->setGaragem ( $row ['garagem'] );
		$property->setPavimento ( $row ['pavimento'] );
		$property->setAreaConstruida ( $row ['area_construida'] );
		$property->setAreaTotal ( $row ['area_total'] );
		$property->setValorCondominio ( $row ['valor_condominio'] );
		$property->setValorIPTU ( $row ['valor_iptu'] );
		
		$property->setEdicula ( $row ['edicula'] );
		$property->setComprimento ( $row ['comprimento'] );
		$property->setLargura ( $row ['largura'] );
		$property->setidUnico ( $row ['idUnico'] );
		
		return $property;
	}
	private function row2property($row) {
		// echo "aqui ainda";
		if (! is_null ( $row ) && $row ['id'] != NULL && $row ['id'] != "") {
			// echo "tem dado";
			$property = new Property ();
			
			$property->setId ( $row ['id'] );
			$property->setStreet ( $row ['street'] );
			$property->setIdentifier ( $row ['identifier'] );
			$property->setNeighborhood ( $row ['neighborhood'] );
			$property->setLatitude ( $row ['latitude'] );
			$property->setLongitude ( $row ['longitude'] );
			$property->setDescription ( $row ['description'] );
			$property->setCity ( $this->cityDAO->findById ( $row ['city'] ) );
			if (! is_null ( $row ['status'] [0] ))
				$property->setStatus ( $this->statusDAO->finById ( $row ['status'] [0] ) );
			$property->setType ( $this->propertyTypeDAO->findById ( $row ['type'] ) );
			$property->setDescription ( $row ['description'] );
			if (! is_null ( $row ['price'] [0] ))
				$property->setPrice ( $row ['price'] [0] );
			$property->setQtdRooms ( $row ['qtd_rooms'] );
			
			/*
			 * Define the store to this property
			 */
			$property->setExclusiveStore ( $row ['exclusive_store'] );
			
			/*
			 * Add the new fields added by version 2
			 */
			$property->setCep ( $row ['cep'] );
			$property->setGaragem ( $row ['garagem'] );
			$property->setPavimento ( $row ['pavimento'] );
			$property->setPosicao ( $row ['posicao'] );
			$property->setAreaConstruida ( $row ['area_construida'] );
			$property->setAreaTotal ( $row ['area_total'] );
			$property->setValorCondominio ( $row ['valor_condominio'] );
			$property->setValorIPTU ( $row ['valor_iptu'] );
			
			$property->setEdicula ( $row ['edicula'] );
			$property->setComprimento ( $row ['comprimento'] );
			$property->setLargura ( $row ['largura'] );
			$property->setidUnico ( $row ['idUnico'] );
			
			return $property;
		} else {
			// echo "sem dado";
			return NULL;
		}
	}
	private function buildQueryMonths($monthsOfYear) {
		for($i = 1; $i < count ( $monthsOfYear ) + 1; $i ++) {
			if ($monthsOfYear [$i]) {
				$sql .= " AND ";
				$month = '';
				switch ($i) {
					case 1 :
						$month = "janeiro";
						break;
					case 2 :
						$month = "fevereiro";
						break;
					case 3 :
						$month = "marco";
						break;
					case 4 :
						$month = "abril";
						break;
					case 5 :
						$month = "maio";
						break;
					case 6 :
						$month = "junho";
						break;
					case 7 :
						$month = "julho";
						break;
					case 8 :
						$month = "agosto";
						break;
					case 9 :
						$month = "setembro";
						break;
					case 10 :
						$month = "outubro";
						break;
					case 11 :
						$month = "novembro";
						break;
					case 12 :
						$month = "dezembro";
						break;
				}
				$sql .= 'm.' . $month . '=true';
			}
		}
		return $sql;
	}
	public function buildQueryDays($daysOfWeek) {
		for($i = 0; $i < count ( $daysOfWeek ); $i ++) {
			if ($daysOfWeek [$i]) {
				$sql .= " AND ";
				$day = '';
				switch ($i) {
					case 0 :
						$day = "domingo";
						break;
					case 1 :
						$day = "segunda";
						break;
					case 2 :
						$day = "terca";
						break;
					case 3 :
						$day = "quarta";
						break;
					case 4 :
						$day = "quinta";
						break;
					case 5 :
						$day = "sexta";
						break;
					case 6 :
						$day = "sabado";
						break;
				}
				$sql .= 'd.' . $day . '=true';
			}
		}
		return $sql;
	}
	
	/*
	 * Methods add By Edilson Justiniano, on day 02/11/2013.
	 * This methods will be used to find all Properties cadastre
	 * on system, or only same.
	 */
	public function findAll($limit, $orderBy) {
		try {
			$sql = "SELECT p.* FROM property AS p ";
			
			if ($orderBy != NULL)
				$sql .= "ORDER BY " . $orderBy;
			
			if ($limit != NULL)
				$sql .= " LIMIT " . $limit . " OFFSET 0";
			
			$stmt = $this->connection->prepare ( $sql );
			
			if ($stmt->execute ()) {
				$flag = false;
				
				while ( $row = $stmt->fetch ( PDO::FETCH_NAMED ) ) {
					$propertys [] = $this->row2property ( $row );
					$flag = true;
				}
				
				if ($flag) {
					return $propertys;
				}
			} else {
				return NULL;
			}
		} catch ( PDOException $exc ) {
			echo $exc->getTraceAsString ();
			print_r ( $stmt->errorInfo () );
			exit ();
		}
	}
	
	/*
	 * Method that will go search and return a qtde of property
	 * stored on system, I can use this information on page of admin
	 * list imoveis
	 */
	public function getCount() {
		try {
			$sql = "SELECT COUNT(p.id) as qtde FROM property AS p";
			
			$stmt = $this->connection->prepare ( $sql );
			
			if ($stmt->execute ()) {
				
				$row = $stmt->fetch ();
				return $row ['qtde'];
			} else {
				return NULL;
			}
		} catch ( PDOException $exc ) {
			echo $exc->getTraceAsString ();
			print_r ( $stmt->errorInfo () );
			exit ();
		}
	}
	
	/*
	 * Method that will do delete the selected Property
	 */
	public function deleteProperty($propertyId) {
		
		/*
		 * Primeiro tenho que verificar se essa propriedade
		 * está cadastrada para umas das tabelas:
		 * rent_to_these_days
		 * rent_to_these_months
		 */
		try {
			
			$sql = "SELECT COUNT(property) as qtde FROM rent_to_these_days WHERE property = :id";
			
			$stmt = $this->connection->prepare ( $sql );
			
			if ($stmt->execute ( array (
					"id" => $propertyId 
			) )) {
				
				$row = $stmt->fetch ();
				if (($row ['qtde']) > 0) {
					return false;
				} else {
					$sql = "SELECT COUNT(property) as qtde FROM rent_to_these_months WHERE property = :id";
					
					$stmt = $this->connection->prepare ( $sql );
					
					if ($stmt->execute ( array (
							"id" => $propertyId 
					) )) {
						$row = $stmt->fetch ();
						if (($row ['qtde']) > 0) {
							return false;
						} else {
							$sql = "DELETE FROM property where id = :id";
							
							$stmt = $this->connection->prepare ( $sql );
							$stmt->execute ( array (
									"id" => $propertyId 
							) );
							
							if ($stmt->execute ()) { // try to execute a delete of property
								return true; // case the delete was executed normally
							} else {
								return false; // case the delete not work
							}
						}
					}
				}
			} else {
				return NULL;
			}
		} catch ( PDOException $exc ) {
			echo $exc->getTraceAsString ();
			print_r ( $stmt->errorInfo () );
			exit ();
		}
	}
	
	/*
	 * Método para fazer o update dos dados básicos de um imóvel.
	 * Semelhante ao método insertProperty
	 * //
	 */
	// public function updateProperty(Property $property) {
	// echo 'teste';
	
	// try {
	// $sql = "UPDATE property SET
	// street =:street,
	// identifier =:identifier,
	// neighborhood =:neighborhood,
	// city =:city,
	// status =:status,
	// type =:type,
	// price =:price,
	// description =:description,
	// qtd_rooms =:qtdRooms,
	// cep =:cep,
	// garagem =:garagem,
	// pavimento =:pavimento,
	// posicao =:posicao,
	// area_construida =:areaConstruida,
	// area_total =:areaTotal,
	// valor_condominio =:valorCondominio,
	// valor_iptu =:valorIPTU,
	// edicula =:edicula,
	// comprimento =:comprimento,
	// largura =:largura,
	// idUnico =: idUnico,
	// exclusive_store =:exclusive_store
	// WHERE id =:id";
	// $stmt = $this->connection->prepare($sql);
	// $params = array (
	// ":street" => $property->getStreet (),
	// ":identifier" => $property->getIdentifier (),
	// ":neighborhood" => $property->getNeighborhood (),
	// ":city" => $property->getCity ()->getId (),
	// ":status" => $property->getStatus ()->getId (),
	// ":type" => $property->getType ()->getId (),
	// ":price" => $property->getPrice (),
	// ":description" => $property->getDescription (),
	// ":qtdRooms" => $property->getQtdRooms (),
	// ":cep" => $property->getCep (),
	// ":garagem" => $property->getGaragem (),
	// ":pavimento" => $property->getPavimento (),
	// ":posicao" => $property->getPosicao (),
	// ":areaConstruida" => $property->getAreaConstruida (),
	// ":areaTotal" => $property->getAreaTotal (),
	// ":valorCondominio" => $property->getValorCondominio (),
	// ":valorIPTU" => $property->getValorIPTU (),
	// ":edicula" => $property->getEdicula (),
	// ":comprimento" => $property->getComprimento (),
	// ":largura" => $property->getLargura (),
	// ":idUnico"=>$property->getidUnico(),
	// ":exclusive_store" => $property->getExclusiveStore (),
	// ":id" => $property->getId ()
	// );
	
	// // $stmt = $this->connection->prepare ( $sql );
	// $teste = $stmt->execute ( $params );
	// if (!is_null($teste)) {
	// return TRUE;
	// } else {
	// return FALSE;
	// }
	// } catch ( PDOException $exc ) {
	// var_dump($stmt->errorInfo());
	// echo $exc->getTraceAsString ();
	// print_r ( $stmt->errorInfo () );
	// exit ();
	// }
	// }
	public function updateProperty(Property $property) {
		try {
			// status =:status,
			// price =:price,
			$sql = "UPDATE property SET
              street =:street,
              identifier =:identifier,
              neighborhood =:neighborhood,
              latitude =:latitude,
              longitude =:longitude,
              city =:city,
              
              type =:type,
              
              description =:description,
              qtd_rooms =:qtdRooms,
              cep =:cep,
              garagem =:garagem,
              pavimento =:pavimento,
              posicao =:posicao,
              area_construida =:areaConstruida,
              area_total =:areaTotal,
              valor_condominio =:valorCondominio,
              valor_iptu =:valorIPTU,
              edicula =:edicula,
              comprimento =:comprimento,
              largura =:largura,
              exclusive_store =:exclusive_store
              WHERE id =:id";
			
			$params = array (
					":street" => $property->getStreet (),
					":identifier" => $property->getIdentifier (),
					":neighborhood" => $property->getNeighborhood (),
					":latitude" => $property->getLatitude (),
					":longitude" => $property->getLongitude (),
					":city" => $property->getCity ()->getId (),
					// ":status" => $property->getStatus ()->getId (),
					":type" => $property->getType ()->getId (),
					// ":price" => $property->getPrice (),
					":description" => $property->getDescription (),
					":qtdRooms" => $property->getQtdRooms (),
					":cep" => $property->getCep (),
					":garagem" => $property->getGaragem (),
					":pavimento" => $property->getPavimento (),
					":posicao" => $property->getPosicao (),
					":areaConstruida" => $property->getAreaConstruida (),
					":areaTotal" => $property->getAreaTotal (),
					":valorCondominio" => $property->getValorCondominio (),
					":valorIPTU" => $property->getValorIPTU (),
					":edicula" => $property->getEdicula (),
					":comprimento" => $property->getComprimento (),
					":largura" => $property->getLargura (),
					":id" => $property->getId (),
					":exclusive_store" => $property->getExclusiveStore () 
			);
			
			$stmt = $this->connection->prepare ( $sql );
			if ($stmt->execute ( $params )) {
				return TRUE;
			} else {
				return FALSE;
			}
		} catch ( PDOException $exc ) {
			echo $exc->getTraceAsString ();
			print_r ( $stmt->errorInfo () );
			exit ();
		}
	}
	
	public function updatePropertyActive($active, $idUnico) {
		try {
			$sql = "UPDATE property SET 
					active = :active 
					WHERE property.idUnico = :idUnico";
			$params= array(
					":active" => $active,
					":idUnico" => $idUnico
			);
			$stmt = $this->connection->prepare ($sql);
			if($stmt->execute($params)){
				return true;
			}else{
				return false;
			}
		} catch ( PDOException $exc ) {
			echo $exc->getTraceAsString ();
			print_r ( $stmt->errorInfo () );
			exit ();
		}
	}
}

?>