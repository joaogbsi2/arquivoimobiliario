<?php

/*
 * INCLUDE SECTOR
 */

// include the file of configuration
// require_once './config.php';
// require_once '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_ENTITIES . 'Property.class.php';
require_once PATH_MODEL_ENTITIES . 'Detail.class.php';
require_once PATH_MODEL_ENTITIES . 'PropertyDetail.class.php';

/**
 * Description of UserServiceDAO
 *
 * @author Daniel
 */
class PropertyDetailDAO {
	private $connection;
	function __construct($connection) {
		$this->connection = $connection;
	}
	public function addDetailinProperty(Detail $detail, Property $property) {
		$success = FALSE;
		$this->connection->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		try {
			$sql = "INSERT INTO property_detail(id_property, id_detail)
              VALUES (:property, :detail);";
			
			$stmt = $this->connection->prepare ( $sql );
			
			$params = array (
					"property" => $property->getid (),
					"detail" => $detail->getId () 
			);
			
			$success = $stmt->execute ( $params );
		} catch ( PDOException $exc ) {
			print_r ( $stmt->errorInfo () );
			echo "<br />";
			echo $exc->getTraceAsString ();
		}
		return $success;
	}
	public function findById($id) {
		try {
			$sql = "SELECT * FROM property_detail WHERE id = :id;";
			$stmt = $this->connection->prepare ( $sql );
			$stmt->execute ( array (
					"id" => $id 
			) );
			$serviceProvider = $this->row2propertyDetail ( $stmt->fetch () );
			return $serviceProvider;
		} catch ( PDOException $exc ) {
			echo $exc->getTraceAsString ();
		}
	}
	public function findByCriteria($criteriaString) {
		$this->connection->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		$sql = "SELECT * FROM  property_detail " . $criteriaString;
		
		try {
			$stmt = $this->connection->prepare ( $sql );
			if ($stmt->execute ()) {
				if ($stmt->fetch ()) {
					while ( $row = $stmt->fetch () ) {
						$propertyDetails [] = $this->row2propertyDetail ( $row );
					}
				return $propertyDetails;
				}else
					return null;
			} else {
				return NULL;
			}
		} catch ( PDOException $exc ) {
			print_r ( $stmt->errorInfo () );
			echo '<br />';
			echo $exc->getTraceAsString ();
			exit ();
		}
	}
	public function row2propertyDetail($row) {
		$propertyDetail = new PropertyDetail ();
		$propertyDAO = new PropertyDAO ( $this->connection );
		$detailDAO = new DetailDAO ( $this->connection );
		
		$propertyDetail->setDetail ( $detailDAO->findById ( $row ['id_detail'] ) );
		$propertyDetail->setProperty ( $propertyDAO->findProperty ( $row ['id_property'] ) );
		
		return $propertyDetail;
	}
	
	/*
	 * Method to delete all serviceProvider of this user
	 */
	public function deleteDetaislInProperty($propertyId) {
		try {
			$sql = "DELETE FROM property_detail WHERE id_property = :id_property";
			
			$stmt = $this->connection->prepare ( $sql );
			$stmt->execute ( array (
					":id_property" => $propertyId 
			) );
			return true;
		} catch ( PDOException $exc ) {
			var_dump ( $stmt->errorInfo () );
			echo "<br />";
			echo $exc->getTraceAsString ();
			exit ();
		}
		return false;
	}
}

?>
