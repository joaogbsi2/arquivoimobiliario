<?php

/*
 * INCLUDE SECTOR
 */

// include the file of configuration
// require_once './config.php';
// require_once '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_ENTITIES . 'Property.class.php';
require_once PATH_MODEL_ENTITIES . 'Near.class.php';
require_once PATH_MODEL_ENTITIES . 'PropertyNear.class.php';

/**
 * Description of UserServiceDAO
 *
 * @author Daniel
 */
class PropertyNearDAO {
	private $connection;
	function __construct($connection) {
		$this->connection = $connection;
	}
	public function addNearInProperty(Near $near, Property $property) {
		$success = FALSE;
		$this->connection->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		try {
			$sql = "INSERT INTO property_near(id_property, id_near)
              VALUES (:property, :near);";
			
			$stmt = $this->connection->prepare ( $sql );
			
			$params = array (
					"property" => $property->getid (),
					"near" => $near->getId () 
			);
			
			$success = $stmt->execute ( $params );
		} catch ( PDOException $exc ) {
			print_r ( $stmt->errorInfo () );
			echo "<br />";
			echo $exc->getTraceAsString ();
		}
		return $success;
	}
	public function findById($id) {
		try {
			$sql = "SELECT * FROM property_near WHERE id = :id;";
			$stmt = $this->connection->prepare ( $sql );
			$stmt->execute ( array (
					"id" => $id 
			) );
			$serviceProvider = $this->row2propertyNear ( $stmt->fetch () );
			return $serviceProvider;
		} catch ( PDOException $exc ) {
			echo $exc->getTraceAsString ();
		}
	}
	public function findByCriteria($criteriaString) {
		$this->connection->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		$sql = "SELECT * FROM  property_near " . $criteriaString;
		
		try {
			$stmt = $this->connection->prepare ( $sql );
			if ($stmt->execute ()) {
				if ($stmt->fetch ()) {
					while ( $row = $stmt->fetch () ) {
						$propertyNears [] = $this->row2propertyNear ( $row );
					}
					return $propertyNears;
				} else
					return NULL;
			} else {
				return NULL;
			}
		} catch ( PDOException $exc ) {
			print_r ( $stmt->errorInfo () );
			echo '<br />';
			echo $exc->getTraceAsString ();
			exit ();
		}
	}
	public function row2propertyNear($row) {
		$propertyNear = new PropertyNear ();
		$propertyDAO = new PropertyDAO ( $this->connection );
		$nearDAO = new NearDAO ( $this->connection );
		
		$propertyNear->setNear ( $nearDAO->findById ( $row ['id_near'] ) );
		$propertyNear->setProperty ( $propertyDAO->findProperty ( $row ['id_property'] ) );
		
		return $propertyNear;
	}
	
	/*
	 * Method to delete all serviceProvider of this user
	 */
	public function deleteNearsInProperty($propertyId) {
		try {
			$sql = "DELETE FROM property_near WHERE id_property = :id_property";
			
			$stmt = $this->connection->prepare ( $sql );
			$stmt->execute ( array (
					":id_property" => $propertyId 
			) );
			return true;
		} catch ( PDOException $exc ) {
			var_dump ( $stmt->errorInfo () );
			echo "<br />";
			echo $exc->getTraceAsString ();
			exit ();
		}
		return false;
	}
}

?>
