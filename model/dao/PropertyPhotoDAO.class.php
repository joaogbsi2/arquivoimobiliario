<?php

/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once './config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_ENTITIES .'PropertyPhoto.class.php';
require_once PATH_MODEL_DAO .'PropertyDAO.class.php';


/**
 * Description of PropertyPhoto
 *
 * @author Daniel Fernandes
 */
class PropertyPhotoDAO {

  private $connection;
  private $propertyDAO;

  public function __construct($connection) {
    $this->connection = $connection;
    $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $this->propertyDAO = new PropertyDAO($connection);
  }

  public function insert(PropertyPhoto $propertyPhoto) {
  	echo '';
    try {
      $sql = "INSERT INTO  property_photo(
            path, property)VALUES (:path, :propertyId);";
      
      
      $stmt = $this->connection->prepare($sql);

      $params = array(
          ":path" => $propertyPhoto->getPath(),
          ":propertyId" => $propertyPhoto->getProperty()->getid()
      );

      $stmt = $this->connection->prepare($sql);
      if ($stmt->execute($params)) {
        return $this->connection->lastInsertId('property_photo_id_seq');
      } else {
        return NULL;
      }
    } catch (PDOException $exc) {
      echo $exc->getTraceAsString();
      print_r($stmt->errorInfo());
      exit();
    }
  }

  
  public function findByProperty($property){
  	
      try {
        $sql = "SELECT p.path FROM  property_photo as p WHERE property = :property LIMIT 1";

        $stmt = $this->connection->prepare($sql);
        if ($stmt->execute(array('property' => $property))) {
            $row = $stmt->fetch();
            return $row['path'];
        } else {
            return NULL;
        }
    }catch (PDOException $exc) {
        print_r($stmt->errorInfo());
        echo $exc->getTraceAsString();
        exit();
    }
  }
  public function findByCriteria($criteriaString) {
  	echo '';
    try {
      $sql = "SELECT *
              FROM  property_photo " . $criteriaString;

      $stmt = $this->connection->prepare($sql);
      if ($stmt->execute()) {
        while ($row = $stmt->fetch()) {
          $propertyPhoto[] = $this->row2propertyPhoto($row);
        }
        return $propertyPhoto;
      } else {
        return NULL;
      }
    } catch (PDOException $exc) {
      var_dump($stmt->errorInfo());
      echo "<br /><br />";
      echo $exc->getTraceAsString();
      exit();
    }
  }

  public function getUniqueByCriteria($criteriaString) {
     try {
      $sql = "SELECT *
              FROM  property_photo;" . $criteriaString;

      $stmt = $this->connection->prepare($sql);
      if ($stmt->execute()) {
        $row = $stmt->fetch();
        $propertyPhoto = $this->row2propertyPhoto($row);

        return $propertyPhoto;
      } else {
        return NULL;
      }
    } catch (PDOException $exc) {
      var_dump($stmt->errorInfo());
      echo "<br /><br />";
      echo $exc->getTraceAsString();
      exit();
    }
    return NULL;
  }
  
  
  
  /*
   * Method to delete the property's photo
   */
  public function deletePropertyPhoto($propertyPhotoId){

    try {
      $sql = "DELETE FROM property_photo WHERE id = :id_property_photo";

      $stmt = $this->connection->prepare($sql);
      $stmt->execute(array(":id_property_photo" => $propertyPhotoId));
      return true;
    } catch (PDOException $exc) {
      var_dump($stmt->errorInfo());
      echo "<br />";
      echo $exc->getTraceAsString();
      exit();
    }
    return false;
  }

  public function row2propertyPhoto($row) {
    $propertyPhoto = new PropertyPhoto();
    
    $propertyPhoto->setId($row['id']);
    $propertyPhoto->setPath($row['path']);
    $propertyPhoto->setProperty($this->propertyDAO->findById($row['property']));

    return $propertyPhoto;
  }
  
}

?>