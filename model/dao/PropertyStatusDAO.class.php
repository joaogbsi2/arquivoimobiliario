<?php

/*
 * INCLUDE SECTOR
 */

// include the file of configuration
// require_once './config.php';
// require_once '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_ENTITIES . 'Property.class.php';
require_once PATH_MODEL_ENTITIES . 'Status.class.php';
require_once PATH_MODEL_ENTITIES . 'Store.class.php';
require_once PATH_MODEL_ENTITIES . 'PropertyStatus.class.php';

/**
 * Description of UserServiceDAO
 *
 * @author Daniel
 */
class PropertyStatusDAO {
	private $connection;
	function __construct($connection) {
		$this->connection = $connection;
	}
	public function addStatusinProperty($price, Status $status, Store $store, Property $property) {
		$success = FALSE;
		$this->connection->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		try {
			$sql = "INSERT INTO property_status(price, property_id, status_id, store_id)
              VALUES (:price, :property, :status, :store);";
			
			$stmt = $this->connection->prepare ( $sql );
			
			$params = array (
					"price" => $price,
					"property" => $property->getid (),
					"status" => $status->getId (),
					"store" => $store->getId () 
			);
			
			$success = $stmt->execute ( $params );
		} catch ( PDOException $exc ) {
			print_r ( $stmt->errorInfo () );
			echo "<br />";
			echo $exc->getTraceAsString ();
		}
		return $success;
	}
	public function updateStatusinProperty($price, Status $status, Store $store, Property $property) {
		echo "";
		try {
			$sql = "UPDATE property_status SET 
					price = :price, 
					property_id = :property,
					status_id = :status, 
					store_id = :store 
					WHERE property_id = :property
					AND store_id = :store
					AND status_id = :status";
			
			$params = array (
					":price" => $price,
					":property" => $property->getId (),
					":status" => $status->getId (),
					":store" => $store->getId () 
			);
			$stmt = $this->connection->prepare ( $sql );
			if ($stmt->execute ( $params )) {
				return TRUE;
			} else
				return FALSE;
		} catch ( PDOException $exc ) {
			echo $exc->getTraceAsString ();
			print_r ( $stmt->errorInfo () );
			exit ();
		}
	}
	public function findById($id) {
		try {
			$sql = "SELECT * FROM property_status WHERE id = :id;";
			$stmt = $this->connection->prepare ( $sql );
			$stmt->execute ( array (
					"id" => $id 
			) );
			$serviceProvider = $this->row2propertyStore ( $stmt->fetch () );
			return $serviceProvider;
		} catch ( PDOException $exc ) {
			echo $exc->getTraceAsString ();
		}
	}
	public function findByCriteria($criteriaString) {
		$this->connection->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		$sql = "SELECT * FROM  property_status " . $criteriaString;
		
		try {
			$stmt = $this->connection->prepare ( $sql );
			if ($stmt->execute ()) {
				$propertyStores = array();
				while ( $row = $stmt->fetch () ) {
					$propertyStores [] = $this->row2propertyStatus ( $row );
				}
				if(count($propertyStores) == 0)
					return NULL;
				if (! is_null ( $propertyStores ))
					return $propertyStores;
				else
					return NULL;
			} else {
				return NULL;
			}
		} catch ( PDOException $exc ) {
			print_r ( $stmt->errorInfo () );
			echo '<br />';
			echo $exc->getTraceAsString ();
			exit ();
		}
	}
	public function estaNoBanco($property_id, $status_id, $store_id) {
		$this->connection->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		$sql = "SELECT * FROM  property_status WHERE property_id LIKE '" . $property_id . "' AND status_id LIKE '" . $status_id . "' AND store_id LIKE '" . $store_id . "'";
		
		try {
			$stmt = $this->connection->prepare ( $sql );
			if ($stmt->execute ()) {
				$row = $stmt->fetch ();
				if (! $row) {
					return false;
				}
				if (! is_null ( $row )) {
					return true;
				} else {
					return NULL;
				}
			}
		} catch ( PDOException $exc ) {
			print_r ( $stmt->errorInfo () );
			echo '<br />';
			echo $exc->getTraceAsString ();
			exit ();
		}
	}
	public function row2propertyStatus($row) {
		$propertyStatus = new PropertyStatus ();
		$propertyDAO = new PropertyDAO ( $this->connection );
		$storeDAO = new StoreDAO ( $this->connection );
		$statusDAO = new StatusDAO ( $this->connection );
		
		$propertyStatus->setStatus ( $statusDAO->finById ( $row ['status_id'] ) );
		$propertyStatus->setProperty ( $propertyDAO->findById ( $row ['property_id'] ) );
		$propertyStatus->setStore ( $storeDAO->findById ( $row ['store_id'] ) );
		$propertyStatus->setPrice ( $row ['price'] );
		
		return $propertyStatus;
	}
	
	/*
	 * Method to delete all serviceProvider of this user
	 */
	public function deleteStatuslInProperty($propertyId) {
		try {
			$sql = "DELETE FROM property_status WHERE id_property = :id_property";
			
			$stmt = $this->connection->prepare ( $sql );
			$stmt->execute ( array (
					":id_property" => $propertyId 
			) );
			return true;
		} catch ( PDOException $exc ) {
			var_dump ( $stmt->errorInfo () );
			echo "<br />";
			echo $exc->getTraceAsString ();
			exit ();
		}
		return false;
	}
}

?>
