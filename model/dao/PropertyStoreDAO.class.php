<?php

/*
 * INCLUDE SECTOR
 */

// include the file of configuration
// require_once './config.php';
// require_once '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_ENTITIES . 'Property.class.php';
require_once PATH_MODEL_ENTITIES . 'Store.class.php';
require_once PATH_MODEL_ENTITIES . 'PropertyStore.class.php';
require_once PATH_MODEL_DAO . 'PropertyDAO.class.php';

/**
 * Description of UserServiceDAO
 *
 * @author Daniel
 */
class PropertyStoreDAO {
		
	private $connection;
	function __construct($connection) {
		$this->connection = $connection;
	}
	public function addStoreinProperty($referencia, Store $store, Property $property) {
		$success = FALSE;
		$this->connection->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		try {
			$sql = "INSERT INTO property_store(id_property, id_store, referencia)
              VALUES (:property, :store, :referencia);";
			
			$stmt = $this->connection->prepare ( $sql );
			
			$params = array (
					"property" => $property->getidUnico (),
					"store" => $store->getId (),
					"referencia" => $referencia 
			);
			
			$success = $stmt->execute ( $params );
		} catch ( PDOException $exc ) {
			print_r ( $stmt->errorInfo () );
			echo "<br />";
			echo $exc->getTraceAsString ();
		}
		return $success;
	}
	public function findById($id) {
		try {
			$sql = "SELECT * FROM property_store WHERE id = :id;";
			$stmt = $this->connection->prepare ( $sql );
			$stmt->execute ( array (
					"id" => $id 
			) );
			$serviceProvider = $this->row2propertyStore ( $stmt->fetch () );
			return $serviceProvider;
		} catch ( PDOException $exc ) {
			echo $exc->getTraceAsString ();
		}
	}
	public function findByCriteria($criteriaString) {
		$this->connection->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		$sql = "SELECT * FROM  property_store " . $criteriaString;
		
		try {
			$stmt = $this->connection->prepare ( $sql );
			if ($stmt->execute ()) {
				while ( $row = $stmt->fetch () ) {
					$propertyStores [] = $this->row2propertyStore ( $row );
				}
				return $propertyStores;
			} else {
				return NULL;
			}
			
		} catch ( PDOException $exc ) {
			print_r ( $stmt->errorInfo () );
			echo '<br />';
			echo $exc->getTraceAsString ();
			exit ();
		}
	}

	public function getVetorPropertyStore($criteriaString){
		echo "";
		$this->connection->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		$sql = "SELECT * FROM  property_store " . $criteriaString;
		
		try {
			$stmt = $this->connection->prepare ( $sql );
			if ($stmt->execute ()) {
				while ( $row = $stmt->fetch () ) {
					$propertyStores [] = $this->montaVetorPropertyStore ( $row );
				}
				return $propertyStores;
			} else {
				return NULL;
			}
			
		} catch ( PDOException $exc ) {
			print_r ( $stmt->errorInfo () );
			echo '<br />';
			echo $exc->getTraceAsString ();
			exit ();
		}
	}
	// }
	public function estaNoBanco($idUnico, $id_store) {
		
		$this->connection->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		$sql = "SELECT * FROM  property_store WHERE id_property LIKE '" . $idUnico . "' AND id_store LIKE '" . $id_store . "'";
		
		try {
			$stmt = $this->connection->prepare ( $sql );
		if ($stmt->execute ()) {
				$row = $stmt->fetch ();
				if(!$row){
					return false;
				}
				if (!is_null ( $row )) {
					return true;
			} else {
				return NULL;
			}
		}
		} catch ( PDOException $exc ) {
			print_r ( $stmt->errorInfo () );
			echo '<br />';
			echo $exc->getTraceAsString ();
			exit ();
		}
	}
	public function row2propertyStore($row) {
		if(!is_null($row ['id_property'])){
			$propertyStore = new PropertyStore ();
			$propertyDAO = new PropertyDAO ( $this->connection );
			$storeDAO = new StoreDAO ( $this->connection );
			
			
			$propertyStore->setStore ( $storeDAO->findById ( $row ['id_store'] ) );
			$propertyStore->setProperty ( $propertyDAO->findByIdUnico( $row ['id_property'] ) );
			$propertyStore->setReferencia($row['referencia']);
			
			return $propertyStore;
		}else{
			return null;
		}
	}

	public function montaVetorPropertyStore($row){
		$vPropertyStore;
		$propertyDAO = new PropertyDAO ( $this->connection );
		if (!is_null($propertyDAO->findByIdUnico( $row ['id_property'] ))){
			$vPropertyStore = $row ['id_property'];
		}
		
		return $vPropertyStore;
	}
	
	/*
	 * Method to delete all serviceProvider of this user
	 */
	public function deleteStoreslInProperty($propertyId) {
		try {
			$sql = "DELETE FROM property_store WHERE id_property = :id_property";
			
			$stmt = $this->connection->prepare ( $sql );
			$stmt->execute ( array (
					":id_property" => $propertyId 
			) );
			return true;
		} catch ( PDOException $exc ) {
			var_dump ( $stmt->errorInfo () );
			echo "<br />";
			echo $exc->getTraceAsString ();
			exit ();
		}
		return false;
	}
}

?>
