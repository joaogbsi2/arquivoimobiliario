<?php

/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once './config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_ENTITIES .'PropertyType.class.php';


class PropertyTypeDAO {

  private $connection;

  public function __construct($connection) {
    $this->connection = $connection;

    $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  }

  public function findById($propertyTypeId) {
    try {
      $sql = "SELECT * FROM  property_type WHERE id = :propertyTypeId";

      $stmt = $this->connection->prepare($sql);
      if ($stmt->execute(array('propertyTypeId' => $propertyTypeId))) {
        
        $propertyType = $this->row2propertyType($stmt->fetch());
        return $propertyType;
      } else {
        return NULL;
      }
    } catch (PDOException $exc) {
      print_r($stmt->errorInfo());
      echo $exc->getTraceAsString();
      exit();
    }
  }

  public function findAll() {
    try {
      $sql = "select * from  property_type";

      $stmt = $this->connection->prepare($sql);


      if ($stmt->execute()) {
        while ($row = $stmt->fetch()) {
          $propertyTypeList[] = $this->row2propertyType($row);
        }
        //$result = count($propertyTypeList);
        return $propertyTypeList;
      } else {
        return NULL;
      }
    } catch (PDOException $exc) {
      print_r($stmt->errorInfo());
      echo $exc->getTraceAsString();
      exit();
    }
  }


  public function row2propertyType($row) {
    $propertyType = new PropertyType();

    $propertyType->setId($row['id']);
    $type = $row['type'];
    $propertyType->setType($type);



    return $propertyType;
  }

}

?>
