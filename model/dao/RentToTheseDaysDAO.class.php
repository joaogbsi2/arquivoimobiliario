<?php

/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once './config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_ENTITIES .'RentToTheseDays.class.php';
require_once PATH_MODEL_DAO .'PropertyDAO.class.php';


/**
 * Description of rentToTheseDaysDAO
 *
 * @author Daniel Fernandes
 */
class RentToTheseDaysDAO {

  private $connection;

  function __construct($connection) {
    $this->connection = $connection;
    $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  }

  public function insert(RentToTheseDays $rentToTheseDays) {
    try {
      $sql = "INSERT INTO  rent_to_these_days(
                domingo,
                segunda,
                terca,
                quarta,
                quinta,
                sexta,
                sabado,
                property)
              VALUES(
                :domingo,
                :segunda,
                :terca,
                :quarta,
                :quinta,
                :sexta,
                :sabado,
                :property);";

      $stmt = $this->connection->prepare($sql);

//      echo "DOMINGO:: ".$rentToTheseDays->getDomingo();
//      exit();
      $params = array(
          ":domingo" => $rentToTheseDays->getDomingo(),
          ":segunda" => $rentToTheseDays->getSegunda(),
          ":terca" => $rentToTheseDays->getTerca(),
          ":quarta" => $rentToTheseDays->getQuarta(),
          ":quinta" => $rentToTheseDays->getQuinta(),
          ":sexta" => $rentToTheseDays->getSexta(),
          ":sabado" => $rentToTheseDays->getSabado(),
          ":property" => $rentToTheseDays->getProperty()->getId()
      );

      $stmt = $this->connection->prepare($sql);
      $stmt->execute($params);
    } catch (PDOException $exc) {
      echo $exc->getTraceAsString();
      print_r($stmt->errorInfo());
      exit();
    }
  }

  private function row2rentToTheseDays($row) {
    $rentToTheseDays = new RentToTheseDays();
    $propertyDAO = new PropertyDAO($this->connection);

    $rentToTheseDays->setId($row['id']);
    $rentToTheseDays->setDomingo($row['domingo']);
    $rentToTheseDays->setSegunda($row['segunda']);
    $rentToTheseDays->setTerca($row['terca']);
    $rentToTheseDays->setQuarta($row['quarta']);
    $rentToTheseDays->setQuinta($row['quinta']);
    $rentToTheseDays->setSexta($row['sexta']);
    $rentToTheseDays->setSabado($row['sabado']);
    $rentToTheseDays->setProperty($propertyDAO->findProperty($row['property']));

    return $rentToTheseDays;
  }
  
  
  
  /*
   * Método que irá pegar os detalhes de imoveis que possuem o status = 3
   * temporada. Ou seja, quero pegar os dias que o imovel estará disponível
   */
  public function getRentToTheseDays($propertyId){
      
    try {
        $sql = "SELECT * FROM rent_to_these_days WHERE property = :id";
        
        $stmt = $this->connection->prepare($sql);
        
        if ($stmt->execute( array("id" => $propertyId))) {
            $flag = false;
            
            while ($row = $stmt->fetch()) {
                $days = $this->row2rentToTheseDays($row);
                $flag = true;
            }
            if ($flag) {
              return $days;
            }
        } else {
            return NULL;
        }
        
    } catch (PDOException $exc) {
        echo $exc->getTraceAsString();
        print_r($stmt->errorInfo());
        exit();
    }
      
  }
  
  
  /*
   * Método que irá deletar todos os registros da tabela rent_to_these_days
   * de um determinado imovel que será passado por parâmetroß
   */
  public function deleteByProperty($propertyId){
    
      try {
        $sql = "DELETE FROM rent_to_these_days WHERE property = :id";
        
        $stmt = $this->connection->prepare($sql);
        
        if ($stmt->execute( array("id" => $propertyId))) {
            
            return TRUE;
            
        } else {
            return FALSE;
        }
        
    } catch (PDOException $exc) {
        echo $exc->getTraceAsString();
        print_r($stmt->errorInfo());
        exit();
    }
  }
  
}

?>
