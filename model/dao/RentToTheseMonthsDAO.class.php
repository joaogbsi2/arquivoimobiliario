<?php

/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once './config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_ENTITIES .'RentToTheseMonths.class.php';
require_once PATH_MODEL_DAO .'PropertyDAO.class.php';


/**
 * Description of rentToTheseMonthsDAO
 *
 * @author Daniel Fernandes
 */
class RentToTheseMonthsDAO {

  private $connection;

  function __construct($connection) {
    $this->connection = $connection;
    $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  }

  public function insert(RentToTheseMonths $rentToTheseMonths) {
    try {
      $sql = "INSERT INTO  rent_to_these_months(
                janeiro,
                fevereiro,
                marco,
                abril,
                maio,
                junho,
                julho,
                agosto,
                setembro,
                outubro,
                novembro,
                dezembro,
                property)
              VALUES (
                :janeiro,
                :fevereiro,
                :marco,
                :abril,
                :maio,
                :junho,
                :julho,
                :agosto,
                :setembro,
                :outubro,
                :novembro,
                :dezembro,
                :property);";

      $stmt = $this->connection->prepare($sql);

      $params = array(
                  ":janeiro" => $rentToTheseMonths->getJaneiro(),
                  ":fevereiro" => $rentToTheseMonths->getFevereiro(),
                  ":marco" => $rentToTheseMonths->getMarco(),
                  ":abril" => $rentToTheseMonths->getAbril(),
                  ":maio" => $rentToTheseMonths->getMaio(),
                  ":junho" => $rentToTheseMonths->getJunho(),
                  ":julho" => $rentToTheseMonths->getJulho(),
                  ":agosto" => $rentToTheseMonths->getAgosto(),
                  ":setembro" => $rentToTheseMonths->getSetembro(),
                  ":outubro" => $rentToTheseMonths->getOutubro(),
                  ":novembro" => $rentToTheseMonths->getNovembro(),
                  ":dezembro" => $rentToTheseMonths->getDezembro(),
                  ":property" => $rentToTheseMonths->getProperty()->getId()
      );

      $stmt = $this->connection->prepare($sql);
      $stmt->execute($params);
    } catch (PDOException $exc) {
      echo $exc->getTraceAsString();
      print_r($stmt->errorInfo());
      exit();
    }
  }

  private function row2rentToTheseDays($row) {
    $rentToTheseMonths = new RentToTheseMonths();
    $propertyDAO = new PropertyDAO($this->connection);

    $rentToTheseMonths->setId($row['id']);
    $rentToTheseMonths->setJaneiro($row['janeiro']);
    $rentToTheseMonths->setFevereiro($row['fevereiro']);
    $rentToTheseMonths->setMarco($row['marco']);
    $rentToTheseMonths->setAbril($row['abril']);
    $rentToTheseMonths->setMaio($row['maio']);
    $rentToTheseMonths->setJunho($row['junho']);
    $rentToTheseMonths->setJulho($row['julho']);
    $rentToTheseMonths->setAgosto($row['agosto']);
    $rentToTheseMonths->setSetembro($row['setembro']);
    $rentToTheseMonths->setOutubro($row['outubro']);
    $rentToTheseMonths->setNovembro($row['novembro']);
    $rentToTheseMonths->setDezembro($row['dezembro']);
    $rentToTheseMonths->setProperty($propertyDAO->findProperty($row['property']));

    return $rentToTheseMonths;
  }
  
  
  
  
  
  
  /*
   * Método que irá pegar os detalhes de imoveis que possuem o status = 3
   * temporada. Ou seja, quero pegar os meses que ele também estará 
   * disponível para locação
   */
  public function getRentToTheseMonths($propertyId){
      
    try {
        $sql = "SELECT * FROM rent_to_these_months WHERE property = :id";
        
        $stmt = $this->connection->prepare($sql);
        
        if ($stmt->execute( array("id" => $propertyId))) {
            $flag = false;
            
            $row = $stmt->fetch();
            $months = $this->row2rentToTheseDays($row);
                
            return $months;
           
        } else {
            return NULL;
        }
        
    } catch (PDOException $exc) {
        echo $exc->getTraceAsString();
        print_r($stmt->errorInfo());
        exit();
    }
  }
  
  
  
  
  /*
   * Método que irá deletar todos os registros da tabela rent_to_these_days
   * de um determinado imovel que será passado por parâmetroß
   */
  public function deleteByProperty($propertyId){
      
      try {
        $sql = "DELETE FROM rent_to_these_months WHERE property = :id";
        
        $stmt = $this->connection->prepare($sql);
        
        if ($stmt->execute( array("id" => $propertyId))) {
            
            return TRUE;
            
        } else {
            return FALSE;
        }
        
    } catch (PDOException $exc) {
        echo $exc->getTraceAsString();
        print_r($stmt->errorInfo());
        exit();
    }
  }

}

?>
