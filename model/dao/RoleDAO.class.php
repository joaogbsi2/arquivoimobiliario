<?php

/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once './config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_ENTITIES .'Role.class.php';

/**
 * Description of RoleDAO
 *
 * @author Daniel
 */
class RoleDAO {

  private $connection;

  public function __construct($connection) {
    $this->connection = $connection;
    $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  }

  public function insert(Role $role) {
    $sql = "INSERT INTO role(rule_name)VALUES(?);";

    $stmt = $this->connection->prepare($sql);

    $stmt->bindParam(1, $role->getRoleName());

    $stmt->execute();
  }

  public function findByCriteria($criteriaString) {
    try {
      $role[] = new Role();
      $sql = "SELECT * FROM role " . $criteriaString;

      $stmt = $this->connection->prepare($sql);
      if ($stmt->execute()) {
        while ($row = $stmt->fetch()) {
          $role[] = row2role($row);
        }
        return $role;
      }
    } catch (PDOException $exc) {
      var_dump($stmt->errorInfo());
      echo "<br /><br />";
      echo $exc->getTraceAsString();
      exit();
    }
    return NULL;
  }

  public function getUniqueByCriteria($criteriaString) {
    try {
      $sql = "SELECT * FROM role " . $criteriaString;

      $stmt = $this->connection->prepare($sql);

      if ($stmt->execute()) {
        $row = $stmt->fetch();
        $role = $this->row2role($row);

        return $role;
      }
    } catch (PDOException $exc) {
      var_dump($stmt->errorInfo());
      echo "<br /><br />";
      echo $exc->getTraceAsString();
      exit();
    }
    return NULL;
  }

  public function row2role($row) {
    $role = new Role();
    $role->setId($row['id']);
    $role->setRoleName($row['role_name']);

    return $role;
  }

}

?>
