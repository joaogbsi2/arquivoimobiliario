<?php

/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once './config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_ENTITIES .'Service.class.php';

/**
 * Description of ServiceDAO
 *
 * @author Daniel
 */
class ServiceDAO {

  private $connection;

  function __construct($connection) {
    $this->connection = $connection;
  }

  public function findById($id) {
    $sql = "SELECT * FROM service WHERE id = :id";
    $stmt = $this->connection->prepare($sql);
    if ($stmt->execute(array("id" => $id))) {
      $row = $stmt->fetch();
      $service = $this->row2service($row);
      return $service;
    } else {
      return NULL;
    }
  }

  public function findAll() {
    try {

      $stmt = $this->connection->prepare("SELECT * FROM  service;");

      if ($stmt->execute()) {
        while ($row = $stmt->fetch()) {
          //Create an array of objects for type Service().
          $services[] = $this->row2service($row);
        }
        return $services;
      } else {
        return NULL;
      }
    } catch (PDOException $exc) {
      echo $exc->getTraceAsString();
    }
  }

  function row2service($row) {
    $service = new Service();

    $service->setId($row['id']);
    $service->setServiceDescription($row['service_description']);

    return $service;
  }

  
  /*
   * Método para buscar todos os serviços que iniciarem com 
   * uma determinada letra, passada por parâmetro
   */
  public function getServicesByCriteria($cryteriaString) {
     
     try {

      $stmt = $this->connection->prepare("SELECT * FROM  service " .$cryteriaString);

      if ($stmt->execute()) {
        while ($row = $stmt->fetch()) {
          //Create an array of objects for type Service().
          $services[] = $this->row2service($row);
        }
        return $services;
      } else {
        return NULL;
      }
    } catch (PDOException $exc) {
      echo $exc->getTraceAsString();
    } 
      
  }
}

?>
