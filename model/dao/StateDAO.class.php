<?php

/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once './config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_ENTITIES .'State.class.php';

/**
 * Description of StateDAO
 *
 * @author Daniel /updated by Edilson Justiniano
 */
class StateDAO {

  private $connection;

  function __construct($connection) {
    $this->connection = $connection;
    $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  }

  public function findAll() {
    try {
      $sql = "SELECT * FROM  state;";

      $stmt = $this->connection->prepare($sql);
      if ($stmt->execute()) {
        while ($row = $stmt->fetch()) {
          $states[] = $this->row2state($row);
        }
        return $states;
      } else {
        return NULL;
      }
    } catch (PDOException $exc) {
      print_r($stmt->errorInfo());
      echo $exc->getTraceAsString();
      exit();
    }
  }

  public function findById($id) {
    try {
      $sql = "SELECT * FROM  state WHERE id = :id";

      $stmt = $this->connection->prepare($sql);

      if ($stmt->execute(array("id" => $id))) {
        $state = $this->row2state($stmt->fetch());
        return $state;
      } else {
        return NULL;
      }
    } catch (PDOException $exc) {
      echo $exc->getTraceAsString();
      exit();
    }
  }
  
  public function insert($state){
  	try {
  		$sql = "INSERT INTO  state (
                name,
  				uf)
              VALUES (
                :name,
  				:uf)";
  		$stmt = $this->connection->prepare($sql);
  
  		$params = array(
  				"name" => $state->getName(),
  				"uf" => strtoupper($state->getUf())
  		);
  
  		$stmt->execute($params);
  	} catch (PDOException $exc) {
  		var_dump($stmt->errorInfo());
  		echo "<br />";
  		echo $exc->getTraceAsString();
  		exit();
  	}
  }
  
  public function delete($id) {
  	try {
  		$sql = "SELECT count(c.id) as qtde FROM  city c WHERE state = :state;";
  
  		$stmt = $this->connection->prepare($sql);
  
  		if ($stmt->execute(array('state' => $id))) {
  			$row = $stmt->fetch();
  			if (($row['qtde']) > 0)
  				return false;
  		}
  	} catch (PDOException $exc) {
  		var_dump($stmt->errorInfo());
  		echo "<br />";
  		echo $exc->getTraceAsString();
  		exit();
  	}
  	try {
  		$sql = "DELETE FROM state WHERE id = :id";
  	
  		$stmt = $this->connection->prepare($sql);
  		if ($stmt->execute(array(":id" => $id)))
  			return true;
  	
  		return false;
  	} catch (PDOException $exc) {
  		var_dump($stmt->errorInfo());
  		echo "<br />";
  		echo $exc->getTraceAsString();
  		exit();
  	}
  }

  public function findByCriteria($stringCriteria) {
    try {
      $sql = "SELECT * FROM  state " . $stringCriteria;

      $stmt = $this->connection->prepare($sql);

      if ($stmt->execute()) {
        while ($row = $stmt->fetch()) {
          $states[] = $this->row2state($row);
        }
        return $states;
      } else {
        return NULL;
      }
    } catch (PDOException $exc) {
      echo $exc->getTraceAsString();
      exit();
    }
  }

  public function findUniqueByCriteria($stringCriteria) {
    try {
      $sql = "SELECT * FROM  state " . $stringCriteria;

      $stmt = $this->connection->prepare($sql);

      if ($stmt->execute()) {
        $state = $this->row2state($stmt->fetch());

        return $state;
      } else {
        return NULL;
      }
    } catch (PDOException $exc) {
      echo $exc->getTraceAsString();
    }
  }

  public function row2state($row) {
    $state = new State();

    $state->setId($row['id']);
    $state->setName($row['name']);
    $state->setUf($row['uf']);

    return $state;
  }
  
  public function update($state){
  	try {
  		$sql = "UPDATE state set name = :name, uf = :uf WHERE id = :id";
  		$stmt = $this->connection->prepare($sql);
  
  		$params = array(
  				"name" => $state->getName(),
  				"uf" => $state->getUf(),
  				"id" => $state->getId()
  		);
  
  		if ($stmt->execute($params))
  			return true;
  
  		return false;
  	} catch (PDOException $exc) {
  		var_dump($stmt->errorInfo());
  		echo "<br />";
  		echo $exc->getTraceAsString();
  		exit();
  	}
  }

}

?>
