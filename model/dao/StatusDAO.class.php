<?php

/**
 * Description of UserDAO
 * This class is responsible about every events performed by users.i.e search of status
 * and store data about Status
 *
 * @author Daniel / updated by Edilson Justiniano
 */


/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once './config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_ENTITIES. 'Status.class.php';


/**
 * Description of StatusDAO
 *
 * @author Daniel /updated by Edilson Justiniano
 */
class StatusDAO {

  private $connection;

  function __construct($connection) {
    $this->connection = $connection;
    $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  }

  public function finById($id) {
  	echo '';
    try {
      $sql = "SELECT * FROM  status WHERE id=" . $id;
      $stmt = $this->connection->prepare($sql);
      if ($stmt->execute()) {
        $status = $this->row2status($stmt->fetch());
        return $status;
      } else {
        return NULL;
      }
    } catch (PDOException $exc) {
      echo $exc->getTraceAsString();
      print_r($stmt->errorInfo());
      exit();
    }
  }

  public function findAll() {
    try {
      $sql = "SELECT * FROM  status";

      $stmt = $this->connection->prepare($sql);
      if ($stmt->execute()) {
        while ($row = $stmt->fetch()) {
          $status[] = $this->row2status($row);
        }
        return $status;
      }
      return NULL;
    } catch (PDOException $exc) {
      print_r($stmt->errorInfo());
      echo "<br />";
      echo $exc->getTraceAsString();
      exit();
    }
  }

  private function row2status($row) {
    $status = new Status();
    $status->setId($row['id']);
    $status->setDescription($row['description']);

    return $status;
  }

}

?>
