<?php

/**
 * Description of UserDAO
 * This class is responsible about every events performed by users.i.e Login,
 * cadastre of clients...
 *
 * @author Edilson Justiniano
 */

/*
 * INCLUDE SECTOR
 */

// include the file of configuration
// require_once '../config.php';
// require_once PATH .'config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_ENTITIES . 'Store.class.php';
class StoreDAO {
	private $connection;
	function __construct($connection) {
		$this->connection = $connection;
		$this->connection->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
	}
	public function insert(Store $store) {
		echo '';
		try {
			$sql = "INSERT INTO  store (
                name,
                mail,
                photo,
      			endereco,
      			telefone,
      			responsavel)
              VALUES (
                :name,
                :mail,
                :photo,
      			:endereco,
      			:telefone,
      			:responsavel)";
			$stmt = $this->connection->prepare ( $sql );
			
			$params = array (
					"name" => $store->getName (),
					"mail" => $store->getMail (),
					"photo" => $store->getPhoto (),
					"endereco" => $store->getEndereco (),
					"telefone" => $store->getTelefone (),
					"responsavel" => $store->getResponsavel () 
			);
			
			$stmt->execute ( $params );
		} catch ( PDOException $exc ) {
			var_dump ( $stmt->errorInfo () );
			echo "<br />";
			echo $exc->getTraceAsString ();
			exit ();
		}
	}
	public function update($store) {
		echo '';
		try {
			
			
			$sql = "UPDATE store SET name = :name,
            mail = :mail,
      		endereco = :endereco,
      		telefone = :telefone,
      		responsavel = :responsavel";
			
			if ($store->getPhoto () != "")
				$sql .= ",photo = :photo";
			
			$sql .= " WHERE id = :id;";
			
			$stmt = $this->connection->prepare ( $sql );
			
			if ($store->getPhoto () != "") {
				if ($stmt->execute ( array (
						':name' => $store->getName (),
						':photo' => $store->getPhoto (),
						':mail' => $store->getMail (),
						':endereco' => $store->getEndereco(),
						':telefone' => $store->getTelefone(),
						':responsavel' => $store->getResponsavel(),
						':id' => $store->getId () 
				) ))
					return true;
				
				return false;
			} else {
				if ($stmt->execute ( array (
						':name' => $store->getName (),
						':mail' => $store->getMail (),
						':endereco' => $store->getEndereco(),
						':telefone' => $store->getTelefone(),
						':responsavel' => $store->getResponsavel(),
						':id' => $store->getId () 
				) )) {
					return true;
				}
				return false;
			}
		} catch ( PDOException $exc ) {
			var_dump ( $stmt->errorInfo () );
			echo "<br />";
			echo $exc->getTraceAsString ();
			exit ();
		}
	}
	public function delete($id) {
		try {
			$sql = "SELECT count(p.id) as qtde FROM  property p WHERE store = :store;";
			
			$stmt = $this->connection->prepare ( $sql );
			
			if ($stmt->execute ( array (
					'store' => $id 
			) )) {
				$row = $stmt->fetch ();
				if (($row ['qtde']) > 0)
					return false;
			}
		} catch ( PDOException $exc ) {
			var_dump ( $stmt->errorInfo () );
			echo "<br />";
			echo $exc->getTraceAsString ();
			exit ();
		}
		
		try {
			$sql = "DELETE FROM store WHERE id = :id";
			
			$stmt = $this->connection->prepare ( $sql );
			if ($stmt->execute ( array (
					":id" => $id 
			) ))
				return true;
			
			return false;
		} catch ( PDOException $exc ) {
			var_dump ( $stmt->errorInfo () );
			echo "<br />";
			echo $exc->getTraceAsString ();
			exit ();
		}
	}
	public function findById($id) {
		try {
			$store = new Store ();
			$sql = "SELECT * FROM  store WHERE id = :id;";
			
			$stmt = $this->connection->prepare ( $sql );
			
			if ($stmt->execute ( array (
					'id' => $id 
			) )) {
				while ( $row = $stmt->fetch () ) {
					$store = $this->row2store ( $row );
				}
				return $store;
			}
			return NULL;
		} catch ( PDOException $exc ) {
			var_dump ( $stmt->errorInfo () );
			echo "<br />";
			echo $exc->getTraceAsString ();
			exit ();
		}
	}
	public function findAll($limit = NULL, $orderBy = NULL) {
		try {
			// $users[] = array();
			$sql = "SELECT * FROM  store";
			
			if (! is_null ( $orderBy ))
				$sql .= " ORDER BY " . $orderBy;
			if (! is_null ( $limit ))
				$sql .= " LIMIT " . $limit . " OFFSET 0";
			
			$sql .= ";";
			
			$stmt = $this->connection->prepare ( $sql );
			if ($stmt->execute ()) {
				$flag = false;
				
				while ( $row = $stmt->fetch () ) {
					$stores [] = $this->row2store ( $row );
					$flag = TRUE;
				}
				
				if ($flag) {
					return $stores;
				}
			} else {
				return NULL;
			}
		} catch ( PDOException $exc ) {
			var_dump ( $stmt->errorInfo () );
			echo "<br />";
			echo $exc->getTraceAsString ();
			exit ();
		}
	}
	public function row2store($row) {
		$store = new Store ();
		
		$store->setId ( $row ['id'] );
		$store->setName ( $row ['name'] );
		$store->setMail ( $row ['mail'] );
		$store->setPhoto ( $row ['photo'] );
		$store->setEndereco ( $row ['endereco'] );
		$store->setTelefone ( $row ['telefone'] );
		$store->setResponsavel ( $row ['responsavel'] );
		
		return $store;
	}
	
	/*
	 * Method that will go search and return a qtde of users
	 * stored on system, I can use this information on page of admin
	 * list users
	 */
	public function getCount() {
		try {
			$sql = "SELECT COUNT(s.id) as qtde FROM store AS s";
			
			$stmt = $this->connection->prepare ( $sql );
			
			if ($stmt->execute ()) {
				
				$row = $stmt->fetch ();
				return $row ['qtde'];
			} else {
				return NULL;
			}
		} catch ( PDOException $exc ) {
			echo $exc->getTraceAsString ();
			print_r ( $stmt->errorInfo () );
			exit ();
		}
	}
	
	/*
	 * Verificar se há propriedade linkada a essa imobiliaria
	 */
	public function haveProperty($idStore) {
		try {
			$sql = "SELECT count(p.id) as qtde FROM property WHERE store = :store;";
			
			$stmt = $this->connection->prepare ( $sql );
			
			if ($stmt->execute ( array (
					'store' => $idStore 
			) )) {
				
				$row = $stmt->fetch ();
				return $row ['qtde'];
			}
			return 0;
		} catch ( PDOException $exc ) {
			var_dump ( $stmt->errorInfo () );
			echo "<br />";
			echo $exc->getTraceAsString ();
			exit ();
		}
	}
}

?>
