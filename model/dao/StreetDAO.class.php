<?php

// require_once '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_ENTITIES . 'Street.class.php';
class StreetDAO {
	private $connection;
	function __construct($connection) {
		$this->connection = $connection;
		$this->connection->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
	}
	public function getAll($limit = NULL, $orderBy = "") {
		try {
			$sql = "SELECT * FROM tb_street";
			
			if ($orderBy != "")
				$sql . " ORDER BY " . $orderBy;
			if ($limit != NULL)
				$sql . " LIMIT " . $orderBy;
			$stmt = $this->connection->prepare ( $sql );
			if ($stmt->execute ()) {
				while ( $row = $stmt->fetch () ) {
					$streets [] = $this->row2street ( $row );
				}
				return $streets;
			}
			return NULL;
		} catch ( PDOException $e ) {
			print_r ( $stmt->errorInfo () );
			echo "<br />";
			echo $e->getTraceAsString ();
			exit ();
		}
	}
	public function findById($id) {
		try {
			$sql = "SELECT * FROM tb_street WHERE id = " . $id;
			$stmt = $this->connection->prepare ( $sql );
			if ($stmt->execute ()) {
				$street = $this->row2street ( $stmt->fetch () );
				return $street;
			} else
				return NULL;
		} catch ( PDOException $e ) {
			echo $e->getTraceAsString ();
			print_r ( $stmt->errorInfo () );
			exit ();
		}
	}
	private function row2street($row) {
		$street = new Street ();
		$cityDAO = new CityDAO ( $this->connection );
		
		$street->setId ( $row ['id'] );
		$street->setRua ( $row ['rua'] );
		$street->setBairro ( $row ['bairro'] );
		$street->setCidade ( $cityDAO->findById ( $row ['cidade'] ) );
		
		return $street;
	}
	public function insert($street) {
		try {
			$sentence = "INSERT INTO tb_street (
					rua,
					bairro,
					cidade)
					VALUES (
					:rua,
					:bairro,
					:cidade)";
			$stmt = $this->connection->prepare ( $sentence );
			
			$params = array (
					"rua" => $street->getRua (),
					"bairro" => $street->getBairro (),
					"cidade" => $street->getCidade ()
			);
			$stmt->execute ( $params );
		} catch ( PDOException $e ) {
			echo '<br />';
			echo $e->getTraceAsString ();
			exit ();
		}
	}
	public function getCount() {
		try {
			$sentence = "SELECT COUNT (s.id) as qtde FROM tb_street AS s";
			
			$stmt = $this->connection->prepare ( $sentence );
			
			if ($stmt->execute ()) {
				$row = $stmt->fetch ();
				return $row ['qtde'];
			}
		} catch ( PDOException $e ) {
			echo $e->getTraceAsString ();
			print_r ( $stmt->errorInfo () );
			exit ();
		}
	}
	public function getStreetByName($streetName, $limit = NULL, $orderBy = "") {
		try {
			$sentence = "SELECT * FROM tb_street WHERE UPPER(rua) LIKE '%" . strtoupper ( $streetName ) . "%'";
			
			if ($orderBy != "")
				$sentence .= " ORDER BY " . $orderBy;
			if ($limit != NULL)
				$sentence .= " LIMIT " . $limit;
			
			$stmt = $this->connection->prepare ( $sentence );
			
			if ($stmt->execute ()) {
				while ( $row = $stmt->fetch () ) {
					$streets [] = $this->row2street ( $row );
				}
				return $streets;
			}
			return NULL;
		} catch ( PDOException $e ) {
			print_r ( $stmt->errorInfo () );
			echo '<br />';
			echo $e->getTraceAsString ();
			exit ();
		}
	}
	public function getStreetByBairroName($bairro, $streetName, $limit = NULL, $orderBy = "") {
		try {
			$sentence = "SELECT * FROM tb_street WHERE rua LIKE '%" . strtoupper ( $streetName ) . "%'AND bairro like '%" . strtoupper ( $bairro ) . "'";
			
			if ($orderBy != "")
				$sentence .= " ORDER BY " . $orderBy;
			if ($limit != NULL)
				$sentence .= " LIMIT " . $limit;
			
			$stmt = $this->connection->prepare ( $sentence );
			
			if ($stmt->execute ()) {
				if ($row = $stmt->fetch ()) {
					$streets [] = $this->row2street ( $row );
					return $streets;
				} else
					return null;
			}
			return NULL;
		} catch ( PDOException $e ) {
			print_r ( $stmt->errorInfo () );
			echo '<br />';
			echo $e->getTraceAsString ();
			exit ();
		}
	}
	public function update($street) {
		try {
			$sentence = "UPDATE tb_street set rua = :rua, bairro = :bairro WHERE id = :id";
			// $sentence = "UPDATE tb_street set rua = :rua, bairro = :bairro, cidade = :cidade";
			
			$stmt = $this->connection->prepare ( $sentence );
			
			$params = array (
					"rua" => $street->getRua (),
					"bairro" => $street->getBairro (),
					"cidade" => $street->getCidade ()->getId () 
			);
			$teste = $stmt->execute ( $params );
			if (! is_null ( $teste )) {
				return true;
			}
		} catch ( PDOException $e ) {
			var_dump ( $stmt->errorInfo () );
			echo '<br />';
			echo $e->getTraceAsString ();
			exit ();
		}
	}
}