<?php

/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once './config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_ENTITIES .'User.class.php';
require_once PATH_MODEL_ENTITIES .'Service.class.php';
require_once PATH_MODEL_ENTITIES .'UserService.class.php';



/**
 * Description of UserServiceDAO
 *
 * @author Daniel
 */
class UserServiceDAO {

  private $connection;

  function __construct($connection) {
    $this->connection = $connection;
  }

  public function addServiceProvider(Service $service, User $user) {
    $success = FALSE;
    $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    try {
      $sql = "INSERT INTO user_service(id_user, id_service)
              VALUES (:user, :service);";

      $stmt = $this->connection->prepare($sql);

      $params = array(
          "user" => $user->getId(),
          "service" => $service->getId()
      );
      
      $success = $stmt->execute($params);
    } catch (PDOException $exc) {
      print_r($stmt->errorInfo());
      echo "<br />";
      echo $exc->getTraceAsString();
    }
    return $success;
  }

  public function findById($id) {
    try {
      $sql = "SELECT * FROM user_service WHERE id = :id;";
      $stmt = $this->connection->prepare($sql);
      $stmt->execute(array("id" => $id));
      $serviceProvider = $this->row2userService($stmt->fetch());
      return $serviceProvider;
    } catch (PDOException $exc) {
      echo $exc->getTraceAsString();
    }
  }

  public function findByCriteria($criteriaString) {
    $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "SELECT * FROM  user_service " . $criteriaString;
    
    try {
      $stmt = $this->connection->prepare($sql);
      if ($stmt->execute()) {
        while ($row = $stmt->fetch()) {
          $userService[] = $this->row2userService($row);
        }
        return $userService;
      } else {
        return NULL;
      }
    } catch (PDOException $exc) {
      print_r($stmt->errorInfo());
      echo '<br />';
      echo $exc->getTraceAsString();
      exit();
    }
  }

  public function row2userService($row) {
    $userService = new UserService();
    $userDAO = new UserDAO($this->connection);
    $serviceDAO = new ServiceDAO($this->connection);

    $userService->setService($serviceDAO->findById($row['id_service']));
    $userService->setUser($userDAO->findById($row['id_user']));

    return $userService;
  }

  
  
  /*
   * Method to delete all serviceProvider of this user
   */
  public function deleteServiceProvider($userId){

    try {
      $sql = "DELETE FROM user_service WHERE id_user = :id_user";

      $stmt = $this->connection->prepare($sql);
      $stmt->execute(array(":id_user" => $userId));
      return true;
    } catch (PDOException $exc) {
      var_dump($stmt->errorInfo());
      echo "<br />";
      echo $exc->getTraceAsString();
      exit();
    }
    return false;
  }
  
}

?>
