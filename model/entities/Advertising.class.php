<?php

/*
 * date of creating 2013-07-29
 * 
 * @author Edilson Justiniano
 */

//include the file of configuration
#require_once './config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

class Advertising{
    

    private $id;
    private $position;
    private $photo;
    
    public function getId(){
        return $this->id;
    }
    
    public function setId($id){
        $this->id= $id;
    }
    
    public function getPosition(){
        return $this->position;
    }
    
    public function setPosition($position){
        $this->position= $position;
    }
    
    public function getPhoto(){
        return $this->photo;
    }
    
    public function setPhoto($photo){
        $this->photo= $photo;
    }
    
}

?>
