<?php

/*
 * INCLUDE SECTOR
 */

// include the file of configuration
// require_once './config.php';
// require_once '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_ENTITIES . 'State.class.php';

/**
 * Description of City
 *
 * @author Daniel
 */
class City {
	private $id;
	private $name;
	private $state;
	
	public function getId() {
		return $this->id;
	}
	public function setId($id) {
		$this->id = $id;
	}
	public function getName() {
		return $this->name;
	}
	public function setName($name) {
		$this->name = $name;
	}
	public function getState() {
		return $this->state;
	}
	public function setState(State $state) {
		$this->state = $state;
	}
	public function __toString() {
		return $this->name;
	}
	public function to_array() {
		$array = array (
				"id" => $this->getId (),
				"name" => $this->getName (),
				"state" => $this->getState ()->to_array () 
		);
		return $array;
	}
}

?>
