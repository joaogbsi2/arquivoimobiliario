<?php

/*
 * @author Edilson Justiniano
 * 
 * 
 */


class Decoracao {

  private $id;
  private $title;
  private $subTitle;
  private $description;
  private $photo;
  
  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getTitle() {
    return $this->title;
  }

  public function setTitle($title) {
    $this->title = $title;
  }
  
  public function getSubTitle() {
    return $this->subTitle;
  }

  public function setSubTitle($subTitle) {
    $this->subTitle = $subTitle;
  }
  
  public function getDescription() {
    return $this->description;
  }

  public function setDescription($description) {
    $this->description = $description;
  }
  
  public function getPhoto() {
    return $this->photo;
  }

  public function setPhoto($photo) {
    $this->photo = $photo;
  }
  
}
?>
