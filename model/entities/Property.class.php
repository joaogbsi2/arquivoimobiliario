<?php

/*
 * INCLUDE SECTOR
 */

// include the file of configuration
// require_once './config.php';
// require_once '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_ENTITIES . 'City.class.php';
require_once PATH_MODEL_ENTITIES . 'Status.class.php';
require_once PATH_MODEL_ENTITIES . 'PropertyType.class.php';
require_once PATH_MODEL_ENTITIES . 'Store.class.php';

/**
 * Description of Property
 *
 * @author Daniel /updated by Edilson Justiniano
 */
class Property {
	private $id;
	private $measure;
	private $cep;
	private $street;
	private $identifier;
	private $neighborhood;
	private $latitude;
	private $longitude;
	private $city;
	private $status;
	private $type;
	private $description;
	private $price;
	private $qtdRooms;
	private $contactEmail;
	private $contactPhone;
	private $active;
	private $idUnico;
	
	/*
	 * add by Edilson Justiniano the bellow attribute
	 */
	private $exclusiveStore;
	
	/*
	 * campos adicionados na 2ª versão do sistema para
	 * completar o cadastro de imóveis
	 */
	// private $construcao;
	private $posicao;
	// private $nivel;
	private $garagem;
	// private $face;
	private $pavimento;
	private $areaConstruida;
	private $edicula;
	private $areaTotal;
	// private $terreno;
	private $comprimento;
	private $largura;
	private $valorCondominio;
	private $valorIPTU;
	private $numReferencia;
	public function getId() {
		return $this->id;
	}
	public function setId($id) {
		$this->id = $id;
	}
	public function getMeasure() {
		return $this->measure;
	}
	public function setMeasure($measure) {
		$this->measure = $measure;
	}
	public function getCep() {
		return $this->cep;
	}
	public function setCep($cep) {
		$this->cep = $cep;
	}
	public function getStreet() {
		return $this->street;
	}
	public function setStreet($street) {
		$this->street = $street;
	}
	public function getIdentifier() {
		return $this->identifier;
	}
	public function setIdentifier($identifier) {
		$this->identifier = $identifier;
	}
	public function getNeighborhood() {
		return $this->neighborhood;
	}
	public function setNeighborhood($neighborhood) {
		$this->neighborhood = $neighborhood;
	}
	public function getLatitude() {
		return $this->latitude;
	}
	public function setLatitude($latitude) {
		$this->latitude = $latitude;
	}
	public function getLongitude() {
		return $this->longitude;
	}
	public function setLongitude($longitude) {
		$this->longitude = $longitude;
	}
	public function getCity() {
		return $this->city;
	}
	public function setCity(City $city) {
		$this->city = $city;
	}
	public function getStatus() {
		return $this->status;
	}
	public function setStatus(Status $status) {
		$this->status = $status;
	}
	public function getDescription() {
		return $this->description;
	}
	public function setDescription($description) {
		$this->description = $description;
	}
	public function getType() {
		return $this->type;
	}
	public function setType(PropertyType $propertyType) {
		$this->type = $propertyType;
	}
	public function getPrice() {
		return $this->price;
	}
	public function setPrice($price) {
		$this->price = $price;
	}
	public function getQtdRooms() {
		return $this->qtdRooms;
	}
	public function setQtdRooms($qtdRooms) {
		$this->qtdRooms = $qtdRooms;
	}
	public function getContactEmail() {
		return $this->contactEmail;
	}
	public function setContactEmail($contactEmail) {
		$this->contactEmail = $contactEmail;
	}
	public function getContactPhone() {
		return $this->contactPhone;
	}
	public function setContactPhone($contactPhone) {
		$this->contactPhone = $contactPhone;
	}
	public function getActive() {
		return $this->active;
	}
	public function setActive($active) {
		$this->active = $active;
	}
	
	/**
	 *
	 * @return Add by Edilson Justiniano this attribute Store
	 */
	public function getExclusiveStore() {
		return $this->exclusiveStore;
	}
	public function setExclusiveStore($store) {
		$this->exclusiveStore = $store;
	}
	
	/*
	 * campos adicionados para completar o formulário de
	 * cadastro de imóvel
	 */
	// public function getConstrucao() {
	// return $this->construcao;
	// }
	//
	// public function setConstrucao($contrucao) {
	// $this->construcao = $contrucao;
	// }
	public function getPosicao() {
		return $this->posicao;
	}
	public function setPosicao($posicao) {
		$this->posicao = $posicao;
	}
	
	// public function getNivel() {
	// return $this->nivel;
	// }
	//
	// public function setNivel($nivel) {
	// $this->nivel = $nivel;
	// }
	public function getGaragem() {
		return $this->garagem;
	}
	public function setGaragem($garagem) {
		$this->garagem = $garagem;
	}
	
	// public function getFace() {
	// return $this->face;
	// }
	//
	// public function setFace($face) {
	// $this->face = $face;
	// }
	public function getPavimento() {
		return $this->pavimento;
	}
	public function setPavimento($pavimento) {
		$this->pavimento = $pavimento;
	}
	public function getAreaConstruida() {
		return $this->areaConstruida;
	}
	public function setAreaConstruida($areaConstruida) {
		$this->areaConstruida = $areaConstruida;
	}
	public function getEdicula() {
		return $this->edicula;
	}
	public function setEdicula($edicula) {
		$this->edicula = $edicula;
	}
	public function getAreaTotal() {
		return $this->areaTotal;
	}
	public function setAreaTotal($areaTotal) {
		$this->areaTotal = $areaTotal;
	}
	
	// public function getTerreno() {
	// return $this->terreno;
	// }
	//
	// public function setTerreno($terreno) {
	// $this->terreno = $terreno;
	// }
	public function getComprimento() {
		return $this->comprimento;
	}
	public function setComprimento($comprimento) {
		$this->comprimento = $comprimento;
	}
	public function getLargura() {
		return $this->largura;
	}
	public function setLargura($largura) {
		$this->largura = $largura;
	}
	public function getValorCondominio() {
		return $this->valorCondominio;
	}
	public function setValorCondominio($valor) {
		$this->valorCondominio = $valor;
	}
	public function getValorIPTU() {
		return $this->valorIPTU;
	}
	public function setValorIPTU($valor) {
		$this->valorIPTU = $valor;
	}
	public function getidUnico() {
		return $this->idUnico;
	}
	public function setidUnico($idUnico) {
		$this->idUnico = $idUnico;
	}
	 public function getNumReferencia() {
	         return $this->numReferencia;
	}
	
	 public function setNumReferencia($numReferencia) {
	         $this->numReferencia = $numReferencia;
	}
	
	
	public function to_array() {
		$array = array (
				"id" => $this->getId (),
				"cep" => $this->getCep (),
				"city" => $this->getCity ()->to_array (),
				"description" => $this->getDescription (),
				"identifier" => $this->getIdentifier (),
				"latitude" => $this->getLatitude (),
				"longitude" => $this->getLongitude (),
				"measure" => $this->getMeasure (),
				"neighborhood" => $this->getNeighborhood (),
				"price" => $this->getPrice (),
				"status" => $this->getStatus ()->to_array (),
				"street" => $this->getStreet (),
				"type" => $this->getType ()->to_array (),
				"description" => $this->getDescription (),
				"price" => $this->getPrice (),
				"qtdRooms" => $this->getQtdRooms (),
				"contactEmail" => $this->getContactEmail (),
				"contactPhone" => $this->getContactPhone (),
				"active" => $this->getActive (),
				"exclusiveStore" => $this->getExclusiveStore (),
				// "construcao"=>$this->getConstrucao(),
				"posicao" => $this->getPosicao (),
				// "nivel"=>$this->getNivel(),
				"garagem" => $this->getGaragem (),
				// "face"=>$this->getFace(),
				"pavimento" => $this->getPavimento (),
				"areaConstruida" => $this->getAreaConstruida (),
				"edicula" => $this->getEdicula (),
				"areaTotal" => $this->getAreaTotal (),
				// "terreno"=>$this->getTerreno(),
				"comprimento" => $this->getComprimento (),
				"largura" => $this->getLargura (),
				"valorCondominio" => $this->getValorCondominio (),
				"valorIPTU" => $this->getValorIPTU (),
				"idUnico" => $this->getidUnico () 
				
		);
		return $array;
	}
}
?>
