<?php

/**
 * Description of UserService
 *
 * @author Daniel
 */
class PropertyDetail {
  private $property;
  private $detail;
  
  public function getProperty() {
    return $this->property;
  }

  public function setProperty(Property $property) {
    $this->property = $property;
  }

  public function getDetail() {
    return $this->detail;
  }

  public function setDetail(Detail $detail) {
    $this->detail = $detail;
  }
  
}

?>