<?php

/**
 * Description of UserService
 *
 * @author Daniel
 */
class PropertyNear {
  private $property;
  private $near;
  
  public function getProperty() {
    return $this->property;
  }

  public function setProperty(Property $property) {
    $this->property = $property;
  }

  public function getNear() {
    return $this->near;
  }

  public function setNear(Near $near) {
    $this->near = $near;
  }
  
}

?>