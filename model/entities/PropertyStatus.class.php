<?php

/**
 * Description of UserService
 *
 * @author Edilson Justiniano
 */
class PropertyStatus {
  private $property;
  private $status;
  private $price;
  private $store;
  
  public function getProperty() {
    return $this->property;
  }

  public function setProperty(Property $property) {
    $this->property = $property;
  }

  public function getStatus() {
    return $this->status;
  }

  public function setStatus (Status $status) {
    $this->status = $status;
  }
  
   public function getPrice() {
           return $this->price;
  }
  
   public function setPrice($price) {
           $this->price = $price;
  }
  
  public function getStore() {
           return $this->store;
  }
  
   public function setStore($store) {
           $this->store = $store;
  }
   
}

?>