<?php

/**
 * Description of UserService
 *
 * @author Edilson Justiniano
 */
class PropertyStore {
  private $property;
  private $store;
  private $referencia;
  
  public function getProperty() {
    return $this->property;
  }

  public function setProperty(Property $property) {
    $this->property = $property;
  }

  public function getStore() {
    return $this->store;
  }

  public function setStore(Store $store) {
    $this->store = $store;
  }
  
   public function getReferencia() {
           return $this->referencia;
  }
  
   public function setReferencia($referencia) {
           $this->referencia = $referencia;
  }
  
  
  
}

?>