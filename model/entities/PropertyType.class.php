<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * @author: joao
 */

class PropertyType{
    private $id;
    private $type;
    
    public function getId(){
        return $this->id;
    }
    public function setId($id){
        $this->id = $id;
    }
    
    public function getType(){
        return $this->type;
    }
    
    public function setType($type){
        $this->type = $type;
    }
    
    public function __toString() {
        return $this->type;
    }
    
    
    public function to_array(){
        $array = array("id"=>$this->getId(),
            "type"=>$this->getType()
        );
        return $array;
    }
}
?>
