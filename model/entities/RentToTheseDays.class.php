<?php

/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once './config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_ENTITIES. 'Property.class.php';

/**
 * Description of rentToTheseDays
 *
 * @author Daniel Fernandes / updated by Edilson Justiniano
 */
class RentToTheseDays {

  private $id;
  private $domingo;
  private $segunda;
  private $terca;
  private $quarta;
  private $quinta;
  private $sexta;
  private $sabado;
  private $property;

  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getDomingo() {
    return $this->domingo;
  }

  public function setDomingo($domingo) {
    $this->domingo = $domingo;
  }

  public function getSegunda() {
    return $this->segunda;
  }

  public function setSegunda($segunda) {
    $this->segunda = $segunda;
  }

  public function getTerca() {
    return $this->terca;
  }

  public function setTerca($terca) {
    $this->terca = $terca;
  }

  public function getQuarta() {
    return $this->quarta;
  }

  public function setQuarta($quarta) {
    $this->quarta = $quarta;
  }

  public function getQuinta() {
    return $this->quinta;
  }

  public function setQuinta($quinta) {
    $this->quinta = $quinta;
  }

  public function getSexta() {
    return $this->sexta;
  }

  public function setSexta($sexta) {
    $this->sexta = $sexta;
  }

  public function getSabado() {
    return $this->sabado;
  }

  public function setSabado($sabado) {
    $this->sabado = $sabado;
  }

  public function getProperty() {
    return $this->property;
  }

  public function setProperty(Property $property) {
    $this->property = $property;
  }

}

?>
