<?php

/*
 * INCLUDE SECTOR
 */

//include the file of configuration
#require_once './config.php';
// require_once  '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_MODEL_ENTITIES. 'Property.class.php';

/**
 * Description of rentToTheseMonths
 *
 * @author Daniel Fernandes /updated by Edilson Justiniano
 */
class RentToTheseMonths {

  private $id;
  private $janeiro;
  private $fevereiro;
  private $marco;
  private $abril;
  private $maio;
  private $junho;
  private $julho;
  private $agosto;
  private $setembro;
  private $outubro;
  private $novembro;
  private $dezembro;
  private $property;

  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getJaneiro() {
    return $this->janeiro;
  }

  public function setJaneiro($janeiro) {
    $this->janeiro = $janeiro;
  }

  public function getFevereiro() {
    return $this->fevereiro;
  }

  public function setFevereiro($fevereiro) {
    $this->fevereiro = $fevereiro;
  }

  public function getMarco() {
    return $this->marco;
  }

  public function setMarco($marco) {
    $this->marco = $marco;
  }

  public function getAbril() {
    return $this->abril;
  }

  public function setAbril($abril) {
    $this->abril = $abril;
  }

  public function getMaio() {
    return $this->maio;
  }

  public function setMaio($maio) {
    $this->maio = $maio;
  }

  public function getJunho() {
    return $this->junho;
  }

  public function setJunho($junho) {
    $this->junho = $junho;
  }

  public function getJulho() {
    return $this->julho;
  }

  public function setJulho($julho) {
    $this->julho = $julho;
  }

  public function getAgosto() {
    return $this->agosto;
  }

  public function setAgosto($agosto) {
    $this->agosto = $agosto;
  }

  public function getSetembro() {
    return $this->setembro;
  }

  public function setSetembro($setembro) {
    $this->setembro = $setembro;
  }

  public function getOutubro() {
    return $this->outubro;
  }

  public function setOutubro($outubro) {
    $this->outubro = $outubro;
  }

  public function getNovembro() {
    return $this->novembro;
  }

  public function setNovembro($novembro) {
    $this->novembro = $novembro;
  }

  public function getDezembro() {
    return $this->dezembro;
  }

  public function setDezembro($dezembro) {
    $this->dezembro = $dezembro;
  }

  public function getProperty() {
    return $this->property;
  }

  public function setProperty(Property $property) {
    $this->property = $property;
  }

}

?>
