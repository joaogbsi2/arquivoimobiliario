<?php

/*
 * @author Daniel / updated by Edilson Justiniano
 * 
 * 
 */



class Role {

  private $id;
  private $roleName;
  
  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getRoleName() {
    return $this->roleName;
  }

  public function setRoleName($roleName) {
    $this->roleName = $roleName;
  }
  
}

?>