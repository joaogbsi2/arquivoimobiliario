<?php

class Service {
  private $id;
  private $serviceDescription;
  
  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getServiceDescription() {
    return $this->serviceDescription;
  }

  public function setServiceDescription($serviceDescription) {
    $this->serviceDescription = $serviceDescription;
  }

}

?>
