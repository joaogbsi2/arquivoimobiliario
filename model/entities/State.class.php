<?php
/**
 * Description of State
 *
 * @author Daniel
 */
class State {
  private $id;
  private $name;
  private $uf;
  
  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getName() {
    return $this->name;
  }

  public function setName($name) {
    $this->name = $name;
  }

  public function getUf() {
    return $this->uf;
  }

  public function setUf($uf) {
    $this->uf = $uf;
  }
  
  public function to_array(){
      $array = array("id" => $this->getId(),
          "name"=>$this->getName(),
          "uf"=>$this->getUf());
      return $array;
  }
}

?>
