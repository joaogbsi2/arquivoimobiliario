<?php

/**
 * Description of Status
 *
 * @author Daniel / updated by Edilson Justiniano
 */

class Status {
  private $id;
  private $description;
  
  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getDescription() {
    return $this->description;
  }

  public function setDescription($description) {
    $this->description = $description;
  }

  public function __toString() {
    return $this->getDescription();
  }

  public function to_array(){
      $array = array("id"=>$this->getId(),
          "description"=>$this->getDescription()
      );
      return $array;
  }
}

?>
