<?php

/*
 * @author Edilson Justiniano
 * 
 * 
 */


class Store {

  private $id;
  private $name;
  private $photo;
  private $mail;
  private $endereco;
  private $telefone;
  private $responsavel;
  
  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getName() {
    return $this->name;
  }

  public function setName($name) {
    $this->name = $name;
  }
  
  public function getPhoto() {
    return $this->photo;
  }

  public function setPhoto($photo) {
    $this->photo = $photo;
  }
  
   public function getMail() {
    return $this->mail;
  }

  public function setMail($mail) {
    $this->mail = $mail;
  }
  
   public function getEndereco() {
           return $this->endereco;
  }
  
   public function setEndereco($endereco) {
           $this->endereco = $endereco;
  }
  
   public function getTelefone() {
           return $this->telefone;
  }
  
   public function setTelefone($telefone) {
           $this->telefone = $telefone;
  }
  
   public function getResponsavel() {
           return $this->responsavel;
  }
  
   public function setResponsavel($responsavel) {
           $this->responsavel = $responsavel;
  }
  
  
  
  public function to_array(){
        $array = array("id"=>$this->getId(),
            "name"=>$this->getName(),
            "description"=>$this->getDescription(),
            "photo"=>$this->getPhoto(),
            "mail"=>$this->getMail(),
        	"endereco"=>$this->getEndereco(),
        	"telefone"=>$this->getTelefone(),
        	"responsavel"=>$this->getResponsavel()	
        );
        return $array;
    }
}
?>
