<?php

/**
 * Description of UserService
 *
 * @author Daniel
 */
class UserService {
  private $user;
  private $service;
  
  public function getUser() {
    return $this->user;
  }

  public function setUser(User $user) {
    $this->user = $user;
  }

  public function getService() {
    return $this->service;
  }

  public function setService(Service $service) {
    $this->service = $service;
  }
  
}

?>
