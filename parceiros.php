e<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="images/arquiLogo.png">
	<title>Arquivo Imobiliário</title>
	<link href="style.css" rel="stylesheet" type="text/css" />

</head>

<body>

	<!-- GERAL -->
	<div id="geral">


		<!-- TOPO -->
		<div id="topo">
			<div id="topoCont">
				<div id="logo">
					<a href="index.php"><img src="images/logotipo.png" /></a>
				</div>

				<div id="cxUser">
					<table width="130">
						<tr>
							<td width="30" align="justify" class="texto">Login</td>
							<td width="80"><input name="user" type="text" class="campo1"
								id="user" /></td>
							<td width="10"></td>
						</tr>
						<tr>
							<td align="right" class="texto">Senha</td>
							<td><input name="senha" type="text" class="campo1" id="senha" /></td>
							<td><a href="#"><img src="images/bt_logarHome.png" /></a></td>
						</tr>
					</table>
				</div>

				<div id="cxSocial">
					<div id="btIcons">
						<a href="#"><img src="images/bt_home.png" /></a>
					</div>
					<div id="btIcons">
						<a href="#"><img src="images/bt_face.png" /></a>
					</div>
					<div id="btIcons">
						<a href="#"><img src="images/bt_pesquisa.png" /></a>
					</div>
					<div id="btIcons">
						<a href="#"><img src="images/bt_email.png" /></a>
					</div>
				</div>
			</div>
		</div>
		<!-- /TOPO -->


		<!-- MAIN CONTEUDO -->
		<div id="mainConteudo">

			<!-- CONTEUDO -->
			<div id="conteudo">

				<!-- CONTEUDO COL 1 -->
				<div id="home_COL1">

					<div id="menuBus">
						<div id="label">
							<table width="275">
								<tr>
									<td colspan="3" align="center"><input type="radio" name="radio"
										id="radio" value="radio" /> Comprar <input type="radio"
										name="radio" id="radio2" value="radio" /> Alugar <input
										type="radio" name="radio" id="radio2" value="radio" />
										Temporada</td>
								</tr>
								<tr>
									<td colspan="3" height="10"></td>
								</tr>
								<tr>
									<td width="93" align="right" class="texto">Tipo:</td>
									<td width="10"></td>
									<td width="150"><select name="tipo" class="campo" id
										title="cidade">
											<option value="label" selected="selected">Label</option>
									</select></td>
								</tr>
								<tr>
									<td colspan="3" height="15"></td>
								</tr>
								<tr>
									<td align="right" class="texto">Estado:</td>
									<td></td>
									<td><select name="estado" class="campo" id title="bairro">
											<option value="label">Label</option>
									</select></td>
								</tr>
								<tr>
									<td colspan="3" height="1"></td>
								</tr>
								<tr>
									<td align="right" class="texto">Cidade:</td>
									<td></td>
									<td><select name="cidade" class="campo" id title="bairro">
											<option value="label">Label</option>
									</select></td>
								</tr>
								<tr>
									<td colspan="3" height="1"></td>
								</tr>
								<tr>
									<td align="right" class="texto">Localidade:</td>
									<td></td>
									<td><select name="localidade" class="campo" id title="bairro">
											<option value="label">Label</option>
									</select></td>
								</tr>
								<tr>
									<td colspan="3" height="15"></td>
								</tr>
								<tr>
									<td align="right" class="texto">Valor Mín:</td>
									<td></td>
									<td><select name="val_min" class="campo" id title="bairro">
											<option value="label">Label</option>
									</select></td>
								</tr>
								<tr>
									<td colspan="3" height="1"></td>
								</tr>
								<tr>
									<td align="right" class="texto">Valor Máx:</td>
									<td></td>
									<td><select name="val_max" class="campo" id title="bairro">
											<option value="label">Label</option>
									</select></td>
								</tr>
								<tr>
									<td colspan="3" height="1"></td>
								</tr>
								<tr>
									<td align="right" class="texto">Quartos:</td>
									<td></td>
									<td><select name="quartos" class="campo" id title="bairro">
											<option value="label">Label</option>
									</select></td>
								</tr>
								<tr>
									<td colspan="3" height="15"></td>
								</tr>
								<tr>
									<td colspan="2" align="right" class="texto2">Entre com código:</td>
									<td><input name="codigo" type="text" class="campo2" id="codigo" /></td>
								</tr>
								<tr>
									<td colspan="3" height="1"></td>
								</tr>
								<tr>
									<td align="right" class="texto">&nbsp;</td>
									<td></td>
									<td align="right"><a href="imoveisbusca.php"><img
											src="images/bt_send.png" name="bt_send" id="bt_send" /></a></td>
								</tr>
							</table>
						</div>
					</div>

					<div id="menuDown">
						<div id="cx1">
							<a href="#"><img src="images/bt_cadImov.png" /></a>
						</div>
						<div id="cx2">
							<a href="#"><img src="images/bt_crieUser.png" /></a>
						</div>
					</div>

					<div id="cxParceiros">
						<div id="cxTit">
							<p>Parceiros</p>
						</div>
						<div id="cxLog">
							<img src="images/logParc.png" />
						</div>
					</div>

				</div>
				<!-- /CONTEUDO COL 1 -->

				<!-- CONTEUDO COL 2 -->
				<div id="home_COL2">
					<!-- Menu -->
					<div id="menu">
						<div id="btmenu1">
							<a href="quemsomos.php"><img src="images/btmenu1.png"></a>
						</div>
						<div id="btmenu2">
							<a href="parceiros.php"><img src="images/btmenu2.png"></a>
						</div>
						<div id="btmenu3">
							<a href="#"><img src="images/btmenu3.png"></a>
						</div>
						<div id="btmenu4">
							<a href="#"><img src="images/btmenu4.png"></a>
						</div>
						<div id="btmenu5">
							<a href="servicos.php"><img src="images/btmenu5.png"></a>
						</div>
					</div>
					<!-- /Menu -->

					<!-- Conteudo Serviços -->
					<div id="contParc">
						<div id="Tit">Parceiros</div>

						<div id="cxDesc">
							<div id="foto">
								<img src="images/imgLgpar.jpg" />
							</div>
							<div id="text">Lorem ipsum dolor sit amet, consectetur adipiscing
								elit. Morbi sit amet tristique ligula. Nullam sed lobortis erat.
								Nam tempor adipiscing pulvinar.</div>
						</div>

						<div id="cxDesc">
							<div id="foto">
								<img src="images/imgLgpar.jpg" />
							</div>
							<div id="text">Lorem ipsum dolor sit amet, consectetur adipiscing
								elit. Morbi sit amet tristique ligula. Nullam sed lobortis erat.
								Nam tempor adipiscing pulvinar.</div>
						</div>

						<div id="cxDesc">
							<div id="foto">
								<img src="images/imgLgpar.jpg" />
							</div>
							<div id="text">Lorem ipsum dolor sit amet, consectetur adipiscing
								elit. Morbi sit amet tristique ligula. Nullam sed lobortis erat.
								Nam tempor adipiscing pulvinar.</div>
						</div>

						<div id="cxDesc">
							<div id="foto">
								<img src="images/imgLgpar.jpg" />
							</div>
							<div id="text">Lorem ipsum dolor sit amet, consectetur adipiscing
								elit. Morbi sit amet tristique ligula. Nullam sed lobortis erat.
								Nam tempor adipiscing pulvinar.</div>
						</div>

						<div id="cxDesc">
							<div id="foto">
								<img src="images/imgLgpar.jpg" />
							</div>
							<div id="text">Lorem ipsum dolor sit amet, consectetur adipiscing
								elit. Morbi sit amet tristique ligula. Nullam sed lobortis erat.
								Nam tempor adipiscing pulvinar.</div>
						</div>

						<div id="cxDesc">
							<div id="foto">
								<img src="images/imgLgpar.jpg" />
							</div>
							<div id="text">Lorem ipsum dolor sit amet, consectetur adipiscing
								elit. Morbi sit amet tristique ligula. Nullam sed lobortis erat.
								Nam tempor adipiscing pulvinar.</div>
						</div>


					</div>
					<!-- /Conteudo Serviços -->

				</div>
				<!-- /CONTEUDO COL 2 -->


			</div>
			<!-- CONTEUDO -->

		</div>
		<!-- /MAIN CONTEUDO -->


		<!-- FOOTER -->
		<div id="footer">
			<div id="conteudoFooter">
				<div id="ass">2013 © Arquivo Imobiliário. Todos os direitos
					reservados.</div>
			</div>
		</div>
		<!-- /FOOTER -->



	</div>
	<!-- /GERAL -->


</body>

</html>