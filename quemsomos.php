﻿<?php


$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}
require_once PATH_ACTION . 'index.action.php';
require_once PATH_CONTROLLER . 'StatusController.class.php';
require_once PATH_ACTION . 'adminServices.action.php';
require_once PATH_MODEL_ENTITIES . 'Advertising.class.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="images/arquiLogo.png">
	<title>Arquivo Imobiliário</title>
	<link href="estilo.css" rel="stylesheet" type="text/css" />

	<!-- jquery ui theme and select multiple theme -->
	<link rel="stylesheet" type="text/css"
		href="css/jquery.multiselect.css" />
	<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
	<!-- Style of Advertisings -->
	<style type="text/css">
<!--
.advertising {
	width: 150px;
	height: 95px;
	margin-left: 3px;
}
-->
</style>

	<!-- Modernizr do Slider em jquery das propagandas principais -->
	<script src="js/modernizr.js"></script>
	<!-- CSS do slider em jquery -->
	<link rel="stylesheet" href="css/flexslider.css" type="text/css"
		media="screen" />

</head>

<body>

	<!-- JS Content -->
	<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>

	<!-- includes for multiple select -->
	<script type="text/javascript" src="js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/jquery.multiselect.min.js"></script>
	<script type="text/javascript" src="js/jquery.validate.min.js"></script>
	<!-- end multiple select include -->

	<!-- includes for jquery price -->
	<script type="text/javascript" src="js/jquery.price_format.1.7.min.js"></script>
	<!-- end jquery price include -->
    
    

	<!-- Início Geral-->
	<div id="geral">
		<header id="topo">
			<?php
			require_once PATH_INCLUDES_USERS . 'header.php';
			?>
		</header>
		<!-- Inicio Main Conteudo -->
		<div id="mainConteudo">

			<!-- Inicio conteudo -->
			<div id="conteudo">

				<!-- Inicio Conteudo File1 -->
				<div id="home_file1">

					<!-- Inicio Coluna Direita -->
					<div id="hf1_Col1">
					<?php
					require_once PATH_INCLUDES_USERS . 'form-search.php';
					/*Inicio divulgacao*/
					require_once PATH_INCLUDES_USERS . 'window-parceiros.php';
					/*Fim divulgacao*/
					?>
					</div>
					<!-- Fim Coluna direita -->
					<!-- Inicio Coluna esquerda -->
					<!-- Incio Menu -->
					<div id="hf1_Col2">
					<?php
					require_once PATH_INCLUDES_USERS . 'main-menu.php';
					?>
					<!-- Conteudo Serviços -->
					<div id="contQuem">
						<div id="Tit">Quem Somos</div>
							<div id="Desc">
								<div id="lipsum">
									<p class="quemSomosPage">O Arquivo imobiliário.com, nasceu da
										idéia de reunir em um só lugar, as ofertas de imóveis para
										compra, venda e locação, de maneira sistemática, organizada e
										segmentada por região de interesse.</p>
									<p class="quemSomosPage">Além de agregar valor ao usuário ao
										disponibilizar listagem de prestadores de serviços e
										informações para casa.</p>
									<p class="quemSomosPage">A empresa atua com foco na aproximação
										entre as imobiliárias e o consumidor final através de um
										mecanismo de busca, que permite ao interessado selecionar o que
										deseja e acionar a imobiliária ofertante a partir do próprio
										site.</p>
									<p class="quemSomosPage">O modelo de negócio é baseado no
										conceito de plataforma multilateral aonde se estabelece a
										criação de valor e a interação entre diferentes grupos.</p>
									<h2 class="subtitleQuemSomos">Missão:</h2>
									<p class="quemSomosPage">Participar da vida das pessoas,
										oferecendo ferramentas que facilitem a realização de suas
										aspirações e agilizem o seu dia a dia.</p>
									<h2 class="subtitleQuemSomos">Visão:</h2>
									<p class="quemSomosPage">Sermos um das principais empresas de
										classificados e prestação de serviços de imóveis on-line do
										Brasil.</p>
									<h2 class="subtitleQuemSomos">Valores:</h2>
									<p class="quemSomosPage">Nossos valores representam a maneira de
										se trabalhar em nossa organização.</p>
									<ul>
										<li>Integridade</li>
										<li>Ética</li>
										<li>Foco no consumidor</li>
										<li>Compromisso social</li>
										<li>Qualidade e Excelência</li>
										<li>Inovação</li>
									</ul>
								</div>
							</div>

						</div>
					<!-- /Conteudo Serviços -->

					</div>
					<!-- Fim Coluna esquerda -->
				</div>
				<!-- Fim Conteudo File1 -->
			</div>
			<!-- Fim conteudo -->

		</div>
		<!-- Fim Main Conteudo -->
		<!-- Inicio Footer -->
		<footer id="footer">
		<?php
		require_once PATH_INCLUDES_ADMIN . 'footer.html';
		?>
		</footer>
		<!-- Fim Footer -->
	</div>
	<!-- Fim Geral-->

<?php
require_once PATH_INCLUDES_USERS . 'functions-js-form-search.html';
?>
</body>

</html>