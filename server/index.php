<?php
session_start();

require 'config.php';
require 'Db.php';

require 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim(array(
		'debug' => false
	   ));

$app->contentType("aplication/json");

$app->error(function (Exception $e = null) use ($app){
	echo '{"error":{"text":"'.$e->getMessage().'"}}';
});

function formatJson($obj){
	echo json_encode($obj);
}

//includes
include("costumer.php");

$app->run();