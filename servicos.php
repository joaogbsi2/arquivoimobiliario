<?php

// Here (detailsProperty.action.php) we get the property id and we'll perform
// a fetch from the property with this id and we show it here.

/*
 * INCLUDE SECTOR
 */

// include the file of configuration
// require_once '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_INCLUDES_ADMIN . 'session.php';
require_once PATH_ACTION . 'index.action.php';
require_once PATH_CONTROLLER . 'StatusController.class.php';
require_once PATH_ACTION . 'adminServices.action.php';
require_once PATH_MODEL_ENTITIES . 'Advertising.class.php';

require_once PATH_MODEL_ENTITIES . 'User.class.php';
require_once PATH_CONTROLLER . 'ServiceController.class.php';
require_once PATH_MODEL_ENTITIES . 'Service.class.php';

require_once PATH_CONTROLLER . 'UserController.class.php';
require_once PATH_CONTROLLER . 'UserServiceController.class.php';
require_once PATH_MODEL_ENTITIES . 'UserService.class.php';

$letter = null;
if (isset ( $_GET ['letter'] ) && $_GET ['letter'] != NULL && $_GET ['letter'] != "")
	$letter = trim ( $_GET ['letter'] );

if (! is_null ( $letter )) {
	
	$serviceController = new ServiceController ();
	$servicos = $serviceController->getServicesByCriteria ( trim ( strtoupper ( $letter ) ) );
}

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon" href="images/arquiLogo.png">
<title>Arquivo Imobiliário</title>
<link href="estilo.css" rel="stylesheet" type="text/css" />

<!-- jquery ui theme and select multiple theme -->
<link rel="stylesheet" type="text/css" href="css/jquery.multiselect.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
<!-- Style of Advertisings -->
<style type="text/css">
<!--
.advertising {
	width: 150px;
	height: 95px;
	margin-left: 3px;
}
-->
</style>

<!-- Modernizr do Slider em jquery das propagandas principais -->
<script src="js/modernizr.js"></script>
<!-- CSS do slider em jquery -->
<link rel="stylesheet" href="css/flexslider.css" type="text/css"
	media="screen" />


</head>

<body>

	<!-- GERAL -->
	<div id="geral">
		<header id="topo">
			<?php
			require_once PATH_INCLUDES_USERS . 'header.php';
			?>
		</header>

		<!-- MAIN CONTEUDO -->
		<div id="mainConteudo">

			<!-- CONTEUDO -->
			<div id="conteudo">

				<!-- CONTEUDO COL 1 -->
				<div id="home_file1">

					<!-- Inicio Coluna Direita -->
					<div id="hf1_Col1">
					<?php
					require_once PATH_INCLUDES_USERS . 'form-search.php';
					/*Inicio divulgacao*/
					require_once PATH_INCLUDES_USERS . 'window-parceiros.php';
					/*Fim divulgacao*/
					?>
					</div>
					<!-- Fim Coluna direita -->
					<!-- Inicio Coluna Esquerda -->
					<div id="hf1_Col2">
						<?php
						/* Menu navegação + banner */
						require_once PATH_INCLUDES_USERS . 'main-menu.php';
						?>
					</div>
					<!-- Fim Coluna Esquerda -->

				</div>
				<!-- /CONTEUDO COL 1 -->

				<!-- Inicio Conteudo 2 -->
				<div id="home_file2">
					<!-- Inicio Coluna Serviço -->
					<div id="contServ">
						<div id="Tit">Serviços para sua casa</div>
						<div id="cxBusca">
							<div class="cxLetra">
								<a href="?letter=a">A</a>
							</div>
							<div class="cxLetra">
								<a href="?letter=b">B</a>
							</div>
							<div class="cxLetra">
								<a href="?letter=c">C</a>
							</div>
							<div class="cxLetra">
								<a href="?letter=d">D</a>
							</div>
							<div class="cxLetra">
								<a href="?letter=e">E</a>
							</div>
							<div class="cxLetra">
								<a href="?letter=f">F</a>
							</div>
							<div class="cxLetra">
								<a href="?letter=g">G</a>
							</div>
							<div class="cxLetra">
								<a href="?letter=h">H</a>
							</div>
							<div class="cxLetra">
								<a href="?letter=i">I</a>
							</div>
							<div class="cxLetra">
								<a href="?letter=j">J</a>
							</div>
							<div class="cxLetra">
								<a href="?letter=k">K</a>
							</div>
							<div class="cxLetra">
								<a href="?letter=l">L</a>
							</div>
							<div class="cxLetra">
								<a href="?letter=m">M</a>
							</div>
							<div class="cxLetra">
								<a href="?letter=n">N</a>
							</div>
							<div class="cxLetra">
								<a href="?letter=o">O</a>
							</div>
							<div class="cxLetra">
								<a href="?letter=p">P</a>
							</div>
							<div class="cxLetra">
								<a href="?letter=q">Q</a>
							</div>
							<div class="cxLetra">
								<a href="?letter=r">R</a>
							</div>
							<div class="cxLetra">
								<a href="?letter=s">S</a>
							</div>
							<div class="cxLetra">
								<a href="?letter=t">T</a>
							</div>
							<div class="cxLetra">
								<a href="?letter=u">U</a>
							</div>
							<div class="cxLetra">
								<a href="?letter=v">V</a>
							</div>
							<div class="cxLetra">
								<a href="?letter=w">W</a>
							</div>
							<div class="cxLetra">
								<a href="?letter=x">X</a>
							</div>
							<div class="cxLetra">
								<a href="?letter=y">Y</a>
							</div>
							<div class="cxLetra">
								<a href="?letter=z">Z</a>
							</div>
						</div>
						<div id ="cxDesc">
							<?php
							if (! is_null ( $letter )) {
								if (is_array ( $servicos )){
									$userServiceController = new UserServiceController ();
																										
									foreach ( $servicos as $iter ) {?>
										<div id="Tit"><?php echo $iter->getServiceDescription(); ?></div>
										<table width="650">
										<?php
										$usersService = $userServiceController->getUserServiceProvider ( $iter->getId () );
										if (! is_null ( $usersService ))
											foreach ( $usersService as $iterUser ){?>
												<tr>
													<td width="180"><?php echo $iterUser->getUser()->getName(); ?></td>
													<td width="250" align="right"><?php echo $iterUser->getUser()->getPhone(); ?></td>
													<td width="230" align="right"><?php echo $iterUser->getUser()->getEmail(); ?></td>
												</tr>
											<?php
											}
										else{?>
											<td width="220">Não há clientes que realizam esse serviço!</td>  
										<?php
										}?>
										</table>
									<?php
									}
								} else{?>
									<div id="Tit">Não há serviços cadastrados com essa condição!</div>
								<?php
								}
							} else{?>
								<div id="Tit">Por favor, selecione uma das letras acima!</div>
							<?php
							}
							?>
						</div>
					</div>
					<!-- Fim Coluna Serviço -->
				</div>
				<!-- Fim Conteudo 2 -->

			</div>
			<!-- CONTEUDO -->

		</div>
		<!-- /MAIN CONTEUDO -->
		
		<!-- Inicio Footer -->
		<footer id="footer">
		<?php
		require_once PATH_INCLUDES_ADMIN . 'footer.html';
		?>
		</footer>
		<!-- Fim Footer -->
	</div>
	<!-- /GERAL -->

<?php
require_once PATH_INCLUDES_USERS . 'functions-js-form-search.html';
?>
    
</body>

</html>