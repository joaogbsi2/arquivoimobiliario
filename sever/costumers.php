<?php

$app->get("/costumers", function(){

	$sql = "SELECT costumerId, contactName, phone FROM costumer";
	$stmt = DB::prepare($sql);
	$stmt->execute();
	formatJson($stmt->fetchAll());
});

$app->get("/costumers/:id", function($id){
	$sql = "SELECT costumerId, contactName, phone FROM costumer WHERE costumerId='$id'";
	$stmt = DB::prepare($sql);
	$stmt->execute();
	formatJson($stmt->fetchAll());
});

$app->post("/costumers/:id", function($id){
	
	$data=json_decode(\Slim\Slim::getInstance()->request()->getBody());
	
	if($data->isUpdate){
		$sql = "UPDATE custumer SET contactName=?, phone=? WHERE custumerId=?";
		$stmt = DB::prepare($sql);
		$stmt->execute(array(
			$data->contactName,
			$data->phone,
			$data->custumerId
			)
		);
	}
	else{
		$sql="INSERT INTO costumer (costumerId, contactName, phone) VALUES (?,?,?)";
		$stmt = DB::prepare($sql);
		$stmt->execute(array(
			$data->costumerId,
			$data->contactName,
			$data->phone
			)
		);
	}
	formatJson($data);
}); 

$app->delete("/costumer/:id", function($id){
	$sql="DELETE FROM costumer WHERE costumerId=?";
	$stmt=DB::prepare($sql);
	$stmt->execute(array($id));
	formatJson(true);
});

?>