<?php

/*
 * INCLUDE SECTOR
 */

// $_SERVER['DOCUMENT_ROOT'] = "/home/arqui937/public_html/";
// define ("PATH", $_SERVER['DOCUMENT_ROOT']);

// include the file of configuration
// require_once '/home/arqui937/public_html/config.php';
// // require_once '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_ACTION . 'index.action.php';
require_once PATH_CONTROLLER . 'StatusController.class.php';
require_once PATH_ACTION . 'adminServices.action.php';
require_once PATH_MODEL_ENTITIES . 'Advertising.class.php';
require_once PATH_CONTROLLER . 'StoreController.class.php';
require_once PATH_MODEL_ENTITIES . 'Store.class.php';

if (isset ( $_GET ['code'] ) && $_GET ['code'] != NULL) {
	
	$codeStore = $_GET ['code'];
	$storeController = new StoreController ();
	$storeSelected = $storeController->getById ( $codeStore );
} else {
	echo "Por favor, selecione uma imobiliária para prosseguir!";
	exit ();
}

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon" href="images/arquiLogo.png">
<title>Arquivo Imobiliário</title>
<link href="style.css" rel="stylesheet" type="text/css" />

<!-- jquery ui theme and select multiple theme -->
<link rel="stylesheet" type="text/css" href="css/jquery.multiselect.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />

<!-- Modernizr do Slider em jquery das propagandas principais -->
<script src="js/modernizr.js"></script>
<!-- CSS do slider em jquery -->
<link rel="stylesheet" href="css/flexslider.css" type="text/css"
	media="screen" />


<style type="text/css">
<!--
#formulario {
	width: 500px;
	min-height: 300px;
	max-height: 450px;
	float: left;
	background: #F7F7F7;
}

#formulario #titulo {
	width: 500px;
	font-size: 18px;
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	color: #FFF;
	margin: 10px 0 0 0;
	color: #333;
	text-align: center;
	float: left;
}

#formulario #form {
	width: 460px;
	float: left;
	margin: 20px 0 0 20px;
}

#formulario #form .txt1 {
	font-family: 'GothanMedium';
	font-size: 14px;
	color: #0D547F;
}

#formulario #form .campo1 {
	width: 350px;
	height: 30px;
	float: left;
	font-size: 14px;
	color: #333;
}

#formulario #form .campo2 {
	width: 350px;
	height: 80px;
	float: left;
	font-size: 14px;
	color: #333;
}
-->
</style>

</head>

<body>

	<!-- GERAL -->
	<div id="geral">


		<!-- TOPO -->
    <?php
				require_once PATH_INCLUDES_USERS . 'header.php';
				?>
    <!-- /TOPO -->


		<!-- MAIN CONTEUDO -->
		<div id="mainConteudo">

			<!-- CONTEUDO -->
			<div id="conteudo">

				<!-- CONTEUDO COL 1 -->
				<div id="home_COL1">

					<!-- FORM SEARCH -->
                    <?php
																				require_once PATH_INCLUDES_USERS . 'form-search.php';
																				?>
                    <!-- /FORM SEARCH -->

					<!-- CAIXA PARCEIROS -->
                    <?php
																				require_once PATH_INCLUDES_USERS . 'window-parceiros.php';
																				?>
                    <!-- /CAIXA PARCEIROS -->

				</div>
				<!-- /CONTEUDO COL 1 -->

				<!-- CONTEUDO COL 2 -->
				<div id="home_COL2">
					<!-- Menu -->
					<div id="menu">
						<div id="btmenu1">
							<a href="quemsomos.php"><img src="images/btmenu1.png"></a>
						</div>
						<div id="btmenu2">
							<a href="parceiros.php"><img src="images/btmenu2.png"></a>
						</div>
						<div id="btmenu3">
							<a href="#"><img src="images/btmenu3.png"></a>
						</div>
						<div id="btmenu4">
							<a href="#"><img src="images/btmenu4.png"></a>
						</div>
						<div id="btmenu5">
							<a href="servicos.php"><img src="images/btmenu5.png"></a>
						</div>
					</div>
					<!-- /Menu -->
                
                <?php
																$adminServicesController = new AdminServicesController ();
																
																$mainAdvertisings = $adminServicesController->getAllMainAdvertisings (); // pegar todas as principais propagandas
																?>
                <div id="bannerImg">
						<div id="container" class="cf" style="margin-top: 53px;">


							<div id="main" role="main" style="width: 664px;">
								<section class="slider">
									<div class="flexslider">
										<ul class="slides">
                       <?php
																							if (! is_null ( $mainAdvertisings ) && is_array ( $mainAdvertisings )) {
																								foreach ( $mainAdvertisings as $iterMainAdvertisings ) {
																									?>

                          <li><img width="670" height="300"
												src="<?php echo URL_MAIN_ADVERTISINGS_PHOTOS. basename($iterMainAdvertisings->getPhoto()); ?>" />
											</li>
                       <?php
																								}
																							}
																							?>

                      </ul>
									</div>
								</section>

							</div>

						</div>

					</div>
                
                
                <?php
																
																if (isset ( $_GET ['success'] )) {
																	?>
                        <script type="text/javascript">
                            window.alert("Sua mensagem foi enviada com sucesso!");
                        </script>
                <?php
																} else if (isset ( $_GET ['error'] ) && $_GET ['error'] != NULL) {
																	if ($_GET ['error'] == 1) {
																		?>
                        <script type="text/javascript">
                            window.alert("Por favor, informe seu nome!");
                        </script>
                 <?php
																	} else if ($_GET ['error'] == 2) {
																		?>
                        <script type="text/javascript">
                            window.alert("Por favor, informe um e-mail válido!");
                        </script>
                 <?php
																	} else if ($_GET ['error'] == 3) {
																		?>
                        <script type="text/javascript">
                            window.alert("Por favor, preencha o campo mensagem!");
                        </script>
                 <?php
																	} else {
																		?>
                        <script type="text/javascript">
                            window.alert("Erro ao enviar sua mensagem! Tente novamente!");
                        </script>
                 <?php
																	}
																}
																?>
                
                <!-- Conteudo Serviços -->
					<div id="contHm">
						<div id="cxCont1">
							<div id="formulario">
								<div id="titulo">Agendar visita</div>
								<div id="form">
									<form id="frmContact" method="post"
										action="action/contactStore.action.php" accept-charset="UTF-8">
										<table width="457">
											<tr>
												<td width="65" align="right" class="txt1">* Nome:</td>
												<td width="1"></td>
												<td width="350"><input name="name"
													class="campo1 textfield required" id="name" /></td>
											</tr>
											<tr>
												<td align="right" class="txt1">* E-mail:</td>
												<td width="1"></td>
												<td><input name="mail" placeholder="exemplo@exemplo.com.br"
													class="campo1 textfield required" id="mail" /></td>
											</tr>
											<!--                                    <tr>
                                        <td align="right" class="txt1">Telefone:</td>
                                        <td width="1"></td>
                                        <td><input name="phone" class="textfield" id="contactPhone" size="13" maxlength="13" onKeyPress="fone(this,document.frmContact.phone)" /></td>
                                    </tr>                   -->
											<!--                                    <tr>
                                        <td align="right" class="txt1">* Assunto:</td>
                                        <td width="1"></td>
                                        <td>
                                            <select name="subject" class="campo1" id="subject">
                                                <option value="Parceria">Parceria</option>
                                                <option value="Propaganda">Propaganda</option>
                                                <option value="Imobiliária">Imobiliária</option>
                                                <option value="Outros">Outros</option>
                                            </select>
                                        </td>
                                    </tr>-->
											<tr>
												<td align="right" class="txt1">* Mensagem:</td>
												<td width="1"></td>
												<td><textarea name="msg" class="required" id="msg" cols="45"
														rows="8"></textarea></td>
											</tr>
											<tr>
												<td colspan="2"></td>
												<td align="right">
													<!--<a href="#">--> <input type="hidden" name="to"
													value="<?php echo $storeSelected->getMail(); ?>" /> <input
													type="hidden" name="codeStore"
													value="<?php echo $storeSelected->getId(); ?>" /> <input
													type="submit" name="Enviar" id="Enviar" value="Enviar" /> <!--<img src="images/bt_enviar.png" width="100" height="30" />-->
													<!--</a>-->
												</td>
											</tr>
										</table>
									</form>
								</div>
							</div>
						</div>

					</div>
					<!-- /Conteudo Serviços -->

				</div>
				<!-- /CONTEUDO COL 2 -->


			</div>
			<!-- CONTEUDO -->

		</div>
		<!-- /MAIN CONTEUDO -->


		<!-- FOOTER -->
    <?php
				require_once PATH_INCLUDES_ADMIN . 'footer.html';
				?>
    <!-- /FOOTER -->



	</div>
	<!-- /GERAL -->

	<!-- End conteiner -->
	<!-- JS Content and JSLibary -->
	<script type="text/javascript" src="js/jquery-1.8.3.js"></script>

	<!-- includes for multiple select -->
	<script type="text/javascript" src="js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/jquery.multiselect.min.js"></script>
	<!-- end multiple select include -->

	<!-- includes for maskedinput -->
	<script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>
	<!-- end maskedinput includes -->

	<!-- includes for jquery validate and their messages -->
	<script type="text/javascript" src="js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="js/messages_pt_BR.js"></script>
	<!-- end jquery validate and their messages includes -->

	<!-- includes for jquery price -->
	<script type="text/javascript" src="js/jquery.price_format.1.7.min.js"></script>
	<!-- end jquery price include -->

     
<?php
require_once PATH_INCLUDES_USERS . 'functions-js-form-search.html';
?>
    
</body>

</html>