<?php

/*
 * INCLUDE SECTOR
 */

// $_SERVER['DOCUMENT_ROOT'] = "/home/arqui937/public_html/";
// define ("PATH", $_SERVER['DOCUMENT_ROOT']);

// include the file of configuration
// require_once '/home/arqui937/public_html/config.php';
// require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';

$filename = '/home/arqui937/public_html/config.php';
if (file_exists ( $filename )) {
	require_once '/home/arqui937/public_html/config.php';
} else {
	require_once '/opt/lampp/htdocs/arquivoImobiliario/config.php';
}

require_once PATH_ACTION . 'index.action.php';
require_once PATH_CONTROLLER . 'StatusController.class.php';
require_once PATH_ACTION . 'adminServices.action.php';
require_once PATH_MODEL_ENTITIES . 'Advertising.class.php';

require_once PATH_CONTROLLER . 'TechnologyController.class.php';
require_once PATH_MODEL_ENTITIES . 'Technology.class.php';

/*
 * Buscar a Notícia cadastrada
 */
$tecControllerDetails = new TechnologyController ();
$tecDetails = $tecControllerDetails->findAll ();

if (is_null ( $tecDetails ))
	$tecDetails = new Technology ();

$pathTechnologyPhoto = URL_TECHNOLOGY_PHOTOS . basename ( $tecDetails->getPhoto () );

?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon" href="images/arquiLogo.png">
<title>Arquivo Imobiliário</title>
<!-- <link href="style.css" rel="stylesheet" type="text/css" /> -->
<link href="estilo.css" rel="stylesheet" type="text/css" />

<!-- jquery ui theme and select multiple theme -->
<link rel="stylesheet" type="text/css" href="css/jquery.multiselect.css" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />

<!-- Modernizr do Slider em jquery das propagandas principais -->
<script src="js/modernizr.js"></script>
<!-- CSS do slider em jquery -->
<link rel="stylesheet" href="css/flexslider.css" type="text/css"
	media="screen" />

</head>

<body>

	<!-- JS Content -->
	<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>

	<!-- includes for multiple select -->
	<script type="text/javascript" src="js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/jquery.multiselect.min.js"></script>
	<script type="text/javascript" src="js/jquery.validate.min.js"></script>
	<!-- end multiple select include -->

	<!-- includes for jquery price -->
	<script type="text/javascript" src="js/jquery.price_format.1.7.min.js"></script>
	<!-- end jquery price include -->

	<!-- GERAL -->
	<div id="geral">


		<header id="topo">
			<?php
				require_once PATH_INCLUDES_USERS . 'header.php';
			?>
		</header>


		<!-- MAIN CONTEUDO -->
		<div id="mainConteudo">

			<!-- CONTEUDO -->
			<div id="conteudo">

				<!-- Inicio Conteudo File1 -->
				<div id="home_file1">
					<!-- Inicio coluna Esquerda -->
					<div id="hf1_Col1">
						<?php
						require_once PATH_INCLUDES_USERS . 'form-search.php';
						/*Inicio divulgacao*/
						require_once PATH_INCLUDES_USERS . 'window-parceiros.php';
						/*Fim divulgacao*/
						?>
					</div>
					<!-- fim coluna Esquerda -->					
				
					<!-- fim Conteudo File1 -->
					<!-- Inicio Coluna direita -->				
					<div id="hf1_Col2">
						<!-- Incio Menu -->
						<?php
							require_once PATH_INCLUDES_USERS . 'main-menu.php';
						?>
						<!-- Fim Menu -->
						<!-- Inicio Noticia -->
						<div id="contHm">
							<div id="Tit">Tecnologia</div>

							<div id="cxCont1">
								<div id="tit"><?php echo $tecDetails->getTitle(); ?></div>
								<div id="desc">
									<p>
										<img width="400px" height="280px"
											src="<?php echo $pathTechnologyPhoto; ?>" align="left"
											style="padding: 10px 10px 10px 10px" />
									</p>
									<p>&nbsp;</p>
									<p style="font-size: 12pt;"><?php echo nl2br($tecDetails->getDescription()); ?></p>
								</div>
							</div>	

						</div>
						<!-- Fim Noticia -->
					</div>
					<!-- Fim coluna direita -->					
				</div>
				<!-- Fim conteudo File1 -->
				</div>
				<!-- CONTEUDO -->

			</div>
			<!-- /MAIN CONTEUDO -->


		<footer id="footer">
		<?php
		require_once PATH_INCLUDES_ADMIN . 'footer.html';
		?>
		</footer>



		</div>
		<!-- /GERAL -->

<?php
require_once PATH_INCLUDES_USERS . 'functions-js-form-search.html';
?>


</body>

</html>